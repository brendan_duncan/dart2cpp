#dart2cpp

dart2cpp is a highly experimental work-in-progress prototype to investigate
the possibilities of a transformer that can convert Dart source code into
C++.

**This is an experimental work in progress and is not usable yet**

##Background
I'm a fan of the Dart programming language, and the more I use it the less
I want to use other languages. Dart's syntax is similar to other languages
I'm used to, such as C++/Java/C# (though perhaps only superficially in places). I've spent quite 
a bit of time translating C++ code into Dart, which actually isn't too dificult. Dart 
currently already has multiple output targets, the Dart VM for running native Dart code, 
and dart2js to transform Dart into Javascript to run everywhere else. 
**What if...** instead of transforming Dart into Javascript, we could transform it into 
something that's able to compile into a native executable? I'm most familiar with C++ so 
I decided to see if I could write a converter to translate Dart into C++. **What if...** You could
write a web app in a single language, that could compile to PNaCl, such that the run-time environment
would choose the most performant combination of available code to run based on the capabilities of
the browser. You could write a worker thread that would compile to PNaCl, Dart and Javascript; 
browsers with PNaCl support would load the PNaCl version; browsers with the Dart VM would load
the Dart version, and everyone else would load the Javascript version. From a PNaCl perspective, 
this would mean you could write for PNaCl and still have cross browser support. That seems pretty cool to me.

##Dart to C++
The first thing I discovered is that while it's not too hard to translate
C++ into Dart by hand (just take away the variable types, don't delete things,
rethink how memory pointers work, etc), C++ can't natively do things that Dart 
can. C++ requires static types, and in Dart we don't have variable types at all, 
a variable is simply a pointer to an object and the type of object can change;
Dart uses garbage collection; Dart makes things like closures trivial to use; etc etc.

For the foundation of the translator, I'm using the Dart analyzer library, which parses
Dart code and produces an Abstract Syntax Tree (AST). The Dart Editor uses this for static
code analysis. From this AST I generate some structures to have a high level view of the
program, and from these structures and the AST it's able to generate C++ for all of the
various language features.

I've been going through the Dart language specification and the core libraries, and peice by peice
ensuring the Dart code translates into C++ that is able to compile and produces the same
results as the Dart version. Many of the Dart language features, things you take for granted in
Dart, took some creative solutions in C++.

So far so good, much of the language is translating to working C++ now. Most of what is left is
an implementation of the core library classes (like int, String, etc) that require external
implementations. This work is progressing nicely, and the longer I've been working on this
"idea", the more confident I am that the whole thing isn't completely crazy.
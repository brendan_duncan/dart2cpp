/****************************************************************************
 * Copyright (C) 2014 by Brendan Duncan.                                    *
 *                                                                          *
 * This file is part of Dart2Cpp,                                           *
 * https://github.com/brendan-duncan/dart2cpp.                              *
 *                                                                          *
 * Licensed under the Apache License, Version 2.0 (the "License");          *
 * you may not use this file except in compliance with the License.         *
 * You may obtain a copy of the License at                                  *
 *                                                                          *
 * http://www.apache.org/licenses/LICENSE-2.0                               *
 *                                                                          *
 * Unless required by applicable law or agreed to in writing, software      *
 * distributed under the License is distributed on an "AS IS" BASIS,        *
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. *
 * See the License for the specific language governing permissions and      *
 * limitations under the License.                                           *
 ****************************************************************************/
import 'dart:io';
import 'package:args/args.dart';
import 'package:ccompilers/ccompilers.dart';
import 'package:dart2cpp/dart2cpp.dart';

void printUsage() {
  print('Usage: dart dart2cpp.dart '
        '[--output|-o <output_directory>] '
        '[--compile|-c] '
        '<source.dart>');
}

void main(List argv) {
  if (argv.isEmpty) {
    printUsage();
    return;
  }

  var parser = new ArgParser();
  parser.addOption('output', abbr: 'o', defaultsTo: 'out');
  parser.addFlag('compile', abbr: 'c', defaultsTo: true);

  var args;
  try {
    args = parser.parse(argv);
  } catch (e) {
    print(e);
    printUsage();
    return;
  }

  if (args.rest.isEmpty) {
    printUsage();
    return;
  }

  String outPath = args['output'];

  Project project = new Project();
  String path = new File(Platform.script.toFilePath()).parent.path;

  for (var src in args.rest) {
    File f = new File(src);
    String name = f.path.split(new RegExp(r'(/|\\)')).last;

    D2COutput out = dart2cpp(f.path);

    new File('$outPath/${name}.cc')
        ..createSync(recursive: true)
        ..writeAsStringSync(out.code);

    if (!out.hasMainFunction) {
      if (args['compile']) {
        print('No main function found for ${f.path}, not compiling.');
        continue;
      }
    }

    if (args['compile']) {
      project.build('$outPath/$name', '$outPath/${name}.cc');
    }
  }
}


class Project {
  CommandLineTool _compiler;
  CommandLineArguments _compilerArgs;
  CommandLineTool _linker;
  CommandLineArguments _linkerArgs;
  String _operatingSystem;

  int _bits;
  Map _compilerDefine = {};
  List _compilerInclude = [];
  List _compilerInput = [];
  List _linkerInput = [];
  List _linkerLibpath = [];
  String _linkerOutput = '';
  String output = '';
  List _unusedFileExtensions = ['exp', 'lib', 'o', 'obj', 'pdb', 'ilk'];

  Project() {
    String path = new File(Platform.script.toFilePath()).parent.parent.path;
    _compilerInclude = ['${path}/lib/cpp'];
  }

  int build(String name, String src) {
    _compilerInput = [src];
    _linkerInput = [name];
    _linkerOutput = name;
    _setup();
    return _build();
  }

  int _build() {
    var tasks = new List<CommandLineTask>();
    var compile = new CommandLineTask(() => _compiler.run(_compilerArgs.arguments));
    compile.before = 'Compile...\n${_compiler.executable} ${_compilerArgs}';
    compile.success = 'Compilation succesful.';
    compile.fail = 'Compilation failed.';
    tasks.add(compile);

    for (var task in tasks) {
      var result = task.execute();
      if (result.exitCode != 0) {
        return result.exitCode;
      }
    }

    output = _linkerOutput;
    _clean(Directory.current.path, _unusedFileExtensions);
    return 0;
  }

  void _setupArgumentsForPosixCompiler() {
    // Compiler
    var args = _compilerArgs;
    args.add('-c');
    args.addAll(['-fPIC', '-Wall']);
    args.add('-m32', test: _bits == 32);
    args.add('-m64', test: _bits == 64);
    args.addAll(_compilerInclude, prefix: '-I');
    args.addKeys(_compilerDefine, prefix: '-D');
    args.addAll(_compilerInput);
  }

  void _setupArgumentsForPosixLinker() {
    // Linker
    var args = _linkerArgs;
    args.addAll(_addExtension(_linkerInput, '.o'));
    args.add('-m32', test: _bits == 32);
    args.add('-m64', test: _bits == 64);
    args.addAll(_linkerLibpath, prefix: '-L');
  }

  void _setupArgumentsOnLinux() {
    // Compiler
    _setupArgumentsForPosixCompiler();
    // Linker
    _setupArgumentsForPosixLinker();
    var args = _linkerArgs;
    args.add('-shared');
    args.add('-o');
    args.add(_linkerOutput, prefix: 'lib', suffix: '.so');
  }

  void _setupArgumentsOnMacOS() {
    // Compiler
    var args = _compilerArgs;
    args.addAll(['-std=c++11', '-stdlib=libc++']);
    args.addAll(['-o', _linkerOutput]);
    args.addAll(['-fPIC', '-Wall']);
    args.add('-m32', test: _bits == 32);
    args.add('-m64', test: _bits == 64);
    args.addAll(_compilerInclude, prefix: '-I');
    args.addKeys(_compilerDefine, prefix: '-D');
    args.addAll(_compilerInput);
  }

  void _setupArgumentsOnWindows() {
    // Compiler
    _linkerOutput += '.exe';
    var args = _compilerArgs;
    args.addAll(_compilerInclude, prefix: '-I');
    args.addKeys(_compilerDefine, prefix: '-D');

    args.addAll(['/EHsc', '/Od', '/ZI', '/W3', '/WX-', '/Od', '/Oy-',
                 '/Gm', '/RTC1', '/MDd', '/GS', '/fp:precise', '/Zc:wchar_t',
                 '/Zc:forScope', '/Gd', '/TP', '/analyze-']);
    args.add('/D WIN32 /D _DEBUG /D _CONSOLE /D _LIB');
    args.addAll(_compilerInput);
    args.add('/link /out:${_linkerOutput}');
  }

  void _setup() {
    _operatingSystem = Platform.operatingSystem;
    _bits = DartSDK.getVmBits();
    _setupArguments();
    _setupTools();
  }

  void _setupArguments() {
    _compilerArgs = new CommandLineArguments();
    _linkerArgs = new CommandLineArguments();
    switch(_operatingSystem) {
      case 'linux':
        _setupArgumentsOnLinux();
        break;
      case 'macos':
        _setupArgumentsOnMacOS();
        break;
      case 'windows':
        _setupArgumentsOnWindows();
        break;
      default:
        _errorUnsupportedOperatingSystem();
    }
  }

  void _setupTools() {
    switch(_operatingSystem) {
      case 'macos':
        _compiler = new Gpp();
        _linker = new Gcc();
        _compiler.executable = 'clang++';
        break;
      case 'linux':
        _compiler = new Gpp();
        _linker = new Gcc();
        break;
      case 'windows':
        _compiler = new Msvc(bits: _bits);
        _linker = new Mslink(bits: _bits);
        break;
      default:
        _errorUnsupportedOperatingSystem();
    }
  }

  void _clean(String path, List<String> extensions) {
    var directory = new Directory(path);
    if(!directory.existsSync()) {
      return;
    }

    var list = directory.listSync(recursive: false);
    for(var file in list) {
      if(file is! FileSystemEntity) {
        continue;
      }

      for(var extension in extensions) {
        if(extension == null || extension.isEmpty) {
          continue;
        }

        if(file.path.endsWith('.$extension')) {
          file.deleteSync(recursive: false);
          break;
        }
      }
    }
  }

  List<String> _addExtension(List<String> files, String extension) {
    var length = files.length;
    var result = new List<String>(length);
    for(var i = 0; i < length; i++) {
      var file = files[i];
      if(file.indexOf('.') == -1) {
        result[i] = '$file$extension';
      } else {
        result[i] = file;
      }
    }

    return result;
  }

  void _errorUnsupportedOperatingSystem() {
    throw new StateError('Unsupported operating system $_operatingSystem');
  }
}

/****************************************************************************
 * Copyright (C) 2014 by Brendan Duncan.                                    *
 *                                                                          *
 * This file is part of Dart2Cpp,                                           *
 * https://github.com/brendan-duncan/dart2cpp.                              *
 *                                                                          *
 * Licensed under the Apache License, Version 2.0 (the "License");          *
 * you may not use this file except in compliance with the License.         *
 * You may obtain a copy of the License at                                  *
 *                                                                          *
 * http://www.apache.org/licenses/LICENSE-2.0                               *
 *                                                                          *
 * Unless required by applicable law or agreed to in writing, software      *
 * distributed under the License is distributed on an "AS IS" BASIS,        *
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. *
 * See the License for the specific language governing permissions and      *
 * limitations under the License.                                           *
 ****************************************************************************/
#ifndef dart_internal_h
#define dart_internal_h

d2c::Var makeListFixedLength(const d2c::Var &growableList) {
  return growableList;
}

d2c::Var printToConsole(const d2c::Var &line) {
  std::cout << line.stringValue() << std::endl;
  return null;
}

d2c::Var __factory_Symbol(const d2c::TypeSet &__types__, const d2c::Var &name) {
  throw "TODO: implement";
}

#endif  // dart_internal_h

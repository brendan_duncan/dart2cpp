#ifndef dart_collection_hash_map_impl_h
#define dart_collection_hash_map_impl_h

HashMap__::HashMap__(const d2c::Var &__named__)
    : super() {
}

HashMap__::HashMap__(HashMap__::__identity__ *)
    : super() {
}

d2c::Var HashMap__::containsValue(const d2c::Var &value) {
  throw "TODO: implement";
}

d2c::Var HashMap__::containsKey(const d2c::Var &key) {
  throw "TODO: implement";
}

d2c::Var HashMap__::operator[](const d2c::Var &key) {
  throw "TODO: implement";
}

// operator []=
d2c::Var HashMap__::__array_set(const d2c::Var &index, const d2c::Var &value) {
  throw "TODO: implement";
}

d2c::Var HashMap__::putIfAbsent(const d2c::Var &key, const d2c::Var &ifAbsent) {
  throw "TODO: implement";
}

d2c::Var HashMap__::addAll(const d2c::Var &other) {
  throw "TODO: implement";
}

d2c::Var HashMap__::remove(const d2c::Var &key) {
  throw "TODO: implement";
}

d2c::Var HashMap__::clear() {
  throw "TODO: implement";
}

d2c::Var HashMap__::forEach(const d2c::Var &f) {
  throw "TODO: implement";
}

d2c::Var HashMap__::__get_keys() {
  throw "TODO: implement";
}

d2c::Var HashMap__::__get_values() {
  throw "TODO: implement";
}

d2c::Var HashMap__::__get_length() {
  throw "TODO: implement";
}

d2c::Var HashMap__::__get_isEmpty() {
  throw "TODO: implement";
}

d2c::Var HashMap__::__get_isNotEmpty() {
  throw "TODO: implement";
}

d2c::Var __factory_HashMap(const d2c::TypeSet &__types__,
                           const d2c::Var &__named__) {
  return d2c::Var(new HashMap__(__named__));
}

d2c::Var __factory_HashMap_identity(const d2c::TypeSet &__types__) {
  return d2c::Var(new HashMap__((HashMap__::__identity__ *)NULL));
}

#endif  // dart_collection_hash_map_impl_h

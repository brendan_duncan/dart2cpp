#ifndef dart_collection_hash_set_h
#define dart_collection_hash_set_h

d2c::Var __factory_HashSet(const d2c::TypeSet &__types__,
                           const d2c::Var &__named__);

d2c::Var __factory_HashSet_identity(const d2c::TypeSet &__types__);

#endif  // dart_collection_hash_set_h

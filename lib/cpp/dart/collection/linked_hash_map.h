#ifndef dart_collection_linked_hash_map_h
#define dart_collection_linked_hash_map_h

class LinkedHashMap__ : public LinkedHashMap {
 public:
  typedef LinkedHashMap super;

  typedef std::unordered_map<d2c::Var, d2c::Var> Data__;
  Data__ data_;

  LinkedHashMap__(const d2c::TypeSet &__types__,
                  const std::initializer_list<d2c::Var> &init = {});

  static void updateClassInheritance__(d2c::Object *object,
                                       const d2c::TypeSet &__types__) {
    object->addInheritance__<LinkedHashMap__>(__types__);
    super::updateClassInheritance__(object, __types__);
  }

  d2c::Var containsValue(const d2c::Var &value);

  d2c::Var containsKey(const d2c::Var &key);

  d2c::Var operator[](const d2c::Var &key);

  // operator []=
  virtual d2c::Var __array_set(const d2c::Var &index, const d2c::Var &value);

  d2c::Var putIfAbsent(const d2c::Var &key, const d2c::Var &ifAbsent);

  d2c::Var addAll(const d2c::Var &other);

  d2c::Var remove(const d2c::Var &key);

  d2c::Var clear();

  d2c::Var forEach(const d2c::Var &f);

  d2c::Var toString();

  d2c::Var __get_keys();

  d2c::Var __get_values();

  d2c::Var __get_length();

  d2c::Var __get_isEmpty();

  d2c::Var __get_isNotEmpty();
};

REGISTER_MAP(LinkedHashMap__)

d2c::Var __factory_LinkedHashMap(const d2c::TypeSet &__types__,
                                 const d2c::Var &__named__);

d2c::Var __factory_LinkedHashMap_identity(const d2c::TypeSet &__types__);

#endif  // dart_collection_linked_hash_map_h

#ifndef dart_collection_linked_hash_map_impl_h
#define dart_collection_linked_hash_map_impl_h

#include "linked_hash_map.h"

LinkedHashMap__::LinkedHashMap__(const d2c::TypeSet &__types__,
                                 const std::initializer_list<d2c::Var> &init)
    : super(super::__constructor__(__types__)) {
  updateClassInheritance__(this, __types__);
}

d2c::Var LinkedHashMap__::containsValue(const d2c::Var &value) {
  for (Data__::iterator iter = data_.begin(); iter != data_.end(); ++iter) {
    if (iter->second == value) {
      return true;
    }
  }
  return false;
}

d2c::Var LinkedHashMap__::containsKey(const d2c::Var &key) {
  return data_.find(key) != data_.end();
}

d2c::Var LinkedHashMap__::operator[](const d2c::Var &key) {
  return data_[key];
}

// operator []=
d2c::Var LinkedHashMap__::__array_set(const d2c::Var &index,
                                      const d2c::Var &value) {
  data_[index] = value;
  return null;
}

d2c::Var LinkedHashMap__::putIfAbsent(const d2c::Var &key,
                                      const d2c::Var &ifAbsent) {
  if (data_.find(key) == data_.end()) {
    data_[key] = ifAbsent();
  }
  return null;
}

d2c::Var LinkedHashMap__::addAll(const d2c::Var &other) {
  throw "TODO: implement";
}

d2c::Var LinkedHashMap__::remove(const d2c::Var &key) {
  Data__::iterator iter = data_.find(key);
  if (iter != data_.end()) {
    data_.erase(iter);
  }
  return null;
}

d2c::Var LinkedHashMap__::clear() {
  data_.clear();
  return null;
}

d2c::Var LinkedHashMap__::forEach(const d2c::Var &f) {
  for (Data__::iterator iter = data_.begin(); iter != data_.end(); ++iter) {
    f(iter->first, iter->second);
  }
  return null;
}

d2c::Var LinkedHashMap__::toString() {
  d2c::Str s = "{";
  bool first = true;

  for (std::unordered_map<d2c::Var, d2c::Var>::iterator it = data_.begin();
       it != data_.end(); ++it) {
    if (!first) {
      s += ", ";
    }

    d2c::Var st = it->first.toString();
    s += st.stringValue();
    s += ": ";
    st = it->second.toString();
    s += st.stringValue();

    first = false;
  }

  s += "}";
  return s;
}

d2c::Var LinkedHashMap__::__get_keys() {
  throw "TODO: implement";
}

d2c::Var LinkedHashMap__::__get_values() {
  throw "TODO: implement";
}

d2c::Var LinkedHashMap__::__get_length() {
  return static_cast<std::int64_t>(data_.size());
}

d2c::Var LinkedHashMap__::__get_isEmpty() {
  return data_.empty();
}

d2c::Var LinkedHashMap__::__get_isNotEmpty() {
  return !data_.empty();
}

// __named__:
// bool equals(K key1, K key2),
// int hashCode(K key),
// bool isValidKey(potentialKey)
d2c::Var __factory_LinkedHashMap(const d2c::TypeSet &__types__,
                                 const d2c::Var &__named__) {
  return d2c::Var(new LinkedHashMap__(__types__));
}

d2c::Var __factory_LinkedHashMap_identity(const d2c::TypeSet &__types__) {
  return d2c::Var(new LinkedHashMap__(__types__));
}

#endif  // dart_collection_linked_hash_map_impl_h

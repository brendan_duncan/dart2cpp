#ifndef dart_collection_linked_hash_set_h
#define dart_collection_linked_hash_set_h

class LinkedHashSet__ : public LinkedHashSet {
 public:
  typedef LinkedHashSet super;

  LinkedHashSet__(const d2c::TypeSet &__types__,
                  const std::initializer_list<d2c::Var> &init = {});

  static void updateClassInheritance__(d2c::Object *object,
                                       const d2c::TypeSet &__types__) {
    object->addInheritance__<LinkedHashSet__>(__types__);
    super::updateClassInheritance__(object, __types__);
  }

  // LinkedHashSet
  d2c::Var forEach(const d2c::Var &f);

  d2c::Var __get_iterator();

  // Set
  d2c::Var contains(const d2c::Var &key);

  d2c::Var add(const d2c::Var &key);

  d2c::Var addAll(const d2c::Var &other);

  d2c::Var remove(const d2c::Var &key);

  d2c::Var lookup(const d2c::Var &object);

  d2c::Var removeAll(const d2c::Var &elements);

  d2c::Var retainAll(const d2c::Var &elements);

  d2c::Var removeWhere(const d2c::Var &test);

  d2c::Var retainWhere(const d2c::Var &test);

  d2c::Var containsAll(const d2c::Var &other);

  d2c::Var intersection(const d2c::Var &other);

  d2c::Var union__(const d2c::Var &other);

  d2c::Var difference(const d2c::Var &other);

  d2c::Var clear();

  d2c::Var toSet();

  d2c::Var __get_length();

 protected:
  bool containsItem(const d2c::Var &item);

  friend class Iterator__;

  typedef std::unordered_set<d2c::Var> Data__;
  typedef std::vector<d2c::Var> OrderedData__;
  Data__ data_;
  OrderedData__ orderedData_;

  class Iterator__ : public __library_core_dart_core::Iterator {
   public:
    typedef __library_core_dart_core::Iterator super;

    Iterator__(LinkedHashSet__ *set, int s = 0, int e = -1);

    d2c::Var moveNext();

    d2c::Var __get_current();

   protected:
    d2c::Var set_;
    LinkedHashSet__ *object_;
    int index_;
    int start_;
    int end_;
  };
};

d2c::Var __factory_LinkedHashSet(const d2c::TypeSet &__types__,
                                 const d2c::Var &__named__);

d2c::Var __factory_LinkedHashSet_identity(const d2c::TypeSet &__types__);

#endif  // dart_collection_linked_hash_set_h

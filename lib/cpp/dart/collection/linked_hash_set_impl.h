#ifndef dart_collection_linked_hash_set_impl_h
#define dart_collection_linked_hash_set_impl_h

LinkedHashSet__::LinkedHashSet__(const d2c::TypeSet &__types__,
                                 const std::initializer_list<d2c::Var> &init)
    : super(super::__constructor__(__types__)) {
  updateClassInheritance__(this, __types__);
}

d2c::Var LinkedHashSet__::forEach(const d2c::Var &f) {
  for (Data__::iterator iter = data_.begin(); iter != data_.end(); ++iter) {
    f(*iter);
  }
  return null;
}

d2c::Var LinkedHashSet__::__get_iterator() {
  return d2c::Var(new Iterator__(this));
}

bool LinkedHashSet__::containsItem(const d2c::Var &item) {
  for (Data__::iterator iter = data_.begin(); iter != data_.end(); ++iter) {
    if ((*iter) == item) {
      return true;
    }
  }
  return data_.find(item) != data_.end();
}

d2c::Var LinkedHashSet__::contains(const d2c::Var &value) {
  return containsItem(value);
}

d2c::Var LinkedHashSet__::add(const d2c::Var &value) {
  if (!containsItem(value)) {
    data_.insert(value);
    orderedData_.push_back(value);
  }
  return null;
}

d2c::Var LinkedHashSet__::addAll(const d2c::Var &iterable) {
  d2c::Var iterator = iterable.invoke(TK___get_iterator__)();
  if (iterator.isNull()) {
    return null;
  }
  while (iterator.invoke(TK_moveNext__)().boolValue()) {
    d2c::Var e = iterator.invoke(TK___get_current__)();
    if (!containsItem(e)) {
      data_.insert(e);
      orderedData_.push_back(e);
    }
  }
  return null;
}

d2c::Var LinkedHashSet__::remove(const d2c::Var &value) {
  Data__::iterator iter = data_.find(value);
  if (iter != data_.end()) {
    data_.erase(iter);
  }

  OrderedData__::iterator oIter =
      std::find(orderedData_.begin(), orderedData_.end(), value);

  if (oIter != orderedData_.end()) {
    orderedData_.erase(oIter);
  }
  return null;
}

d2c::Var LinkedHashSet__::lookup(const d2c::Var &object) {
  throw "TODO: implement";
}

d2c::Var LinkedHashSet__::removeAll(const d2c::Var &elements) {
  throw "TODO: implement";
}

d2c::Var LinkedHashSet__::retainAll(const d2c::Var &elements) {
  throw "TODO: implement";
}

d2c::Var LinkedHashSet__::removeWhere(const d2c::Var &test) {
  throw "TODO: implement";
}

d2c::Var LinkedHashSet__::retainWhere(const d2c::Var &test) {
  throw "TODO: implement";
}

d2c::Var LinkedHashSet__::containsAll(const d2c::Var &other) {
  throw "TODO: implement";
}

d2c::Var LinkedHashSet__::intersection(const d2c::Var &other) {
  throw "TODO: implement";
}

d2c::Var LinkedHashSet__::union__(const d2c::Var &other) {
  throw "TODO: implement";
}

d2c::Var LinkedHashSet__::difference(const d2c::Var &other) {
  throw "TODO: implement";
}

d2c::Var LinkedHashSet__::clear() {
  data_.clear();
  orderedData_.clear();
  return null;
}

d2c::Var LinkedHashSet__::toSet() {
  return d2c::Var(this);
}

d2c::Var LinkedHashSet__::__get_length() {
  return static_cast<std::int64_t>(data_.size());
}

d2c::Var __factory_LinkedHashSet(const d2c::TypeSet &__types__,
                                 const d2c::Var &__named__) {
  return d2c::Var(new LinkedHashSet__(__types__));
}

d2c::Var __factory_LinkedHashSet_identity(const d2c::TypeSet &__types__) {
  return d2c::Var(new LinkedHashSet__(__types__));
}

LinkedHashSet__::Iterator__::Iterator__(LinkedHashSet__ *set, int s, int e)
    : set_(set)
    , object_(set)
    , index_(s >= 0 ? s - 1 : -1)
    , start_(s)
    , end_(e) {
  if (start_ < 0) {
    start_ = -1;
  }
  if (end_ == -1) {
    end_ = static_cast<int>(object_->data_.size());
  }
}

d2c::Var LinkedHashSet__::Iterator__::moveNext() {
  index_++;
  return index_ >= start_ && index_ < end_;
}

d2c::Var LinkedHashSet__::Iterator__::__get_current() {
  return object_->orderedData_[index_];
}

#endif  // dart_collection_linked_hash_set_impl_h

/****************************************************************************
 * Copyright (C) 2014 by Brendan Duncan.                                    *
 *                                                                          *
 * This file is part of Dart2Cpp,                                           *
 * https://github.com/brendan-duncan/dart2cpp.                              *
 *                                                                          *
 * Licensed under the Apache License, Version 2.0 (the "License");          *
 * you may not use this file except in compliance with the License.         *
 * You may obtain a copy of the License at                                  *
 *                                                                          *
 * http://www.apache.org/licenses/LICENSE-2.0                               *
 *                                                                          *
 * Unless required by applicable law or agreed to in writing, software      *
 * distributed under the License is distributed on an "AS IS" BASIS,        *
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. *
 * See the License for the specific language governing permissions and      *
 * limitations under the License.                                           *
 ****************************************************************************/
#ifndef dart_core_bool_h
#define dart_core_bool_h

class Bool__ : public Bool {
 public:
  typedef Bool super;

  bool data_;

  Bool__(bool b = false);

  static void updateClassInheritance__(d2c::Object *object,
                                       const d2c::TypeSet &__types__ = {}) {
    object->addInheritance__<Bool__>(__types__);
    super::updateClassInheritance__(object, __types__);
  }

  bool isBool__() {
    return true;
  }

  bool boolValue__() {
    return data_;
  }

  virtual d2c::Var operator!();

  virtual d2c::Var operator==(const d2c::Var &other);

  virtual d2c::Var operator&&(const d2c::Var &other);

  virtual d2c::Var operator||(const d2c::Var &other);

  d2c::Var toString();
};

REGISTER_BOOL(Bool__)

d2c::Var __factory_Bool_fromEnvironment(const d2c::TypeSet &__types__,
                                        const d2c::Var &name,
                                        const d2c::Var &__named__);

#endif  // dart_core_bool_h

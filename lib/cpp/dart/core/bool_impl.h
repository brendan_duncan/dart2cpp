/****************************************************************************
 * Copyright (C) 2014 by Brendan Duncan.                                    *
 *                                                                          *
 * This file is part of Dart2Cpp,                                           *
 * https://github.com/brendan-duncan/dart2cpp.                              *
 *                                                                          *
 * Licensed under the Apache License, Version 2.0 (the "License");          *
 * you may not use this file except in compliance with the License.         *
 * You may obtain a copy of the License at                                  *
 *                                                                          *
 * http://www.apache.org/licenses/LICENSE-2.0                               *
 *                                                                          *
 * Unless required by applicable law or agreed to in writing, software      *
 * distributed under the License is distributed on an "AS IS" BASIS,        *
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. *
 * See the License for the specific language governing permissions and      *
 * limitations under the License.                                           *
 ****************************************************************************/
#ifndef dart_core_bool_impl_h
#define dart_core_bool_impl_h

Bool__::Bool__(bool b)
    : super(super::__constructor__())
    , data_(b) {
  updateClassInheritance__(this);
}

d2c::Var Bool__::operator!() {
  return !data_;
}

d2c::Var Bool__::operator==(const d2c::Var &other) {
  if (!d2c::is_type<Bool__>(other)) {
    throw d2c::Var(d2c::TypeFactory::CreateTypeError());
  }
  return data_ == other.boolValue();
}

d2c::Var Bool__::operator&&(const d2c::Var &other) {
  if (!d2c::is_type<Bool__>(other)) {
    throw d2c::Var(d2c::TypeFactory::CreateTypeError());
  }
  return data_ && other.boolValue();
}

d2c::Var Bool__::operator||(const d2c::Var &other) {
  if (!d2c::is_type<Bool__>(other)) {
    throw d2c::Var(d2c::TypeFactory::CreateTypeError());
  }
  return data_ || other.boolValue();
}

d2c::Var Bool__::toString() {
  return data_ ? "true" : "false";
}

d2c::Var __factory_Bool_fromEnvironment(const d2c::TypeSet &__types__,
                                        const d2c::Var &name,
                                        const d2c::Var &__named__) {
  d2c::Str env = getenv(name.toString().stringValue().c_str());
  if (env == "true") {
    return true;
  } else if (env == "false") {
    return false;
  }
  if (d2c::cast<d2c::NamedArguments>(__named__).contains(TK_containsKey__)) {
    return d2c::cast<d2c::NamedArguments>(__named__).get(TK_defaultValue__);
  }
  return false;
}

#endif  // dart_core_bool_impl_h

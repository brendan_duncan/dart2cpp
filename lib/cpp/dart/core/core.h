/****************************************************************************
 * Copyright (C) 2014 by Brendan Duncan.                                    *
 *                                                                          *
 * This file is part of Dart2Cpp,                                           *
 * https://github.com/brendan-duncan/dart2cpp.                              *
 *                                                                          *
 * Licensed under the Apache License, Version 2.0 (the "License");          *
 * you may not use this file except in compliance with the License.         *
 * You may obtain a copy of the License at                                  *
 *                                                                          *
 * http://www.apache.org/licenses/LICENSE-2.0                               *
 *                                                                          *
 * Unless required by applicable law or agreed to in writing, software      *
 * distributed under the License is distributed on an "AS IS" BASIS,        *
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. *
 * See the License for the specific language governing permissions and      *
 * limitations under the License.                                           *
 ****************************************************************************/
#ifndef dart_core_h
#define dart_core_h

#include "bool.h"
#include "date_time.h"
#include "double.h"
#include "errors.h"
#include "exceptions.h"
#include "expando.h"
#include "function.h"
#include "integer.h"
#include "list.h"
#include "map_iterable.h"
#include "object.h"
#include "regexp.h"
#include "stopwatch.h"
#include "string.h"
#include "string_buffer.h"
#include "uri.h"

#include "bool_impl.h"
#include "date_time_impl.h"
#include "double_impl.h"
#include "errors_impl.h"
#include "exceptions_impl.h"
#include "expando_impl.h"
#include "function_impl.h"
#include "integer_impl.h"
#include "list_impl.h"
#include "map_iterable_impl.h"
#include "object_impl.h"
#include "regexp_impl.h"
#include "stopwatch_impl.h"
#include "string_buffer_impl.h"
#include "string_impl.h"
#include "uri_impl.h"

d2c::Var identical(d2c::Var a, d2c::Var b) {
  return a.object_ == b.object_;
}

d2c::Var identityHashCode(d2c::Var a) {
  if (a.object_ == nullptr) {
    return 0;
  }
  return a.object_->identityHashCode__();
}

#endif  // dart_core_h

/****************************************************************************
 * Copyright (C) 2014 by Brendan Duncan.                                    *
 *                                                                          *
 * This file is part of Dart2Cpp,                                           *
 * https://github.com/brendan-duncan/dart2cpp.                              *
 *                                                                          *
 * Licensed under the Apache License, Version 2.0 (the "License");          *
 * you may not use this file except in compliance with the License.         *
 * You may obtain a copy of the License at                                  *
 *                                                                          *
 * http://www.apache.org/licenses/LICENSE-2.0                               *
 *                                                                          *
 * Unless required by applicable law or agreed to in writing, software      *
 * distributed under the License is distributed on an "AS IS" BASIS,        *
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. *
 * See the License for the specific language governing permissions and      *
 * limitations under the License.                                           *
 ****************************************************************************/
#ifndef dart_core_date_time_h
#define dart_core_date_time_h

d2c::Var __factory_DateTime__internal(
    const d2c::TypeSet &__types__, const d2c::Var &year, const d2c::Var &month,
    const d2c::Var &day, const d2c::Var &hour, const d2c::Var &minute,
    const d2c::Var &second, const d2c::Var &millisecond, const d2c::Var &isUtc);

d2c::Var __factory_DateTime__now(const d2c::TypeSet &__types__);

void __constructor_DateTime__internal(
    DateTime *self, const d2c::Var &year, const d2c::Var &month,
    const d2c::Var &day, const d2c::Var &hour, const d2c::Var &minute,
    const d2c::Var &second, const d2c::Var &millisecond, const d2c::Var &isUtc);

void __constructor_DateTime__now(DateTime *self);

d2c::Var __method_DateTime___get_timeZoneName(DateTime *self);

d2c::Var __method_DateTime___get_timeZoneOffset(DateTime *self);

d2c::Var __method_DateTime___get_year(DateTime *self);

d2c::Var __method_DateTime___get_day(DateTime *self);

d2c::Var __method_DateTime___get_hour(DateTime *self);

d2c::Var __method_DateTime___get_minute(DateTime *self);

d2c::Var __method_DateTime___get_second(DateTime *self);

d2c::Var __method_DateTime___get_millisecond(DateTime *self);

d2c::Var __method_DateTime___get_weekday(DateTime *self);

d2c::Var __method_DateTime___get_month(DateTime *self);

d2c::Var __static_DateTime__brokenDownDateToMillisecondsSinceEpoch(
    const d2c::Var &year, const d2c::Var &month, const d2c::Var &day,
    const d2c::Var &hour, const d2c::Var &minute, const d2c::Var &second,
    const d2c::Var &millisecond, const d2c::Var &isUtc);

#endif  // dart_core_date_time_h

/****************************************************************************
 * Copyright (C) 2014 by Brendan Duncan.                                    *
 *                                                                          *
 * This file is part of Dart2Cpp,                                           *
 * https://github.com/brendan-duncan/dart2cpp.                              *
 *                                                                          *
 * Licensed under the Apache License, Version 2.0 (the "License");          *
 * you may not use this file except in compliance with the License.         *
 * You may obtain a copy of the License at                                  *
 *                                                                          *
 * http://www.apache.org/licenses/LICENSE-2.0                               *
 *                                                                          *
 * Unless required by applicable law or agreed to in writing, software      *
 * distributed under the License is distributed on an "AS IS" BASIS,        *
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. *
 * See the License for the specific language governing permissions and      *
 * limitations under the License.                                           *
 ****************************************************************************/
#ifndef dart_core_double_h
#define dart_core_double_h

class Double__ : public Double {
 public:
  typedef Double super;

  double data_;

  Double__(double init);

  static void updateClassInheritance__(d2c::Object *object,
                                       const d2c::TypeSet &__types__ = {}) {
    object->addInheritance__<Double__>(__types__);
    super::updateClassInheritance__(object, __types__);
  }

  bool isDouble__() {
    return true;
  }

  double doubleValue__() {
    return data_;
  }

  d2c::Var remainder(const d2c::Var &other);

  d2c::Var operator==(const d2c::Var &other);

  d2c::Var operator+(const d2c::Var &other);

  d2c::Var operator-(const d2c::Var &other);

  d2c::Var operator*(const d2c::Var &other);

  d2c::Var operator%(const d2c::Var &other);

  d2c::Var operator/(const d2c::Var &other);

  d2c::Var __intDiv(const d2c::Var &other);

  d2c::Var operator-();

  d2c::Var abs();

  d2c::Var __get_sign();

  d2c::Var round();

  d2c::Var floor();

  d2c::Var ceil();

  d2c::Var truncate();

  d2c::Var roundToDouble();

  d2c::Var floorToDouble();

  d2c::Var ceilToDouble();

  d2c::Var truncateToDouble();

  d2c::Var toString();

  virtual d2c::Var __get_hashCode();

  virtual d2c::Var compareTo(const d2c::Var &other);

  virtual d2c::Var operator<(const d2c::Var &other);

  virtual d2c::Var operator<=(const d2c::Var &other);

  virtual d2c::Var operator>(const d2c::Var &other);

  virtual d2c::Var operator>=(const d2c::Var &other);

  virtual d2c::Var __get_isNaN();

  virtual d2c::Var __get_isNegative();

  virtual d2c::Var __get_isInfinite();

  virtual d2c::Var __get_isFinite();

  virtual d2c::Var clamp(const d2c::Var &lowerLimit,
                         const d2c::Var &upperLimit);

  virtual d2c::Var toInt();

  virtual d2c::Var toDouble();

  virtual d2c::Var toStringAsFixed(const d2c::Var &fractionDigits);

  virtual d2c::Var toStringAsExponential(const d2c::Var &fractionDigits = null);

  virtual d2c::Var toStringAsPrecision(const d2c::Var &precision);
};

REGISTER_DOUBLE(Double__)

d2c::Var __static_Double_parse(const d2c::Var &source,
                               const d2c::Var &onError = null);

#endif  // dart_core_double_h

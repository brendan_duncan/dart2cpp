/****************************************************************************
 * Copyright (C) 2014 by Brendan Duncan.                                    *
 *                                                                          *
 * This file is part of Dart2Cpp,                                           *
 * https://github.com/brendan-duncan/dart2cpp.                              *
 *                                                                          *
 * Licensed under the Apache License, Version 2.0 (the "License");          *
 * you may not use this file except in compliance with the License.         *
 * You may obtain a copy of the License at                                  *
 *                                                                          *
 * http://www.apache.org/licenses/LICENSE-2.0                               *
 *                                                                          *
 * Unless required by applicable law or agreed to in writing, software      *
 * distributed under the License is distributed on an "AS IS" BASIS,        *
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. *
 * See the License for the specific language governing permissions and      *
 * limitations under the License.                                           *
 ****************************************************************************/
#ifndef dart_core_double_impl_h
#define dart_core_double_impl_h

#include "double.h"

Double__::Double__(double init)
    : super(Double::__constructor__())
    , data_(init) {
  updateClassInheritance__(this);
}

d2c::Var Double__::operator==(const d2c::Var &other) {
  if (other.isInt()) {
    return data_ == static_cast<double>(other.intValue());
  }
  if (other.isDouble()) {
    return data_ == other.doubleValue();
  }
  throw d2c::Var(d2c::TypeFactory::CreateTypeError());
}

d2c::Var Double__::remainder(const d2c::Var &other) {
  if (other.isInt()) {
    return std::fmod(data_, static_cast<double>(other.intValue()));
  } else if (other.isDouble()) {
    return std::fmod(data_, other.doubleValue());
  }
  throw d2c::Var(d2c::TypeFactory::CreateTypeError());
}

d2c::Var Double__::operator+(const d2c::Var &other) {
  if (other.isInt()) {
    return data_ + other.intValue();
  } else if (other.isDouble()) {
    return data_ + other.doubleValue();
  }
  throw d2c::Var(d2c::TypeFactory::CreateTypeError());
}

d2c::Var Double__::operator-(const d2c::Var &other) {
  if (other.isInt()) {
    return data_ - other.intValue();
  } else if (other.isDouble()) {
    return data_ - other.doubleValue();
  }
  throw d2c::Var(d2c::TypeFactory::CreateTypeError());
}

d2c::Var Double__::operator*(const d2c::Var &other) {
  if (other.isInt()) {
    return data_ * other.intValue();
  } else if (other.isDouble()) {
    return data_ * other.doubleValue();
  }
  throw d2c::Var(d2c::TypeFactory::CreateTypeError());
}

d2c::Var Double__::operator%(const d2c::Var &other) {
  if (other.isInt()) {
    return std::fmod(data_, static_cast<double>(other.intValue()));
  }

  if (other.isDouble()) {
    return std::fmod(data_, other.doubleValue());
  }
  throw d2c::Var(d2c::TypeFactory::CreateTypeError());
}

d2c::Var Double__::operator/(const d2c::Var &other) {
  if (other.isInt()) {
    return data_ / other.intValue();
  } else if (other.isDouble()) {
    return data_ / other.doubleValue();
  }
  throw d2c::Var(d2c::TypeFactory::CreateTypeError());
}

d2c::Var Double__::__intDiv(const d2c::Var &other) {
  if (other.isInt()) {
    return static_cast<std::int64_t>(data_ / other.intValue());
  } else if (other.isDouble()) {
    return static_cast<std::int64_t>(data_ / other.doubleValue());
  }
  throw d2c::Var(d2c::TypeFactory::CreateTypeError());
}

d2c::Var Double__::operator-() {
  return -data_;
}

d2c::Var Double__::abs() {
  return std::fabs(data_);
}

d2c::Var Double__::__get_sign() {
  return data_ < 0.0 ? -1 : data_ == 0.0 ? 0 : 1;
}

d2c::Var Double__::round() {
  return static_cast<std::int64_t>(std::round(data_));
}

d2c::Var Double__::floor() {
  return static_cast<std::int64_t>(std::floor(data_));
}

d2c::Var Double__::ceil() {
  return static_cast<std::int64_t>(std::ceil(data_));
}

d2c::Var Double__::truncate() {
  return static_cast<std::int64_t>(std::trunc(data_));
}

d2c::Var Double__::roundToDouble() {
  return std::round(data_);
}

d2c::Var Double__::floorToDouble() {
  return std::floor(data_);
}

d2c::Var Double__::ceilToDouble() {
  return std::ceil(data_);
}

d2c::Var Double__::truncateToDouble() {
  return std::trunc(data_);
}

d2c::Var Double__::toString() {
  if (std::isnan(data_)) {
    if (std::signbit(data_) == 1) {
      return "-NaN";
    }
    return "NaN";
  } else if (std::isinf(data_)) {
    if (std::signbit(data_) == 1) {
      return "-Infinity";
    }
    return "Infinity";
  }
  return d2c::Str(std::to_string(data_));
}

d2c::Var Double__::__get_hashCode() {
  return static_cast<std::int64_t>(std::hash<double>()(data_));
}

d2c::Var Double__::compareTo(const d2c::Var &other) {
  if (!other.isInt() && !other.isDouble()) {
    throw d2c::Var(d2c::TypeFactory::CreateTypeError());
  }
  double o = other.isInt() ? static_cast<double>(other.intValue())
                           : other.doubleValue();
  return data_ < o ? -1 : data_ > o ? 1 : 0;
}

d2c::Var Double__::operator<(const d2c::Var &other) {
  if (!other.isInt() && !other.isDouble()) {
    throw d2c::Var(d2c::TypeFactory::CreateTypeError());
  }
  double o = other.isInt() ? static_cast<double>(other.intValue())
                           : other.doubleValue();
  return data_ < o;
}

d2c::Var Double__::operator<=(const d2c::Var &other) {
  if (!other.isInt() && !other.isDouble()) {
    throw d2c::Var(d2c::TypeFactory::CreateTypeError());
  }
  double o = other.isInt() ? static_cast<double>(other.intValue())
                           : other.doubleValue();
  return data_ <= o;
}

d2c::Var Double__::operator>(const d2c::Var &other) {
  if (!other.isInt() && !other.isDouble()) {
    throw d2c::Var(d2c::TypeFactory::CreateTypeError());
  }
  double o = other.isInt() ? static_cast<double>(other.intValue())
                           : other.doubleValue();
  return data_ > o;
}

d2c::Var Double__::operator>=(const d2c::Var &other) {
  if (!other.isInt() && !other.isDouble()) {
    throw d2c::Var(d2c::TypeFactory::CreateTypeError());
  }
  double o = other.isInt() ? static_cast<double>(other.intValue())
                           : other.doubleValue();
  return data_ >= o;
}

d2c::Var Double__::__get_isNaN() {
  return std::isnan(data_);
}

d2c::Var Double__::__get_isNegative() {
  // just doing a data_ < 0 will fail for -0.0.
  return std::signbit(data_) == 1;
}

d2c::Var Double__::__get_isInfinite() {
  return std::isinf(data_);
}

d2c::Var Double__::__get_isFinite() {
  return !std::isnan(data_) && !std::isinf(data_);
}

d2c::Var Double__::clamp(const d2c::Var &lowerLimit,
                                const d2c::Var &upperLimit) {
  if (!lowerLimit.isInt() && !lowerLimit.isDouble()) {
    throw d2c::Var(d2c::TypeFactory::CreateTypeError());
  }
  double l = lowerLimit.isInt() ? static_cast<double>(lowerLimit.intValue())
                                : lowerLimit.doubleValue();

  if (!upperLimit.isInt() && !upperLimit.isDouble()) {
    throw d2c::Var(d2c::TypeFactory::CreateTypeError());
  }
  double u = upperLimit.isInt() ? static_cast<double>(upperLimit.intValue())
                                : upperLimit.doubleValue();

  return data_ < l ? l : data_ > u ? u : data_;
}

d2c::Var Double__::toInt() {
  if (std::isnan(data_) || std::isinf(data_)) {
    throw d2c::Var(d2c::TypeFactory::CreateUnsupportedError(""));
  }
  return static_cast<std::int64_t>(data_);
}

d2c::Var Double__::toDouble() {
  return d2c::Var(this);
}

d2c::Var Double__::toStringAsFixed(const d2c::Var &fractionDigits) {
  return toString();
}

d2c::Var Double__::toStringAsExponential(
    const d2c::Var &fractionDigits) {
  return toString();
}

d2c::Var Double__::toStringAsPrecision(const d2c::Var &precision) {
  return toString();
}

d2c::Var __static_Double_parse(const d2c::Var &source,
                               const d2c::Var &onError) {
  try {
    if (source.isString()) {
      d2c::Str value = source.stringValue();
      if (value == "NaN") {
        return std::numeric_limits<double>::quiet_NaN();
      } else if (value == "-NaN") {
        return -std::numeric_limits<double>::quiet_NaN();
      } else if (value == "Infinity") {
        return std::numeric_limits<double>::infinity();
      } else if (value == "-Infinity") {
        return -std::numeric_limits<double>::infinity();
      }
      double v = std::stod(value.c_str(), nullptr);
      return v;
    }
  } catch (...) {
  }

  if (onError.isNotNull()) {
    onError(source);
  } else {
    throw d2c::Var(d2c::TypeFactory::CreateFormatException(""));
  }

  return null;
}

#endif  // dart_core_double_impl_h

/****************************************************************************
 * Copyright (C) 2014 by Brendan Duncan.                                    *
 *                                                                          *
 * This file is part of Dart2Cpp,                                           *
 * https://github.com/brendan-duncan/dart2cpp.                              *
 *                                                                          *
 * Licensed under the Apache License, Version 2.0 (the "License");          *
 * you may not use this file except in compliance with the License.         *
 * You may obtain a copy of the License at                                  *
 *                                                                          *
 * http://www.apache.org/licenses/LICENSE-2.0                               *
 *                                                                          *
 * Unless required by applicable law or agreed to in writing, software      *
 * distributed under the License is distributed on an "AS IS" BASIS,        *
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. *
 * See the License for the specific language governing permissions and      *
 * limitations under the License.                                           *
 ****************************************************************************/
#ifndef dart_core_errors_h
#define dart_core_errors_h

class TypeError__ : public TypeError {
 public:
  typedef TypeError super;
  TypeError__();
};
REGISTER_TYPE_ERROR(TypeError__)

class ArgumentError__ : public ArgumentError {
 public:
  typedef ArgumentError super;
  ArgumentError__(const d2c::Var &message = null);
};
REGISTER_ARGUMENT_ERROR(ArgumentError__)

class UnsupportedError__ : public UnsupportedError {
 public:
  typedef UnsupportedError super;
  UnsupportedError__(const d2c::Var &message = null);
};
REGISTER_UNSUPPORTED_ERROR(UnsupportedError__)

class StateError__ : public StateError {
 public:
  typedef StateError super;
  StateError__(const d2c::Var &message = null);
};
REGISTER_STATE_ERROR(StateError__)

class NoSuchMethodError__ : public NoSuchMethodError {
 public:
  typedef NoSuchMethodError super;
  NoSuchMethodError__(const d2c::Var &receiver, const d2c::Var &memberName,
                      const d2c::Var &positionalArguments,
                      const d2c::Var &namedArguments,
                      const d2c::Var &existingArgumentNames);
};
REGISTER_NO_SUCH_METHOD_ERROR(NoSuchMethodError__)

d2c::Var __method_Error___get_stackTrace(Error *self);

d2c::Var __method_NoSuchMethodError_toString(NoSuchMethodError *self);

d2c::Var __static_Error__objectToString(const d2c::Var &object);

#endif  // dart_core_errors_h

/****************************************************************************
 * Copyright (C) 2014 by Brendan Duncan.                                    *
 *                                                                          *
 * This file is part of Dart2Cpp,                                           *
 * https://github.com/brendan-duncan/dart2cpp.                              *
 *                                                                          *
 * Licensed under the Apache License, Version 2.0 (the "License");          *
 * you may not use this file except in compliance with the License.         *
 * You may obtain a copy of the License at                                  *
 *                                                                          *
 * http://www.apache.org/licenses/LICENSE-2.0                               *
 *                                                                          *
 * Unless required by applicable law or agreed to in writing, software      *
 * distributed under the License is distributed on an "AS IS" BASIS,        *
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. *
 * See the License for the specific language governing permissions and      *
 * limitations under the License.                                           *
 ****************************************************************************/
#ifndef dart_core_errors_impl_h
#define dart_core_errors_impl_h

TypeError__::TypeError__()
    : super(super::__constructor__()) {
}

ArgumentError__::ArgumentError__(const d2c::Var &message)
    : super(super::__constructor__(), message) {
}

UnsupportedError__::UnsupportedError__(const d2c::Var &message)
    : super(super::__constructor__(), message) {
}

StateError__::StateError__(const d2c::Var &message)
    : super(super::__constructor__(), message) {
}

NoSuchMethodError__::NoSuchMethodError__(const d2c::Var &receiver,
                                         const d2c::Var &memberName,
                                         const d2c::Var &positionalArguments,
                                         const d2c::Var &namedArguments,
                                         const d2c::Var &existingArgumentNames)
    : super(super::__constructor__(), receiver, memberName, positionalArguments,
            namedArguments, existingArgumentNames) {
}

d2c::Var __method_Error___get_stackTrace(Error *self) {
  throw "TODO: implement";
}

d2c::Var __method_NoSuchMethodError_toString(NoSuchMethodError *self) {
  throw "TODO: implement";
}

d2c::Var __static_Error__objectToString(const d2c::Var &object) {
  throw "TODO: implement";
}

#endif  // dart_core_errors_impl_h

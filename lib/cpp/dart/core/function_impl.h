/****************************************************************************
 * Copyright (C) 2014 by Brendan Duncan.                                    *
 *                                                                          *
 * This file is part of Dart2Cpp,                                           *
 * https://github.com/brendan-duncan/dart2cpp.                              *
 *                                                                          *
 * Licensed under the Apache License, Version 2.0 (the "License");          *
 * you may not use this file except in compliance with the License.         *
 * You may obtain a copy of the License at                                  *
 *                                                                          *
 * http://www.apache.org/licenses/LICENSE-2.0                               *
 *                                                                          *
 * Unless required by applicable law or agreed to in writing, software      *
 * distributed under the License is distributed on an "AS IS" BASIS,        *
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. *
 * See the License for the specific language governing permissions and      *
 * limitations under the License.                                           *
 ****************************************************************************/
#ifndef dart_core_function_impl_h
#define dart_core_function_impl_h

d2c::Var __static_Function_apply(const d2c::Var &function,
                                 const d2c::Var &positionalArguments,
                                 const d2c::Var &namedArguments) {
  if (positionalArguments.isNull()) {
    return function();
  }

  List__ *posArgs = dynamic_cast<List__ *>(positionalArguments.object_);
  if (posArgs == nullptr) {
    throw d2c::Var(d2c::TypeFactory::CreateTypeError());
  }

  size_t numArgs = posArgs->data_.size();
  if (numArgs == 0) {
    return function();
  } else if (numArgs == 1) {
    return function(posArgs->data_[0]);
  } else if (numArgs == 2) {
    return function(posArgs->data_[0], posArgs->data_[1]);
  } else if (numArgs == 3) {
    return function(posArgs->data_[0], posArgs->data_[1], posArgs->data_[2]);
  } else if (numArgs == 4) {
    return function(posArgs->data_[0], posArgs->data_[1], posArgs->data_[2],
             posArgs->data_[3]);
  } else if (numArgs == 5) {
    return function(posArgs->data_[0], posArgs->data_[1], posArgs->data_[2],
             posArgs->data_[3], posArgs->data_[4]);
  } else if (numArgs == 6) {
    return function(posArgs->data_[0], posArgs->data_[1], posArgs->data_[2],
      posArgs->data_[3], posArgs->data_[4], posArgs->data_[5]);
  } else if (numArgs == 7) {
    return function(posArgs->data_[0], posArgs->data_[1], posArgs->data_[2],
      posArgs->data_[3], posArgs->data_[4], posArgs->data_[5],
      posArgs->data_[6]);
  } else if (numArgs == 8) {
    return function(posArgs->data_[0], posArgs->data_[1], posArgs->data_[2],
      posArgs->data_[3], posArgs->data_[4], posArgs->data_[5],
      posArgs->data_[6], posArgs->data_[7]);
  } else if (numArgs == 9) {
    return function(posArgs->data_[0], posArgs->data_[1], posArgs->data_[2],
      posArgs->data_[3], posArgs->data_[4], posArgs->data_[5],
      posArgs->data_[6], posArgs->data_[7], posArgs->data_[8]);
  } else if (numArgs == 10) {
    return function(posArgs->data_[0], posArgs->data_[1], posArgs->data_[2],
      posArgs->data_[3], posArgs->data_[4], posArgs->data_[5],
      posArgs->data_[6], posArgs->data_[7], posArgs->data_[8],
      posArgs->data_[9]);
  }

  throw "Unhandled number of arguments";
}

#endif  // dart_core_function_impl_h

/****************************************************************************
 * Copyright (C) 2014 by Brendan Duncan.                                    *
 *                                                                          *
 * This file is part of Dart2Cpp,                                           *
 * https://github.com/brendan-duncan/dart2cpp.                              *
 *                                                                          *
 * Licensed under the Apache License, Version 2.0 (the "License");          *
 * you may not use this file except in compliance with the License.         *
 * You may obtain a copy of the License at                                  *
 *                                                                          *
 * http://www.apache.org/licenses/LICENSE-2.0                               *
 *                                                                          *
 * Unless required by applicable law or agreed to in writing, software      *
 * distributed under the License is distributed on an "AS IS" BASIS,        *
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. *
 * See the License for the specific language governing permissions and      *
 * limitations under the License.                                           *
 ****************************************************************************/
#ifndef dart_core_integer_h
#define dart_core_integer_h

class Integer__ : public Integer {
 public:
  typedef Integer super;

  std::int64_t data_;

  Integer__(std::int64_t init);

  static void updateClassInheritance__(d2c::Object *object,
                                       const d2c::TypeSet &__types__ = {}) {
    object->addInheritance__<Integer__>(__types__);
    super::updateClassInheritance__(object, __types__);
  }

  bool isInt__() {
    return true;
  }

  virtual std::int64_t intValue__() {
    return data_;
  }

  d2c::Var operator+(const d2c::Var &other);

  d2c::Var operator-(const d2c::Var &other);

  d2c::Var operator*(const d2c::Var &other);

  d2c::Var operator/(const d2c::Var &other);

  d2c::Var __intDiv(const d2c::Var &other);

  d2c::Var operator==(const d2c::Var &other);

  d2c::Var operator&(const d2c::Var &other);

  d2c::Var operator|(const d2c::Var &other);

  d2c::Var operator^(const d2c::Var &other);

  d2c::Var operator%(const d2c::Var &other);

  d2c::Var operator~();

  d2c::Var operator<<(const d2c::Var &shiftAmount);

  d2c::Var operator>>(const d2c::Var &shiftAmount);

  d2c::Var __get_isEven();

  d2c::Var __get_isOdd();

  d2c::Var __get_bitLength();

  d2c::Var toUnsigned(const d2c::Var &width);

  d2c::Var toSigned(const d2c::Var &width);

  d2c::Var operator-();

  d2c::Var abs();

  d2c::Var __get_sign();

  d2c::Var round();

  d2c::Var floor();

  d2c::Var ceil();

  d2c::Var truncate();

  d2c::Var roundToDouble();

  d2c::Var floorToDouble();

  d2c::Var ceilToDouble();

  d2c::Var truncateToDouble();

  d2c::Var toString();

  d2c::Var toRadixString(const d2c::Var &radix);

  d2c::Var remainder(const d2c::Var &other);

  d2c::Var operator<(const d2c::Var &other);

  d2c::Var operator<=(const d2c::Var &other);

  d2c::Var operator>(const d2c::Var &other);

  d2c::Var operator>=(const d2c::Var &other);

  d2c::Var __get_hashCode();

  d2c::Var compareTo(const d2c::Var &other);

  d2c::Var __get_isNaN();

  d2c::Var __get_isNegative();

  d2c::Var __get_isInfinite();

  d2c::Var __get_isFinite();

  d2c::Var clamp(const d2c::Var &lowerLimit, const d2c::Var &upperLimit);

  d2c::Var toInt();

  d2c::Var toDouble();

  d2c::Var toStringAsFixed(const d2c::Var &fractionDigits);

  d2c::Var toStringAsExponential(const d2c::Var &fractionDigits);

  d2c::Var toStringAsPrecision(const d2c::Var &precision);

  virtual void initialize__();
};

REGISTER_INT(Integer__)

d2c::Var __factory_Integer_fromEnvironment(const d2c::TypeSet &__types__,
                                           const d2c::Var &name,
                                           const d2c::Var &__named__);

d2c::Var __static_Integer_parse(const d2c::Var &source,
                                const d2c::Var &__named__);

#endif  // dart_core_integer_h

/****************************************************************************
 * Copyright (C) 2014 by Brendan Duncan.                                    *
 *                                                                          *
 * This file is part of Dart2Cpp,                                           *
 * https://github.com/brendan-duncan/dart2cpp.                              *
 *                                                                          *
 * Licensed under the Apache License, Version 2.0 (the "License");          *
 * you may not use this file except in compliance with the License.         *
 * You may obtain a copy of the License at                                  *
 *                                                                          *
 * http://www.apache.org/licenses/LICENSE-2.0                               *
 *                                                                          *
 * Unless required by applicable law or agreed to in writing, software      *
 * distributed under the License is distributed on an "AS IS" BASIS,        *
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. *
 * See the License for the specific language governing permissions and      *
 * limitations under the License.                                           *
 ****************************************************************************/
#ifndef dart_core_integer_impl_h
#define dart_core_integer_impl_h

#include "integer.h"

Integer__::Integer__(std::int64_t init)
    : super(Integer::__constructor__())
    , data_(init) {
  updateClassInheritance__(this);
}

d2c::Var Integer__::operator+(const d2c::Var &other) {
  if (other.isInt()) {
    return data_ + other.intValue();
  }
  if (other.isDouble()) {
    return data_ + other.doubleValue();
  }
  throw d2c::Var(d2c::TypeFactory::CreateTypeError());
}

d2c::Var Integer__::operator-(const d2c::Var &other) {
  if (other.isInt()) {
    return data_ - other.intValue();
  }
  if (other.isDouble()) {
    return data_ - other.doubleValue();
  }
  throw d2c::Var(d2c::TypeFactory::CreateTypeError());
}

d2c::Var Integer__::operator*(const d2c::Var &other) {
  if (other.isInt()) {
    return data_ * other.intValue();
  }
  if (other.isDouble()) {
    return data_ * other.doubleValue();
  }
  throw d2c::Var(d2c::TypeFactory::CreateTypeError());
}

d2c::Var Integer__::operator/(const d2c::Var &other) {
  if (other.isInt()) {
    return static_cast<double>(data_) / static_cast<double>(other.intValue());
  }
  if (other.isDouble()) {
    return static_cast<double>(data_ / other.doubleValue());
  }
  throw d2c::Var(d2c::TypeFactory::CreateTypeError());
}

d2c::Var Integer__::__intDiv(const d2c::Var &other) {
  if (other.isInt()) {
    return static_cast<std::int64_t>(data_ / other.intValue());
  }
  if (other.isDouble()) {
    return static_cast<std::int64_t>(data_ / other.doubleValue());
  }
  throw d2c::Var(d2c::TypeFactory::CreateTypeError());
}

d2c::Var Integer__::operator==(const d2c::Var &other) {
  if (other.isDouble()) {
    return static_cast<double>(data_) == other.doubleValue();
  }
  if (other.isInt()) {
    return data_ == other.intValue();
  }
  throw d2c::Var(d2c::TypeFactory::CreateTypeError());
}

d2c::Var Integer__::operator&(const d2c::Var &other) {
  if (!other.isInt()) {
    throw d2c::Var(d2c::TypeFactory::CreateTypeError());
  }
  return data_ & other.intValue();
}

d2c::Var Integer__::operator|(const d2c::Var &other) {
  if (!other.isInt()) {
    throw d2c::Var(d2c::TypeFactory::CreateTypeError());
  }
  return data_ | other.intValue();
}

d2c::Var Integer__::operator^(const d2c::Var &other) {
  if (!other.isInt()) {
    throw d2c::Var(d2c::TypeFactory::CreateTypeError());
  }
  return data_ ^ other.intValue();
} d2c::Var Integer__::
operator%(const d2c::Var &other) {
  if (other.isDouble()) {
    double n = static_cast<double>(data_);
    return std::fmod(n, other.doubleValue());
  }

  if (!other.isInt()) {
    throw d2c::Var(d2c::TypeFactory::CreateTypeError());
  }

  std::int64_t n = data_;
  std::int64_t d = other.intValue();

  std::int64_t r = n % d;
  if (r == 0) {
    return (std::int64_t)0;
  }
  if (r > 0) {
    return r;
  }

  if (d < 0) {
    return r - d;
  }

  return r + d;
}

d2c::Var Integer__::operator~() {
  return ~data_;
}

d2c::Var Integer__::operator<<(const d2c::Var &shiftAmount) {
  if (!shiftAmount.isInt()) {
    throw d2c::Var(d2c::TypeFactory::CreateTypeError());
  }
  return data_ << shiftAmount.intValue();
}

d2c::Var Integer__::operator>>(const d2c::Var &shiftAmount) {
  if (!shiftAmount.isInt()) {
    throw d2c::Var(d2c::TypeFactory::CreateTypeError());
  }
  return data_ >> shiftAmount.intValue();
}

d2c::Var Integer__::__get_isEven() {
  return (data_ & 0x1) == 0;
}

d2c::Var Integer__::__get_isOdd() {
  return (data_ & 0x1) != 0;
}

d2c::Var Integer__::__get_bitLength() {
  throw "TODO: implement";
}

d2c::Var Integer__::toUnsigned(const d2c::Var &width) {
  throw "TODO: implement";
}

d2c::Var Integer__::toSigned(const d2c::Var &width) {
  throw "TODO: implement";
}

d2c::Var Integer__::operator-() {
  return -data_;
}

d2c::Var Integer__::abs() {
  if (data_ < 0) {
    return -data_;
  }
  return d2c::Var(this);
}

d2c::Var Integer__::__get_sign() {
  return (data_ < 0) ? -1 : (data_ == 0) ? 0 : 1;
}

d2c::Var Integer__::round() {
  return d2c::Var(this);
}

d2c::Var Integer__::floor() {
  return d2c::Var(this);
}

d2c::Var Integer__::ceil() {
  return d2c::Var(this);
}

d2c::Var Integer__::truncate() {
  return d2c::Var(this);
}

d2c::Var Integer__::roundToDouble() {
  return (double)data_;
}

d2c::Var Integer__::floorToDouble() {
  return (double)data_;
}

d2c::Var Integer__::ceilToDouble() {
  return (double)data_;
}

d2c::Var Integer__::truncateToDouble() {
  return (double)data_;
}

d2c::Var Integer__::toString() {
  return d2c::Str(std::to_string(data_));
}

d2c::Str __dec2bin(unsigned long long n) {
  std::stringstream stream;
  if (n == 0) {
    return "0";
  }

  while (n != 0) {
    stream << (n & 1);
    n >>= 1;
  }

  std::string s = stream.str();
  return std::string(s.rbegin(), s.rend());
}

d2c::Var Integer__::toRadixString(const d2c::Var &radix) {
  if (!radix.isInt()) {
    throw d2c::Var(
        d2c::TypeFactory::CreateArgumentError("Invalid Radix Value"));
  }

  int r = static_cast<int>(radix.intValue());
  if (r == 10) {
    return d2c::Str(std::to_string(data_));
  } else if (r == 16) {
    std::stringstream stream;
    stream << std::hex << data_;
    return d2c::Str(stream.str());
  } else if (r == 8) {
    std::stringstream stream;
    stream << std::oct << data_;
    return d2c::Str(stream.str());
  } else if (r == 2) {
    return d2c::Str(__dec2bin((unsigned long long)data_));
  }

  // TODO what to do with an unsupported radix...
  throw d2c::Var(d2c::TypeFactory::CreateArgumentError("Invalid Radix Value"));
}

d2c::Var Integer__::remainder(const d2c::Var &other) {
  if (other.isDouble()) {
    return std::fmod(static_cast<double>(data_), other.doubleValue());
  }
  if (!other.isInt()) {
    throw d2c::Var(d2c::TypeFactory::CreateTypeError());
  }

  std::int64_t n = data_;
  std::int64_t d = other.intValue();

  std::int64_t r = n % d;
  return r;
}

d2c::Var Integer__::operator<(const d2c::Var &other) {
  if (other.isInt()) {
    return data_ < other.intValue();
  } else if (other.isDouble()) {
    return data_ < other.doubleValue();
  }
  throw d2c::Var(d2c::TypeFactory::CreateTypeError());
}

d2c::Var Integer__::operator<=(const d2c::Var &other) {
  if (other.isInt()) {
    return data_ <= other.intValue();
  } else if (other.isDouble()) {
    return data_ <= other.doubleValue();
  }
  throw d2c::Var(d2c::TypeFactory::CreateTypeError());
}

d2c::Var Integer__::operator>(const d2c::Var &other) {
  if (other.isInt()) {
    return data_ > other.intValue();
  } else if (other.isDouble()) {
    return data_ > other.doubleValue();
  }
  throw d2c::Var(d2c::TypeFactory::CreateTypeError());
}

d2c::Var Integer__::operator>=(const d2c::Var &other) {
  if (other.isInt()) {
    return data_ >= other.intValue();
  } else if (other.isDouble()) {
    return data_ >= other.doubleValue();
  }
  throw d2c::Var(d2c::TypeFactory::CreateTypeError());
}

d2c::Var Integer__::__get_hashCode() {
  return static_cast<std::int64_t>(std::hash<std::int64_t>()(data_));
}

d2c::Var Integer__::compareTo(const d2c::Var &other) {
  if (other.isInt()) {
    return data_ < other.intValue() ? -1 : data_ == other.intValue() ? 0 : 1;
  } else if (other.isDouble()) {
    return data_ < other.doubleValue() ? -1 : data_ == other.doubleValue() ? 0
                                                                           : 1;
  }
  throw d2c::Var(d2c::TypeFactory::CreateTypeError());
}

d2c::Var Integer__::__get_isNaN() {
  return false;
}

d2c::Var Integer__::__get_isNegative() {
  return data_ < 0;
}

d2c::Var Integer__::__get_isInfinite() {
  return false;
}

d2c::Var Integer__::__get_isFinite() {
  return true;
}

d2c::Var Integer__::clamp(const d2c::Var &lowerLimit,
                          const d2c::Var &upperLimit) {
  std::int64_t l;
  if (lowerLimit.isInt()) {
    l = lowerLimit.intValue();
  } else if (lowerLimit.isDouble()) {
    l = static_cast<std::int64_t>(lowerLimit.doubleValue());
  } else {
    throw d2c::Var(d2c::TypeFactory::CreateTypeError());
  }

  std::int64_t u;
  if (upperLimit.isInt()) {
    u = upperLimit.intValue();
  } else if (upperLimit.isDouble()) {
    u = static_cast<std::int64_t>(upperLimit.doubleValue());
  } else {
    throw d2c::Var(d2c::TypeFactory::CreateTypeError());
  }

  return (data_ < l) ? l : (data_ > u) ? u : data_;
}

d2c::Var Integer__::toInt() {
  return d2c::Var(this);
}

d2c::Var Integer__::toDouble() {
  return static_cast<double>(data_);
}

d2c::Var Integer__::toStringAsFixed(const d2c::Var &fractionDigits) {
  return toString();
}

d2c::Var Integer__::toStringAsExponential(const d2c::Var &fractionDigits) {
  return toString();
}

d2c::Var Integer__::toStringAsPrecision(const d2c::Var &precision) {
  return toString();
}

void Integer__::initialize__() {
  super::initialize__();
}

d2c::Var __static_Integer_parse(const d2c::Var &source,
                                const d2c::Var &__named__) {
  int radix = 10;
  if (d2c::cast<d2c::NamedArguments>(__named__).contains(TK_radix__)) {
    radix = static_cast<int>(
        d2c::cast<d2c::NamedArguments>(__named__).get(TK_radix__).intValue());
  }
  try {
    if (source.isString()) {
      d2c::Str value = source.stringValue();
      std::int64_t v = std::stoll(value.c_str(), NULL, radix);
      return v;
    }
  } catch (...) {
  }

  if (d2c::cast<d2c::NamedArguments>(__named__).contains(TK_onError__)) {
    d2c::Var onError =
        d2c::cast<d2c::NamedArguments>(__named__).get(TK_onError__);
    onError(source);
  }

  throw d2c::Var(d2c::TypeFactory::CreateFormatException(""));
}

d2c::Var __factory_Integer_fromEnvironment(const d2c::TypeSet &__types__,
                                           const d2c::Var &name,
                                           const d2c::Var &__named__) {
  std::string env = getenv(name.toString().stringValue().c_str());

  if (env.size() > 0) {
    try {
      std::int64_t v = std::stoll(env);
      return v;
    } catch (...) {
      // Exception was thrown due to the string not being parsable as an int.
      // The defaultValue will consequently be returned.
    }
  }

  if (d2c::cast<d2c::NamedArguments>(__named__).contains(TK_defaultValue__)) {
    return d2c::cast<d2c::NamedArguments>(__named__).get(TK_defaultValue__);
  }

  return 0;
}

#endif  // dart_core_integer_impl_h

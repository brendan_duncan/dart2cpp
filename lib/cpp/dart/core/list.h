/****************************************************************************
 * Copyright (C) 2014 by Brendan Duncan.                                    *
 *                                                                          *
 * This file is part of Dart2Cpp,                                           *
 * https://github.com/brendan-duncan/dart2cpp.                              *
 *                                                                          *
 * Licensed under the Apache License, Version 2.0 (the "License");          *
 * you may not use this file except in compliance with the License.         *
 * You may obtain a copy of the License at                                  *
 *                                                                          *
 * http://www.apache.org/licenses/LICENSE-2.0                               *
 *                                                                          *
 * Unless required by applicable law or agreed to in writing, software      *
 * distributed under the License is distributed on an "AS IS" BASIS,        *
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. *
 * See the License for the specific language governing permissions and      *
 * limitations under the License.                                           *
 ****************************************************************************/
#ifndef dart_core_list_h
#define dart_core_list_h

class List__ : public List {
 public:
  typedef List super;
  std::vector<d2c::Var> data_;
  d2c::TypeSet typeParameters_;

  List__(const d2c::TypeSet &__types__,
         const std::initializer_list<d2c::Var> &init);

  List__(const d2c::TypeSet &__types__, const std::vector<d2c::Var> &data);

  List__(const d2c::TypeSet &__types__, int length,
         const d2c::Var &fill = null);

  static void updateClassInheritance__(d2c::Object *object,
                                       const d2c::TypeSet &__types__) {
    object->addInheritance__<List__>(__types__);
    super::updateClassInheritance__(object, __types__);
  }

  virtual bool isList__() {
    return true;
  }

  virtual const std::vector<d2c::Var> &listValue__() {
    return data_;
  }

  d2c::Var operator[](const d2c::Var &index);

  // operator []=
  d2c::Var __array_set(const d2c::Var &index, const d2c::Var &value);

  d2c::Var __get_length();

  d2c::Var __set_length(const d2c::Var &newLength);

  d2c::Var add(const d2c::Var &value);

  d2c::Var addAll(const d2c::Var &iterable);

  d2c::Var __get_reversed();

  d2c::Var sort(const d2c::Var &compare = null);

  d2c::Var shuffle(const d2c::Var &random = null);

  d2c::Var indexOf(const d2c::Var &element,
                   const d2c::Var &start = (std::int64_t)0);

  d2c::Var lastIndexOf(const d2c::Var &element, const d2c::Var &start = null);

  d2c::Var clear();

  d2c::Var insert(const d2c::Var &index, const d2c::Var &element);

  d2c::Var insertAll(const d2c::Var &index, const d2c::Var &iterable);

  d2c::Var setAll(const d2c::Var &index, const d2c::Var &iterable);

  d2c::Var remove(const d2c::Var &value);

  d2c::Var removeAt(const d2c::Var &index);

  d2c::Var removeLast();

  d2c::Var removeWhere(const d2c::Var &test);

  d2c::Var retainWhere(const d2c::Var &test);

  d2c::Var sublist(const d2c::Var &start, const d2c::Var &end = null);

  d2c::Var getRange(const d2c::Var &start, const d2c::Var &end);

  d2c::Var setRange(const d2c::Var &start, const d2c::Var &end,
                    const d2c::Var &iterable,
                    const d2c::Var &skipCount = (std::int64_t)0);

  d2c::Var removeRange(const d2c::Var &start, const d2c::Var &end);

  d2c::Var fillRange(const d2c::Var &start, const d2c::Var &end,
                     const d2c::Var &fillValue = null);

  d2c::Var replaceRange(const d2c::Var &start, const d2c::Var &end,
                        const d2c::Var &replacement);

  d2c::Var asMap();

  // From Iterable
  d2c::Var __get_iterator();

  d2c::Var map(const d2c::Var &f);

  d2c::Var where(const d2c::Var &test);

  d2c::Var expand(const d2c::Var &f);

  d2c::Var contains(const d2c::Var &element);

  d2c::Var forEach(const d2c::Var &f);

  d2c::Var reduce(const d2c::Var &combine);

  d2c::Var fold(const d2c::Var &initialValue, const d2c::Var &combine);

  d2c::Var every(const d2c::Var &test);

  d2c::Var any(const d2c::Var &test);

  d2c::Var toList(const d2c::Var &__named__ = d2c::namedArgs({}, {}));

  d2c::Var toSet();

  d2c::Var toString();

  d2c::Var __get_isEmpty();

  d2c::Var __get_isNotEmpty();

  d2c::Var take(const d2c::Var &n);

  d2c::Var takeWhile(const d2c::Var &test);

  d2c::Var skip(const d2c::Var &n);

  d2c::Var skipWhile(const d2c::Var &test);

  d2c::Var __get_first();

  d2c::Var __get_last();

  d2c::Var __get_single();

  d2c::Var firstWhere(const d2c::Var &test,
                      const d2c::Var &__named__ = d2c::namedArgs({}, {}));

  d2c::Var lastWhere(const d2c::Var &test,
                     const d2c::Var &__named__ = d2c::namedArgs({}, {}));

  d2c::Var singleWhere(const d2c::Var &test);

  d2c::Var elementAt(const d2c::Var &index);

  class Iterator__ : public Iterator {
   public:
    typedef Iterator super;

    Iterator__(List__ *list, int s = 0, int e = -1);

    d2c::Var moveNext();

    d2c::Var __get_current();

   protected:
    d2c::Var list_;
    List__ *object_;
    int index_;
    int start_;
    int end_;
  };
};

REGISTER_LIST(List__)

d2c::Var __factory_List(const d2c::TypeSet &__types__, const d2c::Var &length);

d2c::Var __factory_List_filled(const d2c::TypeSet &__types__,
                               const d2c::Var &length, const d2c::Var &fill);

#endif  // dart_core_list_h

/****************************************************************************
 * Copyright (C) 2014 by Brendan Duncan.                                    *
 *                                                                          *
 * This file is part of Dart2Cpp,                                           *
 * https://github.com/brendan-duncan/dart2cpp.                              *
 *                                                                          *
 * Licensed under the Apache License, Version 2.0 (the "License");          *
 * you may not use this file except in compliance with the License.         *
 * You may obtain a copy of the License at                                  *
 *                                                                          *
 * http://www.apache.org/licenses/LICENSE-2.0                               *
 *                                                                          *
 * Unless required by applicable law or agreed to in writing, software      *
 * distributed under the License is distributed on an "AS IS" BASIS,        *
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. *
 * See the License for the specific language governing permissions and      *
 * limitations under the License.                                           *
 ****************************************************************************/
#ifndef dart_core_list_impl_h
#define dart_core_list_impl_h

#include "list.h"

List__::List__(const d2c::TypeSet &__types__,
               const std::initializer_list<d2c::Var> &init)
    : super(super::__constructor__(__types__))
    , data_(init)
    , typeParameters_(__types__) {
  updateClassInheritance__(this, __types__);
}

List__::List__(const d2c::TypeSet &__types__, const std::vector<d2c::Var> &data)
    : super(super::__constructor__(__types__))
    , data_(data)
    , typeParameters_(__types__) {
  updateClassInheritance__(this, __types__);
}

List__::List__(const d2c::TypeSet &__types__, int length, const d2c::Var &fill)
    : super(super::__constructor__(__types__))
    , data_(length, fill)
    , typeParameters_(__types__) {
  updateClassInheritance__(this, __types__);
}

d2c::Var List__::operator[](const d2c::Var &index) {
  return data_[static_cast<size_t>(index.intValue())];
}

// operator []=
d2c::Var List__::__array_set(const d2c::Var &index, const d2c::Var &value) {
  data_[static_cast<int>(index.intValue())] = value;
  return null;
}

d2c::Var List__::__get_length() {
  return d2c::Var(new Integer__((std::int64_t)data_.size()));
}

d2c::Var List__::__set_length(const d2c::Var &newLength) {
  data_.resize(static_cast<int>(newLength.intValue()));
  return null;
}

d2c::Var List__::add(const d2c::Var &value) {
  data_.push_back(value);
  return null;
}

d2c::Var List__::addAll(const d2c::Var &iterable) {
  d2c::Var iterator = iterable.invoke(TK___get_iterator__)();
  if (iterator.isNull()) {
    return null;
  }
  while (iterator.invoke(TK_moveNext__)().boolValue()) {
    d2c::Var e = iterator.invoke(TK___get_current__)();
    add(e);
  }
  return null;
}

d2c::Var List__::__get_reversed() {
  std::vector<d2c::Var> reversed(data_.begin(), data_.end());
  std::reverse(reversed.begin(), reversed.end());
  return d2c::Var(new List__(typeParameters_, reversed));
}

d2c::Var List__::sort(const d2c::Var &compare) {
  if (data_.empty()) {
    return null;
  }

  std::sort(data_.begin(), data_.end());

  return null;
}

d2c::Var List__::shuffle(const d2c::Var &random) {
  if (data_.empty()) {
    return null;
  }

  std::mt19937 random_;

  for (size_t i = 0; i < data_.size(); ++i) {
    std::int64_t j;
    if (random.isNull()) {
      j = random_() % data_.size();
    } else {
      j = random.invoke(TK_nextInt__)(static_cast<std::int64_t>(data_.size()));
    }
    if (j < 0) {
      j = -j;
    }
    
    size_t k = static_cast<size_t>(j) % data_.size();

    d2c::Var v = data_[i];
    data_[i] = data_[k];
    data_[k] = v;
  }

  return null;
}

d2c::Var List__::indexOf(const d2c::Var &element, const d2c::Var &start) {
  if (data_.empty()) {
    return null;
  }

  int s = start.isInt() ? static_cast<int>(start.intValue()) : 0;
  for (size_t i = s; i < data_.size(); ++i) {
    if (data_[i] == element) {
      return static_cast<std::int64_t>(i);
    }
  }

  return -1LL;
}

d2c::Var List__::lastIndexOf(const d2c::Var &element, const d2c::Var &start) {
  if (data_.empty()) {
    return -1LL;
  }

  int s = start.isInt() ? static_cast<int>(start.intValue()) : 0;
  int len = static_cast<int>(data_.size());
  for (int i = len - 1; i >= s; --i) {
    if (data_[i] == element) {
      return static_cast<std::int64_t>(i);
    }
  }

  return -1LL;
}

d2c::Var List__::clear() {
  data_.clear();
  return null;
}

d2c::Var List__::insert(const d2c::Var &index, const d2c::Var &element) {
  int i = static_cast<int>(index.intValue());
  data_.insert(data_.begin() + i, element);
  return null;
}

d2c::Var List__::insertAll(const d2c::Var &index, const d2c::Var &iterable) {
  int i = static_cast<int>(index.intValue());
  d2c::Var iterator = iterable.invoke(TK___get_iterator__)();
  if (iterator.isNull()) {
    return null;
  }
  while (iterator.invoke(TK_moveNext__)().boolValue()) {
    d2c::Var e = iterator.invoke(TK___get_current__)();
    data_.insert(data_.begin() + i, e);
    i++;
  }

  return null;
}

d2c::Var List__::setAll(const d2c::Var &index, const d2c::Var &iterable) {
  int i = static_cast<int>(index.intValue());
  d2c::Var iterator = iterable.invoke(TK___get_iterator__)();
  if (iterator.isNull()) {
    return null;
  }
  while (iterator.invoke(TK_moveNext__)().boolValue()) {
    d2c::Var e = iterator.invoke(TK___get_current__)();
    data_[i++] = e;
  }

  return null;
}

d2c::Var List__::remove(const d2c::Var &value) {
  for (std::vector<d2c::Var>::iterator iter = data_.begin();
       iter != data_.end(); ++iter) {
    if ((*iter) == value) {
      data_.erase(iter);
      return true;
    }
  }
  return false;
}

d2c::Var List__::removeAt(const d2c::Var &index) {
  int i = static_cast<int>(index.intValue());
  d2c::Var e = data_[i];
  data_.erase(data_.begin() + i);
  return e;
}

d2c::Var List__::removeLast() {
  d2c::Var e = *data_.rbegin().base();
  data_.erase(data_.rbegin().base());
  return e;
}

d2c::Var List__::removeWhere(const d2c::Var &test) {
  throw "TODO: implement";
}

d2c::Var List__::retainWhere(const d2c::Var &test) {
  throw "TODO: implement";
}

d2c::Var List__::sublist(const d2c::Var &start, const d2c::Var &end) {
  int s = static_cast<int>(start.intValue());
  int e = static_cast<int>(end.intValue());
  if (e > static_cast<int>(data_.size())) {
    e = static_cast<int>(data_.size());
  }
  std::vector<d2c::Var> vec(data_.begin() + s, data_.begin() + e);
  return d2c::Var(new List__(typeParameters_, vec));
}

d2c::Var List__::getRange(const d2c::Var &start, const d2c::Var &end) {
  int s = static_cast<int>(start.intValue());
  int e = static_cast<int>(end.intValue());
  return d2c::Var(new List__::Iterator__(this, s, e));
}

d2c::Var List__::setRange(const d2c::Var &start, const d2c::Var &end,
                          const d2c::Var &iterable, const d2c::Var &skipCount) {
  throw "TODO: implement";
}

d2c::Var List__::removeRange(const d2c::Var &start, const d2c::Var &end) {
  throw "TODO: implement";
}

d2c::Var List__::fillRange(const d2c::Var &start, const d2c::Var &end,
                           const d2c::Var &fillValue) {
  int s = static_cast<int>(start.intValue());
  int e = static_cast<int>(end.intValue());
  for (int i = s; i <= e; ++i) {
    data_[i] = fillValue;
  }
  return null;
}

d2c::Var List__::replaceRange(const d2c::Var &start, const d2c::Var &end,
                              const d2c::Var &replacement) {
  throw "TODO: implement";
}

d2c::Var List__::asMap() {
  throw "TODO: implement";
}

d2c::Var List__::__get_iterator() {
  return d2c::Var(new List__::Iterator__(this));
}

d2c::Var List__::map(const d2c::Var &f) {
  return d2c::Var(new MapIterable__({}, d2c::Var(this), f));
}

d2c::Var List__::where(const d2c::Var &test) {
  throw "TODO: implement";
}

d2c::Var List__::expand(const d2c::Var &f) {
  throw "TODO: implement";
}

d2c::Var List__::contains(const d2c::Var &element) {
  for (std::vector<d2c::Var>::iterator iter = data_.begin();
       iter != data_.end(); ++iter) {
    if (*iter == element) {
      return true;
    }
  }
  return false;
}

d2c::Var List__::forEach(const d2c::Var &f) {
  for (std::vector<d2c::Var>::iterator iter = data_.begin();
       iter != data_.end(); ++iter) {
    f(*iter);
  }
  return null;
}

d2c::Var List__::reduce(const d2c::Var &combine) {
  throw "TODO: implement";
}

d2c::Var List__::fold(const d2c::Var &initialValue, const d2c::Var &combine) {
  throw "TODO: implement";
}

d2c::Var List__::every(const d2c::Var &test) {
  throw "TODO: implement";
}

d2c::Var List__::any(const d2c::Var &test) {
  throw "TODO: implement";
}

d2c::Var List__::toList(const d2c::Var &__named__) {
  return d2c::Var(this);
}

d2c::Var List__::toSet() {
  throw "TODO: implement";
}

d2c::Var List__::toString() {
  d2c::Str s = "[";
  for (size_t i = 0; i < data_.size(); ++i) {
    if (i != 0) {
      s += ", ";
    }
    d2c::Var st = data_[i].toString();
    s += d2c::cast<d2c::String>(st).data_;
  }
  s += "]";
  return s;
}

d2c::Var List__::__get_isEmpty() {
  return data_.empty();
}

d2c::Var List__::__get_isNotEmpty() {
  return !data_.empty();
}

d2c::Var List__::take(const d2c::Var &n) {
  int num = static_cast<int>(n.intValue());
  if (num > static_cast<int>(data_.size())) {
    num = static_cast<int>(data_.size());
  }
  std::vector<d2c::Var> sub(data_.begin(), data_.begin() + num);
  return d2c::Var(new List__(typeParameters_, sub));
}

d2c::Var List__::takeWhile(const d2c::Var &test) {
  throw "TODO: implement";
}

d2c::Var List__::skip(const d2c::Var &n) {
  int num = static_cast<int>(n.intValue());
  std::vector<d2c::Var> sub(data_.begin() + num, data_.end());
  return d2c::Var(new List__(typeParameters_, sub));
}

d2c::Var List__::skipWhile(const d2c::Var &test) {
  throw "TODO: implement";
}

d2c::Var List__::__get_first() {
  return *data_.begin();
}

d2c::Var List__::__get_last() {
  return *data_.rbegin();
}

d2c::Var List__::__get_single() {
  if (data_.size() != 1) {
    throw d2c::Var(d2c::TypeFactory::CreateStateError(""));
  }
  return data_[0];
}

d2c::Var List__::firstWhere(const d2c::Var &test, const d2c::Var &__named__) {
  throw "TODO: implement";
}

d2c::Var List__::lastWhere(const d2c::Var &test, const d2c::Var &__named__) {
  throw "TODO: implement";
}

d2c::Var List__::singleWhere(const d2c::Var &test) {
  throw "TODO: implement";
}

d2c::Var List__::elementAt(const d2c::Var &index) {
  return data_[(unsigned int)index.intValue()];
}

d2c::Var __factory_List(const d2c::TypeSet &__types__, const d2c::Var &length) {
  int len = length.isInt() ? static_cast<int>(length.intValue()) : 0;
  return d2c::Var(new List__(__types__, len));
}

d2c::Var __factory_List_filled(const d2c::TypeSet &__types__,
                               const d2c::Var &length, const d2c::Var &fill) {
  int len = length.isInt() ? static_cast<int>(length.intValue()) : 0;
  return d2c::Var(new List__(__types__, len, fill));
}

List__::Iterator__::Iterator__(List__ *list, int s, int e)
    : list_(list)
    , object_(list)
    , index_(s >= 0 ? s - 1 : -1)
    , start_(s)
    , end_(e) {
  if (start_ < 0) {
    start_ = -1;
  }
  if (end_ == -1) {
    end_ = static_cast<int>(object_->data_.size());
  }
}

d2c::Var List__::Iterator__::moveNext() {
  index_++;
  return index_ >= start_ && index_ < end_;
}

d2c::Var List__::Iterator__::__get_current() {
  return object_->data_[index_];
}

#endif  // dart_core_list_impl_h

/****************************************************************************
 * Copyright (C) 2014 by Brendan Duncan.                                    *
 *                                                                          *
 * This file is part of Dart2Cpp,                                           *
 * https://github.com/brendan-duncan/dart2cpp.                              *
 *                                                                          *
 * Licensed under the Apache License, Version 2.0 (the "License");          *
 * you may not use this file except in compliance with the License.         *
 * You may obtain a copy of the License at                                  *
 *                                                                          *
 * http://www.apache.org/licenses/LICENSE-2.0                               *
 *                                                                          *
 * Unless required by applicable law or agreed to in writing, software      *
 * distributed under the License is distributed on an "AS IS" BASIS,        *
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. *
 * See the License for the specific language governing permissions and      *
 * limitations under the License.                                           *
 ****************************************************************************/
#ifndef dart_core_map_iterable_impl_h
#define dart_core_map_iterable_impl_h

MapIterable__::MapIterable__(const d2c::TypeSet __types__,
                             const d2c::Var parent, const d2c::Var mapFunction)
    : super(super::__constructor__(__types__))
    , parent_(parent)
    , mapFunction_(mapFunction) {
}

d2c::Var MapIterable__::__get_iterator() {
  return d2c::Var(new Iterator__(this));
}

d2c::Var MapIterable__::map(const d2c::Var &f) {
  return d2c::Var(new MapIterable__({}, d2c::Var(this), mapFunction_));
}

d2c::Var MapIterable__::where(const d2c::Var &test) {
  throw "TODO: implement";
  return null;
}

d2c::Var MapIterable__::expand(const d2c::Var &f) {
  throw "TODO: implement";
  return null;
}

d2c::Var MapIterable__::contains(const d2c::Var &element) {
  throw "TODO: implement";
  return null;
}

d2c::Var MapIterable__::forEach(const d2c::Var &f) {
  throw "TODO: implement";
  return null;
}

d2c::Var MapIterable__::reduce(const d2c::Var &combine) {
  throw "TODO: implement";
  return null;
}

d2c::Var MapIterable__::fold(const d2c::Var &initialValue,
                             const d2c::Var &combine) {
  throw "TODO: implement";
  return null;
}

d2c::Var MapIterable__::every(const d2c::Var &test) {
  throw "TODO: implement";
  return null;
}

d2c::Var MapIterable__::join(const d2c::Var &separator) {
  throw "TODO: implement";
  return null;
}

d2c::Var MapIterable__::any(const d2c::Var &test) {
  throw "TODO: implement";
  return null;
}

d2c::Var MapIterable__::toList(const d2c::Var &__named__) {
  throw "TODO: implement";
  return null;
}

d2c::Var MapIterable__::toSet() {
  throw "TODO: implement";
  return null;
}

d2c::Var MapIterable__::__get_length() {
  return parent_.invoke(TK___get_length__)();
}

d2c::Var MapIterable__::__get_isEmpty() {
  return parent_.invoke(TK___get_isEmpty__)();
}

d2c::Var MapIterable__::__get_isNotEmpty() {
  return parent_.invoke(TK___get_isNotEmpty__)();
}

d2c::Var MapIterable__::take(const d2c::Var &n) {
  throw "TODO: implement";
  return null;
}

d2c::Var MapIterable__::takeWhile(const d2c::Var &test) {
  throw "TODO: implement";
  return null;
}

d2c::Var MapIterable__::skip(const d2c::Var &n) {
  throw "TODO: implement";
  return null;
}

d2c::Var MapIterable__::skipWhile(const d2c::Var &test) {
  throw "TODO: implement";
  return null;
}

d2c::Var MapIterable__::__get_first() {
  throw "TODO: implement";
  return null;
}

d2c::Var MapIterable__::__get_last() {
  throw "TODO: implement";
  return null;
}

d2c::Var MapIterable__::__get_single() {
  throw "TODO: implement";
  return null;
}

d2c::Var MapIterable__::firstWhere(const d2c::Var &test,
                                   const d2c::Var &__named__) {
  throw "TODO: implement";
  return null;
}

d2c::Var MapIterable__::lastWhere(const d2c::Var &test,
                                  const d2c::Var &__named__) {
  throw "TODO: implement";
  return null;
}

d2c::Var MapIterable__::singleWhere(const d2c::Var &test) {
  throw "TODO: implement";
  return null;
}

d2c::Var MapIterable__::elementAt(const d2c::Var &index) {
  throw "TODO: implement";
  return null;
}

void MapIterable__::initialize__() {
  super::initialize__();
}

MapIterable__::Iterator__::Iterator__(MapIterable__ *iterable)
    : super(super::__constructor__())
    , iterable_(iterable) {
  MapIterable__ *m = static_cast<MapIterable__ *>(iterable);
  iterator_ = m->parent_.invoke(TK___get_iterator__)();
}

d2c::Var MapIterable__::Iterator__::moveNext() {
  return iterator_.invoke(TK_moveNext__)();
}

d2c::Var MapIterable__::Iterator__::__get_current() {
  MapIterable__ *m = static_cast<MapIterable__ *>(iterable_.object_);
  return m->mapFunction_(iterator_.invoke(TK___get_current__)());
}

#endif  // dart_core_map_iterable_impl_h

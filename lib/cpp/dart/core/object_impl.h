/****************************************************************************
 * Copyright (C) 2014 by Brendan Duncan.                                    *
 *                                                                          *
 * This file is part of Dart2Cpp,                                           *
 * https://github.com/brendan-duncan/dart2cpp.                              *
 *                                                                          *
 * Licensed under the Apache License, Version 2.0 (the "License");          *
 * you may not use this file except in compliance with the License.         *
 * You may obtain a copy of the License at                                  *
 *                                                                          *
 * http://www.apache.org/licenses/LICENSE-2.0                               *
 *                                                                          *
 * Unless required by applicable law or agreed to in writing, software      *
 * distributed under the License is distributed on an "AS IS" BASIS,        *
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. *
 * See the License for the specific language governing permissions and      *
 * limitations under the License.                                           *
 ****************************************************************************/
#ifndef dart_core_object_impl_h
#define dart_core_object_impl_h

d2c::Var __method_Object___get_hashCode(Object *self) {
  return d2c::Var(std::int64_t(self));
}

d2c::Var __method_Object_toString(Object *self) {
  return "Object";
}

d2c::Var __method_Object_noSuchMethod(Object *self,
                                      const d2c::Var &invocation) {
  d2c::Var memberName = invocation.invoke(TK___get_memberName__);
  d2c::Var posArgs = invocation.invoke(TK___get_positionalArguments__);
  d2c::Var namedArgs = invocation.invoke(TK___get_namedArguments__);
  throw d2c::Var(d2c::TypeFactory::CreateNoSuchMethodError(
      d2c::Var(self), memberName, posArgs, namedArgs, null));
}

d2c::Var __method_Object___get_runtimeType(Object *self) {
  throw "TODO: implement";
}

#endif  // dart_core_object_impl_h

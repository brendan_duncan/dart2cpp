/****************************************************************************
 * Copyright (C) 2014 by Brendan Duncan.                                    *
 *                                                                          *
 * This file is part of Dart2Cpp,                                           *
 * https://github.com/brendan-duncan/dart2cpp.                              *
 *                                                                          *
 * Licensed under the Apache License, Version 2.0 (the "License");          *
 * you may not use this file except in compliance with the License.         *
 * You may obtain a copy of the License at                                  *
 *                                                                          *
 * http://www.apache.org/licenses/LICENSE-2.0                               *
 *                                                                          *
 * Unless required by applicable law or agreed to in writing, software      *
 * distributed under the License is distributed on an "AS IS" BASIS,        *
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. *
 * See the License for the specific language governing permissions and      *
 * limitations under the License.                                           *
 ****************************************************************************/
#ifndef dart_core_string_h
#define dart_core_string_h

class String__ : public String {
 public:
  typedef String super;

  d2c::Str data_;

  String__(const d2c::Str &init);

  static void updateClassInheritance__(d2c::Object *object,
                                       const d2c::TypeSet &__types__ = {}) {
    object->addInheritance__<String__>(__types__);
    super::updateClassInheritance__(object, __types__);
  }

  bool isString__() {
    return true;
  }

  const d2c::Str &stringValue__() {
    return data_;
  }

  d2c::Var toString() {
    return d2c::Var(this);
  }

  d2c::Var operator[](const d2c::Var &index);

  d2c::Var codeUnitAt(const d2c::Var &index);

  d2c::Var __get_length();

  d2c::Var operator==(const d2c::Var &other);

  d2c::Var endsWith(const d2c::Var &other);

  d2c::Var startsWith(const d2c::Var &pattern,
                      const d2c::Var &index = (std::int64_t)0);

  d2c::Var indexOf(const d2c::Var &pattern, const d2c::Var &start = null);

  d2c::Var lastIndexOf(const d2c::Var &pattern, const d2c::Var &start = null);

  d2c::Var __get_isEmpty();

  d2c::Var __get_isNotEmpty();

  d2c::Var __get_hashCode();

  d2c::Var operator+(const d2c::Var &other);

  d2c::Var substring(const d2c::Var &startIndex,
                     const d2c::Var &endIndex = null);

  d2c::Var trim();

  d2c::Var contains(const d2c::Var &other,
                    const d2c::Var &startIndex = (std::int64_t)0);

  d2c::Var replaceFirst(const d2c::Var &from, const d2c::Var &to);

  d2c::Var replaceAll(const d2c::Var &from, const d2c::Var &replace);

  d2c::Var replaceAllMapped(const d2c::Var &from, const d2c::Var &replace);

  d2c::Var split(const d2c::Var &pattern);

  d2c::Var splitMapJoin(const d2c::Var &pattern, const d2c::Var &__named__);

  d2c::Var __get_codeUnits();

  d2c::Var __get_runes();

  d2c::Var toLowerCase();

  d2c::Var toUpperCase();

  virtual void initialize__();
};

REGISTER_STRING(String__)

d2c::Var __factory_String_fromCharCodes(const d2c::TypeSet &__types__,
                                        const d2c::Var &charCodes);

d2c::Var __factory_String_fromCharCode(const d2c::TypeSet &__types__,
                                       const d2c::Var &charCode);

d2c::Var __factory_String_fromEnvironment(const d2c::TypeSet &__types__,
                                          const d2c::Var &charCodes,
                                          const d2c::Var &__named__);

#endif  // dart_core_string_h

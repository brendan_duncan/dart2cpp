/****************************************************************************
 * Copyright (C) 2014 by Brendan Duncan.                                    *
 *                                                                          *
 * This file is part of Dart2Cpp,                                           *
 * https://github.com/brendan-duncan/dart2cpp.                              *
 *                                                                          *
 * Licensed under the Apache License, Version 2.0 (the "License");          *
 * you may not use this file except in compliance with the License.         *
 * You may obtain a copy of the License at                                  *
 *                                                                          *
 * http://www.apache.org/licenses/LICENSE-2.0                               *
 *                                                                          *
 * Unless required by applicable law or agreed to in writing, software      *
 * distributed under the License is distributed on an "AS IS" BASIS,        *
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. *
 * See the License for the specific language governing permissions and      *
 * limitations under the License.                                           *
 ****************************************************************************/
#ifndef dart_core_string_buffer_h
#define dart_core_string_buffer_h

class StringBuffer__ : public StringBuffer {
public:
  typedef StringBuffer super;

  StringBuffer__(const d2c::Var &content);

  static void updateClassInheritance__(d2c::Object *object,
                                       const d2c::TypeSet &__types__ = {}) {
    object->addInheritance__<StringBuffer__>(__types__);
    super::updateClassInheritance__(object, __types__);
  }

  d2c::Var __get_length();

  d2c::Var write(const d2c::Var &obj);

  d2c::Var writeCharCode(const d2c::Var &charCode);

  d2c::Var clear();

  d2c::Var toString();

protected:
  d2c::Str data_;
};

d2c::Var __factory_StringBuffer(const d2c::TypeSet &__types__,
                                const d2c::Var &content);

d2c::Var __method_StringBuffer___get_length(StringBuffer *self);

d2c::Var __method_StringBuffer_write(StringBuffer *self, const d2c::Var &obj);

d2c::Var __method_StringBuffer_writeCharCode(StringBuffer *self,
                                             const d2c::Var &charCode);

d2c::Var __method_StringBuffer_clear(StringBuffer *self);

d2c::Var __method_StringBuffer_toString(StringBuffer *self);

#endif  // dart_core_string_buffer_h

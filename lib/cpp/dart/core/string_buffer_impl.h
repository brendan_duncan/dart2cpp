/****************************************************************************
 * Copyright (C) 2014 by Brendan Duncan.                                    *
 *                                                                          *
 * This file is part of Dart2Cpp,                                           *
 * https://github.com/brendan-duncan/dart2cpp.                              *
 *                                                                          *
 * Licensed under the Apache License, Version 2.0 (the "License");          *
 * you may not use this file except in compliance with the License.         *
 * You may obtain a copy of the License at                                  *
 *                                                                          *
 * http://www.apache.org/licenses/LICENSE-2.0                               *
 *                                                                          *
 * Unless required by applicable law or agreed to in writing, software      *
 * distributed under the License is distributed on an "AS IS" BASIS,        *
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. *
 * See the License for the specific language governing permissions and      *
 * limitations under the License.                                           *
 ****************************************************************************/
#ifndef dart_core_string_buffer_impl_h
#define dart_core_string_buffer_impl_h

StringBuffer__::StringBuffer__(const d2c::Var &content) {
  data_ = content.toString().stringValue();
}

d2c::Var StringBuffer__::__get_length() {
  return static_cast<std::int64_t>(data_.size());
}

d2c::Var StringBuffer__::write(const d2c::Var &obj) {
  data_ += obj.toString().stringValue();
  return null;
}

d2c::Var StringBuffer__::writeCharCode(const d2c::Var &charCode) {
  data_ += static_cast<char>(charCode.intValue());
  return null;
}

d2c::Var StringBuffer__::clear() {
  data_.clear();
  return null;
}

d2c::Var StringBuffer__::toString() {
  return data_;
}

d2c::Var __factory_StringBuffer(const d2c::TypeSet &__types__,
                                const d2c::Var &content) {
  return d2c::Var(new StringBuffer__(content));
}

d2c::Var __method_StringBuffer___get_length(StringBuffer *) {
  throw "This should not have been called.";
}

d2c::Var __method_StringBuffer_write(StringBuffer *, const d2c::Var &) {
  throw "This should not have been called.";
}

d2c::Var __method_StringBuffer_writeCharCode(StringBuffer *, 
                                             const d2c::Var &) {
  throw "This should not have been called.";
}

d2c::Var __method_StringBuffer_clear(StringBuffer *) {
  throw "This should not have been called.";
}

d2c::Var __method_StringBuffer_toString(StringBuffer *) {
  throw "This should not have been called.";
}

#endif  // dart_core_string_buffer_impl_h

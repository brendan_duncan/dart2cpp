/****************************************************************************
 * Copyright (C) 2014 by Brendan Duncan.                                    *
 *                                                                          *
 * This file is part of Dart2Cpp,                                           *
 * https://github.com/brendan-duncan/dart2cpp.                              *
 *                                                                          *
 * Licensed under the Apache License, Version 2.0 (the "License");          *
 * you may not use this file except in compliance with the License.         *
 * You may obtain a copy of the License at                                  *
 *                                                                          *
 * http://www.apache.org/licenses/LICENSE-2.0                               *
 *                                                                          *
 * Unless required by applicable law or agreed to in writing, software      *
 * distributed under the License is distributed on an "AS IS" BASIS,        *
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. *
 * See the License for the specific language governing permissions and      *
 * limitations under the License.                                           *
 ****************************************************************************/
#ifndef dart_core_string_impl_h
#define dart_core_string_impl_h

#include "string.h"

String__::String__(const d2c::Str &init)
    : super(super::__constructor__())
    , data_(init) {
  updateClassInheritance__(this);
}

d2c::Var String__::operator[](const d2c::Var &index) {
  char cs[2] = {data_[static_cast<int>(index.intValue())], 0};
  return d2c::Var(new String__(cs));
}

d2c::Var String__::codeUnitAt(const d2c::Var &index) {
  return d2c::Var(
      new Integer__((std::int64_t)data_[static_cast<int>(index.intValue())]));
}

d2c::Var String__::__get_length() {
  return d2c::Var(new Integer__((std::int64_t)data_.size()));
}

d2c::Var String__::__get_hashCode() {
  return static_cast<std::int64_t>(std::hash<d2c::Str>()(data_));
}

d2c::Var String__::operator==(const d2c::Var &other) {
  if (!d2c::is_type<String__>(other)) {
    return false;
  }
  const d2c::Str &s2 = d2c::cast<String__>(other).data_;
  return data_ == s2;
}

d2c::Var String__::endsWith(const d2c::Var &other) {
  d2c::Str ostr = other.stringValue();
  if (ostr.size() > data_.size()) {
    return false;
  }
  size_t si = data_.size() - ostr.size();
  return memcmp(data_.c_str() + si, ostr.c_str(), ostr.size()) == 0;
}

d2c::Var String__::startsWith(const d2c::Var &pattern,
                              const d2c::Var &index) {
  d2c::Str ostr = pattern.stringValue();
  if (ostr.size() > data_.size()) {
    return false;
  }
  int i = 0;
  if (index.isNull()) {
    i = (int)index.intValue();
  }
  if (i + ostr.size() > data_.size()) {
    return false;
  }
  return memcmp(data_.c_str() + i, ostr.c_str(), ostr.size()) == 0;
}

d2c::Var String__::indexOf(const d2c::Var &pattern,
                           const d2c::Var &start) {
  throw "TODO: implement";
}

d2c::Var String__::lastIndexOf(const d2c::Var &pattern,
                               const d2c::Var &start) {
  throw "TODO: implement";
}

d2c::Var String__::__get_isEmpty() {
  return data_.empty();
}

d2c::Var String__::__get_isNotEmpty() {
  return !data_.empty();
}

d2c::Var String__::operator+(const d2c::Var &other) {
  return data_ + other.stringValue();
}

d2c::Var String__::substring(const d2c::Var &startIndex,
                             const d2c::Var &endIndex) {
  int si = startIndex.isNotNull() ? (int)startIndex.intValue() : 0;
  int ei = endIndex.isNotNull() ? (int)endIndex.intValue() : -1;
  return data_.substring(si, ei);
}

d2c::Var String__::trim() {
  throw "TODO: implement";
}

d2c::Var String__::contains(const d2c::Var &other,
                            const d2c::Var &startIndex) {
  throw "TODO: implement";
}

d2c::Var String__::replaceFirst(const d2c::Var &from,
                                const d2c::Var &to) {
  throw "TODO: implement";
}

d2c::Var String__::replaceAll(const d2c::Var &from,
                              const d2c::Var &replace) {
  throw "TODO: implement";
}

d2c::Var String__::replaceAllMapped(const d2c::Var &from,
                                    const d2c::Var &replace) {
  throw "TODO: implement";
}

d2c::Var String__::split(const d2c::Var &pattern) {
  throw "TODO: implement";
}

d2c::Var String__::splitMapJoin(const d2c::Var &pattern,
                                const d2c::Var &__named__) {
  throw "TODO: implement";
}

d2c::Var String__::__get_codeUnits() {
  throw "TODO: implement";
}

d2c::Var String__::__get_runes() {
  throw "TODO: implement";
}

d2c::Var String__::toLowerCase() {
  throw "TODO: implement";
}

d2c::Var String__::toUpperCase() {
  throw "TODO: implement";
}

void String__::initialize__() {
  super::initialize__();
}

d2c::Var __factory_String_fromCharCodes(const d2c::TypeSet &__types__,
                                        const d2c::Var &charCodes) {
  const std::vector<d2c::Var> &l = charCodes.listValue();
  char *s = new char[l.size() + 1];
  for (size_t i = 0; i < l.size(); ++i) {
    s[i] = (char)l[i].intValue();
  }
  s[l.size()] = 0;
  return d2c::Str(s, l.size(), false);
}

d2c::Var __factory_String_fromCharCode(const d2c::TypeSet &__types__,
                                       const d2c::Var &charCode) {
  throw "TODO: implement";
}

d2c::Var __factory_String_fromEnvironment(const d2c::TypeSet &__types__,
                                          const d2c::Var &charCodes,
                                          const d2c::Var &__named__) {
  throw "TODO: implement";
}

#endif  // dart_core_string_impl_h

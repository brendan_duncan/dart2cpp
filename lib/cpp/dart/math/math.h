/****************************************************************************
 * Copyright (C) 2014 by Brendan Duncan.                                    *
 *                                                                          *
 * This file is part of Dart2Cpp,                                           *
 * https://github.com/brendan-duncan/dart2cpp.                              *
 *                                                                          *
 * Licensed under the Apache License, Version 2.0 (the "License");          *
 * you may not use this file except in compliance with the License.         *
 * You may obtain a copy of the License at                                  *
 *                                                                          *
 * http://www.apache.org/licenses/LICENSE-2.0                               *
 *                                                                          *
 * Unless required by applicable law or agreed to in writing, software      *
 * distributed under the License is distributed on an "AS IS" BASIS,        *
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. *
 * See the License for the specific language governing permissions and      *
 * limitations under the License.                                           *
 ****************************************************************************/
#ifndef dart_math_h
#define dart_math_h

#include "random.h"

namespace d2c_math {
double toDouble(const d2c::Var &a) {
  return a.isInt()
             ? static_cast<double>(a.intValue())
             : a.isDouble()
                   ? a.doubleValue()
                   : throw d2c::Var(d2c::TypeFactory::CreateTypeError());
}
}

d2c::Var atan2(const d2c::Var &a, const d2c::Var &b) {
  double da = d2c_math::toDouble(a);
  double db = d2c_math::toDouble(b);
  return std::atan2(da, db);
}

d2c::Var pow(const d2c::Var &x, const d2c::Var &exponent) {
  double da = d2c_math::toDouble(x);
  double db = d2c_math::toDouble(exponent);
  return std::pow(da, db);
}

d2c::Var sin(const d2c::Var &x) {
  double da = d2c_math::toDouble(x);
  return std::sin(da);
}

d2c::Var cos(const d2c::Var &x) {
  double da = d2c_math::toDouble(x);
  return std::cos(da);
}

d2c::Var tan(const d2c::Var &x) {
  double da = d2c_math::toDouble(x);
  return std::tan(da);
}

d2c::Var acos(const d2c::Var &x) {
  double da = d2c_math::toDouble(x);
  return std::acos(da);
}

d2c::Var asin(const d2c::Var &x) {
  double da = d2c_math::toDouble(x);
  return std::asin(da);
}

d2c::Var atan(const d2c::Var &x) {
  double da = d2c_math::toDouble(x);
  return std::atan(da);
}

d2c::Var sqrt(const d2c::Var &x) {
  double da = d2c_math::toDouble(x);
  return std::sqrt(da);
}

d2c::Var exp(const d2c::Var &x) {
  double da = d2c_math::toDouble(x);
  return std::exp(da);
}

d2c::Var log(const d2c::Var &x) {
  double da = d2c_math::toDouble(x);
  return std::log(da);
}

#endif // dart_math_h

/****************************************************************************
 * Copyright (C) 2014 by Brendan Duncan.                                    *
 *                                                                          *
 * This file is part of Dart2Cpp,                                           *
 * https://github.com/brendan-duncan/dart2cpp.                              *
 *                                                                          *
 * Licensed under the Apache License, Version 2.0 (the "License");          *
 * you may not use this file except in compliance with the License.         *
 * You may obtain a copy of the License at                                  *
 *                                                                          *
 * http://www.apache.org/licenses/LICENSE-2.0                               *
 *                                                                          *
 * Unless required by applicable law or agreed to in writing, software      *
 * distributed under the License is distributed on an "AS IS" BASIS,        *
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. *
 * See the License for the specific language governing permissions and      *
 * limitations under the License.                                           *
 ****************************************************************************/
#ifndef dart_math_random_h
#define dart_math_random_h

class Random__ : public Random {
 public:
  typedef Random super;
  // mersenne_twister_engine random generator
  std::mt19937 random_;

  Random__(const d2c::Var &seed)
      : super(super::__constructor__()) {
    if (seed.isNull()) {
      seed = 0;
    }
    if (!seed.isInt()) {
      throw d2c::Var(d2c::TypeFactory::CreateTypeError());
    }

    std::int64_t s = seed.intValue();
    random_.seed(static_cast<int>(s));
  }

  virtual d2c::Var nextInt(const d2c::Var &max) {
    if (!max.isInt()) {
      throw d2c::Var(d2c::TypeFactory::CreateTypeError());
    }
    std::int64_t mx = max.intValue();

    return static_cast<std::int64_t>(nextDouble() * mx);
  }

  virtual d2c::Var nextDouble() {
    std::int64_t r = (random_() % (random_.max() - random_.min()));
    if (r < 0) {
      r = -r;
    }
    return r / static_cast<double>(random_.max() - random_.min());
  }

  virtual d2c::Var nextBool() {
    std::int64_t r = (random_() % (random_.max() - random_.min()));
    return (r & 1) == 0;
  }
};

d2c::Var __factory_Random(const d2c::TypeSet &__types__, const d2c::Var &seed) {
  return d2c::Var(new Random__(seed));
}

#endif  // dart_math_random_h

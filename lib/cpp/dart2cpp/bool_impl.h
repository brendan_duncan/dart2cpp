/****************************************************************************
 * Copyright (C) 2014 by Brendan Duncan.                                    *
 *                                                                          *
 * This file is part of Dart2Cpp,                                           *
 * https://github.com/brendan-duncan/dart2cpp.                              *
 *                                                                          *
 * Licensed under the Apache License, Version 2.0 (the "License");          *
 * you may not use this file except in compliance with the License.         *
 * You may obtain a copy of the License at                                  *
 *                                                                          *
 * http://www.apache.org/licenses/LICENSE-2.0                               *
 *                                                                          *
 * Unless required by applicable law or agreed to in writing, software      *
 * distributed under the License is distributed on an "AS IS" BASIS,        *
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. *
 * See the License for the specific language governing permissions and      *
 * limitations under the License.                                           *
 ****************************************************************************/
#ifndef dart2cpp_bool_impl_h
#define dart2cpp_bool_impl_h

namespace d2c {
Var Bool::__factory_Bool_fromEnvironment(const d2c::TypeSet &, const Var &name,
                                         const Var &defaultValue) {
  Str s = cast<String>(name).data_;
  return defaultValue;
}

Bool::Bool(bool b)
    : super()
    , data_(b) {
  addInheritance__<Bool>();
}

bool Bool::boolValue__() {
  return data_;
}

Var Bool::operator!() {
  return !data_;
}

Var Bool::operator==(const Var &other) {
  if (!is_type<Bool>(other)) {
    throw d2c::Var(TypeFactory::CreateTypeError());
    return false;
  }
  return data_ == static_cast<Bool *>(other.object_)->data_;
}

Var Bool::operator&&(const Var &other) {
  if (!is_type<Bool>(other)) {
    throw d2c::Var(TypeFactory::CreateTypeError());
    return false;
  }
  return data_ && static_cast<Bool *>(other.object_)->data_;
}

Var Bool::operator||(const Var &other) {
  if (!is_type<Bool>(other)) {
    throw d2c::Var(TypeFactory::CreateTypeError());
    return false;
  }
  return data_ || static_cast<Bool *>(other.object_)->data_;
}

Var Bool::toString() {
  return data_ ? "true" : "false";
}

}  // namespace d2c
#endif  // dart2cpp_bool_impl_h

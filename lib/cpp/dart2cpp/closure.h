/****************************************************************************
 * Copyright (C) 2014 by Brendan Duncan.                                    *
 *                                                                          *
 * This file is part of Dart2Cpp,                                           *
 * https://github.com/brendan-duncan/dart2cpp.                              *
 *                                                                          *
 * Licensed under the Apache License, Version 2.0 (the "License");          *
 * you may not use this file except in compliance with the License.         *
 * You may obtain a copy of the License at                                  *
 *                                                                          *
 * http://www.apache.org/licenses/LICENSE-2.0                               *
 *                                                                          *
 * Unless required by applicable law or agreed to in writing, software      *
 * distributed under the License is distributed on an "AS IS" BASIS,        *
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. *
 * See the License for the specific language governing permissions and      *
 * limitations under the License.                                           *
 ****************************************************************************/
#ifndef dart2cpp_closure_h
#define dart2cpp_closure_h

#include "object.h"
#include <memory>

namespace d2c {
class Var;

class ClosureCapture {
 public:
  ClosureCapture();
  ~ClosureCapture();
};
typedef std::shared_ptr<ClosureCapture> ClosureCapturePtr;

#define D2C_CLOSURE(N, f) \
                          \
  d2c::closure##N(d2c::ClosureCapturePtr(new d2c::ClosureCapture()), f)

template <typename T>
Var closure0(ClosureCapturePtr capture, T f);

template <typename T>
Var closure1(ClosureCapturePtr capture, T f);

template <typename T>
Var closure2(ClosureCapturePtr capture, T f);

template <typename T>
Var closure3(ClosureCapturePtr capture, T f);

template <typename T>
Var closure4(ClosureCapturePtr capture, T f);

template <typename T>
Var closure5(ClosureCapturePtr capture, T f);

template <typename T>
Var closure6(ClosureCapturePtr capture, T f);

template <typename T>
Var closure7(ClosureCapturePtr capture, T f);

template <typename T>
Var closure8(ClosureCapturePtr capture, T f);

template <typename T>
Var closure9(ClosureCapturePtr capture, T f);

template <typename T>
Var closure10(ClosureCapturePtr capture, T f);

template <typename T>
class Closure0 : public Object {
 public:
  typedef Object super;
  T function;

  Closure0(T f);

  bool isFunction__() {
    return true;
  }

  Var call_0();

  virtual Var toString();

  virtual void initialize__();
};

template <typename T>
class Closure1 : public Object {
 public:
  typedef Object super;
  T function;

  Closure1(T f);

  bool isFunction__() {
    return true;
  }

  Var call_1(const Var &p1);

  virtual Var toString();

  virtual void initialize__();
};

template <typename T>
class Closure2 : public Object {
 public:
  typedef Object super;
  T function;

  Closure2(T f);

  bool isFunction__() {
    return true;
  }

  Var call_2(const Var &p1, const Var &p2);

  virtual Var toString();

  virtual void initialize__();
};

template <typename T>
class Closure3 : public Object {
 public:
  typedef Object super;
  T function;

  Closure3(T f);

  bool isFunction__() {
    return true;
  }

  Var call_3(const Var &p1, const Var &p2, const Var &p3);

  virtual Var toString();

  virtual void initialize__();
};

template <typename T>
class Closure4 : public Object {
 public:
  typedef Object super;
  T function;

  Closure4(T f);

  bool isFunction__() {
    return true;
  }

  Var call_4(const Var &p1, const Var &p2, const Var &p3, const Var &p4);

  virtual Var toString();

  virtual void initialize__();
};

template <typename T>
class Closure5 : public Object {
 public:
  typedef Object super;
  T function;

  Closure5(T f);

  bool isFunction__() {
    return true;
  }

  Var call_5(const Var &p1, const Var &p2, const Var &p3, const Var &p4,
             const Var &p5);

  virtual Var toString();

  virtual void initialize__();
};

template <typename T>
class Closure6 : public Object {
 public:
  typedef Object super;
  T function;

  Closure6(T f);

  bool isFunction__() {
    return true;
  }

  Var call_6(const Var &p1, const Var &p2, const Var &p3, const Var &p4,
             const Var &p5, const Var &p6);

  virtual Var toString();

  virtual void initialize__();
};

template <typename T>
class Closure7 : public Object {
 public:
  typedef Object super;
  T function;

  Closure7(T f);

  bool isFunction__() {
    return true;
  }

  Var call_7(const Var &p1, const Var &p2, const Var &p3, const Var &p4,
             const Var &p5, const Var &p6, const Var &p7);

  virtual Var toString();

  virtual void initialize__();
};

template <typename T>
class Closure8 : public Object {
 public:
  typedef Object super;
  T function;

  Closure8(T f);

  bool isFunction__() {
    return true;
  }

  Var call_8(const Var &p1, const Var &p2, const Var &p3, const Var &p4,
             const Var &p5, const Var &p6, const Var &p7, const Var &p8);

  virtual Var toString();

  virtual void initialize__();
};

template <typename T>
class Closure9 : public Object {
 public:
  typedef Object super;
  T function;

  Closure9(T f);

  bool isFunction__() {
    return true;
  }

  Var call_9(const Var &p1, const Var &p2, const Var &p3, const Var &p4,
             const Var &p5, const Var &p6, const Var &p7, const Var &p8,
             const Var &p9);

  virtual Var toString();

  virtual void initialize__();
};

template <typename T>
class Closure10 : public Object {
 public:
  typedef Object super;
  T function;

  Closure10(T f);

  bool isFunction__() {
    return true;
  }

  Var call_10(const Var &p1, const Var &p2, const Var &p3, const Var &p4,
              const Var &p5, const Var &p6, const Var &p7, const Var &p8,
              const Var &p9, const Var &p10);

  virtual Var toString();

  virtual void initialize__();
};
}  // namespace d2c
#endif  // dart2cpp_closure_h

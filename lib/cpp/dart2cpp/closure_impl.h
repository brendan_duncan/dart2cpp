/****************************************************************************
 * Copyright (C) 2014 by Brendan Duncan.                                    *
 *                                                                          *
 * This file is part of Dart2Cpp,                                           *
 * https://github.com/brendan-duncan/dart2cpp.                              *
 *                                                                          *
 * Licensed under the Apache License, Version 2.0 (the "License");          *
 * you may not use this file except in compliance with the License.         *
 * You may obtain a copy of the License at                                  *
 *                                                                          *
 * http://www.apache.org/licenses/LICENSE-2.0                               *
 *                                                                          *
 * Unless required by applicable law or agreed to in writing, software      *
 * distributed under the License is distributed on an "AS IS" BASIS,        *
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. *
 * See the License for the specific language governing permissions and      *
 * limitations under the License.                                           *
 ****************************************************************************/
#ifndef dart2cpp_closure_impl_h
#define dart2cpp_closure_impl_h

#include "closure.h"
#include "var.h"

namespace d2c {
ClosureCapture::ClosureCapture() {
  Var::startClosureCapture();
}

ClosureCapture::~ClosureCapture() {
  Var::stopClosureCapture();
}

template <typename T>
Var closure0(ClosureCapturePtr capture, T f) {
  return Var(new Closure0<T>(f));
}

template <typename T>
Var closure1(ClosureCapturePtr capture, T f) {
  return Var(new Closure1<T>(f));
}

template <typename T>
Var closure2(ClosureCapturePtr capture, T f) {
  Var v(new Closure2<T>(f));
  return v;
}

template <typename T>
Var closure3(ClosureCapturePtr capture, T f) {
  return Var(new Closure3<T>(f));
}

template <typename T>
Var closure4(ClosureCapturePtr capture, T f) {
  return Var(new Closure4<T>(f));
}

template <typename T>
Var closure5(ClosureCapturePtr capture, T f) {
  return Var(new Closure5<T>(f));
}

template <typename T>
Var closure6(ClosureCapturePtr capture, T f) {
  return Var(new Closure6<T>(f));
}

template <typename T>
Var closure7(ClosureCapturePtr capture, T f) {
  return Var(new Closure7<T>(f));
}

template <typename T>
Var closure8(ClosureCapturePtr capture, T f) {
  return Var(new Closure8<T>(f));
}

template <typename T>
Var closure9(ClosureCapturePtr capture, T f) {
  return Var(new Closure9<T>(f));
}

template <typename T>
Var closure10(ClosureCapturePtr capture, T f) {
  return Var(new Closure10<T>(f));
}

template <typename T>
Closure0<T>::Closure0(T f)
    : function(f) {
}

template <typename T>
Var Closure0<T>::call_0() {
  return function();
}

template <typename T>
void Closure0<T>::initialize__() {
  super::initialize__();
  methods_[TK_call__][0] = d2c::bind(this, &Closure0<T>::call_0);
}

template <typename T>
Var Closure0<T>::toString() {
  return "Closure";
}

template <typename T>
Closure1<T>::Closure1(T f)
    : function(f) {
}

template <typename T>
Var Closure1<T>::call_1(const Var &p1) {
  return function(p1);
}

template <typename T>
void Closure1<T>::initialize__() {
  super::initialize__();
  methods_[TK_call__][1] = d2c::bind(this, &Closure1<T>::call_1);
}

template <typename T>
Var Closure1<T>::toString() {
  return "Closure";
}

template <typename T>
Closure2<T>::Closure2(T f)
    : function(f) {
}

template <typename T>
Var Closure2<T>::call_2(const Var &p1, const Var &p2) {
  return function(p1, p2);
}

template <typename T>
void Closure2<T>::initialize__() {
  super::initialize__();
  methods_[TK_call__][2] = d2c::bind(this, &Closure2<T>::call_2);
}

template <typename T>
Var Closure2<T>::toString() {
  return "Closure";
}

template <typename T>
Closure3<T>::Closure3(T f)
    : function(f) {
}

template <typename T>
Var Closure3<T>::call_3(const Var &p1, const Var &p2, const Var &p3) {
  return function(p1, p2, p3);
}

template <typename T>
void Closure3<T>::initialize__() {
  super::initialize__();
  methods_[TK_call__][3] = d2c::bind(this, &Closure3<T>::call_3);
}

template <typename T>
Var Closure3<T>::toString() {
  return "Closure";
}

template <typename T>
Closure4<T>::Closure4(T f)
    : function(f) {
}

template <typename T>
Var Closure4<T>::call_4(const Var &p1, const Var &p2, const Var &p3,
                        const Var &p4) {
  return function(p1, p2, p3, p4);
}

template <typename T>
void Closure4<T>::initialize__() {
  super::initialize__();
  methods_[TK_call__][4] = d2c::bind(this, &Closure4<T>::call_4);
}

template <typename T>
Var Closure4<T>::toString() {
  return "Closure";
}

template <typename T>
Closure5<T>::Closure5(T f)
    : function(f) {
}

template <typename T>
Var Closure5<T>::call_5(const Var &p1, const Var &p2, const Var &p3,
                        const Var &p4, const Var &p5) {
  return function(p1, p2, p3, p4, p5);
}

template <typename T>
void Closure5<T>::initialize__() {
  super::initialize__();
  methods_[TK_call__][5] = d2c::bind(this, &Closure5<T>::call_5);
}

template <typename T>
Var Closure5<T>::toString() {
  return "Closure";
}

template <typename T>
Closure6<T>::Closure6(T f)
    : function(f) {
}

template <typename T>
Var Closure6<T>::call_6(const Var &p1, const Var &p2, const Var &p3,
                        const Var &p4, const Var &p5, const Var &p6) {
  return function(p1, p2, p3, p4, p5, p6);
}

template <typename T>
void Closure6<T>::initialize__() {
  super::initialize__();
  methods_[TK_call__][6] = d2c::bind(this, &Closure6<T>::call_6);
}

template <typename T>
Var Closure6<T>::toString() {
  return "Closure";
}

template <typename T>
Closure7<T>::Closure7(T f)
    : function(f) {
}

template <typename T>
Var Closure7<T>::call_7(const Var &p1, const Var &p2, const Var &p3,
                        const Var &p4, const Var &p5, const Var &p6,
                        const Var &p7) {
  return function(p1, p2, p3, p4, p5, p6, p7);
}

template <typename T>
void Closure7<T>::initialize__() {
  super::initialize__();
  methods_[TK_call__][7] = d2c::bind(this, &Closure7<T>::call_7);
}

template <typename T>
Var Closure7<T>::toString() {
  return "Closure";
}

template <typename T>
Closure8<T>::Closure8(T f)
    : function(f) {
}

template <typename T>
Var Closure8<T>::call_8(const Var &p1, const Var &p2, const Var &p3,
                        const Var &p4, const Var &p5, const Var &p6,
                        const Var &p7, const Var &p8) {
  return function(p1, p2, p3, p4, p5, p6, p7, p8);
}

template <typename T>
void Closure8<T>::initialize__() {
  super::initialize__();
  methods_[TK_call__][8] = d2c::bind(this, &Closure8<T>::call_8);
}

template <typename T>
Var Closure8<T>::toString() {
  return "Closure";
}

template <typename T>
Closure9<T>::Closure9(T f)
    : function(f) {
}

template <typename T>
Var Closure9<T>::call_9(const Var &p1, const Var &p2, const Var &p3,
                        const Var &p4, const Var &p5, const Var &p6,
                        const Var &p7, const Var &p8, const Var &p9) {
  return function(p1, p2, p3, p4, p5, p6, p7, p8, p9);
}

template <typename T>
void Closure9<T>::initialize__() {
  super::initialize__();
  methods_[TK_call__][9] = d2c::bind(this, &Closure9<T>::call_9);
}

template <typename T>
Var Closure9<T>::toString() {
  return "Closure";
}

template <typename T>
Closure10<T>::Closure10(T f)
    : function(f) {
}

template <typename T>
Var Closure10<T>::call_10(const Var &p1, const Var &p2, const Var &p3,
                          const Var &p4, const Var &p5, const Var &p6,
                          const Var &p7, const Var &p8, const Var &p9,
                          const Var &p10) {
  return function(p1, p2, p3, p4, p5, p6, p7, p8, p9, p10);
}

template <typename T>
void Closure10<T>::initialize__() {
  super::initialize__();
  methods_[TK_call__][10] = d2c::bind(this, &Closure10<T>::call_10);
}

template <typename T>
Var Closure10<T>::toString() {
  return "Closure";
}

}  // namespace d2c
#endif  // dart2cpp_closure_impl_h

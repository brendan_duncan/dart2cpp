/****************************************************************************
 * Copyright (C) 2014 by Brendan Duncan.                                    *
 *                                                                          *
 * This file is part of Dart2Cpp,                                           *
 * https://github.com/brendan-duncan/dart2cpp.                              *
 *                                                                          *
 * Licensed under the Apache License, Version 2.0 (the "License");          *
 * you may not use this file except in compliance with the License.         *
 * You may obtain a copy of the License at                                  *
 *                                                                          *
 * http://www.apache.org/licenses/LICENSE-2.0                               *
 *                                                                          *
 * Unless required by applicable law or agreed to in writing, software      *
 * distributed under the License is distributed on an "AS IS" BASIS,        *
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. *
 * See the License for the specific language governing permissions and      *
 * limitations under the License.                                           *
 ****************************************************************************/
#ifndef dart2cpp_h
#define dart2cpp_h

#include <algorithm>
#include <cstdint>
#include <functional>
#include <iostream>
#include <limits>
#include <memory>
#include <random>
#include <sstream>
#include <string>
#include <unordered_map>
#include <unordered_set>
#include <vector>

#include "str.h"
#include "token.h"
#include "type_set.h"
#include "type.h"
#include "type_factory.h"

#include "object.h"
#include "var.h"
#include "null.h"
#include "constants.h"

#include "assert.h"
#include "bool.h"
#include "delegate.h"
#include "double.h"
#include "function.h"
#include "integer.h"
#include "invocation.h"
#include "iterable.h"
#include "iterator.h"
#include "closure.h"
#include "list.h"
#include "map.h"
#include "named_arguments.h"
#include "number.h"
#include "print.h"
#include "string.h"
#include "symbol.h"

#include "errors.h"

#include "symbols.h"
#include "null_impl.h"
#include "errors_impl.h"

#include "assert_impl.h"
#include "bool_impl.h"
#include "closure_impl.h"
#include "delegate_impl.h"
#include "double_impl.h"
#include "function_impl.h"
#include "integer_impl.h"
#include "iterable_impl.h"
#include "iterator_impl.h"
#include "invocation_impl.h"
#include "list_impl.h"
#include "type_impl.h"
#include "type_set_impl.h"
#include "type_factory_impl.h"
#include "map_impl.h"
#include "named_arguments_impl.h"
#include "number_impl.h"
#include "object_impl.h"
#include "string_impl.h"
#include "symbol_impl.h"
#include "token_impl.h"
#include "var_impl.h"

#endif

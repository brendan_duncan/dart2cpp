/****************************************************************************
 * Copyright (C) 2014 by Brendan Duncan.                                    *
 *                                                                          *
 * This file is part of Dart2Cpp,                                           *
 * https://github.com/brendan-duncan/dart2cpp.                              *
 *                                                                          *
 * Licensed under the Apache License, Version 2.0 (the "License");          *
 * you may not use this file except in compliance with the License.         *
 * You may obtain a copy of the License at                                  *
 *                                                                          *
 * http://www.apache.org/licenses/LICENSE-2.0                               *
 *                                                                          *
 * Unless required by applicable law or agreed to in writing, software      *
 * distributed under the License is distributed on an "AS IS" BASIS,        *
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. *
 * See the License for the specific language governing permissions and      *
 * limitations under the License.                                           *
 ****************************************************************************/
#ifndef dart2cpp_delegate_h
#define dart2cpp_delegate_h

#include <exception>

namespace d2c {
class Var;
class DelegateBase;

/**
 * This code is derived from:
 * http://www.codeproject.com/Articles/7150/Member-Function-Pointers-and-the-Fastest-Possible
 */
class Delegate {
 public:
  class TypeException : public std::exception {
   public:
    const char *what() const throw() {
      return "Invalid type for delegate.";
    }
  };

  Delegate();

  Delegate(const Delegate &h);

  Delegate(const d2c::Var &object, const Delegate &h);

  Delegate(DelegateBase *d);

  ~Delegate();

  Delegate &operator=(const Delegate &h);

  bool operator==(const Delegate &h);

  bool operator!=(const Delegate &h);

  bool operator<(const Delegate &h);

  operator bool() const {
    return isValid();
  }

  bool isValid() const;

  Var operator()();

  Var operator()(const Var &p1);

  Var operator()(const Var &p1, const Var &p2);

  Var operator()(const Var &p1, const Var &p2, const Var &p3);

  Var operator()(const Var &p1, const Var &p2, const Var &p3, const Var &p4);

  Var operator()(const Var &p1, const Var &p2, const Var &p3, const Var &p4,
                 const Var &p5);

  Var operator()(const Var &p1, const Var &p2, const Var &p3, const Var &p4,
                 const Var &p5, const Var &p6);

  Var operator()(const Var &p1, const Var &p2, const Var &p3, const Var &p4,
                 const Var &p5, const Var &p6, const Var &p7);

  Var operator()(const Var &p1, const Var &p2, const Var &p3, const Var &p4,
                 const Var &p5, const Var &p6, const Var &p7, const Var &p8);

  Var operator()(const Var &p1, const Var &p2, const Var &p3, const Var &p4,
                 const Var &p5, const Var &p6, const Var &p7, const Var &p8,
                 const Var &p9);

  Var operator()(const Var &p1, const Var &p2, const Var &p3, const Var &p4,
                 const Var &p5, const Var &p6, const Var &p7, const Var &p8,
                 const Var &p9, const Var &p10);

  size_t hashCode() const;

  const DelegateBase *constDelegateBase() const {
    return delegate_;
  }

  DelegateBase *delegateBase() {
    return delegate_;
  }

 protected:
  d2c::Var object_;
  DelegateBase *delegate_;
};

// N=0
template <typename X, typename Y>
static Delegate bind(Y *x, Var (X::*func)());  // method

template <typename X>
static Delegate bind(Var (X::*func)());  // static method

static Delegate bind(Var (*func)());  // function

// N=1
template <typename X, typename Y>
static Delegate bind(Y *x, Var (X::*func)(const Var &p1));

template <typename X>
static Delegate bind(Var (X::*func)(const Var &p1));

static Delegate bind(Var (*func)(const Var &p1));

// N=2
template <typename X, typename Y>
static Delegate bind(Y *x, Var (X::*func)(const Var &p1, const Var &p2));

template <typename X>
static Delegate bind(Var (X::*func)(const Var &p1, const Var &p2));

static Delegate bind(Var (*func)(const Var &p1, const Var &p2));

// N=3
template <typename X, typename Y>
static Delegate bind(Y *x, Var (X::*func)(const Var &p1, const Var &p2,
                                          const Var &p3));

template <typename X>
static Delegate bind(Var (X::*func)(const Var &p1, const Var &p2,
                                    const Var &p3));

static Delegate bind(Var (*func)(const Var &p1, const Var &p2, const Var &p3));

// N=4
template <typename X, typename Y>
static Delegate bind(Y *x, Var (X::*func)(const Var &p1, const Var &p2,
                                          const Var &p3, const Var &p4));

template <typename X>
static Delegate bind(Var (X::*func)(const Var &p1, const Var &p2, const Var &p3,
                                    const Var &p4));

static Delegate bind(Var (*func)(const Var &p1, const Var &p2, const Var &p3,
                                 const Var &p4));

// N=5
template <typename X, typename Y>
static Delegate bind(Y *x,
                     Var (X::*func)(const Var &p1, const Var &p2, const Var &p3,
                                    const Var &p4, const Var &p5));

template <typename X>
static Delegate bind(Var (X::*func)(const Var &p1, const Var &p2, const Var &p3,
                                    const Var &p4, const Var &p5));

static Delegate bind(Var (*func)(const Var &p1, const Var &p2, const Var &p3,
                                 const Var &p4, const Var &p5));

// N=6
template <typename X, typename Y>
static Delegate bind(Y *x, Var (X::*func)(const Var &p1, const Var &p2,
                                          const Var &p3, const Var &p4,
                                          const Var &p5, const Var &p6));

template <typename X>
static Delegate bind(Var (X::*func)(const Var &p1, const Var &p2, const Var &p3,
                                    const Var &p4, const Var &p5,
                                    const Var &p6));

static Delegate bind(Var (*func)(const Var &p1, const Var &p2, const Var &p3,
                                 const Var &p4, const Var &p5, const Var &p6));

// N=7
template <typename X, typename Y>
static Delegate bind(Y *x,
                     Var (X::*func)(const Var &p1, const Var &p2, const Var &p3,
                                    const Var &p4, const Var &p5, const Var &p6,
                                    const Var &p7));

template <typename X>
static Delegate bind(Var (X::*func)(const Var &p1, const Var &p2, const Var &p3,
                                    const Var &p4, const Var &p5, const Var &p6,
                                    const Var &p7));

static Delegate bind(Var (*func)(const Var &p1, const Var &p2, const Var &p3,
                                 const Var &p4, const Var &p5, const Var &p6,
                                 const Var &p7));

// N=8
template <typename X, typename Y>
static Delegate bind(Y *x,
                     Var (X::*func)(const Var &p1, const Var &p2, const Var &p3,
                                    const Var &p4, const Var &p5, const Var &p6,
                                    const Var &p7, const Var &p8));

template <typename X>
static Delegate bind(Var (X::*func)(const Var &p1, const Var &p2, const Var &p3,
                                    const Var &p4, const Var &p5, const Var &p6,
                                    const Var &p7, const Var &p8));

static Delegate bind(Var (*func)(const Var &p1, const Var &p2, const Var &p3,
                                 const Var &p4, const Var &p5, const Var &p6,
                                 const Var &p7, const Var &p8));

// N=9
template <typename X, typename Y>
static Delegate bind(Y *x,
                     Var (X::*func)(const Var &p1, const Var &p2, const Var &p3,
                                    const Var &p4, const Var &p5, const Var &p6,
                                    const Var &p7, const Var &p8,
                                    const Var &p9));

template <typename X>
static Delegate bind(Var (X::*func)(const Var &p1, const Var &p2, const Var &p3,
                                    const Var &p4, const Var &p5, const Var &p6,
                                    const Var &p7, const Var &p8,
                                    const Var &p9));

static Delegate bind(Var (*func)(const Var &p1, const Var &p2, const Var &p3,
                                 const Var &p4, const Var &p5, const Var &p6,
                                 const Var &p7, const Var &p8, const Var &p9));

// N=10
template <typename X, typename Y>
static Delegate bind(Y *x,
                     Var (X::*func)(const Var &p1, const Var &p2, const Var &p3,
                                    const Var &p4, const Var &p5, const Var &p6,
                                    const Var &p7, const Var &p8, const Var &p9,
                                    const Var &p10));

template <typename X>
static Delegate bind(Var (X::*func)(const Var &p1, const Var &p2, const Var &p3,
                                    const Var &p4, const Var &p5, const Var &p6,
                                    const Var &p7, const Var &p8, const Var &p9,
                                    const Var &p10));

static Delegate bind(Var (*func)(const Var &p1, const Var &p2, const Var &p3,
                                 const Var &p4, const Var &p5, const Var &p6,
                                 const Var &p7, const Var &p8, const Var &p9,
                                 const Var &p10));

}  // namespace d2c
#endif  // dart2cpp_delegate_h

/****************************************************************************
 * Copyright (C) 2014 by Brendan Duncan.                                    *
 *                                                                          *
 * This file is part of Dart2Cpp,                                           *
 * https://github.com/brendan-duncan/dart2cpp.                              *
 *                                                                          *
 * Licensed under the Apache License, Version 2.0 (the "License");          *
 * you may not use this file except in compliance with the License.         *
 * You may obtain a copy of the License at                                  *
 *                                                                          *
 * http://www.apache.org/licenses/LICENSE-2.0                               *
 *                                                                          *
 * Unless required by applicable law or agreed to in writing, software      *
 * distributed under the License is distributed on an "AS IS" BASIS,        *
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. *
 * See the License for the specific language governing permissions and      *
 * limitations under the License.                                           *
 ****************************************************************************/
#ifndef dart2cpp_delegate1_h
#define dart2cpp_delegate1_h

namespace d2c {

class Delegate1 : public DelegateBase {
 protected:
  typedef Var (*StaticFunctionPtr)(const Var &p1);
  typedef Var (DelegateDetail::GenericClass::*GenericMemFn)(const Var &p1);
  typedef DelegateDetail::ClosurePtr<GenericMemFn, StaticFunctionPtr>
      ClosureType;
  class BindType {
   public:
    BindType() {
    }

    Var invoke(const Var &p1);

    ClosureType closure_;
  };

 public:
  typedef Delegate1 Type;

  Delegate1();

  Delegate1(const Delegate &x);

  Delegate1(const Delegate1 &x);

  virtual size_t hashCode() const {
    return static_cast<size_t>(classHashCode_ + bind_.closure_.hashCode());
  }

  void operator=(const Delegate1 &x);

  bool operator==(const Delegate1 &x) const;

  bool operator!=(const Delegate1 &x) const;

  bool operator<(const Delegate1 &x) const;

  bool operator>(const Delegate1 &x) const;

  // Binding to non-const member functions
  template <typename X, typename Y>
  Delegate1(Y *pthis, Var (X::*function_to_bind)(const Var &p1));

  template <typename X, typename Y>
  void bind(Y *pthis, Var (X::*function_to_bind)(const Var &p1));

  // Binding to const member functions.
  template <typename X, typename Y>
  Delegate1(const Y *pthis, Var (X::*function_to_bind)(const Var &p1) const);

  template <typename X, typename Y>
  void bind(const Y *pthis, Var (X::*function_to_bind)(const Var &p1) const);

  // Static functions. We convert them into a member function call.
  // This constructor also provides implicit conversion
  Delegate1(Var (*function_to_bind)(const Var &p1));

  // for efficiency, prevent creation of a temporary
  void operator=(Var (*function_to_bind)(const Var &p1));

  void bind(Var (*function_to_bind)(const Var &p1));

  // Invoke the delegate
  Var operator()(const Var &p1) const;

 private:
  typedef struct SafeBoolStruct {
    int a_data_pointer_to_this_is_0_on_buggy_compilers;
    StaticFunctionPtr sNonZero;
  } UselessTypedef;

  typedef StaticFunctionPtr SafeBoolStruct::*Unspecified_bool_type;

 public:
  operator Unspecified_bool_type() const;

  // necessary to allow ==0 to work despite the safe_bool idiom
  bool operator==(StaticFunctionPtr funcptr);

  bool operator!=(StaticFunctionPtr funcptr);

  bool operator!() const;

  bool empty() const;

  void clear();

  // Conversion to and from the DelegateMomento storage class
  const DelegateMomento &getMemento();

  void setMemento(const DelegateMomento &any);

 private:
  BindType bind_;
  size_t classHashCode_;
};

inline Var Delegate1::BindType::invoke(const Var &p1) {
  return (*(closure_.getStaticFunction()))(p1);
}

inline Delegate1::Delegate1() {
  clear();
}

inline Delegate1::Delegate1(const Delegate &x) {
  Delegate1 *d = dynamic_cast<Delegate1 *>(
      const_cast<DelegateBase *>(x.constDelegateBase()));
  if (d != nullptr) {
    classHashCode_ = d->classHashCode_;
    bind_.closure_.copyFrom(this, d->bind_.closure_);
  } else {
    clear();
  }
}

inline Delegate1::Delegate1(const Delegate1 &x) {
  classHashCode_ = x.classHashCode_;
  bind_.closure_.copyFrom(this, x.bind_.closure_);
}

inline void Delegate1::operator=(const Delegate1 &x) {
  classHashCode_ = x.classHashCode_;
  bind_.closure_.copyFrom(this, x.bind_.closure_);
}

inline bool Delegate1::operator==(const Delegate1 &x) const {
  return bind_.closure_.isEqual(x.bind_.closure_);
}

inline bool Delegate1::operator!=(const Delegate1 &x) const {
  return !bind_.closure_.isEqual(x.bind_.closure_);
}

inline bool Delegate1::operator<(const Delegate1 &x) const {
  return bind_.closure_.isLess(x.bind_.closure_);
}

inline bool Delegate1::operator>(const Delegate1 &x) const {
  return x.bind_.closure_.isLess(bind_.closure_);
}

// Binding to non-const member functions
template <typename X, typename Y>
inline Delegate1::Delegate1(Y *pthis,
                            Var (X::*function_to_bind)(const Var &p1)) {
  classHashCode_ = typeid(X).hash_code();
  bind_.closure_.bindMemFunc(DelegateDetail::implicit_cast<X *>(pthis),
                             function_to_bind);
}

template <typename X, typename Y>
inline void Delegate1::bind(Y *pthis,
                            Var (X::*function_to_bind)(const Var &p1)) {
  classHashCode_ = typeid(X).hash_code();
  bind_.closure_.bindMemFunc(DelegateDetail::implicit_cast<X *>(pthis),
                             function_to_bind);
}

// Binding to const member functions.
template <typename X, typename Y>
inline Delegate1::Delegate1(const Y *pthis,
                            Var (X::*function_to_bind)(const Var &p1) const) {
  classHashCode_ = typeid(X).hash_code();
  bind_.closure_.bindConstMemFunc(
      DelegateDetail::implicit_cast<const X *>(pthis), function_to_bind);
}

template <typename X, typename Y>
inline void Delegate1::bind(const Y *pthis,
                            Var (X::*function_to_bind)(const Var &p1) const) {
  classHashCode_ = typeid(X).hash_code();
  bind_.closure_.bindConstMemFunc(
      DelegateDetail::implicit_cast<const X *>(pthis), function_to_bind);
}

// Static functions. We convert them into a member function call.
// This constructor also provides implicit conversion
inline Delegate1::Delegate1(Var (*function_to_bind)(const Var &p1)) {
  classHashCode_ = 0;
  bind(function_to_bind);
}

// for efficiency, prevent creation of a temporary
inline void Delegate1::operator=(Var (*function_to_bind)(const Var &p1)) {
  classHashCode_ = 0;
  bind(function_to_bind);
}

inline void Delegate1::bind(Var (*function_to_bind)(const Var &p1)) {
  classHashCode_ = 0;
  bind_.closure_.bindStaticFunc(&bind_, &BindType::invoke, function_to_bind);
}

// Invoke the delegate
inline Var Delegate1::operator()(const Var &p1) const {
  return (bind_.closure_.getClosureThis()->*
          (bind_.closure_.getClosureMemPtr()))(p1);
}

inline Delegate1::operator Unspecified_bool_type() const {
  return empty() ? 0 : &SafeBoolStruct::sNonZero;
}

// necessary to allow ==0 to work despite the safe_bool idiom
inline bool Delegate1::operator==(StaticFunctionPtr funcptr) {
  return bind_.closure_.isEqualToStaticFuncPtr(funcptr);
}

inline bool Delegate1::operator!=(StaticFunctionPtr funcptr) {
  return !bind_.closure_.isEqualToStaticFuncPtr(funcptr);
}

inline bool Delegate1::operator!() const {  // Is it bound to anything?
  return !bind_.closure_;
}

inline bool Delegate1::empty() const {
  return !bind_.closure_;
}

inline void Delegate1::clear() {
  bind_.closure_.clear();
  classHashCode_ = 0;
}

// Conversion to and from the DelegateMomento storage class
inline const DelegateMomento &Delegate1::getMemento() {
  return bind_.closure_;
}

inline void Delegate1::setMemento(const DelegateMomento &any) {
  bind_.closure_.copyFrom(this, any);
}

}  // d2c
#endif  // dart2cpp_delegate1_h

/****************************************************************************
 * Copyright (C) 2014 by Brendan Duncan.                                    *
 *                                                                          *
 * This file is part of Dart2Cpp,                                           *
 * https://github.com/brendan-duncan/dart2cpp.                              *
 *                                                                          *
 * Licensed under the Apache License, Version 2.0 (the "License");          *
 * you may not use this file except in compliance with the License.         *
 * You may obtain a copy of the License at                                  *
 *                                                                          *
 * http://www.apache.org/licenses/LICENSE-2.0                               *
 *                                                                          *
 * Unless required by applicable law or agreed to in writing, software      *
 * distributed under the License is distributed on an "AS IS" BASIS,        *
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. *
 * See the License for the specific language governing permissions and      *
 * limitations under the License.                                           *
 ****************************************************************************/
#ifndef dart2cpp_delegate9_h
#define dart2cpp_delegate9_h

namespace d2c {

class Delegate9 : public DelegateBase {
 protected:
  typedef Var (*StaticFunctionPtr)(const Var &p1, const Var &p2, const Var &p3,
                                   const Var &p4, const Var &p5, const Var &p6,
                                   const Var &p7, const Var &p8, const Var &p9);

  typedef Var (DelegateDetail::GenericClass::*GenericMemFn)(
      const Var &p1, const Var &p2, const Var &p3, const Var &p4, const Var &p5,
      const Var &p6, const Var &p7, const Var &p8, const Var &p9);

  typedef DelegateDetail::ClosurePtr<GenericMemFn, StaticFunctionPtr>
      ClosureType;

  class BindType {
   public:
    BindType() {
    }

    Var invoke(const Var &p1, const Var &p2, const Var &p3, const Var &p4,
               const Var &p5, const Var &p6, const Var &p7, const Var &p8,
               const Var &p9);

    ClosureType closure_;
  };

 public:
  typedef Delegate9 Type;

  Delegate9();

  Delegate9(const Delegate &x);

  Delegate9(const Delegate9 &x);

  // Binding to non-const member functions
  template <typename X, typename Y>
  Delegate9(Y *pthis, Var (X::*function_to_bind)(const Var &p1, const Var &p2,
                                                 const Var &p3, const Var &p4,
                                                 const Var &p5, const Var &p6,
                                                 const Var &p7, const Var &p8,
                                                 const Var &p9));

  // Binding to const member functions.
  template <typename X, typename Y>
  Delegate9(const Y *pthis,
            Var (X::*function_to_bind)(const Var &p1, const Var &p2,
                                       const Var &p3, const Var &p4,
                                       const Var &p5, const Var &p6,
                                       const Var &p7, const Var &p8,
                                       const Var &p9) const);

  // Static functions. We convert them into a member function call.
  // This constructor also provides implicit conversion
  Delegate9(Var (*function_to_bind)(const Var &p1, const Var &p2, const Var &p3,
                                    const Var &p4, const Var &p5, const Var &p6,
                                    const Var &p7, const Var &p8,
                                    const Var &p9));

  virtual size_t hashCode() const {
    return static_cast<size_t>(classHashCode_ + bind_.closure_.hashCode());
  }

  void operator=(const Delegate9 &x);

  bool operator==(const Delegate9 &x) const;

  bool operator!=(const Delegate9 &x) const;

  bool operator<(const Delegate9 &x) const;

  bool operator>(const Delegate9 &x) const;

  template <typename X, typename Y>
  void bind(Y *pthis, Var (X::*function_to_bind)(const Var &p1, const Var &p2,
                                                 const Var &p3, const Var &p4,
                                                 const Var &p5, const Var &p6,
                                                 const Var &p7, const Var &p8,
                                                 const Var &p9));

  template <typename X, typename Y>
  void bind(const Y *pthis,
            Var (X::*function_to_bind)(const Var &p1, const Var &p2,
                                       const Var &p3, const Var &p4,
                                       const Var &p5, const Var &p6,
                                       const Var &p7, const Var &p8,
                                       const Var &p9) const);

  // for efficiency, prevent creation of a temporary
  void operator=(Var (*function_to_bind)(
      const Var &p1, const Var &p2, const Var &p3, const Var &p4, const Var &p5,
      const Var &p6, const Var &p7, const Var &p8, const Var &p9));

  void bind(Var (*function_to_bind)(const Var &p1, const Var &p2, const Var &p3,
                                    const Var &p4, const Var &p5, const Var &p6,
                                    const Var &p7, const Var &p8,
                                    const Var &p9));

  // Invoke the delegate
  Var operator()(const Var &p1, const Var &p2, const Var &p3, const Var &p4,
                 const Var &p5, const Var &p6, const Var &p7, const Var &p8,
                 const Var &p9) const;

 private:
  typedef struct SafeBoolStruct {
    int a_data_pointer_to_this_is_0_on_buggy_compilers;
    StaticFunctionPtr sNonZero;
  } UselessTypedef;

  typedef StaticFunctionPtr SafeBoolStruct::*Unspecified_bool_type;

 public:
  operator Unspecified_bool_type() const;

  // necessary to allow ==0 to work despite the safe_bool idiom
  bool operator==(StaticFunctionPtr funcptr);

  bool operator!=(StaticFunctionPtr funcptr);

  bool operator!() const;

  bool empty() const;

  void clear();

  // Conversion to and from the DelegateMomento storage class
  const DelegateMomento &getMemento();

  void setMemento(const DelegateMomento &any);

 private:
  size_t classHashCode_;
  BindType bind_;
};

Var Delegate9::BindType::invoke(const Var &p1, const Var &p2, const Var &p3,
                                const Var &p4, const Var &p5, const Var &p6,
                                const Var &p7, const Var &p8, const Var &p9) {
  return (*(closure_.getStaticFunction()))(p1, p2, p3, p4, p5, p6, p7, p8, p9);
}

Delegate9::Delegate9() {
  clear();
}

Delegate9::Delegate9(const Delegate &x) {
  Delegate9 *d = dynamic_cast<Delegate9 *>(
      const_cast<DelegateBase *>(x.constDelegateBase()));
  if (d != NULL) {
    classHashCode_ = d->classHashCode_;
    bind_.closure_.copyFrom(this, d->bind_.closure_);
  } else {
    clear();
  }
}

Delegate9::Delegate9(const Delegate9 &x) {
  classHashCode_ = x.classHashCode_;
  bind_.closure_.copyFrom(this, x.bind_.closure_);
}

void Delegate9::operator=(const Delegate9 &x) {
  classHashCode_ = x.classHashCode_;
  bind_.closure_.copyFrom(this, x.bind_.closure_);
}

bool Delegate9::operator==(const Delegate9 &x) const {
  return bind_.closure_.isEqual(x.bind_.closure_);
}

bool Delegate9::operator!=(const Delegate9 &x) const {
  return !bind_.closure_.isEqual(x.bind_.closure_);
}

bool Delegate9::operator<(const Delegate9 &x) const {
  return bind_.closure_.isLess(x.bind_.closure_);
}

bool Delegate9::operator>(const Delegate9 &x) const {
  return x.bind_.closure_.isLess(bind_.closure_);
}

// Binding to non-const member functions
template <typename X, typename Y>
Delegate9::Delegate9(Y *pthis,
                     Var (X::*function_to_bind)(const Var &p1, const Var &p2,
                                                const Var &p3, const Var &p4,
                                                const Var &p5, const Var &p6,
                                                const Var &p7, const Var &p8,
                                                const Var &p9)) {
  classHashCode_ = typeid(X).hash_code();
  bind_.closure_.bindMemFunc(DelegateDetail::implicit_cast<X *>(pthis),
                             function_to_bind);
}

template <typename X, typename Y>
void Delegate9::bind(Y *pthis,
                     Var (X::*function_to_bind)(const Var &p1, const Var &p2,
                                                const Var &p3, const Var &p4,
                                                const Var &p5, const Var &p6,
                                                const Var &p7, const Var &p8,
                                                const Var &p9)) {
  classHashCode_ = typeid(X).hash_code();
  bind_.closure_.bindMemFunc(DelegateDetail::implicit_cast<X *>(pthis),
                             function_to_bind);
}

// Binding to const member functions.
template <typename X, typename Y>
Delegate9::Delegate9(const Y *pthis,
                     Var (X::*function_to_bind)(const Var &p1, const Var &p2,
                                                const Var &p3, const Var &p4,
                                                const Var &p5, const Var &p6,
                                                const Var &p7, const Var &p8,
                                                const Var &p9) const) {
  classHashCode_ = typeid(X).hash_code();
  bind_.closure_.bindConstMemFunc(
      DelegateDetail::implicit_cast<const X *>(pthis), function_to_bind);
}

template <typename X, typename Y>
void Delegate9::bind(const Y *pthis,
                     Var (X::*function_to_bind)(const Var &p1, const Var &p2,
                                                const Var &p3, const Var &p4,
                                                const Var &p5, const Var &p6,
                                                const Var &p7, const Var &p8,
                                                const Var &p9) const) {
  classHashCode_ = typeid(X).hash_code();
  bind_.closure_.bindConstMemFunc(
      DelegateDetail::implicit_cast<const X *>(pthis), function_to_bind);
}

// Static functions. We convert them into a member function call.
// This constructor also provides implicit conversion
Delegate9::Delegate9(Var (*function_to_bind)(
    const Var &p1, const Var &p2, const Var &p3, const Var &p4, const Var &p5,
    const Var &p6, const Var &p7, const Var &p8, const Var &p9)) {
  classHashCode_ = 0;
  bind(function_to_bind);
}

// for efficiency, prevent creation of a temporary
void Delegate9::operator=(Var (*function_to_bind)(
    const Var &p1, const Var &p2, const Var &p3, const Var &p4, const Var &p5,
    const Var &p6, const Var &p7, const Var &p8, const Var &p9)) {
  classHashCode_ = 0;
  bind(function_to_bind);
}

void Delegate9::bind(Var (*function_to_bind)(
    const Var &p1, const Var &p2, const Var &p3, const Var &p4, const Var &p5,
    const Var &p6, const Var &p7, const Var &p8, const Var &p9)) {
  classHashCode_ = 0;
  bind_.closure_.bindStaticFunc(&bind_, &BindType::invoke, function_to_bind);
}

// Invoke the delegate
Var Delegate9::operator()(const Var &p1, const Var &p2, const Var &p3,
                          const Var &p4, const Var &p5, const Var &p6,
                          const Var &p7, const Var &p8, const Var &p9) const {
  return (bind_.closure_.getClosureThis()->*
          (bind_.closure_.getClosureMemPtr()))(p1, p2, p3, p4, p5, p6, p7, p8,
                                               p9);
}

Delegate9::operator Unspecified_bool_type() const {
  return empty() ? 0 : &SafeBoolStruct::sNonZero;
}

// necessary to allow ==0 to work despite the safe_bool idiom
bool Delegate9::operator==(StaticFunctionPtr funcptr) {
  return bind_.closure_.isEqualToStaticFuncPtr(funcptr);
}

bool Delegate9::operator!=(StaticFunctionPtr funcptr) {
  return !bind_.closure_.isEqualToStaticFuncPtr(funcptr);
}

bool Delegate9::operator!() const {  // Is it bound to anything?
  return !bind_.closure_;
}

bool Delegate9::empty() const {
  return !bind_.closure_;
}

void Delegate9::clear() {
  classHashCode_ = 0;
  bind_.closure_.clear();
}

// Conversion to and from the DelegateMomento storage class
const DelegateMomento &Delegate9::getMemento() {
  return bind_.closure_;
}

void Delegate9::setMemento(const DelegateMomento &any) {
  bind_.closure_.copyFrom(this, any);
}

}  // d2c
#endif  // dart2cpp_delegate9_h

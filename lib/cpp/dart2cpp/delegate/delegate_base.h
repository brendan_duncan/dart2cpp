/****************************************************************************
 * Copyright (C) 2014 by Brendan Duncan.                                    *
 *                                                                          *
 * This file is part of Dart2Cpp,                                           *
 * https://github.com/brendan-duncan/dart2cpp.                              *
 *                                                                          *
 * Licensed under the Apache License, Version 2.0 (the "License");          *
 * you may not use this file except in compliance with the License.         *
 * You may obtain a copy of the License at                                  *
 *                                                                          *
 * http://www.apache.org/licenses/LICENSE-2.0                               *
 *                                                                          *
 * Unless required by applicable law or agreed to in writing, software      *
 * distributed under the License is distributed on an "AS IS" BASIS,        *
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. *
 * See the License for the specific language governing permissions and      *
 * limitations under the License.                                           *
 ****************************************************************************/
#ifndef dart2cpp_delegate_base_h
#define dart2cpp_delegate_base_h

namespace d2c {

class DelegateBase {
 public:
  DelegateBase();

  /// Adding a virtual class will allow dynamic_cast to work.
  virtual ~DelegateBase();

  /// The referenceCount is used by Delegate to treat pointers to specific
  /// Delegate# classes as smart pointers.
  const unsigned int &reference_count() const {
    return reference_count_;
  }

  virtual size_t hashCode() const = 0;

 protected:
  friend class Delegate;
  unsigned int reference_count_;
};

}  // namespace d2c
#endif  // dart2cpp_delegate_base_h

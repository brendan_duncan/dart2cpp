/****************************************************************************
 * Copyright (C) 2014 by Brendan Duncan.                                    *
 *                                                                          *
 * This file is part of Dart2Cpp,                                           *
 * https://github.com/brendan-duncan/dart2cpp.                              *
 *                                                                          *
 * Licensed under the Apache License, Version 2.0 (the "License");          *
 * you may not use this file except in compliance with the License.         *
 * You may obtain a copy of the License at                                  *
 *                                                                          *
 * http://www.apache.org/licenses/LICENSE-2.0                               *
 *                                                                          *
 * Unless required by applicable law or agreed to in writing, software      *
 * distributed under the License is distributed on an "AS IS" BASIS,        *
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. *
 * See the License for the specific language governing permissions and      *
 * limitations under the License.                                           *
 ****************************************************************************/
#ifndef dart2cpp_delegate_detail_h
#define dart2cpp_delegate_detail_h

namespace d2c {

// Does the compiler uses Microsoft's member function pointer structure?
// If so, it needs special treatment.
#if defined(_MSC_VER)
#define FASTDLGT_MICROSOFT_MFP
#define FASTDLGT_HASINHERITANCE_KEYWORDS
#endif

namespace DelegateDetail {
template <class OutputClass, class InputClass>
OutputClass implicit_cast(InputClass input) {
  return input;
}

template <class OutputClass, class InputClass>
union horrible_union {
  OutputClass out;
  InputClass in;
};

template <class OutputClass, class InputClass>
OutputClass horrible_cast(const InputClass input) {
  horrible_union<OutputClass, InputClass> u;
  typedef int ERROR_CantUseHorrible_cast[sizeof(InputClass) == sizeof(u) &&
                                                 sizeof(InputClass) ==
                                                     sizeof(OutputClass)
                                             ? 1
                                             : -1];
  u.in = input;
  return u.out;
}

#ifdef FASTDLGT_MICROSOFT_MFP
#ifdef FASTDLGT_HASINHERITANCE_KEYWORDS
class __single_inheritance GenericClass;
#endif
class GenericClass {};
#else
class GenericClass;
#endif

const int SINGLE_MEMFUNCPTR_SIZE = sizeof(void (GenericClass::*)());

template <int N>
struct SimplifyMemFunc {
  template <typename X, typename XFuncType, class GenericMemFuncType>
  static GenericClass *Convert(X *pthis, XFuncType function_to_bind,
                               GenericMemFuncType &bound_func) {
    typedef char
        ERROR_Unsupported_member_function_pointer_on_this_compiler[N - 100];
    return 0;
  }
};

template <>
struct SimplifyMemFunc<SINGLE_MEMFUNCPTR_SIZE> {
  template <typename X, typename XFuncType, class GenericMemFuncType>
  static GenericClass *Convert(X *pthis, XFuncType function_to_bind,
                               GenericMemFuncType &bound_func) {
    bound_func = reinterpret_cast<GenericMemFuncType>(function_to_bind);
    return reinterpret_cast<GenericClass *>(pthis);
  }
};

#ifdef FASTDLGT_MICROSOFT_MFP
template <>
struct SimplifyMemFunc<SINGLE_MEMFUNCPTR_SIZE + sizeof(int)> {
  template <typename X, typename XFuncType, class GenericMemFuncType>
  static GenericClass *Convert(X *pthis, XFuncType function_to_bind,
                               GenericMemFuncType &bound_func) {
    // We need to use a horrible_cast to do this conversion.
    // In MSVC, a multiple inheritance member pointer is internally defined as:
    union {
      XFuncType func;
      struct {
        GenericMemFuncType funcaddress;  // points to the actual member function
        int delta;  // #BYTES to be added to the 'this' pointer
      } s;
    } u;
    // Check that the horrible_cast will work
    typedef int ERROR_CantUsehorrible_cast
        [sizeof(function_to_bind) == sizeof(u.s) ? 1 : -1];
    u.func = function_to_bind;
    bound_func = u.s.funcaddress;
    return reinterpret_cast<GenericClass *>(reinterpret_cast<char *>(pthis) +
                                            u.s.delta);
  }
};

struct MicrosoftVirtualMFP {
  void (GenericClass::*codeptr)();  // points to the actual member function
  int delta;                        // #bytes to be added to the 'this' pointer
  int vtable_index;                 // or 0 if no virtual inheritance
};

struct GenericVirtualClass : virtual public GenericClass {
  typedef GenericVirtualClass *(GenericVirtualClass::*ProbePtrType)();
  GenericVirtualClass *GetThis() {
    return this;
  }
};

template <>
struct SimplifyMemFunc<SINGLE_MEMFUNCPTR_SIZE + 2 * sizeof(int)> {
  template <typename X, typename XFuncType, class GenericMemFuncType>
  static GenericClass *Convert(X *pthis, XFuncType function_to_bind,
                               GenericMemFuncType &bound_func) {
    union {
      XFuncType func;
      GenericClass *(X::*ProbeFunc)();
      MicrosoftVirtualMFP s;
    } u;

    u.func = function_to_bind;
    bound_func = reinterpret_cast<GenericMemFuncType>(u.s.codeptr);

    union {
      GenericVirtualClass::ProbePtrType virtfunc;
      MicrosoftVirtualMFP s;
    } u2;

    // Check that the horrible_cast<>s will work
    typedef int ERROR_CantUsehorrible_cast
        [sizeof(function_to_bind) == sizeof(u.s) &&
                 sizeof(function_to_bind) == sizeof(u.ProbeFunc) &&
                 sizeof(u2.virtfunc) == sizeof(u2.s)
             ? 1
             : -1];
    // Unfortunately, taking the address of a MF prevents it from being inlined,
    // so
    // this next line can't be completely optimised away by the compiler.
    u2.virtfunc = &GenericVirtualClass::GetThis;
    u.s.codeptr = u2.s.codeptr;
    return (pthis->*u.ProbeFunc)();
  }
};

template <>
struct SimplifyMemFunc<SINGLE_MEMFUNCPTR_SIZE + 3 * sizeof(int)> {
  template <typename X, typename XFuncType, class GenericMemFuncType>
  static GenericClass *Convert(X *pthis, XFuncType function_to_bind,
                               GenericMemFuncType &bound_func) {
    // The member function pointer is 16 bytes long. We can't use a normal cast,
    // but we can use a union to do the conversion.
    union {
      XFuncType func;
      // In VC++ and ICL, an unknown_inheritance member pointer
      // is internally defined as:
      struct {
        // points to the actual member function
        GenericMemFuncType m_funcaddress;
        int delta;          // #bytes to be added to the 'this' pointer
        int vtordisp;       // #bytes to add to 'this' to find the vtable
        int vtable_index;   // or 0 if no virtual inheritance
      } s;
    } u;

    // Check that the horrible_cast will work
    typedef int
        ERROR_CantUsehorrible_cast[sizeof(XFuncType) == sizeof(u.s) ? 1 : -1];
    
    u.func = function_to_bind;
    bound_func = u.s.funcaddress;
    int virtual_delta = 0;
    
    if (u.s.vtable_index) {
      const int *vtable =
          *reinterpret_cast<const int *const *>(
              reinterpret_cast<const char *>(pthis) + u.s.vtordisp);

      // 'vtable_index' tells us where in the table we should be looking.
      virtual_delta =
          u.s.vtordisp +
          *reinterpret_cast<const int *>(
              reinterpret_cast<const char *>(vtable) + u.s.vtable_index);
    }

    // The int at 'virtual_delta' gives us the amount to add to 'this'.
    // Finally we can add the three components together. Phew!
    return reinterpret_cast<GenericClass *>(reinterpret_cast<char *>(pthis) +
                                            u.s.delta + virtual_delta);
  };
};
#endif  // MS/Intel hacks
}  // namespace DelegateDetail

class DelegateMomento {
 public:
  DelegateMomento()
      : this_(nullptr)
      , function_(nullptr) {
  }

  DelegateMomento(const DelegateMomento &right)
      : this_(right.this_)
      , function_(right.function_) {
  }

  void clear() {
    this_ = nullptr;
    function_ = nullptr;
  }

  bool isEqual(const DelegateMomento &x) const {
    return this_ == x.this_ && function_ == x.function_;
  }

  /// Provide a strict weak ordering for DelegateMomentos.
  bool isLess(const DelegateMomento &right) const {
    if (this_ != right.this_) {
      return this_ < right.this_;
    }
    return memcmp(&function_, &right.function_, sizeof(function_)) < 0;
  }

  /// Is it bound to anything?
  bool operator!() const {
    return this_ == nullptr && function_ == nullptr;
  }

  /// Is it bound to anything?
  bool empty() const {
    return this_ == nullptr && function_ == nullptr;
  }

  DelegateMomento &operator=(const DelegateMomento &right) {
    setMomentoFrom(right);
    return *this;
  }

  bool operator<(const DelegateMomento &right) {
    return isLess(right);
  }

  bool operator>(const DelegateMomento &right) {
    return right.isLess(*this);
  }

 protected:
  void setMomentoFrom(const DelegateMomento &right) {
    function_ = right.function_;
    this_ = right.this_;
  }

 protected:
  typedef void (DelegateDetail::GenericClass::*GenericMemFuncType)();
  DelegateDetail::GenericClass *this_;
  GenericMemFuncType function_;
};

namespace DelegateDetail {
template <class GenericMemFunc, class StaticFuncPtr>
class ClosurePtr : public DelegateMomento {
 public:
  // These functions are for setting the delegate to a member function.

  // Here's the clever bit: we convert an arbitrary member function into a
  // standard form. XMemFunc should be a member function of typename X, but I
  // can't enforce that here. It needs to be enforced by the wrapper class.
  template <typename X, typename XMemFunc>
  void bindMemFunc(X *pthis, XMemFunc function_to_bind) {
    this_ = SimplifyMemFunc<sizeof(function_to_bind)>::Convert(
        pthis, function_to_bind, function_);
  }

  // For const member functions, we only need a const typename Pointer.
  // Since we know that the member function is const, it's safe to
  // remove the const qualifier from the 'this' pointer with a const_cast.
  template <typename X, typename XMemFunc>
  void bindConstMemFunc(const X *pthis, XMemFunc function_to_bind) {
    this_ = SimplifyMemFunc<sizeof(function_to_bind)>::Convert(
        const_cast<X *>(pthis), function_to_bind, function_);
  }

  // These functions are required for invoking the stored function
  GenericClass *getClosureThis() const {
    return this_;
  }

  GenericMemFunc getClosureMemPtr() const {
    return reinterpret_cast<GenericMemFunc>(function_);
  }

  size_t hashCode() const {
    // Hack to get around the fact that you can't get a data pointer from
    // a function pointer, and therefore I don't know how to compute a hash
    // for one.
    char buf[sizeof(function_)];
    memcpy(&buf, &function_, sizeof(function_));
    size_t h = std::hash<char *>()(buf);
    return h;
  }

  template <class DerivedClass>
  void copyFrom(DerivedClass *pParent, const DelegateMomento &right) {
    setMomentoFrom(right);
  }

  template <class DerivedClass, typename ParentInvokerSig>
  void bindStaticFunc(DerivedClass *pParent,
                      ParentInvokerSig static_function_invoker,
                      StaticFuncPtr function_to_bind) {
    if (function_to_bind == nullptr) {
      function_ = nullptr;
    } else {
      // We'll be ignoring the 'this' pointer, but we need to make sure we pass
      // a valid value to bindMemFunc().
      bindMemFunc(pParent, static_function_invoker);
    }

    this_ = horrible_cast<GenericClass *>(function_to_bind);
  }

  StaticFuncPtr getStaticFunction() const {
    return horrible_cast<StaticFuncPtr>(this);
  }

  /// Does the closure contain this static function?
  bool isEqualToStaticFuncPtr(StaticFuncPtr funcptr) {
    if (funcptr == nullptr) {
      return empty();
    }
    return (funcptr == reinterpret_cast<StaticFuncPtr>(getStaticFunction()));
  }
};

}  // namespace DelegateDetail

}  // d2c
#endif  // dart2cpp_delegate_detail_h

/****************************************************************************
 * Copyright (C) 2014 by Brendan Duncan.                                    *
 *                                                                          *
 * This file is part of Dart2Cpp,                                           *
 * https://github.com/brendan-duncan/dart2cpp.                              *
 *                                                                          *
 * Licensed under the Apache License, Version 2.0 (the "License");          *
 * you may not use this file except in compliance with the License.         *
 * You may obtain a copy of the License at                                  *
 *                                                                          *
 * http://www.apache.org/licenses/LICENSE-2.0                               *
 *                                                                          *
 * Unless required by applicable law or agreed to in writing, software      *
 * distributed under the License is distributed on an "AS IS" BASIS,        *
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. *
 * See the License for the specific language governing permissions and      *
 * limitations under the License.                                           *
 ****************************************************************************/
#ifndef dart2cpp_delegate_impl_h
#define dart2cpp_delegate_impl_h

#include "delegate.h"
#include "var.h"

#include "delegate/delegate_detail.h"
#include "delegate/delegate_base.h"
#include "delegate/delegate0.h"
#include "delegate/delegate1.h"
#include "delegate/delegate2.h"
#include "delegate/delegate3.h"
#include "delegate/delegate4.h"
#include "delegate/delegate5.h"
#include "delegate/delegate6.h"
#include "delegate/delegate7.h"
#include "delegate/delegate8.h"
#include "delegate/delegate9.h"
#include "delegate/delegate10.h"

namespace d2c {

DelegateBase::DelegateBase()
    : reference_count_(0) {
}

DelegateBase::~DelegateBase() {
}

Delegate::Delegate()
    : delegate_(nullptr) {
}

Delegate::Delegate(const Delegate &h)
    : object_(h.object_)
    , delegate_(h.delegate_) {
  if (delegate_) {
    delegate_->reference_count_++;
  }
}

Delegate::Delegate(const d2c::Var &object, const Delegate &h)
    : object_(object)
    , delegate_(h.delegate_) {
  if (delegate_) {
    delegate_->reference_count_++;
  }
}

Delegate::Delegate(DelegateBase *d)
    : delegate_(d) {
  if (delegate_ != nullptr) {
    delegate_->reference_count_++;
  }
}

Delegate::~Delegate() {
  if (delegate_ != nullptr) {
    delegate_->reference_count_--;
    if (delegate_->reference_count_ == 0) {
      delete delegate_;
      delegate_ = nullptr;
    }
  }
}

Delegate &Delegate::operator=(const Delegate &h) {
  object_ = h.object_;

  if (delegate_ != nullptr) {
    delegate_->reference_count_--;
    if (delegate_->reference_count_ == 0) {
      delete delegate_;
      delegate_ = nullptr;
    }
  }

  delegate_ = h.delegate_;

  if (delegate_ != nullptr) {
    delegate_->reference_count_++;
  }

  return *this;
}

bool Delegate::operator==(const Delegate &h) {
  return delegate_ == h.delegate_;
}

bool Delegate::operator!=(const Delegate &h) {
  return delegate_ != h.delegate_;
}

bool Delegate::operator<(const Delegate &h) {
  return delegate_ < h.delegate_;
}

bool Delegate::isValid() const {
  return delegate_ != nullptr;
}

Var Delegate::operator()() {
  if (delegate_ == nullptr) {
    return null;
  }
  Delegate0 *f = dynamic_cast<Delegate0 *>(delegate_);
  if (f == nullptr) {
    return null;
  }
  return (*f)();
}

Var Delegate::operator()(const Var &p1) {
  if (delegate_ == nullptr) {
    return null;
  }
  Delegate1 *f = dynamic_cast<Delegate1 *>(delegate_);
  if (f == nullptr) {
    return null;
  }
  return (*f)(p1);
}

Var Delegate::operator()(const Var &p1, const Var &p2) {
  if (delegate_ == nullptr) {
    return null;
  }
  Delegate2 *f = dynamic_cast<Delegate2 *>(delegate_);
  if (f == nullptr) {
    return null;
  }
  return (*f)(p1, p2);
}

Var Delegate::operator()(const Var &p1, const Var &p2, const Var &p3) {
  Delegate3 *f = dynamic_cast<Delegate3 *>(delegate_);
  if (f == nullptr) {
    return null;
  }
  return (*f)(p1, p2, p3);
}

Var Delegate::operator()(const Var &p1, const Var &p2, const Var &p3,
                         const Var &p4) {
  Delegate4 *f = dynamic_cast<Delegate4 *>(delegate_);
  if (f == nullptr) {
    return null;
  }
  return (*f)(p1, p2, p3, p4);
}

Var Delegate::operator()(const Var &p1, const Var &p2, const Var &p3,
                         const Var &p4, const Var &p5) {
  Delegate5 *f = dynamic_cast<Delegate5 *>(delegate_);
  if (f == nullptr) {
    return null;
  }
  return (*f)(p1, p2, p3, p4, p5);
}

Var Delegate::operator()(const Var &p1, const Var &p2, const Var &p3,
                         const Var &p4, const Var &p5, const Var &p6) {
  Delegate6 *f = dynamic_cast<Delegate6 *>(delegate_);
  if (f == nullptr) {
    return null;
  }
  return (*f)(p1, p2, p3, p4, p5, p6);
}

Var Delegate::operator()(const Var &p1, const Var &p2, const Var &p3,
                         const Var &p4, const Var &p5, const Var &p6,
                         const Var &p7) {
  Delegate7 *f = dynamic_cast<Delegate7 *>(delegate_);
  if (f == nullptr) {
    return null;
  }
  return (*f)(p1, p2, p3, p4, p5, p6, p7);
}

Var Delegate::operator()(const Var &p1, const Var &p2, const Var &p3,
                         const Var &p4, const Var &p5, const Var &p6,
                         const Var &p7, const Var &p8) {
  Delegate8 *f = dynamic_cast<Delegate8 *>(delegate_);
  if (f == nullptr) {
    return null;
  }
  return (*f)(p1, p2, p3, p4, p5, p6, p7, p8);
}

Var Delegate::operator()(const Var &p1, const Var &p2, const Var &p3,
                         const Var &p4, const Var &p5, const Var &p6,
                         const Var &p7, const Var &p8, const Var &p9) {
  Delegate9 *f = dynamic_cast<Delegate9 *>(delegate_);
  if (f == nullptr) {
    return null;
  }
  return (*f)(p1, p2, p3, p4, p5, p6, p7, p8, p9);
}

Var Delegate::operator()(const Var &p1, const Var &p2, const Var &p3,
                         const Var &p4, const Var &p5, const Var &p6,
                         const Var &p7, const Var &p8, const Var &p9,
                         const Var &p10) {
  Delegate10 *f = dynamic_cast<Delegate10 *>(delegate_);
  if (f == nullptr) {
    return null;
  }
  return (*f)(p1, p2, p3, p4, p5, p6, p7, p8, p9, p10);
}

size_t Delegate::hashCode() const {
  if (delegate_ != nullptr) {
    return delegate_->hashCode();
  }
  return 0;
}

// N=0
template <typename X, typename Y>
Delegate bind(Y *x, Var (X::*func)()) {
  return new Delegate0((X *)x, func);
}

template <typename X>
Delegate bind(Var (X::*func)()) {
  return new Delegate0(func);
}

Delegate bind(Var (*func)()) {
  return new Delegate0(func);
}

// N=1
template <typename X, typename Y>
Delegate bind(Y *x, Var (X::*func)(const Var &p1)) {
  return new Delegate1((X *)x, func);
}

template <typename X>
Delegate bind(Var (X::*func)(const Var &p1)) {
  return new Delegate1(func);
}

Delegate bind(Var (*func)(const Var &p1)) {
  return new Delegate1(func);
}

// N=2
template <typename X, typename Y>
Delegate bind(Y *x, Var (X::*func)(const Var &p1, const Var &p2)) {
  return new Delegate2((X *)x, func);
}

template <typename X>
Delegate bind(Var (X::*func)(const Var &p1, const Var &p2)) {
  return new Delegate2(func);
}

Delegate bind(Var (*func)(const Var &p1, const Var &p2)) {
  return new Delegate2(func);
}

// N=3
template <typename X, typename Y>
Delegate bind(Y *x,
              Var (X::*func)(const Var &p1, const Var &p2, const Var &p3)) {
  return new Delegate3((X *)x, func);
}

template <typename X>
Delegate bind(Var (X::*func)(const Var &p1, const Var &p2, const Var &p3)) {
  return new Delegate3(func);
}

Delegate bind(Var (*func)(const Var &p1, const Var &p2, const Var &p3)) {
  return new Delegate3(func);
}

// N=4
template <typename X, typename Y>
Delegate bind(Y *x, Var (X::*func)(const Var &p1, const Var &p2, const Var &p3,
                                   const Var &p4)) {
  return new Delegate4((X *)x, func);
}

template <typename X>
Delegate bind(Var (X::*func)(const Var &p1, const Var &p2, const Var &p3,
                             const Var &p4)) {
  return new Delegate4(func);
}

Delegate bind(Var (*func)(const Var &p1, const Var &p2, const Var &p3,
                          const Var &p4)) {
  return new Delegate4(func);
}

// N=5
template <typename X, typename Y>
Delegate bind(Y *x, Var (X::*func)(const Var &p1, const Var &p2, const Var &p3,
                                   const Var &p4, const Var &p5)) {
  return new Delegate5((X *)x, func);
}

template <typename X>
Delegate bind(Var (X::*func)(const Var &p1, const Var &p2, const Var &p3,
                             const Var &p4, const Var &p5)) {
  return new Delegate5(func);
}

Delegate bind(Var (*func)(const Var &p1, const Var &p2, const Var &p3,
                          const Var &p4, const Var &p5)) {
  return new Delegate5(func);
}

// N=6
template <typename X, typename Y>
Delegate bind(Y *x,
              Var (X::*func)(const Var &p1, const Var &p2, const Var &p3,
                             const Var &p4, const Var &p5, const Var &p6)) {
  return new Delegate6((X *)x, func);
}

template <typename X>
Delegate bind(Var (X::*func)(const Var &p1, const Var &p2, const Var &p3,
                             const Var &p4, const Var &p5, const Var &p6)) {
  return new Delegate6(func);
}

Delegate bind(Var (*func)(const Var &p1, const Var &p2, const Var &p3,
                          const Var &p4, const Var &p5, const Var &p6)) {
  return new Delegate6(func);
}

// N=7
template <typename X, typename Y>
Delegate bind(Y *x, Var (X::*func)(const Var &p1, const Var &p2, const Var &p3,
                                   const Var &p4, const Var &p5, const Var &p6,
                                   const Var &p7)) {
  return new Delegate7((X *)x, func);
}

template <typename X>
Delegate bind(Var (X::*func)(const Var &p1, const Var &p2, const Var &p3,
                             const Var &p4, const Var &p5, const Var &p6,
                             const Var &p7)) {
  return new Delegate7(func);
}

Delegate bind(Var (*func)(const Var &p1, const Var &p2, const Var &p3,
                          const Var &p4, const Var &p5, const Var &p6,
                          const Var &p7)) {
  return new Delegate7(func);
}

// N=8
template <typename X, typename Y>
Delegate bind(Y *x, Var (X::*func)(const Var &p1, const Var &p2, const Var &p3,
                                   const Var &p4, const Var &p5, const Var &p6,
                                   const Var &p7, const Var &p8)) {
  return new Delegate8((X *)x, func);
}

template <typename X>
Delegate bind(Var (X::*func)(const Var &p1, const Var &p2, const Var &p3,
                             const Var &p4, const Var &p5, const Var &p6,
                             const Var &p7, const Var &p8)) {
  return new Delegate8(func);
}

Delegate bind(Var (*func)(const Var &p1, const Var &p2, const Var &p3,
                          const Var &p4, const Var &p5, const Var &p6,
                          const Var &p7, const Var &p8)) {
  return new Delegate8(func);
}

// N=9
template <typename X, typename Y>
Delegate bind(Y *x,
              Var (X::*func)(const Var &p1, const Var &p2, const Var &p3,
                             const Var &p4, const Var &p5, const Var &p6,
                             const Var &p7, const Var &p8, const Var &p9)) {
  return new Delegate9((X *)x, func);
}

template <typename X>
Delegate bind(Var (X::*func)(const Var &p1, const Var &p2, const Var &p3,
                             const Var &p4, const Var &p5, const Var &p6,
                             const Var &p7, const Var &p8, const Var &p9)) {
  return new Delegate9(func);
}

Delegate bind(Var (*func)(const Var &p1, const Var &p2, const Var &p3,
                          const Var &p4, const Var &p5, const Var &p6,
                          const Var &p7, const Var &p8, const Var &p9)) {
  return new Delegate9(func);
}

// N=10
template <typename X, typename Y>
Delegate bind(Y *x, Var (X::*func)(const Var &p1, const Var &p2, const Var &p3,
                                   const Var &p4, const Var &p5, const Var &p6,
                                   const Var &p7, const Var &p8, const Var &p9,
                                   const Var &p10)) {
  return new Delegate10((X *)x, func);
}

template <typename X>
Delegate bind(Var (X::*func)(const Var &p1, const Var &p2, const Var &p3,
                             const Var &p4, const Var &p5, const Var &p6,
                             const Var &p7, const Var &p8, const Var &p9,
                             const Var &p10)) {
  return new Delegate10(func);
}

Delegate bind(Var (*func)(const Var &p1, const Var &p2, const Var &p3,
                          const Var &p4, const Var &p5, const Var &p6,
                          const Var &p7, const Var &p8, const Var &p9,
                          const Var &p10)) {
  return new Delegate10(func);
}

}  // d2c
#endif  // dart2cpp_delegate_impl_h

/****************************************************************************
 * Copyright (C) 2014 by Brendan Duncan.                                    *
 *                                                                          *
 * This file is part of Dart2Cpp,                                           *
 * https://github.com/brendan-duncan/dart2cpp.                              *
 *                                                                          *
 * Licensed under the Apache License, Version 2.0 (the "License");          *
 * you may not use this file except in compliance with the License.         *
 * You may obtain a copy of the License at                                  *
 *                                                                          *
 * http://www.apache.org/licenses/LICENSE-2.0                               *
 *                                                                          *
 * Unless required by applicable law or agreed to in writing, software      *
 * distributed under the License is distributed on an "AS IS" BASIS,        *
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. *
 * See the License for the specific language governing permissions and      *
 * limitations under the License.                                           *
 ****************************************************************************/
#ifndef dart2cpp_double_h
#define dart2cpp_double_h

#include "number.h"

namespace d2c {

/**
 * An Object storage for a double value.
 *
 * This is part of the dart2cpp base library representation of the dart:core
 * primary classes, not intented to be a full reproduction of the dart:core
 * version, but enough for testing the translation of language features without
 * the baggage of the full dart:core library. The dart:core version of the class
 * will take precedence over this one if the dart:core library is imported by
 * the translator.
 */
class Double : public Number {
 public:
  typedef Number super;

  double data_;

  Double(double d);

  bool isDouble__() {
    return true;
  }

  virtual double doubleValue__() {
    return data_;
  }

  virtual Var abs();

  virtual Var toDouble();

  virtual Var toInteger();

  virtual Var toString();

  virtual Var isNegative();

  virtual Var isPositive();

  virtual Var isInfinite();

  virtual Var isFinite();

  virtual Var isNaN();

  virtual void initialize__();
};

REGISTER_BASE_DOUBLE(Double)

}  // namespace d2c
#endif  // dart2cpp_double_h

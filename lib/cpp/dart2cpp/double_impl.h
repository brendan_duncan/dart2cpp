/****************************************************************************
 * Copyright (C) 2014 by Brendan Duncan.                                    *
 *                                                                          *
 * This file is part of Dart2Cpp,                                           *
 * https://github.com/brendan-duncan/dart2cpp.                              *
 *                                                                          *
 * Licensed under the Apache License, Version 2.0 (the "License");          *
 * you may not use this file except in compliance with the License.         *
 * You may obtain a copy of the License at                                  *
 *                                                                          *
 * http://www.apache.org/licenses/LICENSE-2.0                               *
 *                                                                          *
 * Unless required by applicable law or agreed to in writing, software      *
 * distributed under the License is distributed on an "AS IS" BASIS,        *
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. *
 * See the License for the specific language governing permissions and      *
 * limitations under the License.                                           *
 ****************************************************************************/
#ifndef dart2cpp_double_impl_h
#define dart2cpp_double_impl_h

#include "double.h"
#include "var.h"
// <cmath> puts things in both the global and std namespace. Using math.h
// in the std namespace instead, to avoid polluting the global namespace.
namespace std {
#include <math.h>
}

namespace d2c {

Double::Double(double d)
    : super()
    , data_(d) {
  addInheritance__<Double>();
}

Var Double::abs() {
  return std::fabs(data_);
}

Var Double::toString() {
  return Var(new String(std::to_string(data_)));
}

Var Double::toDouble() {
  return Var(this);
}

Var Double::toInteger() {
  return Var(new Integer(static_cast<std::int64_t>(data_)));
}

Var Double::isNegative() {
  return data_ < 0.0;
}

Var Double::isPositive() {
  return data_ >= 0.0;
}

Var Double::isInfinite() {
  return std::isinf(data_);
}

Var Double::isFinite() {
  return std::isfinite(data_);
}

Var Double::isNaN() {
  return std::isnan(data_);
}

void Double::initialize__() {
  super::initialize__();
  methods_[TK_abs__][0] = d2c::bind(this, &Double::abs);
  methods_[TK_toDouble__][0] = d2c::bind(this, &Double::toDouble);
  methods_[TK_toInteger__][0] = d2c::bind(this, &Double::toInteger);
  methods_[TK_isNegative__][0] = d2c::bind(this, &Double::isNegative);
  methods_[TK_isPositive__][0] = d2c::bind(this, &Double::isPositive);
  methods_[TK_isInfinite__][0] = d2c::bind(this, &Double::isInfinite);
  methods_[TK_isFinite__][0] = d2c::bind(this, &Double::isFinite);
  methods_[TK_isNaN__][0] = d2c::bind(this, &Double::isNaN);
}

}  // namespace d2c
#endif  // dart2cpp_double_impl_h

/****************************************************************************
 * Copyright (C) 2014 by Brendan Duncan.                                    *
 *                                                                          *
 * This file is part of Dart2Cpp,                                           *
 * https://github.com/brendan-duncan/dart2cpp.                              *
 *                                                                          *
 * Licensed under the Apache License, Version 2.0 (the "License");          *
 * you may not use this file except in compliance with the License.         *
 * You may obtain a copy of the License at                                  *
 *                                                                          *
 * http://www.apache.org/licenses/LICENSE-2.0                               *
 *                                                                          *
 * Unless required by applicable law or agreed to in writing, software      *
 * distributed under the License is distributed on an "AS IS" BASIS,        *
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. *
 * See the License for the specific language governing permissions and      *
 * limitations under the License.                                           *
 ****************************************************************************/
#ifndef dartcpp_core_errors_h
#define dartcpp_core_errors_h

#include "object.h"

namespace d2c {

class Error : public d2c::Object {
 public:
  typedef d2c::Object super;

  Error();

  d2c::Var toString();

  d2c::Var stackTrace();

  virtual void initialize__();
};

class AssertionError : public Error {
 public:
  typedef Error super;
};

class TypeError : public AssertionError {
 public:
  typedef AssertionError super;
};

REGISTER_BASE_TYPE_ERROR(TypeError)

class CastError : public Error {
 public:
  typedef Error super;
};

class NullThrownError : public Error {
 public:
  typedef Error super;

  d2c::Var toString();
};

class ArgumentError : public Error {
 public:
  typedef Error super;

  d2c::Var message_;

  ArgumentError(const Var &message = d2c::Var());

  d2c::Var toString();
};

REGISTER_BASE_ARGUMENT_ERROR(ArgumentError)

class NoSuchMethodError : public Error {
 public:
  typedef Error super;
  d2c::Var receiver_;
  d2c::Var memberName_;
  d2c::Var positionalArguments_;
  d2c::Var namedArguments_;
  d2c::Var existingArgumentNames_;

  NoSuchMethodError(const d2c::Var &receiver, const d2c::Var &memberName,
                    const d2c::Var &positionalArguments,
                    const d2c::Var &namedArguments,
                    const d2c::Var &existingArgumentNames);

  d2c::Var toString();
};
REGISTER_BASE_NO_SUCH_METHOD_ERROR(NoSuchMethodError)

}  // namespace d2c
#endif  // dart2cpp_errors_h

/****************************************************************************
 * Copyright (C) 2014 by Brendan Duncan.                                    *
 *                                                                          *
 * This file is part of Dart2Cpp,                                           *
 * https://github.com/brendan-duncan/dart2cpp.                              *
 *                                                                          *
 * Licensed under the Apache License, Version 2.0 (the "License");          *
 * you may not use this file except in compliance with the License.         *
 * You may obtain a copy of the License at                                  *
 *                                                                          *
 * http://www.apache.org/licenses/LICENSE-2.0                               *
 *                                                                          *
 * Unless required by applicable law or agreed to in writing, software      *
 * distributed under the License is distributed on an "AS IS" BASIS,        *
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. *
 * See the License for the specific language governing permissions and      *
 * limitations under the License.                                           *
 ****************************************************************************/
#ifndef dart2cpp_errors_impl_h
#define dart2cpp_errors_impl_h

#include "errors.h"
#include "var.h"

namespace d2c {

Error::Error()
    : super() {
}

d2c::Var Error::toString() {
  return "Error!";
}

d2c::Var Error::stackTrace() {
  return null;
}

void Error::initialize__() {
  super::initialize__();
  methods_[TK_stackTrace__][0] = d2c::bind(this, &Error::stackTrace);
}

d2c::Var NullThrownError::toString() {
  return "Throw of null";
}

ArgumentError::ArgumentError(const Var &msg)
    : message_(msg) {
}

d2c::Var ArgumentError::toString() {
  if (!message_.isNull()) {
    return d2c::Var("Illegal argument(s): ") + message_;
  }
  return "Illegal argument(s)";
}

NoSuchMethodError::NoSuchMethodError(const d2c::Var &receiver,
                                     const d2c::Var &memberName,
                                     const d2c::Var &positionalArguments,
                                     const d2c::Var &namedArguments,
                                     const d2c::Var &existingArgumentNames)
    : receiver_(receiver)
    , memberName_(memberName)
    , positionalArguments_(positionalArguments)
    , namedArguments_(namedArguments)
    , existingArgumentNames_(existingArgumentNames) {
}

d2c::Var NoSuchMethodError::toString() {
  return d2c::STR__("NoSuchMethodError: ") + 
    receiver_.toString().stringValue()  + d2c::STR__(".") + 
    memberName_.toString().stringValue();
}

}  // namespace d2c
#endif  // dartcpp_errors_impl_h

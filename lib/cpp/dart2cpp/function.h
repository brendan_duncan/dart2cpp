/****************************************************************************
 * Copyright (C) 2014 by Brendan Duncan.                                    *
 *                                                                          *
 * This file is part of Dart2Cpp,                                           *
 * https://github.com/brendan-duncan/dart2cpp.                              *
 *                                                                          *
 * Licensed under the Apache License, Version 2.0 (the "License");          *
 * you may not use this file except in compliance with the License.         *
 * You may obtain a copy of the License at                                  *
 *                                                                          *
 * http://www.apache.org/licenses/LICENSE-2.0                               *
 *                                                                          *
 * Unless required by applicable law or agreed to in writing, software      *
 * distributed under the License is distributed on an "AS IS" BASIS,        *
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. *
 * See the License for the specific language governing permissions and      *
 * limitations under the License.                                           *
 ****************************************************************************/
#ifndef dart2cpp_function_h
#define dart2cpp_function_h

#include "object.h"
#include "delegate.h"

namespace d2c {

Var function(const Delegate &f);

Var function(const Var &object, const Delegate &f);

class Function : public Object {
 public:
  typedef Object super;

  Function(const Delegate &f);

  bool isFunction__() {
    return true;
  }

  virtual Var operator==(const Var &b);

  virtual Var call_0();

  virtual Var call_1(const Var &p1);

  virtual Var call_2(const Var &p1, const Var &p2);

  virtual Var call_3(const Var &p1, const Var &p2, const Var &p3);

  virtual Var call_4(const Var &p1, const Var &p2, const Var &p3,
                     const Var &p4);

  virtual Var call_5(const Var &p1, const Var &p2, const Var &p3, const Var &p4,
                     const Var &p5);

  virtual Var call_6(const Var &p1, const Var &p2, const Var &p3, const Var &p4,
                     const Var &p5, const Var &p6);

  virtual Var call_7(const Var &p1, const Var &p2, const Var &p3, const Var &p4,
                     const Var &p5, const Var &p6, const Var &p7);

  virtual Var call_8(const Var &p1, const Var &p2, const Var &p3, const Var &p4,
                     const Var &p5, const Var &p6, const Var &p7,
                     const Var &p8);

  virtual Var call_9(const Var &p1, const Var &p2, const Var &p3, const Var &p4,
                     const Var &p5, const Var &p6, const Var &p7, const Var &p8,
                     const Var &p9);

  virtual Var call_10(const Var &p1, const Var &p2, const Var &p3,
                      const Var &p4, const Var &p5, const Var &p6,
                      const Var &p7, const Var &p8, const Var &p9,
                      const Var &p10);

  Var __get_hashCode();

  Var toString();

  virtual void initialize__();

  Delegate function_;
};

}  // namespace d2c
#endif  // dart2cpp_function_h

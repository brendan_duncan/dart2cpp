/****************************************************************************
 * Copyright (C) 2014 by Brendan Duncan.                                    *
 *                                                                          *
 * This file is part of Dart2Cpp,                                           *
 * https://github.com/brendan-duncan/dart2cpp.                              *
 *                                                                          *
 * Licensed under the Apache License, Version 2.0 (the "License");          *
 * you may not use this file except in compliance with the License.         *
 * You may obtain a copy of the License at                                  *
 *                                                                          *
 * http://www.apache.org/licenses/LICENSE-2.0                               *
 *                                                                          *
 * Unless required by applicable law or agreed to in writing, software      *
 * distributed under the License is distributed on an "AS IS" BASIS,        *
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. *
 * See the License for the specific language governing permissions and      *
 * limitations under the License.                                           *
 ****************************************************************************/
#ifndef dart2cpp_function_impl_h
#define dart2cpp_function_impl_h

namespace d2c {

Var function(const Delegate &f) {
  return Var(new Function(f));
}

Var function(const Var &object, const Delegate &f) {
  return Var(new Function(Delegate(object, f)));
}

Function::Function(const Delegate &f)
    : super()
    , function_(f) {
  addInheritance__<Object>();
}

Var Function::operator==(const Var &b) {
  if (dynamic_cast<Function *>(b.object_) == nullptr) {
    return false;
  }
  return function_ == static_cast<Function *>(b.object_)->function_;
}

Var Function::call_0() {
  return function_();
}

Var Function::call_1(const Var &p1) {
  return function_(p1);
}

Var Function::call_2(const Var &p1, const Var &p2) {
  return function_(p1, p2);
}

Var Function::call_3(const Var &p1, const Var &p2, const Var &p3) {
  return function_(p1, p2, p3);
}

Var Function::call_4(const Var &p1, const Var &p2, const Var &p3,
                     const Var &p4) {
  return function_(p1, p2, p3, p4);
}

Var Function::call_5(const Var &p1, const Var &p2, const Var &p3, const Var &p4,
                     const Var &p5) {
  return function_(p1, p2, p3, p4, p5);
}

Var Function::call_6(const Var &p1, const Var &p2, const Var &p3, const Var &p4,
                     const Var &p5, const Var &p6) {
  return function_(p1, p2, p3, p4, p5, p6);
}

Var Function::call_7(const Var &p1, const Var &p2, const Var &p3, const Var &p4,
                     const Var &p5, const Var &p6, const Var &p7) {
  return function_(p1, p2, p3, p4, p5, p6, p7);
}

Var Function::call_8(const Var &p1, const Var &p2, const Var &p3, const Var &p4,
                     const Var &p5, const Var &p6, const Var &p7,
                     const Var &p8) {
  return function_(p1, p2, p3, p4, p5, p6, p7, p8);
}

Var Function::call_9(const Var &p1, const Var &p2, const Var &p3, const Var &p4,
                     const Var &p5, const Var &p6, const Var &p7, const Var &p8,
                     const Var &p9) {
  return function_(p1, p2, p3, p4, p5, p6, p7, p8, p9);
}

Var Function::call_10(const Var &p1, const Var &p2, const Var &p3,
                      const Var &p4, const Var &p5, const Var &p6,
                      const Var &p7, const Var &p8, const Var &p9,
                      const Var &p10) {
  return function_(p1, p2, p3, p4, p5, p6, p7, p8, p9, p10);
}

Var Function::__get_hashCode() {
  return static_cast<std::int64_t>(function_.hashCode());
}

Var Function::toString() {
  static Var name("Function");
  return name;
}

void Function::initialize__() {
  super::initialize__();
  methods_[TK_call__][0] = d2c::bind(this, &Function::call_0);
  methods_[TK_call__][1] = d2c::bind(this, &Function::call_1);
  methods_[TK_call__][2] = d2c::bind(this, &Function::call_2);
  methods_[TK_call__][3] = d2c::bind(this, &Function::call_3);
  methods_[TK_call__][4] = d2c::bind(this, &Function::call_4);
  methods_[TK_call__][5] = d2c::bind(this, &Function::call_5);
  methods_[TK_call__][6] = d2c::bind(this, &Function::call_6);
  methods_[TK_call__][7] = d2c::bind(this, &Function::call_7);
  methods_[TK_call__][8] = d2c::bind(this, &Function::call_8);
  methods_[TK_call__][9] = d2c::bind(this, &Function::call_9);
  methods_[TK_call__][10] = d2c::bind(this, &Function::call_10);
}

}  // d2c
#endif  // dart2cpp_function_impl_h

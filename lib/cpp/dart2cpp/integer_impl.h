/****************************************************************************
 * Copyright (C) 2014 by Brendan Duncan.                                    *
 *                                                                          *
 * This file is part of Dart2Cpp,                                           *
 * https://github.com/brendan-duncan/dart2cpp.                              *
 *                                                                          *
 * Licensed under the Apache License, Version 2.0 (the "License");          *
 * you may not use this file except in compliance with the License.         *
 * You may obtain a copy of the License at                                  *
 *                                                                          *
 * http://www.apache.org/licenses/LICENSE-2.0                               *
 *                                                                          *
 * Unless required by applicable law or agreed to in writing, software      *
 * distributed under the License is distributed on an "AS IS" BASIS,        *
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. *
 * See the License for the specific language governing permissions and      *
 * limitations under the License.                                           *
 ****************************************************************************/
#ifndef dart2cpp_integer_impl_h
#define dart2cpp_integer_impl_h

#include "integer.h"
#include "var.h"

namespace d2c {
Var Integer::parse(const Var &string, const Var &radix) {
  return Var(0LL);
}

Integer::Integer(std::int64_t i)
    : super()
    , data_(i) {
  addInheritance__<Integer>();
}

Var Integer::operator<<(const Var &other) {
  if (!d2c::is_type<Integer>(other)) {
    throw "Invalid argument type";
  }
  return data_ << static_cast<Integer *>(other.object_)->data_;
}

Var Integer::operator>>(const Var &other) {
  if (!d2c::is_type<Integer>(other)) {
    throw "Invalid argument type";
  }
  return data_ >> static_cast<Integer *>(other.object_)->data_;
}

Var Integer::operator&(const Var &other) {
  if (!d2c::is_type<Integer>(other)) {
    throw "Invalid argument type";
  }
  return data_ & static_cast<Integer *>(other.object_)->data_;
}

Var Integer::operator|(const Var &other) {
  if (!d2c::is_type<Integer>(other)) {
    throw "Invalid argument type";
  }
  return data_ | static_cast<Integer *>(other.object_)->data_;
}

Var Integer::operator^(const Var &other) {
  if (!d2c::is_type<Integer>(other)) {
    throw "Invalid argument type";
  }
  return data_ ^ static_cast<Integer *>(other.object_)->data_;
} Var Integer::
operator%(const Var &other) {
  d2c::Var c = other;
  if (!d2c::is_type<Integer>(other)) {
    throw "Invalid argument type";
  }

  std::int64_t n = data_;
  std::int64_t d = static_cast<Integer *>(c.object_)->data_;
  std::int64_t r = n % d;
  if (r == 0) {
    return 0LL;
  }
  if (r > 0) {
    return r;
  }

  if (d < 0) {
    return r - d;
  }

  return r + d;
}

Var Integer::operator~() {
  return ~data_;
}

Var Integer::abs() {
  return std::abs(data_);
}

Var Integer::toString() {
  return Var(new String(std::to_string(data_)));
}

Var Integer::toDouble() {
  return Var(new Double(static_cast<double>(data_)));
}

Var Integer::toInteger() {
  return Var(this);
}

Var Integer::isNegative() {
  return data_ < 0.0;
}

Var Integer::isPositive() {
  return data_ >= 0.0;
}

Var Integer::isInfinite() {
  return false;
}

Var Integer::isFinite() {
  return false;
}

Var Integer::isNaN() {
  return false;
}

d2c::Var Integer::__get_hashCode() {
  return static_cast<std::int64_t>(std::hash<std::int64_t>()(data_));
}

void Integer::initialize__() {
  super::initialize__();
  methods_[TK_abs__][0] = d2c::bind(this, &Integer::abs);
  methods_[TK_toDouble__][0] = d2c::bind(this, &Integer::toDouble);
  methods_[TK_toInteger__][0] = d2c::bind(this, &Integer::toInteger);
  methods_[TK_isNegative__][0] = d2c::bind(this, &Integer::isNegative);
  methods_[TK_isPositive__][0] = d2c::bind(this, &Integer::isPositive);
  methods_[TK_isInfinite__][0] = d2c::bind(this, &Integer::isInfinite);
  methods_[TK_isFinite__][0] = d2c::bind(this, &Integer::isFinite);
  methods_[TK_isNaN__][0] = d2c::bind(this, &Integer::isNaN);
}
}  // namespace d2c
#endif  // dart2cpp_integer_impl_h

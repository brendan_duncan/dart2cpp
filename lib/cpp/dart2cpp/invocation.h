/****************************************************************************
* Copyright (C) 2014 by Brendan Duncan.                                    *
*                                                                          *
* This file is part of Dart2Cpp,                                           *
* https://github.com/brendan-duncan/dart2cpp.                              *
*                                                                          *
* Licensed under the Apache License, Version 2.0 (the "License");          *
* you may not use this file except in compliance with the License.         *
* You may obtain a copy of the License at                                  *
*                                                                          *
* http://www.apache.org/licenses/LICENSE-2.0                               *
*                                                                          *
* Unless required by applicable law or agreed to in writing, software      *
* distributed under the License is distributed on an "AS IS" BASIS,        *
* WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. *
* See the License for the specific language governing permissions and      *
* limitations under the License.                                           *
****************************************************************************/
#ifndef dart2cpp_invocation_h
#define dart2cpp_invocation_h

#include "object.h"

namespace d2c {

const int INVOCATION_METHOD = 1;
const int INVOCATION_GETTER = 2;
const int INVOCATION_SETTER = 3;

Var invocation(Var memberName, Var positionalArguments, Var namedArguments,
               int type);

class Invocation : public Object {
public:
  typedef Object super;

  Invocation(Var memberName, Var positionalArguments, Var namedArguments,
             int type);

  d2c::Var __get_memberName();

  d2c::Var __get_positionalArguments();

  d2c::Var __get_namedArguments();

  d2c::Var __get_isMethod();

  d2c::Var __get_isGetter();

  d2c::Var __get_isSetter();

  d2c::Var __get_isAccessor();

  void initialize__();

protected:
  d2c::Var memberName_;
  d2c::Var positionalArguments_;
  d2c::Var namedArguments_;
  d2c::Var isMethod_;
  d2c::Var isGetter_;
  d2c::Var isSetter_;
};

REGISTER_BASE_INVOCATION(Invocation)

} // namespace d2c
#endif // dart2cpp_invocation_h

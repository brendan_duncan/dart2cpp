/****************************************************************************
* Copyright (C) 2014 by Brendan Duncan.                                    *
*                                                                          *
* This file is part of Dart2Cpp,                                           *
* https://github.com/brendan-duncan/dart2cpp.                              *
*                                                                          *
* Licensed under the Apache License, Version 2.0 (the "License");          *
* you may not use this file except in compliance with the License.         *
* You may obtain a copy of the License at                                  *
*                                                                          *
* http://www.apache.org/licenses/LICENSE-2.0                               *
*                                                                          *
* Unless required by applicable law or agreed to in writing, software      *
* distributed under the License is distributed on an "AS IS" BASIS,        *
* WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. *
* See the License for the specific language governing permissions and      *
* limitations under the License.                                           *
****************************************************************************/
#ifndef dart2cpp_invocation_impl_h
#define dart2cpp_invocation_impl_h

#include "invocation.h"

namespace d2c {

Var invocation(Var memberName, Var positionalArguments, Var namedArguments,
               int type) {
  return Var(TypeFactory::CreateInvocation(memberName, positionalArguments,
                                           namedArguments, type));
}

Invocation::Invocation(Var memberName, Var positionalArguments,
                       Var namedArguments, int type)
    : memberName_(memberName)
    , positionalArguments_(positionalArguments)
    , namedArguments_(namedArguments)
    , isMethod_(type == INVOCATION_METHOD)
    , isGetter_(type == INVOCATION_GETTER)
    , isSetter_(type == INVOCATION_SETTER) {
}

Var Invocation::__get_memberName() {
  return memberName_;
}

Var Invocation::__get_positionalArguments() {
  return positionalArguments_;
}

Var Invocation::__get_namedArguments() {
  return namedArguments_;
}

Var Invocation::__get_isMethod() {
  return isMethod_;
}

Var Invocation::__get_isGetter() {
  return isGetter_;
}

Var Invocation::__get_isSetter() {
  return isSetter_;
}

Var Invocation::__get_isAccessor() {
  return __get_isGetter() || __get_isSetter();
}

void Invocation::initialize__() {
  super::initialize__();
  initialized_ = true;
  methods_[TK___get_memberName__][0] =
      d2c::bind(this, &Invocation::__get_memberName);
  methods_[TK___get_positionalArguments__][0] =
      d2c::bind(this, &Invocation::__get_positionalArguments);
  methods_[TK___get_namedArguments__][0] =
      d2c::bind(this, &Invocation::__get_namedArguments);
  methods_[TK___get_isMethod__][0] =
      d2c::bind(this, &Invocation::__get_isMethod);
  methods_[TK___get_isGetter__][0] =
      d2c::bind(this, &Invocation::__get_isGetter);
  methods_[TK___get_isSetter__][0] =
      d2c::bind(this, &Invocation::__get_isSetter);
  methods_[TK___get_isAccessor__][0] =
      d2c::bind(this, &Invocation::__get_isAccessor);
}

}  // namespace d2c

#endif  // dart2cpp_invocation_impl_h

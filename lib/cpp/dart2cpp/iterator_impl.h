/****************************************************************************
 * Copyright (C) 2014 by Brendan Duncan.                                    *
 *                                                                          *
 * This file is part of Dart2Cpp,                                           *
 * https://github.com/brendan-duncan/dart2cpp.                              *
 *                                                                          *
 * Licensed under the Apache License, Version 2.0 (the "License");          *
 * you may not use this file except in compliance with the License.         *
 * You may obtain a copy of the License at                                  *
 *                                                                          *
 * http://www.apache.org/licenses/LICENSE-2.0                               *
 *                                                                          *
 * Unless required by applicable law or agreed to in writing, software      *
 * distributed under the License is distributed on an "AS IS" BASIS,        *
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. *
 * See the License for the specific language governing permissions and      *
 * limitations under the License.                                           *
 ****************************************************************************/
#ifndef dart2cpp_iterator_impl_h
#define dart2cpp_iterator_impl_h

#include "iterator"

namespace d2c {

d2c::Var Iterator::moveNext() {
  return null;
}

d2c::Var Iterator::get_current() {
  return null;
}

void Iterator::initialize__() {
  super::initialize__();
  methods_[TK_moveNext__][0] = d2c::bind(this, &Iterator::moveNext);
  methods_[TK___get_current__][0] = d2c::bind(this, &Iterator::get_current);
}

}  // namespace d2c
#endif  // dart2cpp_iterator_impl_h

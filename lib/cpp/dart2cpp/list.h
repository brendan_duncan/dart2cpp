/****************************************************************************
 * Copyright (C) 2014 by Brendan Duncan.                                    *
 *                                                                          *
 * This file is part of Dart2Cpp,                                           *
 * https://github.com/brendan-duncan/dart2cpp.                              *
 *                                                                          *
 * Licensed under the Apache License, Version 2.0 (the "License");          *
 * you may not use this file except in compliance with the License.         *
 * You may obtain a copy of the License at                                  *
 *                                                                          *
 * http://www.apache.org/licenses/LICENSE-2.0                               *
 *                                                                          *
 * Unless required by applicable law or agreed to in writing, software      *
 * distributed under the License is distributed on an "AS IS" BASIS,        *
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. *
 * See the License for the specific language governing permissions and      *
 * limitations under the License.                                           *
 ****************************************************************************/
#ifndef dart2cpp_list_h
#define dart2cpp_list_h

#include "iterable.h"
#include "symbols.h"

namespace d2c {
class Var;

Var list(const d2c::TypeSet &__types__, const std::initializer_list<Var> &list);

Var const_list(const d2c::TypeSet &__types__,
               const std::initializer_list<Var> &list);

/**
 * An Object storage for an array list.
 *
 * This is part of the dart2cpp base library representation of the dart:core
 * primary classes, not intented to be a full reproduction of the dart:core
 * version, but enough for testing the translation of language features without
 * the baggage of the full dart:core library. The dart:core version of the class
 * will take precedence over this one if the dart:core library is imported by
 * the translator.
 */
class List : public Object {
 public:
  typedef Object super;

  std::vector<Var> data_;

  __D2C_CONSTRUCTOR_TYPEDEF__;
  List(__D2C_CONSTRUCTOR_ARG__ = {});

  List(const d2c::TypeSet &__types__, const std::initializer_list<Var> &list);

  List(const std::vector<Var> &list);

  static Var __factory_List_from(const d2c::TypeSet &__types__,
                                 const Var &list);

  virtual bool isList__() {
    return true;
  }

  virtual const std::vector<Var> &listValue__() {
    return data_;
  }

  Var add(const Var &e);

  Var length();

  Var set_length(const Var &l);

  Var toString();

  Var operator[](const Var &index);

  Var iterator();

  Var __array_set(const Var &index, const Var &value);

  virtual void initialize__();

  d2c::Var __get_add() {
    return d2c::function(d2c::Var(this), methods_[TK_add__][1]);
  }

  class Iterator : public d2c::Iterator {
   public:
    typedef d2c::Iterator super;

    Var list_;
    int index_;

    Iterator(List *list);

    virtual d2c::Var moveNext();

    virtual d2c::Var get_current();
  };
};

REGISTER_BASE_LIST(List)
}  // namespace d2c
#endif  // dart2cpp_list_h

/****************************************************************************
 * Copyright (C) 2014 by Brendan Duncan.                                    *
 *                                                                          *
 * This file is part of Dart2Cpp,                                           *
 * https://github.com/brendan-duncan/dart2cpp.                              *
 *                                                                          *
 * Licensed under the Apache License, Version 2.0 (the "License");          *
 * you may not use this file except in compliance with the License.         *
 * You may obtain a copy of the License at                                  *
 *                                                                          *
 * http://www.apache.org/licenses/LICENSE-2.0                               *
 *                                                                          *
 * Unless required by applicable law or agreed to in writing, software      *
 * distributed under the License is distributed on an "AS IS" BASIS,        *
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. *
 * See the License for the specific language governing permissions and      *
 * limitations under the License.                                           *
 ****************************************************************************/
#ifndef dart2cpp_list_impl_h
#define dart2cpp_list_impl_h

#include "delegate.h"
#include "integer.h"
#include "iterable.h"
#include "list.h"
#include "string.h"
#include "var.h"

namespace d2c {
// Creates a list literal
Var list(const d2c::TypeSet &__types__,
         const std::initializer_list<Var> &list) {
  return Var(TypeFactory::CreateList(__types__, list));
}

// Creates a const list literal.
Var const_list(const d2c::TypeSet &__types__,
               const std::initializer_list<Var> &list) {
  static std::unordered_map<size_t, Var> cache_;
  std::int64_t hash_code = 0;
  std::int64_t index = 5;
  for (const auto &v : list) {
    hash_code = static_cast<size_t>(hash_code +
                                    (v.invoke(TK___get_hashCode__).intValue() *
                                    index));
    index *= 3 - 1;
  }
  size_t id = static_cast<size_t>(hash_code);

  if (cache_.find(id) != cache_.end()) {
    return cache_[id];
  }

  Var l(TypeFactory::CreateList(__types__, list));
  cache_[id] = l;

  return l;
}

List::List(__D2C_CONSTRUCTOR_ARG__) : super() {
  addInheritance__<List>(__types__.ids);
  addInheritance__<Iterable>();
}

List::List(const d2c::TypeSet &__types__,
           const std::initializer_list<Var> &list)
    : super(), data_(list) {
  addInheritance__<List>(__types__);
  addInheritance__<Iterable>();
}

List::List(const std::vector<Var> &list) : super(), data_(list) {
  addInheritance__<List>();
  addInheritance__<Iterable>();
}

Var List::__factory_List_from(const d2c::TypeSet &__types__, const Var &list) {
  if (!is_type<List>(list)) {
    return null;
  }
  List *l = new List(__constructor__(__types__));
  l->data_ = cast<List>(list).data_;
  return Var(l);
}

Var List::operator[](const Var &index) {
  return data_[static_cast<int>(index.intValue())];
}

Var List::iterator() { return Var(new List::Iterator(this)); }

Var List::__array_set(const Var &index, const Var &value) {
  data_[static_cast<int>(index.intValue())] = value;
  return null;
}

Var List::add(const Var &e) {
  data_.push_back(e);
  return null;
}

Var List::length() { return Var(new Integer(static_cast<int>(data_.size()))); }

Var List::set_length(const Var &l) {
  data_.resize(static_cast<int>(l.intValue()));
  return l;
}

Var List::toString() {
  Str s = "[";
  for (size_t i = 0; i < data_.size(); ++i) {
    if (i != 0) {
      s += ", ";
    }
    Var st = data_[i].toString();
    s += cast<String>(st).data_;
  }
  s += "]";
  return Var(new String(s));
}

void List::initialize__() {
  super::initialize__();
  methods_[TK_add__][1] = bind(this, &List::add);
  methods_[TK___get_add__][0] = bind(this, &List::__get_add);
  methods_[TK___get_length__][0] = bind(this, &List::length);
  methods_[TK___set_length__][1] = bind(this, &List::set_length);
  methods_[TK___get_iterator__][0] = bind(this, &List::iterator);
}

List::Iterator::Iterator(List *l) : super(), list_(l), index_(-1) {}

d2c::Var List::Iterator::moveNext() {
  index_++;
  return index_ < (int)static_cast<List *>(list_.object_)->data_.size();
}

d2c::Var List::Iterator::get_current() {
  return static_cast<List *>(list_.object_)->data_[index_];
}

}  // namespace d2c
#endif  // dart2cpp_list_impl_h

/****************************************************************************
 * Copyright (C) 2014 by Brendan Duncan.                                    *
 *                                                                          *
 * This file is part of Dart2Cpp,                                           *
 * https://github.com/brendan-duncan/dart2cpp.                              *
 *                                                                          *
 * Licensed under the Apache License, Version 2.0 (the "License");          *
 * you may not use this file except in compliance with the License.         *
 * You may obtain a copy of the License at                                  *
 *                                                                          *
 * http://www.apache.org/licenses/LICENSE-2.0                               *
 *                                                                          *
 * Unless required by applicable law or agreed to in writing, software      *
 * distributed under the License is distributed on an "AS IS" BASIS,        *
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. *
 * See the License for the specific language governing permissions and      *
 * limitations under the License.                                           *
 ****************************************************************************/
#ifndef dart2cpp_map_h
#define dart2cpp_map_h

#include "object.h"

namespace d2c {
class Var;

Var map(const d2c::TypeSet &__types__, const std::initializer_list<Var> &keys,
        const std::initializer_list<Var> &value);

Var const_map(const d2c::TypeSet &__types__,
              const std::initializer_list<Var> &keys,
              const std::initializer_list<Var> &values);

/**
 * An Object storage for a hash map.
 *
 * This is part of the dart2cpp base library representation of the dart:core
 * primary classes, not intented to be a full reproduction of the dart:core
 * version, but enough for testing the translation of language features without
 * the baggage of the full dart:core library. The dart:core version of the class
 * will take precedence over this one if the dart:core library is imported by
 * the translator.
 */
class Map : public d2c::Object {
 public:
  typedef d2c::Object super;
  std::unordered_map<d2c::Var, d2c::Var> data_;

  __D2C_CONSTRUCTOR_TYPEDEF__;
  Map(__D2C_CONSTRUCTOR_ARG__ = {});

  Map(const d2c::TypeSet &__types__,
      const std::initializer_list<d2c::Var> &keyValues);

  d2c::Var containsKey(const Var &key);

  d2c::Var operator[](const Var &index);

  // operator []=
  virtual d2c::Var __array_set(const d2c::Var &index, const d2c::Var &value);

  d2c::Var toString();

  virtual void initialize__();
};

REGISTER_BASE_MAP(Map)

}  // namespace d2c
#endif  // dart2cpp_map_h

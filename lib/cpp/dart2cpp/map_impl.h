/****************************************************************************
 * Copyright (C) 2014 by Brendan Duncan.                                    *
 *                                                                          *
 * This file is part of Dart2Cpp,                                           *
 * https://github.com/brendan-duncan/dart2cpp.                              *
 *                                                                          *
 * Licensed under the Apache License, Version 2.0 (the "License");          *
 * you may not use this file except in compliance with the License.         *
 * You may obtain a copy of the License at                                  *
 *                                                                          *
 * http://www.apache.org/licenses/LICENSE-2.0                               *
 *                                                                          *
 * Unless required by applicable law or agreed to in writing, software      *
 * distributed under the License is distributed on an "AS IS" BASIS,        *
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. *
 * See the License for the specific language governing permissions and      *
 * limitations under the License.                                           *
 ****************************************************************************/
#ifndef dart2cpp_map_impl_h
#define dart2cpp_map_impl_h

#include "map.h"

namespace d2c {
d2c::Var map(const d2c::TypeSet &__types__,
             const std::initializer_list<Var> &keyValues) {
  return Var(TypeFactory::CreateMap(__types__, keyValues));
}

d2c::Var const_map(const d2c::TypeSet &__types__,
                   const std::initializer_list<Var> &keyValues) {
  static std::unordered_map<size_t, Var> cache_;

  std::int64_t hash_code = 0;
  for (auto kv = keyValues.begin(); kv != keyValues.end(); ++kv) {
    const Var &k = *kv;
    ++kv;
    const Var &v = *kv;
    std::int64_t kh = k.invoke(TK___get_hashCode__).intValue();
    std::int64_t vh = v.invoke(TK___get_hashCode__).intValue();
    hash_code = hash_code + (kh * vh);
  }
  size_t id = static_cast<size_t>(hash_code);

  if (cache_.find(id) != cache_.end()) {
    return cache_[id];
  }

  Var l(TypeFactory::CreateMap(__types__, keyValues));
  cache_[id] = l;

  return l;
}

Map::Map(__D2C_CONSTRUCTOR_ARG__)
    : super() {
  addInheritance__<Map>();
}

Map::Map(const d2c::TypeSet &__types__,
         const std::initializer_list<d2c::Var> &keyValues)
    : super() {
  addInheritance__<Map>(__types__);
  std::initializer_list<d2c::Var>::iterator it;
  for (it = keyValues.begin(); it != keyValues.end(); ++it) {
    Var k = *it;
    ++it;
    data_[k] = *it;
  }
}

d2c::Var Map::containsKey(const Var &key) {
  return data_.find(key) != data_.end();
}

d2c::Var Map::operator[](const Var &key) {
  return data_[key];
}

// operator []=
d2c::Var Map::__array_set(const d2c::Var &index, const d2c::Var &value) {
  data_[index] = value;
  return null;
}

d2c::Var Map::toString() {
  Str s = "{";
  bool first = true;

  for (std::unordered_map<d2c::Var, d2c::Var>::iterator it = data_.begin();
       it != data_.end(); ++it) {
    if (!first) {
      s += ", ";
    }

    d2c::Var st = it->first.toString();
    s += d2c::cast<String>(st).data_;
    s += ": ";
    st = it->second.toString();
    s += d2c::cast<String>(st).data_;

    first = false;
  }

  s += "}";
  return d2c::Var(new String(s));
}

void Map::initialize__() {
  super::initialize__();
  methods_[TK_containsKey__][1] = d2c::bind(this, &Map::containsKey);
}

}  // namespace d2c
#endif  // dart2cpp_map_impl_h

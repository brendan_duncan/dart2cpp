/****************************************************************************
 * Copyright (C) 2014 by Brendan Duncan.                                    *
 *                                                                          *
 * This file is part of Dart2Cpp,                                           *
 * https://github.com/brendan-duncan/dart2cpp.                              *
 *                                                                          *
 * Licensed under the Apache License, Version 2.0 (the "License");          *
 * you may not use this file except in compliance with the License.         *
 * You may obtain a copy of the License at                                  *
 *                                                                          *
 * http://www.apache.org/licenses/LICENSE-2.0                               *
 *                                                                          *
 * Unless required by applicable law or agreed to in writing, software      *
 * distributed under the License is distributed on an "AS IS" BASIS,        *
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. *
 * See the License for the specific language governing permissions and      *
 * limitations under the License.                                           *
 ****************************************************************************/
#ifndef dart2cpp_named_arguments_h
#define dart2cpp_named_arguments_h

namespace d2c {
class Token;
class Var;

// typedef std::initializer_list<std::pair<int, Var> > NamedArgumentsList;
typedef std::initializer_list<Token> KeyInitializer;
typedef std::initializer_list<Var> ValueInitializer;

// Note that I tried initializer_list<pair<int, Var>>, but this caused double
// deletions of certain objects.
Var namedArgs(const KeyInitializer &keys, const ValueInitializer &values);

class NamedArguments : public Object {
 public:
  typedef Object super;

  std::unordered_map<Token, Var> arguments_;

  NamedArguments();

  NamedArguments(const KeyInitializer &keys, const ValueInitializer &values);

  bool contains(const Token &token) const;

  Var get(const Token &token) const;

  virtual Var __get_hashCode();
};

}  // namespace d2c
#endif  // dart2cpp_named_arguments_h

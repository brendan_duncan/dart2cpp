/****************************************************************************
 * Copyright (C) 2014 by Brendan Duncan.                                    *
 *                                                                          *
 * This file is part of Dart2Cpp,                                           *
 * https://github.com/brendan-duncan/dart2cpp.                              *
 *                                                                          *
 * Licensed under the Apache License, Version 2.0 (the "License");          *
 * you may not use this file except in compliance with the License.         *
 * You may obtain a copy of the License at                                  *
 *                                                                          *
 * http://www.apache.org/licenses/LICENSE-2.0                               *
 *                                                                          *
 * Unless required by applicable law or agreed to in writing, software      *
 * distributed under the License is distributed on an "AS IS" BASIS,        *
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. *
 * See the License for the specific language governing permissions and      *
 * limitations under the License.                                           *
 ****************************************************************************/
#ifndef dart2cpp_named_arguments_impl_h
#define dart2cpp_named_arguments_impl_h

#include "named_arguments.h"
#include "var.h"

namespace d2c {
Var namedArgs(const KeyInitializer &keys, const ValueInitializer &values) {
  return Var(new NamedArguments(keys, values));
}

NamedArguments::NamedArguments() {
}

NamedArguments::NamedArguments(const KeyInitializer &keys,
                               const ValueInitializer &values) {
  size_t len = keys.size();
  for (size_t i = 0; i < len; ++i) {
    arguments_[*(keys.begin() + i)] = *(values.begin() + i);
  }
}

bool NamedArguments::contains(const Token &token) const {
  return arguments_.find(token) != arguments_.end();
}

Var NamedArguments::get(const Token &token) const {
  NamedArguments *self = const_cast<NamedArguments *>(this);
  return self->arguments_[token];
}

Var NamedArguments::__get_hashCode() {
  std::int64_t code = 0;
  for (auto i : arguments_) {
    Token tk = i.first;
    code += static_cast<std::int64_t>(tk.hashCode());
    code += i.second.invoke(TK___get_hashCode__).intValue();

  }
  return code;
}

}  // namespace d2c
#endif  // dart2cpp_named_arguments_impl_h

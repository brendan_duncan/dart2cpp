/****************************************************************************
 * Copyright (C) 2014 by Brendan Duncan.                                    *
 *                                                                          *
 * This file is part of Dart2Cpp,                                           *
 * https://github.com/brendan-duncan/dart2cpp.                              *
 *                                                                          *
 * Licensed under the Apache License, Version 2.0 (the "License");          *
 * you may not use this file except in compliance with the License.         *
 * You may obtain a copy of the License at                                  *
 *                                                                          *
 * http://www.apache.org/licenses/LICENSE-2.0                               *
 *                                                                          *
 * Unless required by applicable law or agreed to in writing, software      *
 * distributed under the License is distributed on an "AS IS" BASIS,        *
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. *
 * See the License for the specific language governing permissions and      *
 * limitations under the License.                                           *
 ****************************************************************************/
#ifndef dart2cpp_null_impl_h
#define dart2cpp_null_impl_h

#include "null.h"
#include "var.h"

d2c::Var null(new d2c::Null());

namespace d2c {

d2c::Var null = ::null;

Null::Null()
    : super() {
  addInheritance__<Null>();
}

Var Null::operator==(const Var &other) {
  return other.isNull();
}

Var Null::toString() {
  return "null";
}

}  // d2c
#endif  // dart2cpp_null_impl_h

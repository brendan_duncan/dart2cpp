/****************************************************************************
 * Copyright (C) 2014 by Brendan Duncan.                                    *
 *                                                                          *
 * This file is part of Dart2Cpp,                                           *
 * https://github.com/brendan-duncan/dart2cpp.                              *
 *                                                                          *
 * Licensed under the Apache License, Version 2.0 (the "License");          *
 * you may not use this file except in compliance with the License.         *
 * You may obtain a copy of the License at                                  *
 *                                                                          *
 * http://www.apache.org/licenses/LICENSE-2.0                               *
 *                                                                          *
 * Unless required by applicable law or agreed to in writing, software      *
 * distributed under the License is distributed on an "AS IS" BASIS,        *
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. *
 * See the License for the specific language governing permissions and      *
 * limitations under the License.                                           *
 ****************************************************************************/
#ifndef dart2cpp_number_h
#define dart2cpp_number_h

#include "object.h"

namespace d2c {

/**
 * Base class for integers and doubles.
 *
 * This is part of the dart2cpp base library representation of the dart:core
 * primary classes, not intented to be a full reproduction of the dart:core
 * version, but enough for testing the translation of language features without
 * the baggage of the full dart:core library. The dart:core version of the class
 * will take precedence over this one if the dart:core library is imported by
 * the translator.
 */
class Number : public Object {
 public:
  typedef Object super;

  Number();

  virtual Var operator+(const Var &other);

  virtual Var operator-(const Var &other);

  virtual Var operator*(const Var &other);

  virtual Var operator/(const Var &other);

  virtual Var __intDiv(const Var &other);

  virtual Var operator>(const Var &other);

  virtual Var operator>=(const Var &other);

  virtual Var operator<(const Var &other);

  virtual Var operator<=(const Var &other);

  virtual Var operator==(const Var &other);

  virtual Var operator-();

  virtual Var abs() = 0;

  virtual Var isNegative() = 0;

  virtual Var isPositive() = 0;

  virtual Var isInfinite() = 0;

  virtual Var isFinite() = 0;

  virtual Var isNaN() = 0;

  virtual Var toDouble() = 0;

  virtual Var toInteger() = 0;
};

}  // d2c
#endif  // dart2cpp_number_h

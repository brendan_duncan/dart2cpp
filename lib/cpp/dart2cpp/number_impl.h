/****************************************************************************
 * Copyright (C) 2014 by Brendan Duncan.                                    *
 *                                                                          *
 * This file is part of Dart2Cpp,                                           *
 * https://github.com/brendan-duncan/dart2cpp.                              *
 *                                                                          *
 * Licensed under the Apache License, Version 2.0 (the "License");          *
 * you may not use this file except in compliance with the License.         *
 * You may obtain a copy of the License at                                  *
 *                                                                          *
 * http://www.apache.org/licenses/LICENSE-2.0                               *
 *                                                                          *
 * Unless required by applicable law or agreed to in writing, software      *
 * distributed under the License is distributed on an "AS IS" BASIS,        *
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. *
 * See the License for the specific language governing permissions and      *
 * limitations under the License.                                           *
 ****************************************************************************/
#ifndef dart2cpp_number_impl_h
#define dart2cpp_number_impl_h

namespace d2c {

#define __number_isInt(a) (dynamic_cast<Integer *>(a) != NULL)
#define __number_isDouble(a) (dynamic_cast<Double *>(a) != NULL)

#define __number_value(a)                 \
  (__number_isInt(a)                      \
       ? static_cast<Integer *>(a)->data_ \
       : __number_isDouble(a) ? static_cast<Double *>(a)->data_ : 0)

#define __number_op(a, b, op)                                                  \
  if (__number_isInt(a) && __number_isInt(b))                                  \
    return static_cast<Integer *>(a)                                           \
        ->data_ op static_cast<Integer *>(b)                                   \
        ->data_;                                                               \
  else if (__number_isInt(a) && __number_isDouble(b))                          \
    return static_cast<Integer *>(a)                                           \
        ->data_ op static_cast<Double *>(b)                                    \
        ->data_;                                                               \
  else if (__number_isDouble(a) && __number_isDouble(b))                       \
    return static_cast<Double *>(a)->data_ op static_cast<Double *>(b)->data_; \
  else                                                                         \
  return static_cast<Double *>(a)->data_ op static_cast<Integer *>(b)->data_

Number::Number()
    : super() {
  addInheritance__<Number>();
}

Var Number::operator+(const Var &other) {
  if (!is_type<Number>(other)) {
    return null;
  }
  __number_op(this, other.object_, +);
}

Var Number::operator-(const Var &other) {
  if (!is_type<Number>(other)) {
    return null;
  }
  __number_op(this, other.object_, -);
}

Var Number::operator*(const Var &other) {
  if (!is_type<Number>(other)) {
    return null;
  }
  __number_op(this, other.object_, *);
}

Var Number::operator/(const Var &other) {
  if (!is_type<Number>(other)) {
    return null;
  }

  double n = isDouble__() ? doubleValue__() : static_cast<double>(intValue__());
  double d = other.isDouble() ? other.doubleValue()
                              : static_cast<double>(other.intValue());

  return n / d;
}

Var Number::__intDiv(const Var &other) {
  if (!is_type<Number>(other)) {
    return null;
  }
  return static_cast<std::int64_t>(__number_value(this) /
                                   __number_value(other.object_));
}

Var Number::operator-() {
  return -__number_value(this);
}

Var Number::operator>(const Var &other) {
  if (!is_type<Number>(other)) {
    return null;
  }
  return __number_value(this) > __number_value(other.object_);
}

Var Number::operator>=(const Var &other) {
  if (!is_type<Number>(other)) {
    return null;
  }
  return __number_value(this) >= __number_value(other.object_);
}

Var Number::operator<(const Var &other) {
  if (!is_type<Number>(other)) {
    return null;
  }
  return __number_value(this) < __number_value(other.object_);
}

Var Number::operator<=(const Var &other) {
  if (!is_type<Number>(other)) {
    return null;
  }
  return __number_value(this) <= __number_value(other.object_);
}

Var Number::operator==(const Var &other) {
  if (!is_type<Number>(other)) {
    return null;
  }
  return __number_value(this) == __number_value(other.object_);
}

}  // d2c
#endif  // dart2cpp_number_impl_h

/****************************************************************************
 * Copyright (C) 2014 by Brendan Duncan.                                    *
 *                                                                          *
 * This file is part of Dart2Cpp,                                           *
 * https://github.com/brendan-duncan/dart2cpp.                              *
 *                                                                          *
 * Licensed under the Apache License, Version 2.0 (the "License");          *
 * you may not use this file except in compliance with the License.         *
 * You may obtain a copy of the License at                                  *
 *                                                                          *
 * http://www.apache.org/licenses/LICENSE-2.0                               *
 *                                                                          *
 * Unless required by applicable law or agreed to in writing, software      *
 * distributed under the License is distributed on an "AS IS" BASIS,        *
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. *
 * See the License for the specific language governing permissions and      *
 * limitations under the License.                                           *
 ****************************************************************************/
#ifndef dart2cpp_object_h
#define dart2cpp_object_h

namespace d2c {
class Delegate;
class Token;
class Var;

#define __D2C_NAMED_CONSTRUCTOR_TYPEDEF__(NAME)    \
  struct __constructor_##NAME##__ {                \
    d2c::TypeSet ids;                              \
    __constructor_##NAME##__(d2c::TypeSet &t = {}) \
        : ids(t) {                                 \
    }                                              \
  }

#define __D2C_NAMED_CONSTRUCTOR_ARG__(NAME) \
  const __constructor_##NAME##__ &__types__

#define __D2C_CONSTRUCTOR_TYPEDEF__                         \
  struct __constructor__ {                                  \
    d2c::TypeSet ids;                                       \
    __constructor__(const d2c::TypeSet &t = d2c::TypeSet()) \
        : ids(t) {                                          \
    }                                                       \
  }

#define __D2C_CONSTRUCTOR_ARG__ const __constructor__ &__types__

class Object {
 public:
  __D2C_CONSTRUCTOR_TYPEDEF__;
  Object(__D2C_CONSTRUCTOR_ARG__ = {});

  static void updateClassInheritance__(d2c::Object *object,
                                       const d2c::TypeSet &__types__) {
    object->addInheritance__<Object>(__types__);
  }

  virtual ~Object();

  virtual void initialize__();

  virtual bool isFunction__();

  virtual bool isBool__();

  virtual bool boolValue__();

  virtual bool isInt__();

  virtual std::int64_t intValue__();

  virtual bool isDouble__();

  virtual double doubleValue__();

  virtual bool isString__();

  virtual const Str &stringValue__();

  virtual bool isList__();

  virtual const std::vector<Var> &listValue__();

  virtual bool isMap__();

  virtual const std::unordered_map<Var, Var> &mapValue__();

  virtual Var toString();

  bool hasMethod__(const Token &name);

  Var getMember__(const Token &name);

  Var invoke__(const Token &name);

  Var invoke__(const Token &name, const Var &p1);

  Var invoke__(const Token &name, const Var &p1, const Var &p2);

  Var invoke__(const Token &name, const Var &p1, const Var &p2, const Var &p3);

  Var invoke__(const Token &name, const Var &p1, const Var &p2, const Var &p3,
               const Var &p4);

  Var invoke__(const Token &name, const Var &p1, const Var &p2, const Var &p3,
               const Var &p4, const Var &p5);

  Var invoke__(const Token &name, const Var &p1, const Var &p2, const Var &p3,
               const Var &p4, const Var &p5, const Var &p6);

  Var invoke__(const Token &name, const Var &p1, const Var &p2, const Var &p3,
               const Var &p4, const Var &p5, const Var &p6, const Var &p7);

  Var invoke__(const Token &name, const Var &p1, const Var &p2, const Var &p3,
               const Var &p4, const Var &p5, const Var &p6, const Var &p7,
               const Var &p8);

  Var invoke__(const Token &name, const Var &p1, const Var &p2, const Var &p3,
               const Var &p4, const Var &p5, const Var &p6, const Var &p7,
               const Var &p8, const Var &p9);

  Var invoke__(const Token &name, const Var &p1, const Var &p2, const Var &p3,
               const Var &p4, const Var &p5, const Var &p6, const Var &p7,
               const Var &p8, const Var &p9, const Var &p10);

  virtual Var operator[](const Var &index);

  // operator []=
  virtual Var __array_set(const Var &index, const Var &value);

  virtual Var operator+(const Var &other);

  virtual Var operator-(const Var &other);

  virtual Var operator*(const Var &other);

  virtual Var operator/(const Var &other);

  // operator ~/
  virtual Var __intDiv(const Var &other);

  virtual Var operator<<(const Var &other);

  virtual Var operator>>(const Var &other);

  virtual Var operator>(const Var &other);

  virtual Var operator>=(const Var &other);

  virtual Var operator<(const Var &other);

  virtual Var operator<=(const Var &other);

  virtual Var operator==(const Var &other);

  virtual Var operator!();

  virtual Var operator~();

  virtual Var operator-();

  virtual Var operator&(const Var &other);

  virtual Var operator|(const Var &other);

  virtual Var operator^(const Var &other);

  virtual Var operator%(const Var &other);

  virtual Var operator&&(const Var &other);

  virtual Var operator||(const Var &other);

  virtual Var noSuchMethod(const Var &invocation);

  // int get hashCode;
  virtual Var __get_hashCode();

  virtual Var __get_hashCode_0();

  Var identityHashCode__();

  template <typename T>
  void addInheritance__();

  template <typename T>
  void addInheritance__(const TypeSet &typeParameters);

  template <typename T>
  bool inherits__();

  template <typename T>
  bool inherits__(const TypeSet &typeParameters);

  int referenceCount_;
  bool initialized_;
  // TODO Use a method pointer instead of a Delegate so that 'this' does not
  // need to be bound with it. Then the methods and getters tables can be made
  // static.
  std::unordered_map<Token, std::unordered_map<int, Delegate> > methods_;
  std::unordered_map<Token, Delegate> getters_;
  std::unordered_map<Token, d2c::Var *> members_;
  TypeSet inheritedClasses_;
  // TODO I haven't been able to get Type as a value to work, so wrapping it
  // int a shared_ptr for now. Look into using Type directly as a value.
  std::unordered_map<Token, std::shared_ptr<Type> > typeParameters_;

  static std::unordered_map<size_t, d2c::Var> constObjects_;
};

}  // namespace d2c
#endif  // dart2cpp_object_h

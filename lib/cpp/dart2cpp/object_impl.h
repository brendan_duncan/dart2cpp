/****************************************************************************
 * Copyright (C) 2014 by Brendan Duncan.                                    *
 *                                                                          *
 * This file is part of Dart2Cpp,                                           *
 * https://github.com/brendan-duncan/dart2cpp.                              *
 *                                                                          *
 * Licensed under the Apache License, Version 2.0 (the "License");          *
 * you may not use this file except in compliance with the License.         *
 * You may obtain a copy of the License at                                  *
 *                                                                          *
 * http://www.apache.org/licenses/LICENSE-2.0                               *
 *                                                                          *
 * Unless required by applicable law or agreed to in writing, software      *
 * distributed under the License is distributed on an "AS IS" BASIS,        *
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. *
 * See the License for the specific language governing permissions and      *
 * limitations under the License.                                           *
 ****************************************************************************/
#ifndef dart2cpp_object_impl_h
#define dart2cpp_object_impl_h

#include "object.h"
#include "delegate.h"
#include "symbols.h"

namespace d2c {
Object::Object(__D2C_CONSTRUCTOR_ARG__)
    : referenceCount_(0), initialized_(false) {
  addInheritance__<Object>();
}

Object::~Object() { referenceCount_ = 0; }

void Object::initialize__() {
  initialized_ = true;
  methods_[TK_toString__][0] = d2c::bind(this, &Object::toString);
  methods_[TK___get_hashCode__][0] = d2c::bind(this, &Object::__get_hashCode);
}

template <typename T>
void Object::addInheritance__() {
  Type type(typeid(T).hash_code());
  inheritedClasses_.add(type);
}

template <typename T>
void Object::addInheritance__(const TypeSet &typeParameters) {
  Type type(typeid(T).hash_code(), typeParameters);
  inheritedClasses_.add(type);
}

template <typename T>
bool Object::inherits__() {
  Type t(typeid(T).hash_code());
  return inheritedClasses_.contains(t);
}

template <typename T>
bool Object::inherits__(const TypeSet &typeParameters) {
  Type t(typeid(T).hash_code(), typeParameters);
  return inheritedClasses_.contains(t);
}

bool Object::isFunction__() { return false; }

bool Object::isBool__() { return false; }

bool Object::boolValue__() { throw d2c::Var(TypeFactory::CreateTypeError()); }

bool Object::isInt__() { return false; }

std::int64_t Object::intValue__() {
  throw d2c::Var(TypeFactory::CreateTypeError());
}

bool Object::isDouble__() { return false; }

double Object::doubleValue__() {
  throw d2c::Var(TypeFactory::CreateTypeError());
}

bool Object::isString__() { return false; }

const Str &Object::stringValue__() {
  throw d2c::Var(TypeFactory::CreateTypeError());
}

bool Object::isList__() { return false; }

const std::vector<Var> &Object::listValue__() {
  throw d2c::Var(TypeFactory::CreateTypeError());
}

bool Object::isMap__() { return false; }

const std::unordered_map<Var, Var> &Object::mapValue__() {
  throw d2c::Var(TypeFactory::CreateTypeError());
}

Var Object::toString() {
  static Var objectStr("Object");
  return objectStr;
}

bool Object::hasMethod__(const Token &name) {
  if (!initialized_) {
    initialize__();
  }
  return methods_.find(name) != methods_.end();
}

Var Object::getMember__(const Token &name) {
  if (!initialized_) {
    initialize__();
  }
  if (methods_.find(name) == methods_.end()) {
    if (members_.find(name) != members_.end()) {
      return (*members_[name]);
    }
    return null;
  }

  return d2c::function(d2c::Var(this), methods_[name][0])();
}

Var Object::invoke__(const Token &name) {
  if (!initialized_) {
    initialize__();
  }
  if (getters_.find(name) != getters_.end()) {
    return getters_[name]()();
  }
  if (methods_.find(name) == methods_.end()) {
    if (members_.find(name) != members_.end()) {
      return (*members_[name])();
    }
    return noSuchMethod(
        invocation(symbol(name), list({}, {}), null, INVOCATION_METHOD));
  }
  if (methods_[name].find(0) == methods_[name].end()) {
    return noSuchMethod(
        invocation(symbol(name), list({}, {}), null, INVOCATION_METHOD));
  }
  return methods_[name][0]();
}

Var Object::invoke__(const Token &name, const Var &p1) {
  if (!initialized_) {
    initialize__();
  }
  if (getters_.find(name) != getters_.end()) {
    return getters_[name]()(p1);
  }
  if (methods_.find(name) == methods_.end()) {
    if (members_.find(name) != members_.end()) {
      return (*members_[name])(p1);
    }
    return noSuchMethod(
        invocation(symbol(name), list({}, {p1}), null, INVOCATION_METHOD));
  }
  if (methods_[name].find(1) == methods_[name].end()) {
    return noSuchMethod(
        invocation(symbol(name), list({}, {p1}), null, INVOCATION_METHOD));
  }
  return methods_[name][1](p1);
}

Var Object::invoke__(const Token &name, const Var &p1, const Var &p2) {
  if (!initialized_) {
    initialize__();
  }
  if (getters_.find(name) != getters_.end()) {
    return getters_[name]()(p1, p2);
  }
  if (methods_.find(name) == methods_.end()) {
    if (members_.find(name) != members_.end()) {
      return (*members_[name]);
    }
    return noSuchMethod(
        invocation(symbol(name), list({}, {p1, p2}), null, INVOCATION_METHOD));
  }
  if (methods_[name].find(2) == methods_[name].end()) {
    return noSuchMethod(
        invocation(symbol(name), list({}, {p1, p2}), null, INVOCATION_METHOD));
  }
  return methods_[name][2](p1, p2);
}

Var Object::invoke__(const Token &name, const Var &p1, const Var &p2,
                     const Var &p3) {
  if (!initialized_) {
    initialize__();
  }
  if (getters_.find(name) != getters_.end()) {
    return getters_[name]()(p1, p2, p3);
  }
  if (methods_.find(name) == methods_.end()) {
    if (members_.find(name) != members_.end()) {
      return (*members_[name]);
    }
    return noSuchMethod(invocation(symbol(name), list({}, {p1, p2, p3}), null,
                                   INVOCATION_METHOD));
  }
  if (methods_[name].find(3) == methods_[name].end()) {
    return noSuchMethod(invocation(symbol(name), list({}, {p1, p2, p3}), null,
                                   INVOCATION_METHOD));
  }
  return methods_[name][3](p1, p2, p3);
}

Var Object::invoke__(const Token &name, const Var &p1, const Var &p2,
                     const Var &p3, const Var &p4) {
  if (!initialized_) {
    initialize__();
  }
  if (getters_.find(name) != getters_.end()) {
    return getters_[name]()(p1, p2, p3, p4);
  }
  if (methods_.find(name) == methods_.end()) {
    if (members_.find(name) != members_.end()) {
      return (*members_[name]);
    }
    return noSuchMethod(invocation(symbol(name), list({}, {p1, p2, p3, p4}),
                                   null, INVOCATION_METHOD));
  }
  if (methods_[name].find(4) == methods_[name].end()) {
    return noSuchMethod(invocation(symbol(name), list({}, {p1, p2, p3, p4}),
                                   null, INVOCATION_METHOD));
  }
  return methods_[name][4](p1, p2, p3, p4);
}

Var Object::invoke__(const Token &name, const Var &p1, const Var &p2,
                     const Var &p3, const Var &p4, const Var &p5) {
  if (!initialized_) {
    initialize__();
  }
  if (getters_.find(name) != getters_.end()) {
    return getters_[name]()(p1, p2, p3, p4, p5);
  }
  if (methods_.find(name) == methods_.end()) {
    if (members_.find(name) != members_.end()) {
      return (*members_[name]);
    }
    return noSuchMethod(invocation(symbol(name), list({}, {p1, p2, p3, p4, p5}),
                                   null, INVOCATION_METHOD));
  }
  if (methods_[name].find(5) == methods_[name].end()) {
    return noSuchMethod(invocation(symbol(name), list({}, {p1, p2, p3, p4, p5}),
                                   null, INVOCATION_METHOD));
  }
  return methods_[name][5](p1, p2, p3, p4, p5);
}

Var Object::invoke__(const Token &name, const Var &p1, const Var &p2,
                     const Var &p3, const Var &p4, const Var &p5,
                     const Var &p6) {
  if (!initialized_) {
    initialize__();
  }
  if (getters_.find(name) != getters_.end()) {
    return getters_[name]()(p1, p2, p3, p4, p5, p6);
  }
  if (methods_.find(name) == methods_.end()) {
    if (members_.find(name) != members_.end()) {
      return (*members_[name]);
    }
    return noSuchMethod(invocation(symbol(name),
                                   list({}, {p1, p2, p3, p4, p5, p6}), null,
                                   INVOCATION_METHOD));
  }
  if (methods_[name].find(6) == methods_[name].end()) {
    return noSuchMethod(invocation(symbol(name),
                                   list({}, {p1, p2, p3, p4, p5, p6}), null,
                                   INVOCATION_METHOD));
  }
  return methods_[name][6](p1, p2, p3, p4, p5, p6);
}

Var Object::invoke__(const Token &name, const Var &p1, const Var &p2,
                     const Var &p3, const Var &p4, const Var &p5, const Var &p6,
                     const Var &p7) {
  if (!initialized_) {
    initialize__();
  }
  if (getters_.find(name) != getters_.end()) {
    return getters_[name]()(p1, p2, p3, p4, p5, p6, p7);
  }
  if (methods_.find(name) == methods_.end()) {
    if (members_.find(name) != members_.end()) {
      return (*members_[name]);
    }
    return noSuchMethod(invocation(symbol(name),
                                   list({}, {p1, p2, p3, p4, p5, p6, p7}), null,
                                   INVOCATION_METHOD));
  }
  if (methods_[name].find(7) == methods_[name].end()) {
    return noSuchMethod(invocation(symbol(name),
                                   list({}, {p1, p2, p3, p4, p5, p6, p7}), null,
                                   INVOCATION_METHOD));
  }
  return methods_[name][7](p1, p2, p3, p4, p5, p6, p7);
}

Var Object::invoke__(const Token &name, const Var &p1, const Var &p2,
                     const Var &p3, const Var &p4, const Var &p5, const Var &p6,
                     const Var &p7, const Var &p8) {
  if (!initialized_) {
    initialize__();
  }
  if (getters_.find(name) != getters_.end()) {
    return getters_[name]()(p1, p2, p3, p4, p5, p6, p7, p8);
  }
  if (methods_.find(name) == methods_.end()) {
    if (members_.find(name) != members_.end()) {
      return (*members_[name]);
    }
    return noSuchMethod(invocation(symbol(name),
                                   list({}, {p1, p2, p3, p4, p5, p6, p7, p8}),
                                   null, INVOCATION_METHOD));
  }
  if (methods_[name].find(8) == methods_[name].end()) {
    return noSuchMethod(invocation(symbol(name),
                                   list({}, {p1, p2, p3, p4, p5, p6, p7, p8}),
                                   null, INVOCATION_METHOD));
  }
  return methods_[name][8](p1, p2, p3, p4, p5, p6, p7, p8);
}

Var Object::invoke__(const Token &name, const Var &p1, const Var &p2,
                     const Var &p3, const Var &p4, const Var &p5, const Var &p6,
                     const Var &p7, const Var &p8, const Var &p9) {
  if (!initialized_) {
    initialize__();
  }
  if (getters_.find(name) != getters_.end()) {
    return getters_[name]()(p1, p2, p3, p4, p5, p6, p7, p8, p9);
  }
  if (methods_.find(name) == methods_.end()) {
    if (members_.find(name) != members_.end()) {
      return (*members_[name]);
    }
    return noSuchMethod(
        invocation(symbol(name), list({}, {p1, p2, p3, p4, p5, p6, p7, p8, p9}),
                   null, INVOCATION_METHOD));
  }
  if (methods_[name].find(9) == methods_[name].end()) {
    return noSuchMethod(
        invocation(symbol(name), list({}, {p1, p2, p3, p4, p5, p6, p7, p8, p9}),
                   null, INVOCATION_METHOD));
  }
  return methods_[name][9](p1, p2, p3, p4, p5, p6, p7, p8, p9);
}

Var Object::invoke__(const Token &name, const Var &p1, const Var &p2,
                     const Var &p3, const Var &p4, const Var &p5, const Var &p6,
                     const Var &p7, const Var &p8, const Var &p9,
                     const Var &p10) {
  if (!initialized_) {
    initialize__();
  }
  if (getters_.find(name) != getters_.end()) {
    return getters_[name]()(p1, p2, p3, p4, p5, p6, p7, p8, p9, p10);
  }
  if (methods_.find(name) == methods_.end()) {
    if (members_.find(name) != members_.end()) {
      return (*members_[name]);
    }
    return noSuchMethod(invocation(
        symbol(name), list({}, {p1, p2, p3, p4, p5, p6, p7, p8, p9, p10}), null,
        INVOCATION_METHOD));
  }
  if (methods_[name].find(10) == methods_[name].end()) {
    return noSuchMethod(invocation(
        symbol(name), list({}, {p1, p2, p3, p4, p5, p6, p7, p8, p9, p10}), null,
        INVOCATION_METHOD));
  }
  return methods_[name][10](p1, p2, p3, p4, p5, p6, p7, p8, p9, p10);
}

Var Object::operator[](const Var &) { return noSuchMethod(null); }

Var Object::__array_set(const Var &, const Var &) { return noSuchMethod(null); }

Var Object::operator+(const Var &) { return noSuchMethod(null); }

Var Object::operator-(const Var &) { return noSuchMethod(null); }

Var Object::operator*(const Var &) { return noSuchMethod(null); }

Var Object::operator/(const Var &) { return noSuchMethod(null); }

Var Object::__intDiv(const Var &) { return noSuchMethod(null); }

Var Object::operator<<(const Var &) { return noSuchMethod(null); }

Var Object::operator>>(const Var &) { return noSuchMethod(null); }

Var Object::operator>(const Var &) { return noSuchMethod(null); }

Var Object::operator>=(const Var &) { return noSuchMethod(null); }

Var Object::operator<(const Var &) { return noSuchMethod(null); }

Var Object::operator<=(const Var &) { return noSuchMethod(null); }

Var Object::operator==(const Var &other) {
  if (other.object_ == nullptr) {
    return false;
  }
  return __get_hashCode() == other.object_->__get_hashCode();
}

Var Object::operator!() { return noSuchMethod(null); }

Var Object::operator~() { return noSuchMethod(null); }

Var Object::operator-() { return noSuchMethod(null); }

Var Object::operator&(const Var &) { return noSuchMethod(null); }

Var Object::operator|(const Var &) { return noSuchMethod(null); }

Var Object::operator^(const Var &) { return noSuchMethod(null); } 

Var Object::operator%(const Var &) {
  return noSuchMethod(null);
}

Var Object::operator&&(const Var &) { return noSuchMethod(null); }

Var Object::operator||(const Var &) { return noSuchMethod(null); }

Var Object::noSuchMethod(const Var &invocation) {
  Var memberName = invocation.invoke(TK___get_memberName__);
  Var posArgs = invocation.invoke(TK___get_positionalArguments__);
  Var namedArgs = invocation.invoke(TK___get_namedArguments__);
  throw Var(TypeFactory::CreateNoSuchMethodError(Var(this), memberName, posArgs,
                                                 namedArgs, null));
}

Var Object::__get_hashCode() {
  return static_cast<std::int64_t>((size_t) this);
}

Var Object::__get_hashCode_0() { return __get_hashCode(); }

Var Object::identityHashCode__() {
  return static_cast<std::int64_t>(std::hash<Object *>()(this));
}

}  // d2c
#endif  // dart2cpp_object_impl_h

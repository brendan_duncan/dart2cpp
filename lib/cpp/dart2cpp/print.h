/****************************************************************************
 * Copyright (C) 2014 by Brendan Duncan.                                    *
 *                                                                          *
 * This file is part of Dart2Cpp,                                           *
 * https://github.com/brendan-duncan/dart2cpp.                              *
 *                                                                          *
 * Licensed under the Apache License, Version 2.0 (the "License");          *
 * you may not use this file except in compliance with the License.         *
 * You may obtain a copy of the License at                                  *
 *                                                                          *
 * http://www.apache.org/licenses/LICENSE-2.0                               *
 *                                                                          *
 * Unless required by applicable law or agreed to in writing, software      *
 * distributed under the License is distributed on an "AS IS" BASIS,        *
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. *
 * See the License for the specific language governing permissions and      *
 * limitations under the License.                                           *
 ****************************************************************************/
#ifndef dart2cpp_print_h
#define dart2cpp_print_h

#include "var.h"

namespace d2c {

void print(const Var &v) {
  std::cout << v << std::endl;
}

void print(const Str &s) {
  std::cout << s.c_str() << std::endl;
}

void print(const char *s) {
  std::cout << s << std::endl;
}

void print(int i) {
  std::cout << i << std::endl;
}

void print(double d) {
  std::cout << d << std::endl;
}

void print(bool b) {
  std::cout << (b ? "true" : "false") << std::endl;
}

}  // namespace d2c
#endif  // dart2cpp_print_h

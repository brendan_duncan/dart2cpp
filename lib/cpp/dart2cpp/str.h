/****************************************************************************
* Copyright (C) 2014 by Brendan Duncan.                                    *
*                                                                          *
* This file is part of Dart2Cpp,                                           *
* https://github.com/brendan-duncan/dart2cpp.                              *
*                                                                          *
* Licensed under the Apache License, Version 2.0 (the "License");          *
* you may not use this file except in compliance with the License.         *
* You may obtain a copy of the License at                                  *
*                                                                          *
* http://www.apache.org/licenses/LICENSE-2.0                               *
*                                                                          *
* Unless required by applicable law or agreed to in writing, software      *
* distributed under the License is distributed on an "AS IS" BASIS,        *
* WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. *
* See the License for the specific language governing permissions and      *
* limitations under the License.                                           *
****************************************************************************/
#ifndef dart2cpp_byte_string_h
#define dart2cpp_byte_string_h

namespace d2c {

#define STR__(s) Str(s, sizeof(s) - 1, true)

/**
 * std::string has problems with internal \0 characters, and differs in behavior
 * from Dart strings. For example, std::string("\0").size() is 0, whereas
 * in Dart, "\0".length is 1. This string class helps C++ achieve that behavior.
 * The STR__(s) macro should be used to construct a ByteString from a string
 * constant, so that string lengths are computed correctly.
 */
class Str {
protected:
  class Data {
  public:
    Data(const char *st, size_t sz, bool isStatic)
      : size_(sz)
      , ptr_(st)
      , isStatic_(isStatic) {
    }

    ~Data() {
      if (!isStatic_) {
        delete [] ptr_;
        ptr_ = nullptr;
        size_ = 0;
      }
    }

    size_t size_;
    const char *ptr_;
    bool isStatic_;
  };

public:
  Str() 
      : data_(new Data(nullptr, 0, true)) {
  }

  Str(const char *st, size_t sz, bool isStatic)
      : data_(new Data(st, sz, isStatic)) {
  }

  Str(const char *s)
      : data_(new Data(s, strlen(s), true)) {
  }

  Str(const std::string &s) {
    char *buf = new char[s.size() + 1];
    memcpy(buf, s.c_str(), s.size() + 1);
    data_ = std::shared_ptr<Data>(new Data(buf, s.size(), false));
  }

  Str(const Str &s)
      : data_(s.data_) {
  }

  char operator[](size_t index) const {
    return data_->ptr_[index];
  }

  Str &operator=(const Str &s) {
    if (s.data_ == data_) {
      return *this;
    }

    data_ = s.data_;

    return *this;
  }

  const char *c_str() const { 
    return data_->ptr_; 
  }

  size_t size() const {
    return data_->size_;
  }

  Str substring(int startIndex, int endIndex) {
    if (startIndex < 0) {
      startIndex = 0;
    }
    if (endIndex < 0) {
      endIndex = (int)data_->size_ - 1;
    }
    size_t new_size = (endIndex - startIndex);
    char *new_buf = new char[new_size + 1];
    memcpy(new_buf, data_->ptr_ + startIndex, new_size);
    new_buf[new_size] = '\0';
    return Str(new_buf, new_size, false);
  }

  Str operator+(const Str &s) const {
    size_t new_size = data_->size_ + s.size();
    char *new_buf = new char[new_size];
    memcpy(new_buf, data_->ptr_, data_->size_);
    memcpy(new_buf + data_->size_, s.c_str(), s.size());
    return Str(new_buf, new_size, false);
  }

  Str &operator+=(const Str &s) {
    size_t new_size = data_->size_ + s.size();
    char *new_buf = new char[new_size];
    memcpy(new_buf, data_->ptr_, data_->size_);
    memcpy(new_buf + data_->size_, s.c_str(), s.size());

    data_ = std::shared_ptr<Data>(new Data(new_buf, new_size, false));
    
    return *this;
  }

  Str &operator+=(char c) {
    size_t new_size = data_->size_ + 1;
    char *new_buf = new char[new_size];
    memcpy(new_buf, data_->ptr_, data_->size_);
    new_buf[data_->size_] = c;
    data_ = std::shared_ptr<Data>(new Data(new_buf, new_size, false));

    return *this;
  }

  bool operator==(const Str &s) const {
    if (data_->size_ != s.data_->size_) {
      return false;
    }
    return memcmp(data_->ptr_, s.data_->ptr_, data_->size_) == 0;
  }

  bool operator==(const char *s) const {
    size_t slen = strlen(s);
    if (data_->size_ != slen) {
      return false;
    }
    return memcmp(data_->ptr_, s, data_->size_) == 0;
  }

  bool operator!=(const Str &s) const {
    return !operator==(s);
  }

  bool operator!=(const char *s) const {
    return !operator==(s);
  }

  bool operator<(const Str &s) const {
    if (data_->size_ < s.data_->size_) { 
      return true; 
    }
    if (data_->size_ > s.data_->size_) {
      return false;
    }
    return memcmp(data_->ptr_, s.data_->ptr_, data_->size_) < 0;
  }

  bool empty() const {
    return data_->size_ != 0;
  }

  void clear() {
    if (!data_->isStatic_) {
      delete data_->ptr_;
    }
    data_->ptr_ = nullptr;
    data_->size_ = 0;
  }

  friend std::ostream &operator<<(std::ostream &stream, const Str &s) {
    stream << s.data_->ptr_;
    return stream;
  }

protected:
  std::shared_ptr<Data> data_;
};

}//namespace d2c

// Provides std::hash<d2c::Str> so that d2c::Str can be used as a Key for
// std::unordered_map and std::unordered_set.
namespace std {
  template <>
  struct hash<d2c::Str> {
    size_t operator()(d2c::Str const &s) const {
      return std::hash<const char *>()(s.c_str());
    }
  };
}  // namespace std
#endif//dart2cpp_byte_string_h

/****************************************************************************
 * Copyright (C) 2014 by Brendan Duncan.                                    *
 *                                                                          *
 * This file is part of Dart2Cpp,                                           *
 * https://github.com/brendan-duncan/dart2cpp.                              *
 *                                                                          *
 * Licensed under the Apache License, Version 2.0 (the "License");          *
 * you may not use this file except in compliance with the License.         *
 * You may obtain a copy of the License at                                  *
 *                                                                          *
 * http://www.apache.org/licenses/LICENSE-2.0                               *
 *                                                                          *
 * Unless required by applicable law or agreed to in writing, software      *
 * distributed under the License is distributed on an "AS IS" BASIS,        *
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. *
 * See the License for the specific language governing permissions and      *
 * limitations under the License.                                           *
 ****************************************************************************/
#ifndef dart2cpp_string_h
#define dart2cpp_string_h

#include "object.h"

namespace d2c {
class Var;

/**
 * An Object storage for strings.
 *
 * This is part of the dart2cpp base library representation of the dart:core
 * primary classes, not intented to be a full reproduction of the dart:core
 * version, but enough for testing the translation of language features without
 * the baggage of the full dart:core library. The dart:core version of the class
 * will take precedence over this one if the dart:core library is imported by
 * the translator.
 */
class String : public d2c::Object {
 public:
  typedef d2c::Object super;

  __D2C_CONSTRUCTOR_TYPEDEF__;
  String(__D2C_CONSTRUCTOR_ARG__ = {});

  String(const Str &str);

  String(const char *str);

  static Var __factory_String_fromCharCodes(const TypeSet &__types__,
                                            const Var &charCodes);

  bool isString__() {
    return true;
  }

  const Str &stringValue__() {
    return data_;
  }

  Var length();

  Var substring(const Var &start, const Var &end);

  Var operator==(const Var &other);

  Var operator+(const Var &other);

  Var operator*(const Var &other);

  Var operator<(const Var &other);

  Var toString();

  Var codeUnitAt(const Var &index);

  d2c::Var __get_toString() {
    return d2c::function(d2c::Var(this), methods_[TK_toString__][0]);
  }

  virtual void initialize__();

  Str data_;
};

REGISTER_BASE_STRING(String)

}  // namespace d2c
#endif  // dart2cpp_string_h

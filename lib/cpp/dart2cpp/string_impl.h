/****************************************************************************
 * Copyright (C) 2014 by Brendan Duncan.                                    *
 *                                                                          *
 * This file is part of Dart2Cpp,                                           *
 * https://github.com/brendan-duncan/dart2cpp.                              *
 *                                                                          *
 * Licensed under the Apache License, Version 2.0 (the "License");          *
 * you may not use this file except in compliance with the License.         *
 * You may obtain a copy of the License at                                  *
 *                                                                          *
 * http://www.apache.org/licenses/LICENSE-2.0                               *
 *                                                                          *
 * Unless required by applicable law or agreed to in writing, software      *
 * distributed under the License is distributed on an "AS IS" BASIS,        *
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. *
 * See the License for the specific language governing permissions and      *
 * limitations under the License.                                           *
 ****************************************************************************/
#ifndef dart2cpp_string_impl_h
#define dart2cpp_string_impl_h

#include "integer.h"
#include "string.h"
#include "var.h"

namespace d2c {
String::String(__D2C_CONSTRUCTOR_ARG__)
    : super() {
}

String::String(const Str &str)
    : super()
    , data_(str) {
  addInheritance__<String>();
}

String::String(const char *str)
    : super()
    , data_(str) {
  addInheritance__<String>();
}

Var String::__factory_String_fromCharCodes(const TypeSet &__types__,
                                           const Var &charCodes) {
  const std::vector<d2c::Var> &l = charCodes.listValue();
  char *s = new char[l.size() + 1];
  for (size_t i = 0; i < l.size(); ++i) {
    s[i] = (char)l[i].intValue();
  }
  s[l.size()] = 0;
  return d2c::Str(s, l.size(), false);
}

Var String::length() {
  return static_cast<std::int64_t>(data_.size());
}

Var String::substring(const Var &start, const Var &end) {
  return Var(this);
}

Var String::operator==(const Var &other) {
  if (!dynamic_cast<String *>(other.object_)) {
    return false;
  }
  return data_ == static_cast<String *>(other.object_)->data_;
}

Var String::operator*(const Var &other) {
  if (!other.isInt()) {
    return false;
  }
  std::int64_t n = other.intValue();

  std::string s;
  for (int i = 0; i < n; ++i) {
    s += data_.c_str();
  }

  return Var(Str(s));
}

Var String::operator+(const Var &other) {
  if (!other.isString()) {
    return false;
  }
  return data_ + other.stringValue();
}

Var String::toString() {
  return Var(this);
}

Var String::operator<(const Var &other) {
  return data_ < other.toString().stringValue();
}

Var String::codeUnitAt(const Var &index) {
  size_t i = static_cast<size_t>(index.intValue());
  return (std::int64_t)((unsigned char)(data_[i]));
}

void String::initialize__() {
  super::initialize__();
  methods_[TK___get_length__][0] = d2c::bind(this, &String::length);
  methods_[TK_substring__][2] = d2c::bind(this, &String::substring);
  methods_[TK___get_toString__][0] = d2c::bind(this, &String::__get_toString);
  methods_[TK_codeUnitAt__][1] = d2c::bind(this, &String::codeUnitAt);
}

}  // namespace d2c
#endif  // dart2cpp_string_impl_h

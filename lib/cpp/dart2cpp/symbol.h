#ifndef dart2cpp_symbol_object_h
#define dart2cpp_symbol_object_h

#include "object.h"
#include "token.h"

namespace d2c {
class Var;

Var symbol(const Token &symbol);

 /**
  * Box a Symbol into an Object for use in a Var.
  */
class Symbol : public Object {
 public:
   Symbol(const Token &symbol);

  virtual Var __get_hashCode();

  virtual Var toString();

 protected:
   Token symbol_;
};

REGISTER_BASE_SYMBOL(Symbol)

}  // namespace d2c
#endif  // dart2cpp_symbol_object_h

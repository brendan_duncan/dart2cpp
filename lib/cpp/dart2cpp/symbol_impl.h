#ifndef dart2cpp_symbol_impl_h
#define dart2cpp_symbol_impl_h

#include "symbol.h"

namespace d2c {

Var symbol(const Token &symbol) {
  return Var(TypeFactory::CreateSymbol(symbol));
}

Symbol::Symbol(const Token &symbol) 
    : symbol_(symbol) {
}

Var Symbol::__get_hashCode() {
  return static_cast<std::int64_t>(std::hash<d2c::Token>()(symbol_));
}

Var Symbol::toString() {
  return Str(symbol_.string());
}

} // namespace d2c
#endif // dart2cpp_symbol_impl_h

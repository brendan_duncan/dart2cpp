#ifndef dartcpp_Tokens_h
#define dartcpp_Tokens_h

#include "Token.h"

const d2c::Token TK___array_set__("__array_set");
const d2c::Token TK___get_add__("__get_add");
const d2c::Token TK___get_add__1__("__get_add_1");
const d2c::Token TK___get_current__("__get_current");
const d2c::Token TK___get_hashCode__("__get_hashCode");
const d2c::Token TK___get_isAccessor__("__get_isAccessor");
const d2c::Token TK___get_isGetter__("__get_isGetter");
const d2c::Token TK___get_isMethod__("__get_isMethod");
const d2c::Token TK___get_isSetter__("__get_isSetter");
const d2c::Token TK___get_iterator__("__get_iterator");
const d2c::Token TK___get_length__("__get_length");
const d2c::Token TK___get_memberName__("__get_memberName");
const d2c::Token TK___get_namedArguments__("__get_namedArguments");
const d2c::Token TK___get_positionalArguments__("__get_positionalArguments");
const d2c::Token TK___get_toString__("__get_toString");
const d2c::Token TK___set_length__("__set_length");
const d2c::Token TK_abs__("abs");
const d2c::Token TK_add__("add");
const d2c::Token TK_call__("call_0");
const d2c::Token TK_codeUnitAt__("codeUnitAt");
const d2c::Token TK_contains__("contains");
const d2c::Token TK_containsKey__("containsKey");
const d2c::Token TK_elementAt__("elementAt");
const d2c::Token TK_isFinite__("isFinite");
const d2c::Token TK_isInfinite__("isInfinite");
const d2c::Token TK_isNaN__("isNaN");
const d2c::Token TK_isNegative__("isNegative");
const d2c::Token TK_isPositive__("isPositive");
const d2c::Token TK_moveNext__("moveNext");
const d2c::Token TK_stackTrace__("stackTrace");
const d2c::Token TK_substring__("substring");
const d2c::Token TK_toDouble__("toDouble");
const d2c::Token TK_toInteger__("toInteger");
const d2c::Token TK_toString__("toString");

#endif  // dartcpp_Tokens_h

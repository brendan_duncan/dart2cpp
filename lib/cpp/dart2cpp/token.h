#ifndef dart2cpp_token_h
#define dart2cpp_token_h

namespace d2c {

class Token {
public:
  Token();

  explicit Token(const std::string &string);

  Token(const Token &other);

  bool isValid() const { return hashCode_ != 0; }

  Token &operator=(const Token &other);

  bool operator==(const Token &other) const;

  bool operator==(const std::string &string) const;

  bool operator!=(const Token &other) const;

  bool operator!=(const std::string &other) const;

  const std::string &string() const;

  size_t hashCode() { return hashCode_; }

  size_t hashCode_;
};

} // namespace d2c

// Provides std::hash<d2c::Token> so that d2c::Token can be used as a Key for
// std::unordered_map and std::unordered_set.
namespace std {
template <> struct hash<d2c::Token> {
  typedef d2c::Token argument_type;
  typedef size_t value_type;
  value_type operator()(argument_type const &v) const { return v.hashCode_; }
};
} // namespace std

#endif // dart2cpp_token_h

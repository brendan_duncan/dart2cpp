#ifndef dart2cpp_token_impl_h
#define dart2cpp_token_impl_h

#include "token.h"

namespace d2c {
static std::hash<std::string> string_hash__;

static std::unordered_map<size_t, std::string> &token_string_data() {
  static std::unordered_map<size_t, std::string> *data = nullptr;
  if (data == nullptr) {
    data = new std::unordered_map<size_t, std::string>({{0, ""}});
  }
  return *data;
}

Token::Token() : hashCode_(0) {}

Token::Token(const std::string &string) : hashCode_(string_hash__(string)) {
  token_string_data()[hashCode_] = string;
}

Token::Token(const Token &other) : hashCode_(other.hashCode_) {}

Token &Token::operator=(const Token &other) {
  hashCode_ = other.hashCode_;
  return *this;
}

bool Token::operator==(const Token &other) const {
  return hashCode_ == other.hashCode_;
}

bool Token::operator==(const std::string &string) const {
  return hashCode_ == string_hash__(string);
}

bool Token::operator!=(const Token &other) const {
  return hashCode_ == other.hashCode_;
}

bool Token::operator!=(const std::string &string) const {
  return hashCode_ != string_hash__(string);
}

const std::string &Token::string() const {
  return token_string_data()[hashCode_];
}

} // namespace d2c
#endif // dart2cpp_token_impl_h

/****************************************************************************
 * Copyright (C) 2014 by Brendan Duncan.                                    *
 *                                                                          *
 * This file is part of Dart2Cpp,                                           *
 * https://github.com/brendan-duncan/dart2cpp.                              *
 *                                                                          *
 * Licensed under the Apache License, Version 2.0 (the "License");          *
 * you may not use this file except in compliance with the License.         *
 * You may obtain a copy of the License at                                  *
 *                                                                          *
 * http://www.apache.org/licenses/LICENSE-2.0                               *
 *                                                                          *
 * Unless required by applicable law or agreed to in writing, software      *
 * distributed under the License is distributed on an "AS IS" BASIS,        *
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. *
 * See the License for the specific language governing permissions and      *
 * limitations under the License.                                           *
 ****************************************************************************/
#ifndef dart2cpp_type_h
#define dart2cpp_type_h

#include "type_set.h"

namespace d2c {

template <typename T>
Type objectType(const TypeSet &typeParameters = {});

class Type {
 public:
  Type(size_t id);

  Type(size_t id, const TypeSet &typeParameters);

  Type(const Type &type);

  Type &operator=(const Type &type);

  size_t typeId() const {
    return id_;
  }

  const TypeSet &parameters() const;

  bool operator==(size_t id) const;

  bool operator==(const Type &type) const;

 protected:
  size_t id_;
  TypeSet parameters_;
};
}  // namespace d2c

#endif  // dart2cpp_type_h

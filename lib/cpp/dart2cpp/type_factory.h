/****************************************************************************
 * Copyright (C) 2014 by Brendan Duncan.                                    *
 *                                                                          *
 * This file is part of Dart2Cpp,                                           *
 * https://github.com/brendan-duncan/dart2cpp.                              *
 *                                                                          *
 * Licensed under the Apache License, Version 2.0 (the "License");          *
 * you may not use this file except in compliance with the License.         *
 * You may obtain a copy of the License at                                  *
 *                                                                          *
 * http://www.apache.org/licenses/LICENSE-2.0                               *
 *                                                                          *
 * Unless required by applicable law or agreed to in writing, software      *
 * distributed under the License is distributed on an "AS IS" BASIS,        *
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. *
 * See the License for the specific language governing permissions and      *
 * limitations under the License.                                           *
 ****************************************************************************/
#ifndef dart2cpp_type_factory_h
#define dart2cpp_type_factory_h

namespace d2c {
class Object;
class Str;
class Var;

/**
  * Provides a registration and creation framework for the classes of the
  * basic literal types, such as bool, int, double, String, List, and Map.
  *
  * The dart2cpp library declares the basic literal type objects (int, bool,
  * String, etc) so that they can be created by the Var class. However, the
  * dart:core library declares the version of these classes that should be
  * used for programs that link against dart:core (pretty much every program).
  * This class allows for a registration of the literal type classes, such that
  * if the dart:core library is imported, those classes will override the
  * version in the dart2cpp library, and the Var class will create the
  * correct version of the object.
  */
class TypeFactory {
 public:
  typedef std::initializer_list<Var> InitializerList;
  typedef Object *(*BoolFunction)(bool);
  typedef Object *(*IntFunction)(std::int64_t);
  typedef Object *(*DoubleFunction)(double);
  typedef Object *(*StringFunction)(const Str &);
  typedef Object *(*ListFunction)(const d2c::TypeSet &,
                                  const InitializerList &);
  typedef Object *(*MapFunction)(const d2c::TypeSet &, const InitializerList &);
  typedef Object *(*SymbolFunction)(const d2c::Token &);
  typedef Object *(*InvocationFunction)(const d2c::Var &, const d2c::Var &,
                                        const d2c::Var &, int);
  typedef Object *(*ErrorFunction)();
  typedef Object *(*ArgumentErrorFunction)(d2c::Var message);
  typedef Object *(*UnsupportedErrorFunction)(d2c::Var message);
  typedef Object *(*StateErrorFunction)(d2c::Var message);
  typedef Object *(*NoSuchMethodErrorFunction)(d2c::Var receiver,
                                               d2c::Var memberName,
                                               d2c::Var positionalArguments,
                                               d2c::Var namedArguments,
                                               d2c::Var existingArgumentNames);
  typedef Object *(*FormatExceptionFunction)(d2c::Var message);

  Object *trueLiteral;
  Object *falseLiteral;
  Object *oneLiteral;

  BoolFunction createBool;
  IntFunction createInt;
  DoubleFunction createDouble;
  StringFunction createString;
  ListFunction createList;
  MapFunction createMap;
  SymbolFunction createSymbol;
  InvocationFunction createInvocation;
  ArgumentErrorFunction createArgumentError;
  ErrorFunction createTypeError;
  UnsupportedErrorFunction createUnsupportedError;
  StateErrorFunction createStateError;
  NoSuchMethodErrorFunction createNoSuchMethodError;
  FormatExceptionFunction createFormatException;

  static Object *True();

  static Object *False();

  static Object *One();

  static Object *CreateBool(bool b);

  static Object *CreateInt(std::int64_t i);

  static Object *CreateDouble(double d);

  static Object *CreateString(const Str &s);

  static Object *CreateList(const d2c::TypeSet &__types__,
                            const std::initializer_list<Var> &l);

  static Object *CreateMap(const d2c::TypeSet &__types__,
                           const std::initializer_list<Var> &keyValues);

  static Object *CreateSymbol(const Token &symbol);

  static Object *CreateInvocation(const Var &methodName, const Var &posArgs,
                                  const Var &namedArgs, int type);

  static Object *CreateArgumentError(d2c::Var message);

  static Object *CreateTypeError();

  static Object *CreateUnsupportedError(d2c::Var message);

  static Object *CreateStateError(d2c::Var message);

  static Object *CreateNoSuchMethodError(d2c::Var receiver, d2c::Var memberName,
                                         d2c::Var positionalArguments,
                                         d2c::Var namedArguments,
                                         d2c::Var existingArgumentNames);

  static Object *CreateFormatException(d2c::Var message);

  TypeFactory();

  ~TypeFactory();

  Object *trueValue();

  Object *falseValue();

  Object *oneValue();

  template <typename T>
  void registerBool(T f, int override);

  template <typename T>
  void registerInt(T f, int override);

  template <typename T>
  void registerDouble(T f, int override);

  template <typename T>
  void registerString(T f, int override);

  template <typename T>
  void registerList(T f, int override);

  template <typename T>
  void registerMap(T f, int override);

  template <typename T>
  void registerSymbol(T f, int override);

  template <typename T>
  void registerInvocation(T f, int override);

  template <typename T>
  void registerArgumentError(T f, int override);

  template <typename T>
  void registerUnsupportedError(T f, int override);

  template <typename T>
  void registerStateError(T f, int override);

  template <typename T>
  void registerNoSuchMethodError(T f, int override);

  template <typename T>
  void registerFormatException(T f, int override);

  template <typename T>
  void registerTypeError(T f, int override);

  void resetBoolLiterals();

  void resetIntLiterals();
};

// This static will be instantiated at the top of the program, ensuring it
// will be initialized before other statics that will require using it.
// This is a very useful feature, despite what the 'static naysayers' say,
// they have their place...
static TypeFactory type_factory;

template <typename T, typename A>
Object *objectCreator(A arg);

template <typename T, typename A, typename B>
Object *objectCreator(A a, B b);

template <typename T, typename A, typename B, typename C, typename D>
Object *objectCreator4(A a1, B a2, C a3, D a4);

template <typename T, typename A>
Object *objectCreator5(A a1, A a2, A a3, A a4, A a5);

template <typename T>
Object *objectCreator();

template <typename T, int OVERRIDE>
class BoolRegistration {
 public:
  BoolRegistration() {
    type_factory.registerBool(&objectCreator<T, bool>, OVERRIDE);
  }
};

template <typename T, int OVERRIDE>
class IntRegistration {
 public:
  IntRegistration() {
    type_factory.registerInt(&objectCreator<T, std::int64_t>, OVERRIDE);
  }
};

template <typename T, int OVERRIDE>
class DoubleRegistration {
 public:
  DoubleRegistration() {
    type_factory.registerDouble(&objectCreator<T, double>, OVERRIDE);
  }
};

template <typename T, int OVERRIDE>
class StringRegistration {
 public:
  StringRegistration() {
    type_factory.registerString(&objectCreator<T, const Str &>,
                                OVERRIDE);
  }
};

template <typename T, int OVERRIDE>
class ListRegistration {
 public:
  ListRegistration() {
    type_factory.registerList(
        &objectCreator<T, const d2c::TypeSet &,
                       const std::initializer_list<Var> &>,
        OVERRIDE);
  }
};

template <typename T, int OVERRIDE>
class MapRegistration {
 public:
  MapRegistration() {
    type_factory.registerMap(&objectCreator<T, const d2c::TypeSet &,
                                            const std::initializer_list<Var> &>,
                             OVERRIDE);
  }
};

template <typename T, int OVERRIDE>
class SymbolRegistration {
public:
  SymbolRegistration() {
    type_factory.registerSymbol(&objectCreator<T, const d2c::Token &>,
                                OVERRIDE);
  }
};

template <typename T, int OVERRIDE>
class InvocationRegistration {
public:
  InvocationRegistration() {
    type_factory.registerInvocation(&objectCreator4<T, 
                                                    const d2c::Var &,
                                                    const d2c::Var &,
                                                    const d2c::Var &, 
                                                    int>,
      OVERRIDE);
  }
};

template <typename T, int OVERRIDE>
class ArgumentErrorRegistration {
 public:
  ArgumentErrorRegistration() {
    type_factory.registerArgumentError(&objectCreator<T, d2c::Var>, OVERRIDE);
  }
};

template <typename T, int OVERRIDE>
class UnsupportedErrorRegistration {
 public:
  UnsupportedErrorRegistration() {
    type_factory.registerUnsupportedError(&objectCreator<T, d2c::Var>,
                                          OVERRIDE);
  }
};

template <typename T, int OVERRIDE>
class StateErrorRegistration {
 public:
  StateErrorRegistration() {
    type_factory.registerStateError(&objectCreator<T, d2c::Var>, OVERRIDE);
  }
};

template <typename T, int OVERRIDE>
class NoSuchMethodErrorRegistration {
 public:
  NoSuchMethodErrorRegistration() {
    type_factory.registerNoSuchMethodError(&objectCreator5<T, d2c::Var>,
                                           OVERRIDE);
  }
};

template <typename T, int OVERRIDE>
class TypeErrorRegistration {
 public:
  TypeErrorRegistration() {
    type_factory.registerTypeError(&objectCreator<T>, OVERRIDE);
  }
};

template <typename T, int OVERRIDE>
class FormatExceptionRegistration {
 public:
  FormatExceptionRegistration() {
    type_factory.registerFormatException(&objectCreator<T, d2c::Var>, OVERRIDE);
  }
};

#define REGISTER_BOOL(T) \
  static d2c::BoolRegistration<T, 1> __##T##_registration;

#define REGISTER_BASE_BOOL(T) \
  static d2c::BoolRegistration<T, 0> __BASE_##T##_registration;

#define REGISTER_INT(T) static d2c::IntRegistration<T, 1> __##T##_registration;

#define REGISTER_BASE_INT(T) \
  static d2c::IntRegistration<T, 0> __BASE_##T##_registration;

#define REGISTER_DOUBLE(T) \
  static d2c::DoubleRegistration<T, 1> __##T##_registration;

#define REGISTER_BASE_DOUBLE(T) \
  static d2c::DoubleRegistration<T, 0> __BASE_##T##_registration;

#define REGISTER_STRING(T) \
  static d2c::StringRegistration<T, 1> __##T##_registration;

#define REGISTER_BASE_STRING(T) \
  static d2c::StringRegistration<T, 0> __BASE_##T##_registration;

#define REGISTER_LIST(T) \
  static d2c::ListRegistration<T, 1> __##T##_registration;

#define REGISTER_BASE_LIST(T) \
  static d2c::ListRegistration<T, 0> __BASE_##T##_registration;

#define REGISTER_MAP(T) static d2c::MapRegistration<T, 1> __##T##_registration;

#define REGISTER_BASE_MAP(T) \
  static d2c::MapRegistration<T, 0> __BASE_##T##_registration;

#define REGISTER_SYMBOL(T) \
  static d2c::SymbolRegistration<T, 1> __##T##_registration;

#define REGISTER_BASE_SYMBOL(T) \
  static d2c::SymbolRegistration<T, 0> __BASE_##T##_registration;

#define REGISTER_INVOCATION(T) \
  static d2c::InvocationRegistration<T, 1> __##T##_registration;

#define REGISTER_BASE_INVOCATION(T) \
  static d2c::InvocationRegistration<T, 0> __BASE_##T##_registration;

#define REGISTER_ARGUMENT_ERROR(T) \
  static d2c::ArgumentErrorRegistration<T, 1> __##T##_registration;

#define REGISTER_BASE_ARGUMENT_ERROR(T) \
  static d2c::ArgumentErrorRegistration<T, 0> __BASE_##T##_registration;

#define REGISTER_TYPE_ERROR(T) \
  static d2c::TypeErrorRegistration<T, 1> __##T##_registration;

#define REGISTER_BASE_TYPE_ERROR(T) \
  static d2c::TypeErrorRegistration<T, 0> __BASE_##T##_registration;

#define REGISTER_UNSUPPORTED_ERROR(T) \
  static d2c::UnsupportedErrorRegistration<T, 1> __##T##_registration;

#define REGISTER_BASE_UNSUPPORTED_ERROR(T) \
  static d2c::UnsupportedErrorRegistration<T, 0> __BASE_##T##_registration;

#define REGISTER_STATE_ERROR(T) \
  static d2c::StateErrorRegistration<T, 1> __##T##_registration;

#define REGISTER_BASE_STATE_ERROR(T) \
  static d2c::StateErrorRegistration<T, 0> __BASE_##T##_registration;

#define REGISTER_NO_SUCH_METHOD_ERROR(T) \
  static d2c::NoSuchMethodErrorRegistration<T, 1> __##T##_registration;

#define REGISTER_BASE_NO_SUCH_METHOD_ERROR(T) \
  static d2c::NoSuchMethodErrorRegistration<T, 0> __BASE_##T##_registration;

#define REGISTER_FORMAT_EXCEPTION(T) \
  static d2c::FormatExceptionRegistration<T, 1> __##T##_registration;

#define REGISTER_BASE_FORMAT_EXCEPTION(T) \
  static d2c::FormatExceptionRegistration<T, 0> __BASE_##T##_registration;

}  // namespace d2c
#endif  // dart2cpp_type_factory_h

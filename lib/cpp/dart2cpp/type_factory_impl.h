/****************************************************************************
 * Copyright (C) 2014 by Brendan Duncan.                                    *
 *                                                                          *
 * This file is part of Dart2Cpp,                                           *
 * https://github.com/brendan-duncan/dart2cpp.                              *
 *                                                                          *
 * Licensed under the Apache License, Version 2.0 (the "License");          *
 * you may not use this file except in compliance with the License.         *
 * You may obtain a copy of the License at                                  *
 *                                                                          *
 * http://www.apache.org/licenses/LICENSE-2.0                               *
 *                                                                          *
 * Unless required by applicable law or agreed to in writing, software      *
 * distributed under the License is distributed on an "AS IS" BASIS,        *
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. *
 * See the License for the specific language governing permissions and      *
 * limitations under the License.                                           *
 ****************************************************************************/
#ifndef dart2cpp_type_factory_impl_h
#define dart2cpp_type_factory_impl_h

namespace d2c {
TypeFactory::TypeFactory()
    : trueLiteral(nullptr)
    , falseLiteral(nullptr)
    , oneLiteral(nullptr)
    , createBool(nullptr)
    , createInt(nullptr)
    , createDouble(nullptr)
    , createString(nullptr)
    , createList(nullptr)
    , createMap(nullptr)
    , createSymbol(nullptr)
    , createInvocation(nullptr)
    , createArgumentError(nullptr)
    , createTypeError(nullptr)
    , createUnsupportedError(nullptr)
    , createStateError(nullptr)
    , createNoSuchMethodError(nullptr)
    , createFormatException(nullptr) {
}

TypeFactory::~TypeFactory() {
  delete trueLiteral;
  delete falseLiteral;
  delete oneLiteral;
}

Object *TypeFactory::True() {
  return type_factory.trueValue();
}

Object *TypeFactory::False() {
  return type_factory.falseValue();
}

Object *TypeFactory::One() {
  return type_factory.oneValue();
}

Object *TypeFactory::CreateBool(bool b) {
  return type_factory.createBool(b);
}

Object *TypeFactory::CreateInt(std::int64_t i) {
  return type_factory.createInt(i);
}

Object *TypeFactory::CreateDouble(double d) {
  return type_factory.createDouble(d);
}

Object *TypeFactory::CreateString(const Str &s) {
  return type_factory.createString(s);
}

Object *TypeFactory::CreateList(const d2c::TypeSet &types,
                                const std::initializer_list<Var> &l) {
  return type_factory.createList(types, l);
}

Object *TypeFactory::CreateMap(const d2c::TypeSet &types,
                               const std::initializer_list<Var> &keyValues) {
  return type_factory.createMap(types, keyValues);
}

Object *TypeFactory::CreateSymbol(const Token &s) {
  return type_factory.createSymbol(s);
}

Object *TypeFactory::CreateInvocation(const Var &name, const Var &posArgs,
                                      const Var &namedArgs, int type) {
  return type_factory.createInvocation(name, posArgs, namedArgs, type);
}

Object *TypeFactory::CreateArgumentError(d2c::Var message) {
  return type_factory.createArgumentError(message);
}

Object *TypeFactory::CreateTypeError() {
  return type_factory.createTypeError();
}

Object *TypeFactory::CreateUnsupportedError(d2c::Var message) {
  return type_factory.createUnsupportedError(message);
}

Object *TypeFactory::CreateStateError(d2c::Var message) {
  return type_factory.createStateError(message);
}

Object *TypeFactory::CreateNoSuchMethodError(d2c::Var receiver,
                                             d2c::Var memberName,
                                             d2c::Var positionalArguments,
                                             d2c::Var namedArguments,
                                             d2c::Var existingArgNames) {
  return type_factory.createNoSuchMethodError(receiver, memberName,
                                              positionalArguments,
                                              namedArguments, existingArgNames);
}

Object *TypeFactory::CreateFormatException(d2c::Var message) {
  return type_factory.createFormatException(message);
}

// Use a common object for all true values rather than creating new objects.
Object *TypeFactory::trueValue() {
  if (trueLiteral == nullptr) {
    trueLiteral = CreateBool(true);
    // keep reference-counting vars from deleting it.
    trueLiteral->referenceCount_++;
  }
  return trueLiteral;
}

// Use a common object for all false values rather than creating new objects.
Object *TypeFactory::falseValue() {
  if (falseLiteral == nullptr) {
    falseLiteral = CreateBool(false);
    // keep reference-counting vars from deleting it.
    falseLiteral->referenceCount_++;
  }
  return falseLiteral;
}

// Use a common object for the constant int 1 since it's used frequently.
Object *TypeFactory::oneValue() {
  if (oneLiteral == nullptr) {
    oneLiteral = CreateInt(1);
    // keep reference-counting vars from deleting it.
    oneLiteral->referenceCount_++;
  }
  return oneLiteral;
}

template <typename T>
void TypeFactory::registerBool(T f, int override) {
  if (override == 0 && createBool != nullptr) {
    return;
  }
  resetBoolLiterals();
  createBool = f;
  Var::updateConstants();
}

template <typename T>
void TypeFactory::registerInt(T f, int override) {
  if (override == 0 && createInt != nullptr) {
    return;
  }
  resetIntLiterals();
  createInt = f;
  Var::updateConstants();
}

template <typename T>
void TypeFactory::registerDouble(T f, int override) {
  if (override == 0 && createDouble != nullptr) {
    return;
  }
  createDouble = f;
  Var::updateConstants();
}

template <typename T>
void TypeFactory::registerString(T f, int override) {
  if (override == 0 && createString != nullptr) {
    return;
  }
  createString = f;
  Var::updateConstants();
}

template <typename T>
void TypeFactory::registerList(T f, int override) {
  if (override == 0 && createList != nullptr) {
    return;
  }
  createList = f;
}

template <typename T>
void TypeFactory::registerMap(T f, int override) {
  if (override == 0 && createMap != nullptr) {
    return;
  }
  createMap = f;
}

template <typename T>
void TypeFactory::registerSymbol(T f, int override) {
  if (override == 0 && createSymbol != nullptr) {
    return;
  }
  createSymbol = f;
}

template <typename T>
void TypeFactory::registerInvocation(T f, int override) {
  if (override == 0 && createInvocation != nullptr) {
    return;
  }
  createInvocation = f;
}

template <typename T>
void TypeFactory::registerArgumentError(T f, int override) {
  if (override == 0 && createArgumentError != nullptr) {
    return;
  }
  createArgumentError = f;
}

template <typename T>
void TypeFactory::registerTypeError(T f, int override) {
  if (override == 0 && createTypeError != nullptr) {
    return;
  }
  createTypeError = f;
}

template <typename T>
void TypeFactory::registerUnsupportedError(T f, int override) {
  if (override == 0 && createUnsupportedError != nullptr) {
    return;
  }
  createUnsupportedError = f;
}

template <typename T>
void TypeFactory::registerStateError(T f, int override) {
  if (override == 0 && createStateError != nullptr) {
    return;
  }
  createStateError = f;
}

template <typename T>
void TypeFactory::registerNoSuchMethodError(T f, int override) {
  if (override == 0 && createNoSuchMethodError != nullptr) {
    return;
  }
  createNoSuchMethodError = f;
}

template <typename T>
void TypeFactory::registerFormatException(T f, int override) {
  if (override == 0 && createFormatException != nullptr) {
    return;
  }
  createFormatException = f;
}

void TypeFactory::resetBoolLiterals() {
  if (trueLiteral != nullptr) {
    trueLiteral->referenceCount_--;
    if (trueLiteral->referenceCount_ == 0) {
      delete trueLiteral;
    }
    trueLiteral = nullptr;
  }

  if (falseLiteral != nullptr) {
    falseLiteral->referenceCount_--;
    if (falseLiteral->referenceCount_ == 0) {
      delete falseLiteral;
    }
    falseLiteral = nullptr;
  }
}

void TypeFactory::resetIntLiterals() {
  if (oneLiteral != nullptr) {
    oneLiteral->referenceCount_--;
    if (oneLiteral->referenceCount_ == 0) {
      delete oneLiteral;
    }
    oneLiteral = nullptr;
  }
}

template <typename T>
Object *objectCreator() {
  return new T();
}

template <typename T, typename A>
Object *objectCreator(A arg) {
  return new T(arg);
}

template <typename T, typename A, typename B>
Object *objectCreator(A a, B b) {
  return new T(a, b);
}

template <typename T, typename A, typename B, typename C, typename D>
Object *objectCreator4(A a1, B a2, C a3, D a4) {
  return new T(a1, a2, a3, a4);
}

template <typename T, typename A>
Object *objectCreator5(A a1, A a2, A a3, A a4, A a5) {
  return new T(a1, a2, a3, a4, a5);
}

}  // namespace d2c
#endif  // dart2cpp_type_factory_impl_h

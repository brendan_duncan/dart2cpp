/****************************************************************************
 * Copyright (C) 2014 by Brendan Duncan.                                    *
 *                                                                          *
 * This file is part of Dart2Cpp,                                           *
 * https://github.com/brendan-duncan/dart2cpp.                              *
 *                                                                          *
 * Licensed under the Apache License, Version 2.0 (the "License");          *
 * you may not use this file except in compliance with the License.         *
 * You may obtain a copy of the License at                                  *
 *                                                                          *
 * http://www.apache.org/licenses/LICENSE-2.0                               *
 *                                                                          *
 * Unless required by applicable law or agreed to in writing, software      *
 * distributed under the License is distributed on an "AS IS" BASIS,        *
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. *
 * See the License for the specific language governing permissions and      *
 * limitations under the License.                                           *
 ****************************************************************************/
#ifndef dart2cpp_type_impl_h
#define dart2cpp_type_impl_h

#include "type.h"

size_t std::hash<d2c::Type>::operator()(d2c::Type const &t) const {
  size_t h = t.typeId();
  for (const d2c::Type &arg : t.parameters()) {
    h += std::hash<d2c::Type>()(arg);
  }
  return h;
}

namespace d2c {
template <typename T>
Type objectType(const TypeSet &typeParameters) {
  return Type(typeid(T).hash_code(), typeParameters);
}

Type::Type(size_t id)
    : id_(id) {
}

Type::Type(size_t id, const TypeSet &params)
    : id_(id)
    , parameters_(params) {
}

Type::Type(const Type &type)
    : id_(type.id_)
    , parameters_(type.parameters_) {
}

Type &Type::operator=(const Type &type) {
  id_ = type.id_;
  parameters_ = type.parameters_;
  return *this;
}

const TypeSet &Type::parameters() const {
  return parameters_;
}

bool Type::operator==(size_t id) const {
  return id == id_;
}

bool Type::operator==(const Type &type) const {
  if (type.id_ != id_) {
    return false;
  }
  // If they are the same data type and either type does not have type
  // parameters, then it's a match, since lack of type parameters implies
  // dynamic, which match with anything.
  if (parameters_.empty() || type.parameters_.empty()) {
    return true;
  }

  // Mismatched number of type parameters is a problem.
  if (parameters_.size() < type.parameters_.size()) {
    return false;
  }

  for (size_t i = 0; i < parameters_.size(); ++i) {
    if (i > type.parameters_.size()) {
      // 0 indicates dynamic type, which will match with anything.
      if (parameters_[i].id_ == 0) {
        continue;
      }
      return false;
    }

    // 0 indicates dynamic type, which will match with anything.
    if (parameters_[i].id_ == 0 || type.parameters_[i].id_ == 0) {
      continue;
    }

    if (parameters_[i].id_ != type.parameters_[i].id_) {
      return false;
    }
  }

  // If we've gotten here, all type parameters match.
  return true;
}

}  // namespace d2c
#endif  // dart2cpp_type_impl_h

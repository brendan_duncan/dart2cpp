/****************************************************************************
* Copyright (C) 2014 by Brendan Duncan.                                    *
*                                                                          *
* This file is part of Dart2Cpp,                                           *
* https://github.com/brendan-duncan/dart2cpp.                              *
*                                                                          *
* Licensed under the Apache License, Version 2.0 (the "License");          *
* you may not use this file except in compliance with the License.         *
* You may obtain a copy of the License at                                  *
*                                                                          *
* http://www.apache.org/licenses/LICENSE-2.0                               *
*                                                                          *
* Unless required by applicable law or agreed to in writing, software      *
* distributed under the License is distributed on an "AS IS" BASIS,        *
* WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. *
* See the License for the specific language governing permissions and      *
* limitations under the License.                                           *
****************************************************************************/
#ifndef dart2cpp_type_set_h
#define dart2cpp_type_set_h

namespace d2c {
class Type;
}

// Provides std::hash<d2c::Type> so that d2c::Type can be used as a key for
// std::unordered_map and std::unordered_set.
namespace std {
template <>
struct hash<d2c::Type> {
  typedef d2c::Type argument_type;
  typedef size_t value_type;
  size_t operator()(d2c::Type const &v) const;
};
}  // namespace std

namespace d2c {
class TypeSet {
 public:
  typedef std::vector<Type> Data;
  typedef Data::iterator iterator;
  typedef Data::const_iterator const_iterator;

  TypeSet();

  TypeSet(const std::initializer_list<Type> &init);

  TypeSet(const TypeSet &copy);

  iterator begin();

  const_iterator begin() const;

  iterator end();

  const_iterator end() const;

  size_t size() const;

  bool empty() const;

  void add(const Type &type);

  void add(const TypeSet &set);

  const Type &operator[](size_t index) const;

  bool contains(const Type &type);

  iterator find(const Type &type);

  const_iterator find(const Type &type) const;

 protected:
  Data data_;
};

}  // namespace d2c
#endif  // dart2cpp_type_set_h

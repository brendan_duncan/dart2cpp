/****************************************************************************
* Copyright (C) 2014 by Brendan Duncan.                                    *
*                                                                          *
* This file is part of Dart2Cpp,                                           *
* https://github.com/brendan-duncan/dart2cpp.                              *
*                                                                          *
* Licensed under the Apache License, Version 2.0 (the "License");          *
* you may not use this file except in compliance with the License.         *
* You may obtain a copy of the License at                                  *
*                                                                          *
* http://www.apache.org/licenses/LICENSE-2.0                               *
*                                                                          *
* Unless required by applicable law or agreed to in writing, software      *
* distributed under the License is distributed on an "AS IS" BASIS,        *
* WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. *
* See the License for the specific language governing permissions and      *
* limitations under the License.                                           *
****************************************************************************/
#ifndef dart2cpp_type_set_impl_h
#define dart2cpp_type_set_impl_h

namespace d2c {

static Type kDynamicType(0);

TypeSet::TypeSet() {
}

TypeSet::TypeSet(const std::initializer_list<Type> &init)
    : data_(init) {
}

TypeSet::TypeSet(const TypeSet &copy)
    : data_(copy.data_) {
}

TypeSet::iterator TypeSet::begin() {
  return data_.begin();
}

TypeSet::const_iterator TypeSet::begin() const {
  return data_.begin();
}

TypeSet::iterator TypeSet::end() {
  return data_.end();
}

TypeSet::const_iterator TypeSet::end() const {
  return data_.end();
}

size_t TypeSet::size() const {
  return data_.size();
}

bool TypeSet::empty() const {
  return data_.empty();
}

void TypeSet::add(const Type &type) {
  if (contains(type)) {
    return;
  }
  data_.push_back(type);
}

void TypeSet::add(const TypeSet &set) {
  for (Data::const_iterator iter = set.begin(); iter != set.end(); ++iter) {
    add(*iter);
  }
}

const Type &TypeSet::operator[](size_t index) const {
  if (index >= data_.size()) {
    return kDynamicType;
  }
  return data_[index];
}

bool TypeSet::contains(const Type &type) {
  return find(type) != data_.end();
}

TypeSet::iterator TypeSet::find(const Type &type) {
  return std::find(data_.begin(), data_.end(), type);
}

TypeSet::const_iterator TypeSet::find(const Type &type) const {
  return std::find(data_.begin(), data_.end(), type);
}

}  // namespace d2c
#endif  // dart2cpp_type_set_impl_h

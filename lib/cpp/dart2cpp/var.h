/****************************************************************************
 * Copyright (C) 2014 by Brendan Duncan.                                    *
 *                                                                          *
 * This file is part of Dart2Cpp,                                           *
 * https://github.com/brendan-duncan/dart2cpp.                              *
 *                                                                          *
 * Licensed under the Apache License, Version 2.0 (the "License");          *
 * you may not use this file except in compliance with the License.         *
 * You may obtain a copy of the License at                                  *
 *                                                                          *
 * http://www.apache.org/licenses/LICENSE-2.0                               *
 *                                                                          *
 * Unless required by applicable law or agreed to in writing, software      *
 * distributed under the License is distributed on an "AS IS" BASIS,        *
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. *
 * See the License for the specific language governing permissions and      *
 * limitations under the License.                                           *
 ****************************************************************************/
#ifndef dart2cpp_var_h
#define dart2cpp_var_h

namespace d2c {
class Object;
class Delegate;
class Token;
class Var;

template <typename T>
T &cast(const Var &var);

template <typename T>
bool is_type(const Var &var);

template <typename T>
bool is_type(const Var &var, const TypeSet &typeArgs);

/// Does this Var have a call operator?
bool is_callable(const Var &var);

inline bool bool_value(bool b) {
  return b;
}

bool bool_value(const Var &v);

typedef std::unordered_set<Var *> VarSet;

class Var {
 public:
  Object *object_;

  Var();

  explicit Var(const Object *obj);

  Var(const Str &string);

  Var(const char *string);

  Var(int i);

  Var(std::int64_t i);

  Var(double d);

  Var(bool b);

  Var(const Delegate &f);

  Var(const Var &other);

  Var(Var (*f)());

  Var(Var (*f)(const Var &));

  Var(Var (*f)(const Var &, const Var &));

  Var(Var (*f)(const Var &, const Var &, const Var &));

  Var(Var (*f)(const Var &, const Var &, const Var &, const Var &));

  Var(Var (*f)(const Var &, const Var &, const Var &, const Var &,
               const Var &));

  Var(Var (*f)(const Var &, const Var &, const Var &, const Var &, const Var &,
               const Var &));

  Var(Var (*f)(const Var &, const Var &, const Var &, const Var &, const Var &,
               const Var &, const Var &));

  Var(Var (*f)(const Var &, const Var &, const Var &, const Var &, const Var &,
               const Var &, const Var &, const Var &));

  Var(Var (*f)(const Var &, const Var &, const Var &, const Var &, const Var &,
               const Var &, const Var &, const Var &, const Var &));

  Var(Var (*f)(const Var &, const Var &, const Var &, const Var &, const Var &,
               const Var &, const Var &, const Var &, const Var &,
               const Var &));

  ~Var();

  // dart2cpp doesn't care about const'ness of variables,
  // the const declarations here are just to appease the c++
  // compiler.
  Var &operator=(const Var &v) const;

  operator bool() const;

  bool isBool() const;

  bool boolValue() const;

  bool isInt() const;

  std::int64_t intValue() const;

  bool isDouble() const;

  double doubleValue() const;

  bool isString() const;

  const Str &stringValue() const;

  bool isList() const;

  const std::vector<Var> &listValue() const;

  bool isNull() const;

  bool isNotNull() const;

  bool isFunction() const;

  Var operator+(const Var &other) const;

  Var operator+(std::int64_t i) const;

  friend Var operator+(std::int64_t a, const Var &b);

  Var operator+(double d) const;

  friend Var operator+(double a, const Var &b);

  Var operator+(const Str &string) const;

  friend Var operator+(const Str &a, const Var &b);

  Var &operator+=(const Var &other) const;

  Var operator-(const Var &other) const;

  Var operator-(std::int64_t i) const;

  friend Var operator-(std::int64_t a, const Var &b);

  Var operator-(double d) const;

  friend Var operator-(double a, const Var &b);

  Var &operator-=(const Var &other) const;

  Var operator*(const Var &other) const;

  Var operator*(std::int64_t i) const;

  friend Var operator*(std::int64_t a, const Var &b);

  Var operator*(double d) const;

  friend Var operator*(double a, const Var &b);

  Var &operator*=(const Var &other) const;

  Var operator/(const Var &other) const;

  Var operator/(std::int64_t i) const;

  friend Var operator/(std::int64_t a, const Var &b);

  Var operator/(double d) const;

  Var &operator/=(const Var &other) const;

  // operator ~/
  Var __intDiv(const Var &other) const;

  // operator ~/
  Var __intDiv(std::int64_t i) const;

  // operator ~/
  friend Var __intDiv(std::int64_t a, const Var &b);

  // operator ~/
  Var __intDiv(double d) const;

  // operator ~/
  friend Var __intDiv(double a, const Var &b);

  // operator ~/=
  Var &__intDivAssign(const Var &other) const;

  Var operator<<(const Var &other) const;

  Var operator<<(std::int64_t rhv) const;

  Var &operator<<=(const Var &rhv) const;

  Var &operator<<=(std::int64_t rhv) const;

  Var operator>>(const Var &other) const;

  Var operator>>(std::int64_t rhv) const;

  Var &operator>>=(const Var &rhv) const;

  Var &operator>>=(std::int64_t rhv) const;

  Var operator<(const Var &other) const;

  Var operator<(std::int64_t i) const;

  friend Var operator<(std::int64_t a, const Var &b);

  Var operator<(double d) const;

  friend Var operator<(double a, const Var &b);

  Var operator<=(const Var &other) const;

  Var operator<=(std::int64_t i) const;

  friend Var operator<=(std::int64_t a, const Var &b);

  Var operator<=(double d) const;

  friend Var operator<=(double a, const Var &b);

  Var operator>(const Var &other) const;

  Var operator>(std::int64_t i) const;

  friend Var operator>(std::int64_t a, const Var &b);

  Var operator>(double d) const;

  friend Var operator>(double a, const Var &b);

  Var operator>=(const Var &other) const;

  Var operator>=(std::int64_t i) const;

  friend Var operator>=(std::int64_t a, const Var &b);

  Var operator>=(double d) const;

  friend Var operator>=(double a, const Var &b);

  Var operator==(const Var &other) const;

  Var operator==(std::int64_t i) const;

  friend Var operator==(std::int64_t a, const Var &b);

  Var operator==(double d) const;

  friend Var operator==(double a, const Var &b);

  Var operator==(const char *string) const;

  friend Var operator==(const char *a, const Var &b);

  friend Var operator==(const Str &a, const Var &b);

  Var operator==(bool b) const;

  friend Var operator==(bool a, const Var &b);

  Var operator!=(const Var &other) const;

  Var operator!=(std::int64_t i) const;

  friend Var operator!=(std::int64_t a, const Var &b);

  Var operator!=(double d) const;

  friend Var operator!=(double a, const Var &b);

  Var operator!=(const char *string) const;

  friend Var operator!=(const char *a, const Var &b);

  Var operator!=(bool b) const;

  friend Var operator!=(bool a, const Var &b);

  Var operator!() const;

  Var operator~() const;

  // Negate operator
  Var operator-() const;

  Var operator&(const Var &b) const;

  Var operator&(std::int64_t b) const;

  Var operator&(int b) const;

  friend Var operator&(std::int64_t a, const Var &b);

  Var &operator&=(const Var &other) const;

  Var operator|(const Var &b) const;

  Var operator|(std::int64_t b) const;

  Var operator|(int b) const;

  friend Var operator|(std::int64_t a, const Var &b);

  Var &operator|=(const Var &other) const;

  Var operator^(const Var &b) const;

  Var operator^(std::int64_t b) const;

  Var operator^(int b) const;

  friend Var operator^(std::int64_t a, const Var &b);

  Var &operator^=(const Var &other) const;

  Var operator%(const Var &b) const;

  Var operator%(std::int64_t b) const;

  Var operator%(int b) const;

  friend Var operator%(std::int64_t a, const Var &b);

  Var &operator%=(const Var &other) const;

  Var operator&&(const Var &b) const;

  Var operator&&(bool b) const;

  friend Var operator&&(bool a, const Var &b);

  Var operator||(const Var &b) const;

  Var operator||(bool b) const;

  friend Var operator||(bool a, const Var &b);

  Var operator++() const;

  Var operator--() const;

  Var operator++(int amount) const;

  Var operator--(int amount) const;

  Var operator[](const Var &index) const;

  // operator []=
  Var __array_set(const Var &index, const Var &value) const;

  Var operator()() const;

  Var operator()(const Var &p1) const;

  Var operator()(const Var &p1, const Var &p2) const;

  Var operator()(const Var &p1, const Var &p2, const Var &p3) const;

  Var operator()(const Var &p1, const Var &p2, const Var &p3,
                 const Var &p4) const;

  Var operator()(const Var &p1, const Var &p2, const Var &p3, const Var &p4,
                 const Var &p5) const;

  Var operator()(const Var &p1, const Var &p2, const Var &p3, const Var &p4,
                 const Var &p5, const Var &p6) const;

  Var operator()(const Var &p1, const Var &p2, const Var &p3, const Var &p4,
                 const Var &p5, const Var &p6, const Var &p7) const;

  Var operator()(const Var &p1, const Var &p2, const Var &p3, const Var &p4,
                 const Var &p5, const Var &p6, const Var &p7,
                 const Var &p8) const;

  Var operator()(const Var &p1, const Var &p2, const Var &p3, const Var &p4,
                 const Var &p5, const Var &p6, const Var &p7, const Var &p8,
                 const Var &p9) const;

  Var operator()(const Var &p1, const Var &p2, const Var &p3, const Var &p4,
                 const Var &p5, const Var &p6, const Var &p7, const Var &p8,
                 const Var &p9, const Var &p10) const;

  Var getMember(const Token &name) const;

  Var invoke(const Token &name) const;

  Var invoke(const Token &name, const Var &p1) const;

  Var invoke(const Token &name, const Var &p1, const Var &p2) const;

  Var invoke(const Token &name, const Var &p1, const Var &p2,
             const Var &p3) const;

  Var invoke(const Token &name, const Var &p1, const Var &p2, const Var &p3,
             const Var &p4) const;

  Var invoke(const Token &name, const Var &p1, const Var &p2, const Var &p3,
             const Var &p4, const Var &p5) const;

  Var invoke(const Token &name, const Var &p1, const Var &p2, const Var &p3,
             const Var &p4, const Var &p5, const Var &p6) const;

  Var invoke(const Token &name, const Var &p1, const Var &p2, const Var &p3,
             const Var &p4, const Var &p5, const Var &p6, const Var &p7) const;

  Var invoke(const Token &name, const Var &p1, const Var &p2, const Var &p3,
             const Var &p4, const Var &p5, const Var &p6, const Var &p7,
             const Var &p8) const;

  Var invoke(const Token &name, const Var &p1, const Var &p2, const Var &p3,
             const Var &p4, const Var &p5, const Var &p6, const Var &p7,
             const Var &p8, const Var &p9) const;

  Var invoke(const Token &name, const Var &p1, const Var &p2, const Var &p3,
             const Var &p4, const Var &p5, const Var &p6, const Var &p7,
             const Var &p8, const Var &p9, const Var &p10) const;

  Var cascade(const Var &b);

  Var cascade(const Var &returnObj, const Token &token) const;

  Var cascade(const Token &token) const;

  Var cascade(const Var &returnObj, const Token &token, const Var &p1) const;

  Var cascade(const Token &token, const Var &p1) const;

  Var cascade(const Token &token, const Var &p1, const Var &p2) const;

  Var cascade(const Token &token, const Var &p1, const Var &p2,
              const Var &p3) const;

  Var cascade(const Token &token, const Var &p1, const Var &p2, const Var &p3,
              const Var &p4) const;

  Var cascade(const Token &token, const Var &p1, const Var &p2, const Var &p3,
              const Var &p4, const Var &p5) const;

  Var cascade(const Token &token, const Var &p1, const Var &p2, const Var &p3,
              const Var &p4, const Var &p5, const Var &p6) const;

  Var cascade(const Token &token, const Var &p1, const Var &p2, const Var &p3,
              const Var &p4, const Var &p5, const Var &p6, const Var &p7) const;

  Var cascade(const Token &token, const Var &p1, const Var &p2, const Var &p3,
              const Var &p4, const Var &p5, const Var &p6, const Var &p7,
              const Var &p8) const;

  Var cascade(const Token &token, const Var &p1, const Var &p2, const Var &p3,
              const Var &p4, const Var &p5, const Var &p6, const Var &p7,
              const Var &p8, const Var &p9) const;

  Var cascade(const Token &token, const Var &p1, const Var &p2, const Var &p3,
              const Var &p4, const Var &p5, const Var &p6, const Var &p7,
              const Var &p8, const Var &p9, const Var &p10) const;

  Var toString() const;

  bool implementsType(const Type &type) const;

  void _referenceObject();

  void _unreferenceObject();

  void setObject(Object *obj);

  friend std::ostream &operator<<(std::ostream &stream, const Var &var);

  static void startClosureCapture();

  static void stopClosureCapture();

  void addClosureReference(Var *v, VarSet &visited);

  void removeClosureReference(Var *v, VarSet &visited);

  static bool doClosureCapture_;

  VarSet closureReferences_;

  static void updateConstants();
};
}  // namespace d2c

// Provides std::hash<d2c::Var> so that d2c::Var can be used as a Key for
// std::unordered_map and std::unordered_set. This will invoke the hashCode
// getter of the object, allowing objects to define their hash function.
namespace std {
template <>
struct hash<d2c::Var> {
  size_t operator()(d2c::Var const &v) const;
};
}  // namespace std

#endif  // dart2cpp_var_h

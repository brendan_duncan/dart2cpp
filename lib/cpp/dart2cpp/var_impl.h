/****************************************************************************
 * Copyright (C) 2014 by Brendan Duncan.                                    *
 *                                                                          *
 * This file is part of Dart2Cpp,                                           *
 * https://github.com/brendan-duncan/dart2cpp.                              *
 *                                                                          *
 * Licensed under the Apache License, Version 2.0 (the "License");          *
 * you may not use this file except in compliance with the License.         *
 * You may obtain a copy of the License at                                  *
 *                                                                          *
 * http://www.apache.org/licenses/LICENSE-2.0                               *
 *                                                                          *
 * Unless required by applicable law or agreed to in writing, software      *
 * distributed under the License is distributed on an "AS IS" BASIS,        *
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. *
 * See the License for the specific language governing permissions and      *
 * limitations under the License.                                           *
 ****************************************************************************/
#ifndef dart2cpp_var_impl_h
#define dart2cpp_var_impl_h

#include "null.h"
#include "symbols.h"

size_t std::hash<d2c::Var>::operator()(d2c::Var const &v) const {
  d2c::Var h = v.invoke(TK___get_hashCode__);
  return static_cast<size_t>(h.intValue());
}

namespace d2c {
bool Var::doClosureCapture_ = false;

std::ostream &operator<<(std::ostream &stream, const Var &var) {
  if (var.object_ == nullptr) {
    stream << "null";
  } else {
    stream << var.toString().stringValue();
  }
  return stream;
}

template <typename T>
T &cast(const Var &var) {
  if (var.object_ == nullptr) {
    throw d2c::Var(TypeFactory::CreateTypeError());
  }
  return *reinterpret_cast<T *>(var.object_);
}

template <typename T>
bool is_type(const Var &var) {
  if (dynamic_cast<T *>(var.object_) != nullptr) {
    return true;
  }
  if (var.object_ == nullptr) {
    return typeid(T).hash_code() == typeid(Null).hash_code();
  }
  return var.object_->inherits__<T>();
}

template <typename T>
bool is_type(const Var &var, const TypeSet &typeParams) {
  if (typeParams.empty() && dynamic_cast<T *>(var.object_) != nullptr) {
    return true;
  }
  if (var.object_ == nullptr) {
    return typeid(T).hash_code() == typeid(Null).hash_code();
  }
  return var.object_->inherits__<T>(typeParams);
}

bool is_callable(const Var &var) {
  if (var.object_ == nullptr) {
    return false;
  }

  // Make sure the invoke table is initialized.
  if (!var.object_->initialized_) {
    var.object_->initialize__();
  }

  return (dynamic_cast<Function *>(var.object_) != nullptr) ||
         var.object_->methods_.find(TK_call__) != var.object_->methods_.end();
}

bool bool_value(const Var &v) {
  return v.boolValue();
}

void Var::startClosureCapture() {
  doClosureCapture_ = true;
}

void Var::stopClosureCapture() {
  doClosureCapture_ = false;
}

Var::Var()
    : object_(nullptr) {
}

Var::Var(const Var &v)
    : object_(const_cast<Object *>(v.object_)) {
  _referenceObject();
  if (doClosureCapture_) {
    VarSet visited;
    addClosureReference(const_cast<Var *>(&v), visited);
  }
}

Var::Var(const Object *obj)
    : object_(const_cast<Object *>(obj)) {
  _referenceObject();
}

Var::Var(const Str &s) {
  if (const_strings.find(s) == const_strings.end()) {
    object_ = TypeFactory::CreateString(s);
    const_strings[s] = *this;
  } else {
    object_ = const_strings[s].object_;
  }
  _referenceObject();
}

Var::Var(const char *s) {
  if (const_strings.find(s) == const_strings.end()) {
    object_ = TypeFactory::CreateString(s);
    const_strings[s] = *this;
  } else {
    object_ = const_strings[s].object_;
  }
  _referenceObject();
}

Var::Var(int i) {
  if (const_ints.find(i) == const_ints.end()) {
    object_ = TypeFactory::CreateInt(i);
    const_ints[i] = *this;
  } else {
    object_ = const_ints[i].object_;
  }
  _referenceObject();
}

Var::Var(std::int64_t i) {
  if (const_ints.find(i) == const_ints.end()) {
    object_ = TypeFactory::CreateInt(i);
    const_ints[i] = *this;
  } else {
    object_ = const_ints[i].object_;
  }
  _referenceObject();
}

Var::Var(double d) {
  if (const_doubles.find(d) == const_doubles.end()) {
    object_ = TypeFactory::CreateDouble(d);
    const_doubles[d] = *this;
  } else {
    object_ = const_doubles[d].object_;
  }
  _referenceObject();
}

Var::Var(bool b) {
  if (const_bools.find(b) == const_bools.end()) {
    object_ = TypeFactory::CreateBool(b);
    const_bools[b] = *this;
  } else {
    object_ = const_bools[b].object_;
  }
  _referenceObject();
}

Var::Var(const Delegate &f)
    : object_(new Function(f)) {
  _referenceObject();
}

Var::Var(Var (*f)())
    : object_(new Function(d2c::bind(f))) {
  _referenceObject();
}

Var::Var(Var (*f)(const Var &))
    : object_(new Function(d2c::bind(f))) {
  _referenceObject();
}

Var::Var(Var (*f)(const Var &, const Var &))
    : object_(new Function(d2c::bind(f))) {
  _referenceObject();
}

Var::Var(Var (*f)(const Var &, const Var &, const Var &))
    : object_(new Function(d2c::bind(f))) {
  _referenceObject();
}

Var::Var(Var (*f)(const Var &, const Var &, const Var &, const Var &))
    : object_(new Function(d2c::bind(f))) {
  _referenceObject();
}

Var::Var(Var (*f)(const Var &, const Var &, const Var &, const Var &,
                  const Var &))
    : object_(new Function(d2c::bind(f))) {
  _referenceObject();
}

Var::Var(Var (*f)(const Var &, const Var &, const Var &, const Var &,
                  const Var &, const Var &))
    : object_(new Function(d2c::bind(f))) {
  _referenceObject();
}

Var::Var(Var (*f)(const Var &, const Var &, const Var &, const Var &,
                  const Var &, const Var &, const Var &))
    : object_(new Function(d2c::bind(f))) {
  _referenceObject();
}

Var::Var(Var (*f)(const Var &, const Var &, const Var &, const Var &,
                  const Var &, const Var &, const Var &, const Var &))
    : object_(new Function(d2c::bind(f))) {
  _referenceObject();
}

Var::Var(Var (*f)(const Var &, const Var &, const Var &, const Var &,
                  const Var &, const Var &, const Var &, const Var &,
                  const Var &))
    : object_(new Function(d2c::bind(f))) {
  _referenceObject();
}

Var::Var(Var (*f)(const Var &, const Var &, const Var &, const Var &,
                  const Var &, const Var &, const Var &, const Var &,
                  const Var &, const Var &))
    : object_(new Function(d2c::bind(f))) {
  _referenceObject();
}

Var::~Var() {
  if (!closureReferences_.empty()) {
    for (auto r : closureReferences_) {
      VarSet visited;
      r->removeClosureReference(this, visited);
    }
    closureReferences_.clear();
  }
  _unreferenceObject();
}

void Var::updateConstants() {
  for (auto it = const_ints.begin(); it != const_ints.end(); ++it) {
    const_ints[it->first] = Var(TypeFactory::CreateInt(it->first));
  }

  for (auto it = const_doubles.begin(); it != const_doubles.end(); ++it) {
    const_doubles[it->first] = Var(TypeFactory::CreateDouble(it->first));
  }
}

bool Var::isFunction() const {
  return object_ != nullptr && object_->isFunction__();
}

bool Var::isNull() const {
  return object_ == nullptr || is_type<Null>(*this);
}

bool Var::isNotNull() const {
  return object_ != nullptr && !is_type<Null>(*this);
}

void Var::_referenceObject() {
  if (object_ != nullptr) {
    object_->referenceCount_++;
  }
}

void Var::_unreferenceObject() {
  if (object_ == nullptr) {
    return;
  }
  object_->referenceCount_--;
  if (object_->referenceCount_ == 0) {
    delete object_;
  }
  object_ = nullptr;
}

Var &Var::operator=(const Var &v) const {
  Var *self = const_cast<Var *>(this);

  if (doClosureCapture_) {
    VarSet visited;
    self->addClosureReference(const_cast<Var *>(&v), visited);
  }

  self->setObject(const_cast<Object *>(v.object_));

  return *self;
}

void Var::setObject(Object *obj) {
  if (obj == object_) {
    return;
  }

  _unreferenceObject();
  object_ = const_cast<Object *>(obj);
  _referenceObject();

  for (auto r : closureReferences_) {
    r->setObject(obj);
  }
}

Var::operator bool() const {
  if (object_ == nullptr) {
    throw d2c::Var(TypeFactory::CreateTypeError());
  }
  return object_->boolValue__();
}

bool Var::isBool() const {
  if (object_ == nullptr) {
    throw d2c::Var(TypeFactory::CreateTypeError());
  }
  return object_->isBool__();
}

bool Var::boolValue() const {
  if (object_ == nullptr) {
    throw d2c::Var(TypeFactory::CreateTypeError());
  }
  return object_->boolValue__();
}

bool Var::isInt() const {
  if (object_ == nullptr) {
    throw d2c::Var(TypeFactory::CreateTypeError());
  }
  return object_->isInt__();
}

std::int64_t Var::intValue() const {
  if (object_ == nullptr) {
    throw d2c::Var(TypeFactory::CreateTypeError());
  }
  return object_->intValue__();
}

bool Var::isDouble() const {
  if (object_ == nullptr) {
    throw d2c::Var(TypeFactory::CreateTypeError());
  }
  return object_->isDouble__();
}

double Var::doubleValue() const {
  if (object_ == nullptr) {
    throw d2c::Var(TypeFactory::CreateTypeError());
  }
  return object_->doubleValue__();
}

bool Var::isString() const {
  if (object_ == nullptr) {
    throw d2c::Var(TypeFactory::CreateTypeError());
  }
  return object_->isString__();
}

const Str &Var::stringValue() const {
  if (object_ == nullptr) {
    throw d2c::Var(TypeFactory::CreateTypeError());
  }
  return object_->stringValue__();
}

bool Var::isList() const {
  if (object_ == nullptr) {
    throw d2c::Var(TypeFactory::CreateTypeError());
  }
  return object_->isList__();
}

const std::vector<Var> &Var::listValue() const {
  if (object_ == nullptr) {
    throw d2c::Var(TypeFactory::CreateTypeError());
  }
  return object_->listValue__();
}

Var Var::operator[](const Var &index) const {
  Var i = index;
  if (object_ == nullptr) {
    throw d2c::Var(TypeFactory::CreateTypeError());
  }
  return (*object_)[i];
}

Var Var::__array_set(const Var &index, const Var &value) const {
  if (object_ == nullptr) {
    throw d2c::Var(TypeFactory::CreateTypeError());
  }
  return (*object_).__array_set(index, value);
}

Var Var::operator+(const Var &other) const {
  if (object_ == nullptr) {
    throw d2c::Var(TypeFactory::CreateTypeError());
  }
  Object *a = const_cast<Object *>(object_);
  return *a + other;
}

Var Var::operator+(std::int64_t i) const {
  Object *a = const_cast<Object *>(object_);
  Var v(TypeFactory::CreateInt(i));
  if (object_ == nullptr) {
    throw d2c::Var(TypeFactory::CreateTypeError());
  }
  return *a + v;
}

Var operator+(std::int64_t a, const Var &b) {
  Var aa(TypeFactory::CreateInt(a));
  Object *bb = const_cast<Object *>(b.object_);
  if (bb == nullptr) {
    throw d2c::Var(TypeFactory::CreateTypeError());
  }
  return *aa.object_ + b;
}

Var Var::operator+(double d) const {
  Object *a = const_cast<Object *>(object_);
  Var v(TypeFactory::CreateDouble(d));
  if (object_ == nullptr) {
    throw d2c::Var(TypeFactory::CreateTypeError());
  }
  return *a + v;
}

Var operator+(double a, const Var &b) {
  Var aa(TypeFactory::CreateDouble(a));
  Object *bb = const_cast<Object *>(b.object_);
  if (bb == nullptr) {
    throw d2c::Var(TypeFactory::CreateTypeError());
  }
  return *aa.object_ + b;
}

Var Var::operator+(const Str &s) const {
  Object *a = const_cast<Object *>(object_);
  Var v(TypeFactory::CreateString(s));
  if (object_ == nullptr) {
    throw d2c::Var(TypeFactory::CreateTypeError());
  }
  return *a + v;
}

Var operator+(const Str &a, const Var &b) {
  Var aa(TypeFactory::CreateString(a));
  Object *bb = const_cast<Object *>(b.object_);
  if (bb == nullptr) {
    throw d2c::Var(TypeFactory::CreateTypeError());
  }
  return *aa.object_ + b;
}

Var &Var::operator+=(const Var &other) const {
  if (object_ == nullptr) {
    throw d2c::Var(TypeFactory::CreateTypeError());
  }
  Object *a = const_cast<Object *>(object_);
  *this = Var(*a + other);
  return const_cast<Var &>(*this);
}

Var Var::operator-(const Var &other) const {
  if (object_ == nullptr) {
    throw d2c::Var(TypeFactory::CreateTypeError());
  }
  return *object_ - other;
}

Var Var::operator-(std::int64_t i) const {
  Var v = Var(TypeFactory::CreateInt(i));
  if (object_ == nullptr) {
    throw d2c::Var(TypeFactory::CreateTypeError());
  }
  return *object_ - v;
}

Var operator-(std::int64_t a, const Var &b) {
  Var aa = Var(TypeFactory::CreateInt(a));
  Object *bb = const_cast<Object *>(b.object_);
  if (bb == nullptr) {
    throw d2c::Var(TypeFactory::CreateTypeError());
  }
  return *aa.object_ - b;
}

Var Var::operator-(double d) const {
  Var v = Var(TypeFactory::CreateDouble(d));
  if (object_ == nullptr) {
    throw d2c::Var(TypeFactory::CreateTypeError());
  }
  return *object_ - v;
}

Var operator-(double a, const Var &b) {
  Var aa = Var(TypeFactory::CreateDouble(a));
  Object *bb = const_cast<Object *>(b.object_);
  if (bb == nullptr) {
    throw d2c::Var(TypeFactory::CreateTypeError());
  }
  return *aa.object_ - b;
}

Var &Var::operator-=(const Var &other) const {
  if (object_ == nullptr) {
    throw d2c::Var(TypeFactory::CreateTypeError());
  }
  Object *a = const_cast<Object *>(object_);
  *this = Var(*a - other);
  return const_cast<Var &>(*this);
}

Var Var::operator*(const Var &other) const {
  if (object_ == nullptr) {
    throw d2c::Var(TypeFactory::CreateTypeError());
  }
  return *object_ * other;
}

Var Var::operator*(std::int64_t i) const {
  Var v = Var(TypeFactory::CreateInt(i));
  if (object_ == nullptr) {
    throw d2c::Var(TypeFactory::CreateTypeError());
  }
  return *object_ * v;
}

Var operator*(std::int64_t a, const Var &b) {
  Var aa = Var(TypeFactory::CreateInt(a));
  Object *bb = const_cast<Object *>(b.object_);
  if (bb == nullptr) {
    throw d2c::Var(TypeFactory::CreateTypeError());
  }
  return *aa.object_ * b;
}

Var Var::operator*(double d) const {
  Var v = Var(TypeFactory::CreateDouble(d));
  if (object_ == nullptr) {
    throw d2c::Var(TypeFactory::CreateTypeError());
  }
  return *object_ * v;
}

Var operator*(double a, const Var &b) {
  Var aa = Var(TypeFactory::CreateDouble(a));
  Object *bb = const_cast<Object *>(b.object_);
  if (bb == nullptr) {
    throw d2c::Var(TypeFactory::CreateTypeError());
  }
  return *aa.object_ * b;
}

Var &Var::operator*=(const Var &other) const {
  if (object_ == nullptr) {
    throw d2c::Var(TypeFactory::CreateTypeError());
  }
  Object *a = const_cast<Object *>(object_);
  *this = Var(*a * other);
  return const_cast<Var &>(*this);
}

Var Var::operator/(const Var &other) const {
  if (object_ == nullptr) {
    throw d2c::Var(TypeFactory::CreateTypeError());
  }
  return *object_ / other;
}

Var Var::operator/(std::int64_t i) const {
  Var v = Var(TypeFactory::CreateInt(i));
  if (object_ == nullptr) {
    throw d2c::Var(TypeFactory::CreateTypeError());
  }
  return *object_ / v;
}

Var operator/(std::int64_t a, const Var &b) {
  Var aa = Var(TypeFactory::CreateInt(a));
  Object *bb = const_cast<Object *>(b.object_);
  if (bb == nullptr) {
    throw d2c::Var(TypeFactory::CreateTypeError());
  }
  return *aa.object_ / b;
}

Var Var::operator/(double d) const {
  Var v = Var(TypeFactory::CreateDouble(d));
  if (object_ == nullptr) {
    throw d2c::Var(TypeFactory::CreateTypeError());
  }
  return *object_ / v;
}

Var operator/(double a, const Var &b) {
  Var aa = Var(TypeFactory::CreateDouble(a));
  Object *bb = const_cast<Object *>(b.object_);
  if (bb == nullptr) {
    throw d2c::Var(TypeFactory::CreateTypeError());
  }
  return *aa.object_ / b;
}

Var &Var::operator/=(const Var &other) const {
  if (object_ == nullptr) {
    throw d2c::Var(TypeFactory::CreateTypeError());
  }
  Object *a = const_cast<Object *>(object_);
  *this = Var(*a / other);
  return const_cast<Var &>(*this);
}

Var Var::__intDiv(const Var &other) const {
  if (object_ == nullptr) {
    throw d2c::Var(TypeFactory::CreateTypeError());
  }
  return object_->__intDiv(other);
}

Var Var::__intDiv(std::int64_t i) const {
  Var v = Var(TypeFactory::CreateInt(i));
  if (object_ == nullptr) {
    throw d2c::Var(TypeFactory::CreateTypeError());
  }
  return object_->__intDiv(v);
}

Var __intDiv(std::int64_t a, const Var &b) {
  Var aa = Var(TypeFactory::CreateInt(a));
  Object *bb = const_cast<Object *>(b.object_);
  if (bb == nullptr) {
    throw d2c::Var(TypeFactory::CreateTypeError());
  }
  return aa.object_->__intDiv(b);
}

Var Var::__intDiv(double d) const {
  Var v = Var(TypeFactory::CreateDouble(d));
  if (object_ == nullptr) {
    throw d2c::Var(TypeFactory::CreateTypeError());
  }
  return object_->__intDiv(v);
}

Var __intDiv(double a, const Var &b) {
  Var aa = Var(TypeFactory::CreateDouble(a));
  Object *bb = const_cast<Object *>(b.object_);
  if (bb == nullptr) {
    throw d2c::Var(TypeFactory::CreateTypeError());
  }
  return aa.object_->__intDiv(b);
}

Var &Var::__intDivAssign(const Var &other) const {
  if (object_ == nullptr) {
    throw d2c::Var(TypeFactory::CreateTypeError());
  }
  *this = __intDiv(other);
  return const_cast<Var &>(*this);
}

Var Var::operator<<(const Var &other) const {
  if (object_ == nullptr) {
    throw d2c::Var(TypeFactory::CreateTypeError());
  }
  return *object_ << other;
}

Var Var::operator<<(std::int64_t rhv) const {
  Var a(TypeFactory::CreateInt(rhv));
  if (object_ == nullptr) {
    throw d2c::Var(TypeFactory::CreateTypeError());
  }
  return *object_ << a;
}

Var &Var::operator<<=(const Var &other) const {
  if (object_ == nullptr) {
    throw d2c::Var(TypeFactory::CreateTypeError());
  }
  Object *a = const_cast<Object *>(object_);
  *this = Var(*a << other);
  return const_cast<Var &>(*this);
}

Var &Var::operator<<=(std::int64_t rhs) const {
  if (object_ == nullptr) {
    throw d2c::Var(TypeFactory::CreateTypeError());
  }
  Object *a = const_cast<Object *>(object_);
  *this = Var(*a << rhs);
  return const_cast<Var &>(*this);
}

Var Var::operator>>(const Var &other) const {
  if (object_ == nullptr) {
    throw d2c::Var(TypeFactory::CreateTypeError());
  }
  return *object_ >> other;
}

Var Var::operator>>(std::int64_t rhv) const {
  Var a(TypeFactory::CreateInt(rhv));
  if (object_ == nullptr) {
    throw d2c::Var(TypeFactory::CreateTypeError());
  }
  return *object_ >> a;
}

Var &Var::operator>>=(const Var &other) const {
  if (object_ == nullptr) {
    throw d2c::Var(TypeFactory::CreateTypeError());
  }
  Object *a = const_cast<Object *>(object_);
  *this = Var(*a >> other);
  return const_cast<Var &>(*this);
}

Var &Var::operator>>=(std::int64_t rhs) const {
  if (object_ == nullptr) {
    throw d2c::Var(TypeFactory::CreateTypeError());
  }
  Object *a = const_cast<Object *>(object_);
  *this = Var(*a >> rhs);
  return const_cast<Var &>(*this);
}

Var Var::operator<(const Var &other) const {
  if (object_ == nullptr) {
    throw d2c::Var(TypeFactory::CreateTypeError());
  }
  return *object_ < other;
}

Var Var::operator<(std::int64_t i) const {
  Var other = i;
  if (object_ == nullptr) {
    throw d2c::Var(TypeFactory::CreateTypeError());
  }
  return *object_ < other;
}

Var operator<(std::int64_t a, const Var &b) {
  Var aa = Var(TypeFactory::CreateInt(a));
  Object *bb = const_cast<Object *>(b.object_);
  if (bb == nullptr) {
    throw d2c::Var(TypeFactory::CreateTypeError());
  }
  return *aa.object_ < b;
}

Var Var::operator<(double d) const {
  Var v = Var(TypeFactory::CreateDouble(d));
  if (object_ == nullptr) {
    throw d2c::Var(TypeFactory::CreateTypeError());
  }
  return *object_ < v;
}

Var operator<(double a, const Var &b) {
  Var aa = Var(TypeFactory::CreateDouble(a));
  Object *bb = const_cast<Object *>(b.object_);
  if (bb == nullptr) {
    throw d2c::Var(TypeFactory::CreateTypeError());
  }
  return *aa.object_ < b;
}

Var Var::operator<=(const Var &other) const {
  if (object_ == nullptr) {
    throw d2c::Var(TypeFactory::CreateTypeError());
  }
  return *object_ <= other;
}

Var Var::operator<=(std::int64_t i) const {
  Var other = i;
  if (object_ == nullptr) {
    throw d2c::Var(TypeFactory::CreateTypeError());
  }
  return *object_ <= other;
}

Var operator<=(std::int64_t a, const Var &b) {
  Var aa = Var(TypeFactory::CreateInt(a));
  Object *bb = const_cast<Object *>(b.object_);
  if (bb == nullptr) {
    throw d2c::Var(TypeFactory::CreateTypeError());
  }
  return *aa.object_ <= b;
}

Var Var::operator<=(double d) const {
  Var v = Var(TypeFactory::CreateDouble(d));
  if (object_ == nullptr) {
    throw d2c::Var(TypeFactory::CreateTypeError());
  }
  return *object_ <= v;
}

Var operator<=(double a, const Var &b) {
  Var aa = Var(TypeFactory::CreateDouble(a));
  Object *bb = const_cast<Object *>(b.object_);
  if (bb == nullptr) {
    throw d2c::Var(TypeFactory::CreateTypeError());
  }
  return *aa.object_ <= b;
}

Var Var::operator>(const Var &other) const {
  if (object_ == nullptr) {
    throw d2c::Var(TypeFactory::CreateTypeError());
  }
  return *object_ > other;
}

Var Var::operator>(std::int64_t i) const {
  Var v = Var(TypeFactory::CreateInt(i));
  if (object_ == nullptr) {
    throw d2c::Var(TypeFactory::CreateTypeError());
  }
  return *object_ > v;
}

Var operator>(std::int64_t a, const Var &b) {
  Var aa = Var(TypeFactory::CreateInt(a));
  Object *bb = const_cast<Object *>(b.object_);
  if (bb == nullptr) {
    throw d2c::Var(TypeFactory::CreateTypeError());
  }
  return *aa.object_ > b;
}

Var Var::operator>(double d) const {
  Var v = Var(TypeFactory::CreateDouble(d));
  if (object_ == nullptr) {
    throw d2c::Var(TypeFactory::CreateTypeError());
  }
  return *object_ > v;
}

Var operator>(double a, const Var &b) {
  Var aa = Var(TypeFactory::CreateDouble(a));
  Object *bb = const_cast<Object *>(b.object_);
  if (bb == nullptr) {
    throw d2c::Var(TypeFactory::CreateTypeError());
  }
  return *aa.object_ > b;
}

Var Var::operator>=(const Var &other) const {
  if (object_ == nullptr) {
    throw d2c::Var(TypeFactory::CreateTypeError());
  }
  return (*object_ >= other);
}

Var Var::operator>=(std::int64_t i) const {
  Var v = Var(TypeFactory::CreateInt(i));
  if (object_ == nullptr) {
    throw d2c::Var(TypeFactory::CreateTypeError());
  }
  return (*object_ >= v);
}

Var operator>=(std::int64_t a, const Var &b) {
  Var aa = Var(TypeFactory::CreateInt(a));
  Object *bb = const_cast<Object *>(b.object_);
  if (bb == nullptr) {
    throw d2c::Var(TypeFactory::CreateTypeError());
  }
  return *aa.object_ >= b;
}

Var Var::operator>=(double d) const {
  Var v = Var(TypeFactory::CreateDouble(d));
  if (object_ == nullptr) {
    throw d2c::Var(TypeFactory::CreateTypeError());
  }
  return (*object_ >= v);
}

Var operator>=(double a, const Var &b) {
  Var aa = Var(TypeFactory::CreateDouble(a));
  Object *bb = const_cast<Object *>(b.object_);
  if (bb == nullptr) {
    throw d2c::Var(TypeFactory::CreateTypeError());
  }
  return *aa.object_ >= b;
}

Var Var::operator==(const Var &other) const {
  if (isNull() && other.isNull()) {
    return true;
  }
  if (isNull() || other.isNull()) {
    return false;
  }
  return *object_ == other;
}

Var Var::operator==(std::int64_t i) const {
  Var v = Var(TypeFactory::CreateInt(i));
  if (object_ == nullptr) {
    return false;
  }
  return *object_ == v;
}

Var operator==(std::int64_t a, const Var &b) {
  Var aa = Var(TypeFactory::CreateInt(a));
  Object *bb = const_cast<Object *>(b.object_);
  if (bb == nullptr) {
    throw d2c::Var(TypeFactory::CreateTypeError());
  }
  return *aa.object_ == b;
}

Var Var::operator==(double d) const {
  Var v = Var(TypeFactory::CreateDouble(d));
  if (object_ == nullptr) {
    return false;
  }
  return *object_ == v;
}

Var operator==(double a, const Var &b) {
  Var aa = Var(TypeFactory::CreateDouble(a));
  Object *bb = const_cast<Object *>(b.object_);
  if (bb == nullptr) {
    throw d2c::Var(TypeFactory::CreateTypeError());
  }
  return *aa.object_ == b;
}

Var Var::operator==(const char *s) const {
  Var v = Var(TypeFactory::CreateString(s));
  if (object_ == nullptr) {
    return false;
  }
  return *object_ == v;
}

Var operator==(const char *a, const Var &b) {
  Var aa = Var(TypeFactory::CreateString(a));
  Object *bb = const_cast<Object *>(b.object_);
  if (bb == nullptr) {
    throw d2c::Var(TypeFactory::CreateTypeError());
  }
  return *aa.object_ == b;
}

Var operator==(const Str &a, const Var &b) {
  Var aa = Var(TypeFactory::CreateString(a));
  Object *bb = const_cast<Object *>(b.object_);
  if (bb == nullptr) {
    throw d2c::Var(TypeFactory::CreateTypeError());
  }
  return *aa.object_ == b;
}

Var Var::operator==(bool b) const {
  if (object_ == nullptr || is_type<Null>(*this)) {
    return false;
  }
  Var v(b ? TypeFactory::True() : TypeFactory::False());
  return (*object_ == v);
}

Var operator==(bool a, const Var &b) {
  Object *bb = const_cast<Object *>(b.object_);
  if (bb == nullptr || is_type<Null>(b)) {
    throw d2c::Var(TypeFactory::CreateTypeError());
  }
  Var aa(a ? TypeFactory::True() : TypeFactory::False());
  return *aa.object_ == b;
}

Var Var::operator!=(const Var &other) const {
  return !operator==(other);
}

Var Var::operator!=(std::int64_t i) const {
  return !operator==(i);
}

Var operator!=(std::int64_t a, const Var &b) {
  return !(a == b);
}

Var Var::operator!=(double d) const {
  return !operator==(d);
}

Var operator!=(double a, const Var &b) {
  return !(a == b);
}

Var Var::operator!=(const char *s) const {
  return !operator==(s);
}

Var operator!=(const char *a, const Var &b) {
  return !(a == b);
}

Var Var::operator!=(bool b) const {
  return !operator==(b);
}

Var operator!=(bool a, const Var &b) {
  return !(a == b);
}

Var Var::operator!() const {
  if (isNull()) {
    throw d2c::Var(TypeFactory::CreateTypeError());
  }
  return !(*object_);
}

Var Var::operator~() const {
  if (isNull()) {
    throw d2c::Var(TypeFactory::CreateTypeError());
  }
  return ~(*object_);
}

Var Var::operator-() const {
  if (isNull()) {
    throw d2c::Var(TypeFactory::CreateTypeError());
  }
  return -(*object_);
}

Var Var::operator&(const Var &b) const {
  if (object_ == nullptr || b.object_ == nullptr) {
    throw d2c::Var(TypeFactory::CreateTypeError());
  }
  return *object_ & b;
}

Var Var::operator&(std::int64_t b) const {
  if (object_ == nullptr) {
    throw d2c::Var(TypeFactory::CreateTypeError());
  }
  return *object_ & b;
}

Var Var::operator&(int b) const {
  if (object_ == nullptr) {
    throw d2c::Var(TypeFactory::CreateTypeError());
  }
  return *object_ & b;
}

Var operator&(std::int64_t a, const Var &b) {
  if (b.object_ == nullptr) {
    throw d2c::Var(TypeFactory::CreateTypeError());
  }
  return d2c::Var(a) & b;
}

Var &Var::operator&=(const Var &other) const {
  if (object_ == nullptr) {
    throw d2c::Var(TypeFactory::CreateTypeError());
  }
  Object *a = const_cast<Object *>(object_);
  *this = Var(*a & other);
  return const_cast<Var &>(*this);
}

Var Var::operator|(const Var &b) const {
  if (object_ == nullptr || b.object_ == nullptr) {
    throw d2c::Var(TypeFactory::CreateTypeError());
  }
  return *object_ | b;
}

Var Var::operator|(std::int64_t b) const {
  if (object_ == nullptr) {
    throw d2c::Var(TypeFactory::CreateTypeError());
  }
  return *object_ | b;
}

Var Var::operator|(int b) const {
  if (object_ == nullptr) {
    throw d2c::Var(TypeFactory::CreateTypeError());
  }
  return *object_ | b;
}

Var operator|(std::int64_t a, const Var &b) {
  if (b.object_ == nullptr) {
    throw d2c::Var(TypeFactory::CreateTypeError());
  }
  return d2c::Var(a) | b;
}

Var &Var::operator|=(const Var &other) const {
  if (object_ == nullptr) {
    throw d2c::Var(TypeFactory::CreateTypeError());
  }
  Object *a = const_cast<Object *>(object_);
  *this = Var(*a | other);
  return const_cast<Var &>(*this);
}

Var Var::operator^(const Var &b) const {
  if (object_ == nullptr || b.object_ == nullptr) {
    throw d2c::Var(TypeFactory::CreateTypeError());
  }
  return *object_ ^ b;
}

Var Var::operator^(std::int64_t b) const {
  if (object_ == nullptr) {
    throw d2c::Var(TypeFactory::CreateTypeError());
  }
  return *object_ ^ b;
}

Var Var::operator^(int b) const {
  if (object_ == nullptr) {
    throw d2c::Var(TypeFactory::CreateTypeError());
  }
  return *object_ ^ b;
}

Var operator^(std::int64_t a, const Var &b) {
  if (b.object_ == nullptr) {
    throw d2c::Var(TypeFactory::CreateTypeError());
  }
  return d2c::Var(a) ^ b;
}

Var &Var::operator^=(const Var &other) const {
  if (object_ == nullptr) {
    throw d2c::Var(TypeFactory::CreateTypeError());
  }
  Object *a = const_cast<Object *>(object_);
  *this = Var(*a ^ other);
  return const_cast<Var &>(*this);
}

Var Var::operator%(const Var &b) const {
  if (object_ == nullptr || b.object_ == nullptr) {
    throw d2c::Var(TypeFactory::CreateTypeError());
  }
  return *object_ % b;
}

Var Var::operator%(std::int64_t b) const {
  if (object_ == nullptr) {
    throw d2c::Var(TypeFactory::CreateTypeError());
  }
  return *object_ % b;
}

Var Var::operator%(int b) const {
  if (object_ == nullptr) {
    throw d2c::Var(TypeFactory::CreateTypeError());
  }
  return *object_ % b;
}

Var operator%(std::int64_t a, const Var &b) {
  Var c = b;
  if (b.isNull()) {
    throw d2c::Var(TypeFactory::CreateTypeError());
  }
  return d2c::Var(a) % c;
}

Var &Var::operator%=(const Var &other) const {
  if (object_ == nullptr) {
    throw d2c::Var(TypeFactory::CreateTypeError());
  }
  Object *a = const_cast<Object *>(object_);
  *this = Var(*a % other);
  return const_cast<Var &>(*this);
}

Var Var::operator&&(const Var &b) const {
  if (object_ == nullptr || b.object_ == nullptr) {
    throw d2c::Var(TypeFactory::CreateTypeError());
  }
  return *object_ && b;
}

Var Var::operator&&(bool b) const {
  if (object_ == nullptr) {
    throw d2c::Var(TypeFactory::CreateTypeError());
  }
  return *object_ && b;
}

Var operator&&(bool a, const Var &b) {
  if (b.object_ == nullptr) {
    throw d2c::Var(TypeFactory::CreateTypeError());
  }
  return d2c::Var(a) && b;
}

Var Var::operator||(const Var &b) const {
  if (object_ == nullptr || b.object_ == nullptr) {
    throw d2c::Var(TypeFactory::CreateTypeError());
  }
  return *object_ || b;
}

Var Var::operator||(bool b) const {
  if (object_ == nullptr) {
    throw d2c::Var(TypeFactory::CreateTypeError());
  }
  return *object_ || b;
}

Var operator||(bool a, Var b) {
  if (b.object_ == nullptr) {
    throw d2c::Var(TypeFactory::CreateTypeError());
  }
  return d2c::Var(a) || b;
}

Var Var::operator++() const {
  if (object_ == nullptr) {
    throw d2c::Var(TypeFactory::CreateTypeError());
  }
  Object *a = const_cast<Object *>(object_);
  *this = *a + d2c::Var(TypeFactory::One());
  return const_cast<Var &>(*this);
}

Var Var::operator--() const {
  if (object_ == nullptr) {
    throw d2c::Var(TypeFactory::CreateTypeError());
  }
  Object *a = const_cast<Object *>(object_);
  *this = *a - d2c::Var(TypeFactory::One());
  return const_cast<Var &>(*this);
}

Var Var::operator++(int amount) const {
  if (object_ == nullptr) {
    throw d2c::Var(TypeFactory::CreateTypeError());
  }
  // Postfix increments need to return the value of the object *prior* to
  // being incremeneted, so store the current value in a temp to be returned.
  d2c::Var tmp = *this;
  Object *a = const_cast<Object *>(object_);
  *this = *a + d2c::Var(TypeFactory::One());
  return tmp;
}

Var Var::operator--(int amount) const {
  if (object_ == nullptr) {
    throw d2c::Var(TypeFactory::CreateTypeError());
  }
  // Postfix decremenets need to return the value of the object *prior* to
  // being decremeneted, so store the current value in a temp to be returned.
  d2c::Var tmp = *this;
  Object *a = const_cast<Object *>(object_);
  *this = *a - d2c::Var(TypeFactory::One());
  return tmp;
}

Var Var::operator()() const {
  if (object_ == nullptr) {
    throw d2c::Var(TypeFactory::CreateTypeError());
  }
  return (*object_).invoke__(TK_call__);
}

Var Var::operator()(const Var &p1) const {
  if (object_ == nullptr) {
    throw d2c::Var(TypeFactory::CreateTypeError());
  }
  return (*object_).invoke__(TK_call__, p1);
}

Var Var::operator()(const Var &p1, const Var &p2) const {
  if (object_ == nullptr) {
    throw d2c::Var(TypeFactory::CreateTypeError());
  }
  return (*object_).invoke__(TK_call__, p1, p2);
}

Var Var::operator()(const Var &p1, const Var &p2, const Var &p3) const {
  if (object_ == nullptr) {
    throw d2c::Var(TypeFactory::CreateTypeError());
  }
  return (*object_).invoke__(TK_call__, p1, p2, p3);
}

Var Var::operator()(const Var &p1, const Var &p2, const Var &p3,
                    const Var &p4) const {
  if (object_ == nullptr) {
    throw d2c::Var(TypeFactory::CreateTypeError());
  }
  return (*object_).invoke__(TK_call__, p1, p2, p3, p4);
}

Var Var::operator()(const Var &p1, const Var &p2, const Var &p3, const Var &p4,
                    const Var &p5) const {
  if (object_ == nullptr) {
    throw d2c::Var(TypeFactory::CreateTypeError());
  }
  return (*object_).invoke__(TK_call__, p1, p2, p3, p4, p5);
}

Var Var::operator()(const Var &p1, const Var &p2, const Var &p3, const Var &p4,
                    const Var &p5, const Var &p6) const {
  if (object_ == nullptr) {
    throw d2c::Var(TypeFactory::CreateTypeError());
  }
  return (*object_).invoke__(TK_call__, p1, p2, p3, p4, p5, p6);
}

Var Var::operator()(const Var &p1, const Var &p2, const Var &p3, const Var &p4,
                    const Var &p5, const Var &p6, const Var &p7) const {
  if (object_ == nullptr) {
    throw d2c::Var(TypeFactory::CreateTypeError());
  }
  return (*object_).invoke__(TK_call__, p1, p2, p3, p4, p5, p6, p7);
}

Var Var::operator()(const Var &p1, const Var &p2, const Var &p3, const Var &p4,
                    const Var &p5, const Var &p6, const Var &p7,
                    const Var &p8) const {
  if (object_ == nullptr) {
    throw d2c::Var(TypeFactory::CreateTypeError());
  }
  return (*object_).invoke__(TK_call__, p1, p2, p3, p4, p5, p6, p7, p8);
}

Var Var::operator()(const Var &p1, const Var &p2, const Var &p3, const Var &p4,
                    const Var &p5, const Var &p6, const Var &p7, const Var &p8,
                    const Var &p9) const {
  if (object_ == nullptr) {
    throw d2c::Var(TypeFactory::CreateTypeError());
  }
  return (*object_).invoke__(TK_call__, p1, p2, p3, p4, p5, p6, p7, p8, p9);
}

Var Var::operator()(const Var &p1, const Var &p2, const Var &p3, const Var &p4,
                    const Var &p5, const Var &p6, const Var &p7, const Var &p8,
                    const Var &p9, const Var &p10) const {
  if (object_ == nullptr) {
    throw d2c::Var(TypeFactory::CreateTypeError());
  }
  return (*object_)
      .invoke__(TK_call__, p1, p2, p3, p4, p5, p6, p7, p8, p9, p10);
}

Var Var::getMember(const Token &name) const {
  if (object_ == nullptr) {
    throw d2c::Var(TypeFactory::CreateTypeError());
  }
  return object_->getMember__(name);
}

Var Var::invoke(const Token &name) const {
  if (object_ == nullptr) {
    throw d2c::Var(TypeFactory::CreateTypeError());
  }
  return object_->invoke__(name);
}

Var Var::invoke(const Token &name, const Var &p1) const {
  if (object_ == nullptr) {
    throw d2c::Var(TypeFactory::CreateTypeError());
  }
  return object_->invoke__(name, p1);
}

Var Var::invoke(const Token &name, const Var &p1, const Var &p2) const {
  if (object_ == nullptr) {
    throw d2c::Var(TypeFactory::CreateTypeError());
  }
  return object_->invoke__(name, p1, p2);
}

Var Var::invoke(const Token &name, const Var &p1, const Var &p2,
                const Var &p3) const {
  if (object_ == nullptr) {
    throw d2c::Var(TypeFactory::CreateTypeError());
  }
  return object_->invoke__(name, p1, p2, p3);
}

Var Var::invoke(const Token &name, const Var &p1, const Var &p2, const Var &p3,
                const Var &p4) const {
  if (object_ == nullptr) {
    throw d2c::Var(TypeFactory::CreateTypeError());
  }
  return object_->invoke__(name, p1, p2, p3, p4);
}

Var Var::invoke(const Token &name, const Var &p1, const Var &p2, const Var &p3,
                const Var &p4, const Var &p5) const {
  if (object_ == nullptr) {
    throw d2c::Var(TypeFactory::CreateTypeError());
  }
  return object_->invoke__(name, p1, p2, p3, p4, p5);
}

Var Var::invoke(const Token &name, const Var &p1, const Var &p2, const Var &p3,
                const Var &p4, const Var &p5, const Var &p6) const {
  if (object_ == nullptr) {
    throw d2c::Var(TypeFactory::CreateTypeError());
  }
  return object_->invoke__(name, p1, p2, p3, p4, p5, p6);
}

Var Var::invoke(const Token &name, const Var &p1, const Var &p2, const Var &p3,
                const Var &p4, const Var &p5, const Var &p6,
                const Var &p7) const {
  if (object_ == nullptr) {
    throw d2c::Var(TypeFactory::CreateTypeError());
  }
  return object_->invoke__(name, p1, p2, p3, p4, p5, p6, p7);
}

Var Var::invoke(const Token &name, const Var &p1, const Var &p2, const Var &p3,
                const Var &p4, const Var &p5, const Var &p6, const Var &p7,
                const Var &p8) const {
  if (object_ == nullptr) {
    throw d2c::Var(TypeFactory::CreateTypeError());
  }
  return object_->invoke__(name, p1, p2, p3, p4, p5, p6, p7, p8);
}

Var Var::invoke(const Token &name, const Var &p1, const Var &p2, const Var &p3,
                const Var &p4, const Var &p5, const Var &p6, const Var &p7,
                const Var &p8, const Var &p9) const {
  if (object_ == nullptr) {
    throw d2c::Var(TypeFactory::CreateTypeError());
  }
  return object_->invoke__(name, p1, p2, p3, p4, p5, p6, p7, p8, p9);
}

Var Var::invoke(const Token &name, const Var &p1, const Var &p2, const Var &p3,
                const Var &p4, const Var &p5, const Var &p6, const Var &p7,
                const Var &p8, const Var &p9, const Var &p10) const {
  if (object_ == nullptr) {
    throw d2c::Var(TypeFactory::CreateTypeError());
  }
  return object_->invoke__(name, p1, p2, p3, p4, p5, p6, p7, p8, p9, p10);
}

Var Var::cascade(const Var &b) {
  return *this;
}

Var Var::cascade(const Var &returnObj, const Token &name) const {
  invoke(name);
  return returnObj;
}

Var Var::cascade(const Token &name) const {
  invoke(name);
  return *this;
}

Var Var::cascade(const Var &returnObj, const Token &name, const Var &p1) const {
  invoke(name, p1);
  return returnObj;
}

Var Var::cascade(const Token &name, const Var &p1) const {
  invoke(name, p1);
  return *this;
}

Var Var::cascade(const Token &name, const Var &p1, const Var &p2) const {
  if (name == TK___array_set__) {
    __array_set(p1, p2);
  } else {
    invoke(name, p1, p2);
  }
  return *this;
}

Var Var::cascade(const Token &name, const Var &p1, const Var &p2,
                 const Var &p3) const {
  if (name == TK___array_set__) {
    p1.cascade(name, p2, p3);
  } else {
    invoke(name, p1, p2, p3);
  }
  return *this;
}

Var Var::cascade(const Token &name, const Var &p1, const Var &p2, const Var &p3,
                 const Var &p4) const {
  invoke(name, p1, p2, p3, p4);
  return *this;
}

Var Var::cascade(const Token &name, const Var &p1, const Var &p2, const Var &p3,
                 const Var &p4, const Var &p5) const {
  invoke(name, p1, p2, p3, p4, p5);
  return *this;
}

Var Var::cascade(const Token &name, const Var &p1, const Var &p2, const Var &p3,
                 const Var &p4, const Var &p5, const Var &p6) const {
  invoke(name, p1, p2, p3, p4, p5, p6);
  return *this;
}

Var Var::cascade(const Token &name, const Var &p1, const Var &p2, const Var &p3,
                 const Var &p4, const Var &p5, const Var &p6,
                 const Var &p7) const {
  invoke(name, p1, p2, p3, p4, p5, p6, p7);
  return *this;
}

Var Var::cascade(const Token &name, const Var &p1, const Var &p2, const Var &p3,
                 const Var &p4, const Var &p5, const Var &p6, const Var &p7,
                 const Var &p8) const {
  invoke(name, p1, p2, p3, p4, p5, p6, p7, p8);
  return *this;
}

Var Var::cascade(const Token &name, const Var &p1, const Var &p2, const Var &p3,
                 const Var &p4, const Var &p5, const Var &p6, const Var &p7,
                 const Var &p8, const Var &p9) const {
  invoke(name, p1, p2, p3, p4, p5, p6, p7, p8, p9);
  return *this;
}

Var Var::cascade(const Token &name, const Var &p1, const Var &p2, const Var &p3,
                 const Var &p4, const Var &p5, const Var &p6, const Var &p7,
                 const Var &p8, const Var &p9, const Var &p10) const {
  invoke(name, p1, p2, p3, p4, p5, p6, p7, p8, p9, p10);
  return *this;
}

Var Var::toString() const {
  if (object_ == nullptr) {
    static d2c::Var nullStr("null");
    return nullStr;
  }
  return object_->toString();
}

bool Var::implementsType(const Type &type) const {
  if (object_ == nullptr) {
    return type == Type(typeid(Null).hash_code());
  }

  return object_->inheritedClasses_.contains(type);
}

void Var::addClosureReference(Var *v, VarSet &visited) {
  if (visited.find(this) != visited.end()) {
    return;
  }
  visited.insert(this);
  if (v != this) {
    closureReferences_.insert(v);
    for (auto r : v->closureReferences_) {
      if (r != this) {
        closureReferences_.insert(r);
      }
    }
  }
  v->addClosureReference(this, visited);
  for (auto r : closureReferences_) {
    r->addClosureReference(v, visited);
  }
}

void Var::removeClosureReference(Var *v, VarSet &visited) {
  if (visited.find(this) != visited.end()) {
    return;
  }
  visited.insert(this);
  if (v == this) {
    return;
  }
  VarSet::iterator iter = closureReferences_.find(v);
  if (iter != closureReferences_.end()) {
    closureReferences_.erase(iter);
  }
  for (auto r : closureReferences_) {
    r->removeClosureReference(v, visited);
  }
}
}  // namespace d2c
#endif  // dart2cpp_var_impl_h

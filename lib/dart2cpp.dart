/****************************************************************************
 * Copyright (C) 2014 by Brendan Duncan.                                    *
 *                                                                          *
 * This file is part of Dart2Cpp,                                           *
 * https://github.com/brendan-duncan/dart2cpp.                              *
 *                                                                          *
 * Licensed under the Apache License, Version 2.0 (the "License");          *
 * you may not use this file except in compliance with the License.         *
 * You may obtain a copy of the License at                                  *
 *                                                                          *
 * http://www.apache.org/licenses/LICENSE-2.0                               *
 *                                                                          *
 * Unless required by applicable law or agreed to in writing, software      *
 * distributed under the License is distributed on an "AS IS" BASIS,        *
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. *
 * See the License for the specific language governing permissions and      *
 * limitations under the License.                                           *
 ****************************************************************************/
library dart2cpp;

import 'dart:io';
import 'package:analyzer/analyzer.dart';
import 'package:analyzer/src/generated/scanner.dart';
import 'package:analyzer/src/services/writer.dart';

part 'src/class_definition.dart';
part 'src/core_library.dart';
part 'src/library.dart';
part 'src/symbol.dart';
part 'src/translator.dart';

class D2COutput {
  String code;
  bool hasMainFunction;

  D2COutput(this.code, this.hasMainFunction);
}


D2COutput dart2cpp(String path, {bool loadDartCore: true}) {
  var t = new Translator(loadDartCore: loadDartCore);

  String cpp = t.translate(path);

  if (!t.hasMainFunction) {
    return new D2COutput(cpp, false);
  }

  cpp =
'''
#include <dart2cpp/dart2cpp.h>
$cpp
int main(int __argc, const char *__argv[]) {
''';

  if (t.hasMainArguments) {
    cpp +=
'''
  d2c::Var args = new List();
  for (int i = 0; i < __argc; ++i) {
    d2c::cast<List>(args).add(__argv[i]);
  }
  ${t.libraryList[0].namespace}::main__(args);
  return 0;
}
''';
  } else {
    cpp +=
'''
  ${t.libraryList[0].namespace}::main__();
  return 0;
}
''';
  }


  return new D2COutput(cpp, true);
}

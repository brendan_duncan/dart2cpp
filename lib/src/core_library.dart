/****************************************************************************
 * Copyright (C) 2014 by Brendan Duncan.                                    *
 *                                                                          *
 * This file is part of Dart2Cpp,                                           *
 * https://github.com/brendan-duncan/dart2cpp.                              *
 *                                                                          *
 * Licensed under the Apache License, Version 2.0 (the "License");          *
 * you may not use this file except in compliance with the License.         *
 * You may obtain a copy of the License at                                  *
 *                                                                          *
 * http://www.apache.org/licenses/LICENSE-2.0                               *
 *                                                                          *
 * Unless required by applicable law or agreed to in writing, software      *
 * distributed under the License is distributed on an "AS IS" BASIS,        *
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. *
 * See the License for the specific language governing permissions and      *
 * limitations under the License.                                           *
 ****************************************************************************/
part of dart2cpp;

/**
 * The dart2cpp library provides a few classes from the dart:core library,
 * enough to unit-test core translation features without having to pull in all
 * of dart:core. These classes are not intented to be full-featured
 * implementations of their dart:core counterparts, but enough to test with.
 * Their symbol declarations will be overwritten by the dart:core versions
 * if dart:core is loaded, so they will only be used if dart:core has not been
 * loaded. This Library class represents the dart2cpp library and declares the
 * symbols for its classes, which we have to do manually as that library is
 * not one that gets parsed.
 */
class CoreLibrary extends Library {
  CoreLibrary(Translator translator)
      : super('', translator, 0) {
    namespace = 'd2c';

    ClassDefinition cd = new ClassDefinition(null, 'Object', null);
    var s = _declareSymbol('Object', Symbol.CLASS, extra: cd);

    cd = new ClassDefinition(null, 'Double', 'Number');
    s = _declareSymbol('Double', Symbol.CLASS, extra: cd);

    cd = new ClassDefinition(null, 'Integer', 'Number');
    cd.staticMethods['parse'] = null;
    s = _declareSymbol('Integer', Symbol.CLASS, extra: cd);

    cd = new ClassDefinition(null, 'Bool', 'Object');
    cd.factoryConstructors['fromEnvironment'] = null;
    s = _declareSymbol('Bool', Symbol.CLASS, extra: cd);

    cd = new ClassDefinition(null, 'List', 'Object');
    cd.factoryConstructors['from'] = null;
    s = _declareSymbol('List', Symbol.CLASS, extra: cd);

    cd = new ClassDefinition(null, 'Map', 'Object');
    s = _declareSymbol('Map', Symbol.CLASS, extra: cd);

    cd = new ClassDefinition(null, 'Null', 'Object');
    s = _declareSymbol('Null', Symbol.CLASS, extra: cd);

    s = _declareSymbol('null', Symbol.VARIABLE);

    cd = new ClassDefinition(null, 'Number', 'Object');
    s = _declareSymbol('Number', Symbol.CLASS, extra: cd);

    s = _declareSymbol('print', Symbol.FUNCTION);

    cd = new ClassDefinition(null, 'String', 'Object');
    cd.methods['substring'] = null;
    cd.factoryConstructors['fromCharCodes'] = null;
    s = _declareSymbol('String', Symbol.CLASS, extra: cd);
  }
}


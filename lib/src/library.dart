/****************************************************************************
 * Copyright (C) 2014 by Brendan Duncan.                                    *
 *                                                                          *
 * This file is part of Dart2Cpp,                                           *
 * https://github.com/brendan-duncan/dart2cpp.                              *
 *                                                                          *
 * Licensed under the Apache License, Version 2.0 (the "License");          *
 * you may not use this file except in compliance with the License.         *
 * You may obtain a copy of the License at                                  *
 *                                                                          *
 * http://www.apache.org/licenses/LICENSE-2.0                               *
 *                                                                          *
 * Unless required by applicable law or agreed to in writing, software      *
 * distributed under the License is distributed on an "AS IS" BASIS,        *
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. *
 * See the License for the specific language governing permissions and      *
 * limitations under the License.                                           *
 ****************************************************************************/
part of dart2cpp;

/**
 * TODO This class is getting a bit rediculous. Yes, I am ashamed.
 * Refactor this code so that it's not such a monster.
 */
class Library implements AstVisitor<Object> {
  /// The maximum numer of arguments for a callable. This can be increased
  /// by adding additional parameter support to the Delegate, Closure,
  /// Var and Object classes.
  static const int MAX_ARGUMENTS = 10;

  String filePath;
  String libraryPath;
  String libraryName;
  String namespace;
  Translator translator;
  bool hasExternals = false;
  bool hasMainFunction = false;
  bool hasMainArguments = false;
  List symbolStack = [{}];
  List<String> symbolStackLabels = [];
  List imports = [];
  Map<String, Library> importMap = {};
  Map<String, Library> importNames = {};
  List parts = [];
  CompilationUnit ast;
  Map<String, ClassDeclaration> classes = {};
  Map<String, ClassDefinition> _definedClasses = {};
  Map<String, FunctionDeclaration> functions = {};
  Map<String, VariableDeclaration> variables = {};
  Map<String, VariableDeclaration> constants = {};
  SourceWriter out;
  List<ClassDefinition> classDefinitions = [];

  Library(this.filePath, this.translator, int index)
      : namespace = '__library_${index}' {
    // Give the core library a specific namespace so that we can
    // deterministically refer to classes within it, which are fundamental
    // classes.
    if (filePath.endsWith('dart-sdk/lib/core/core.dart')) {
      namespace = '__library_core';
    }
  }

  void load() {
    imports.add(new CoreLibrary(translator));

    libraryPath = new File(filePath).parent.path;
    ast = parseDartFile(filePath);

    loadTopLevelSymbols(ast);
    loadParts();
    loadImports();
  }

  void loadParts() {
    for (var n in ast.directives) {
      // part "source"
      if (n is PartDirective) {
        String partPath = '${libraryPath}/${n.uri.stringValue}';

        var partAst = parseDartFile(partPath);
        parts.add([n.uri.stringValue, partAst]);

        loadTopLevelSymbols(partAst);

      // library <name>
      } else if (n is LibraryDirective) {
        libraryName = n.name.toString();
        String ln = libraryName.replaceAll('.', '_');
        namespace = '${namespace}_${ln}';
      }
    }
  }

  void loadImports() {
    for (var n in ast.directives) {
      if (n is ImportDirective) {
        importLibrary(n);
      }
    }

    if (translator.loadDartCore) {
      if (!filePath.endsWith('core/core.dart')) {
        String corePath = '${translator.sdkPath}/core/core.dart';
        _importLibraryFile(corePath);
      }
    }
  }

  void importLibrary(ImportDirective node) {
    String importName = node.prefix != null ? node.prefix.toString() : '';
    String path = '';
    String uri = node.uri.stringValue;

    if (uri == 'dart:_internal') {
      uri = 'dart:internal';
    }

    if (uri.startsWith('dart:')) {
      uri = uri.substring('dart:'.length);
      path = '${translator.sdkPath}/$uri';
      if (!uri.endsWith('.dart')) {
        path += '/$uri.dart';
      }
    } else if (uri.startsWith('package:')) {
      uri = uri.substring('package:'.length);
      path = '${translator.packagePath}/$uri';
    } else {
      path = '${libraryPath}/$uri';
    }

    var lib = _importLibraryFile(path);

    if (importName.isNotEmpty) {
      _declareSymbol(importName, Symbol.LIBRARY, library: lib);
      importNames[importName] = lib;
    }
  }

  Library _importLibraryFile(String path) {
    if (importMap.containsKey(path)) {
      return importMap[path];
    }

    Library lib = translator.loadLibrary(path);
    imports.add(lib);
    importMap[path] = lib;

    return lib;
  }

  void loadTopLevelSymbols(CompilationUnit ast) {
    for (var n in ast.declarations) {
      // Global variables
      if (n is TopLevelVariableDeclaration) {
        for (var v in n.variables.variables) {
          var vp = v.parent;
          if (vp.keyword != null && vp.keyword.toString() == 'const') {
            constants[v.name.toString()] = v;
          } else {
            variables[v.name.toString()] = v;
          }

          _declareSymbol(v.name.toString(), Symbol.VARIABLE, node: v,
                         isStatic: true);
        }

      // Functions
      } else if (n is FunctionDeclaration) {
        if (n.externalKeyword != null) {
          hasExternals = true;
        }

        String name = n.name.toString();

        functions[name] = n;
        _declareSymbol(name, Symbol.FUNCTION, node: n);

        if (name == 'main') {
          hasMainFunction = true;
          hasMainArguments = n.functionExpression.parameters != null &&
                          n.functionExpression.parameters.parameters.length > 0;
        }

      // Classes
      } else if (n is ClassDeclaration) {
        classes[_getClassName(n.name)] = n;
        _declareSymbol(_getClassName(n.name).toString(), Symbol.CLASS,
                       node: n);
      // Typedefs
      } else if (n is FunctionTypeAlias) {
        _declareSymbol(_getClassName(n.name).toString(), Symbol.TYPEDEF,
                       node: n);
      // Class aliases
      } else if (n is ClassTypeAlias) {
        _declareSymbol(_getClassName(n.name).toString(), Symbol.CLASS_ALIAS,
                       node: n);
      } else {
        throw 'Unhandled top level symbol type: $n';
      }
    }
  }

  String _getClassName(name) {
    Symbol s = getSymbol(name);
    if (s != null) {
      if (s.type == Symbol.CLASS_ALIAS) {
        ClassTypeAlias ca = s.node;
        return _getClassName(ca.superclass.name.toString());
      }
      if (s.type == Symbol.CLASS){
        return s.name;
      }
    }

    return _renameKeyword(name);
  }

  String _renameKeyword(name) {
    name = name.toString();
    // Rename C++ keywords. A lot of these are due to VC++.
    Map keywordReplacements = {
      'bool': 'Bool',
      'int': 'Integer',
      'double': 'Double',
      'float': 'float__',
      'short': 'short__',
      'long': 'long__',
      'unsigned': 'unsigned__',
      'char': 'char__',
      'std': 'std__',
      'num': 'Number',
      'NULL': 'NULL__',
      'main': 'main__',
      'struct': 'struct__',
      'typeid': 'typeid__',
      'union': 'union__',
      'virtual': 'virtual__',
      'NAN': 'NAN__',
      'NaN': 'NaN__',
      'INFINITY': 'INFINITY__',
      '_SPACE': '_SPACE__',
      '_leave': '_leave__',
      '__value': '__value__',
      'operator': 'operator__',
      'static': 'static__',
      'typedef': 'typedef__',
      'export': 'export__'
    };

    if (keywordReplacements.containsKey(name)) {
      name = keywordReplacements[name];
    }

    return name;
  }

  String generateClassList() {
    out = new SourceWriter();
    // First find all class symbols and do a general declaration of their
    // existance:
    for (var n in classes.values) {
      out.writeln('class ${_getClassName(n.name)};');
    }
    if (classes.isNotEmpty) {
      out.writeln('');
    }
    return out.toString();
  }

  /**
   * Generate definitions for the top level symbols. Implementations will come
   * later. This way, symbols can use other symbols even if they are defined
   * out of order in the source.
   */
  String generateDefinitions() {
    out = new SourceWriter();

    _skipParameterValues = false;

    // Generate all class definitions:
    for (var n in classes.values) {
      _generateClassDefinition(n);
    }
    if (classes.values.isNotEmpty) {
      out.writeln('');
    }

    // Generate constants:
    for (var v in constants.values) {
      if (v.initializer is IntegerLiteral) {
        out.write('const d2c::Var ');
        _visitNode(v);
        out.writeln(';');
      } else if (v.initializer is DoubleLiteral) {
        out.write('const d2c::Var ');
        _visitNode(v);
        out.writeln(';');
      } else if (v.initializer is StringLiteral &&
                 v.initializer is! StringInterpolation) {
        out.write('const d2c::Var ');
        _inVariableDeclaration = true;
        _visitNode(v);
        _inVariableDeclaration = false;
        out.writeln(';');
      } else {
        String vName = _getClassName(v.name);
        out.write('d2c::Var &__get_${vName}() { ');
        out.write('static d2c::Var ${vName}__; ');
        out.write('if (${vName}__.object_ == NULL) { ${vName}__ = ');
        if (v.initializer == null) {
          out.write('null');
        } else {
          _visitNode(v.initializer);
        }
        out.write('; } ');
        out.writeln('return ${vName}__; }');
      }
    }
    if (constants.isNotEmpty) {
      out.writeln('');
    }

    // Generate all function definitions:
    for (var n in functions.values) {
      out.write('d2c::Var ');
      out.write(_getClassName(n.name));
      _pushSymbolStack('@1');
      _visitNode(n.functionExpression.parameters);
      _popSymbolStack('@1');
      out.writeln(';');
      out.writeln('');
    }
    if (functions.isNotEmpty) {
      out.writeln('');
    }

    // Then generate all global variables:
    for (var v in variables.values) {
      String vName = _getClassName(v.name);
      out.write('d2c::Var &__get_${vName}() { ');
      out.write('static d2c::Var ${vName}__; ');
      out.write('if (${vName}__.object_ == NULL) { ${vName}__ = ');
      if (v.initializer == null) {
        out.write('null');
      } else {
        _visitNode(v.initializer);
      }
      out.write('; } ');
      out.writeln('return ${vName}__; }');
    }
    if (variables.isNotEmpty) {
      out.writeln('');
    }

    return out.toString();
  }

  Symbol _generateInheritedClass(name) {
    Symbol ps = getSymbol(name);
    String nameStr = ps != null ? ps.name : name.toString();
    if (nameStr != 'd2c::Object') {
      if (ps != null) {
        if (ps.extra is ClassDefinition) {
          return ps;
        }

        if (!ps.library._definedClasses.containsKey(nameStr)) {
          if (ps.library == this) {
            var pc = classes[nameStr];
            var pcd = _generateClassDefinition(pc);
            ps = getSymbol(name);
            ps.extra = pcd;
          } else {
            // The parent class was defined in another library
            var pc = ps.library.classes[nameStr];
            out.writeln('}//namespace ${namespace}');
            out.writeln('namespace ${ps.library.namespace} {');
            ps.extra = ps.library._generateClassDefinition(pc, out);
            out.writeln('}//namespace ${ps.library.namespace}');
            out.writeln('namespace ${namespace} {');
          }
        } else {
          if (ps.extra == null) {
            ps.extra = ps.library._definedClasses[nameStr];
          }
        }
      }
    }

    return ps;
  }

  void _generateInterfaces(ClassDeclaration c, ClassDefinition cd,
                           Set<ClassDefinition> mixins,
                           Set<ClassDefinition> interfaces) {
    if (c.withClause != null) {
      for (var i in c.withClause.mixinTypes) {
        cd.interfaces.add(i.name);
        var sym = _generateInheritedClass(i.name.toString());
        if (sym == null || sym.extra == null) {
          throw 'Could not find mixin class definition';
        }
        if (mixins.contains(sym.extra)) {
          continue;
        }
        mixins.add(sym.extra);
        _generateInterfaces(sym.node, sym.extra, mixins, interfaces);
      }
    }

    if (c.implementsClause != null) {
      for (var i in c.implementsClause.interfaces) {
        cd.interfaces.add(i.name);
        var sym = _generateInheritedClass(i.name);
        if (sym == null || sym.extra == null) {
          throw 'Could not find interface class definition: ${i.name}';
        }
        if (interfaces.contains(sym.extra)) {
          continue;
        }
        interfaces.add(sym.extra);
        _generateInterfaces(sym.node, sym.extra, mixins, interfaces);
      }
    }
  }

  ClassDefinition _generateClassDefinition(ClassDeclaration c,
                                           [SourceWriter srcOut]) {
    // Probably a class alias.
    if (c == null) {
      return null;
    }

    String name = _getClassName(c.name);
    String parent = c.extendsClause == null ? 'd2c::Object' :
                    _getClassName(c.extendsClause.superclass.name);

    if (_definedClasses.containsKey(name)) {
      return _definedClasses[name];
    }

    SourceWriter savedOut = out;
    if (srcOut != null) {
      out = srcOut;
    }

    Symbol ps = _generateInheritedClass(/*c.extendsClause == null ? parent :
                                        c.extendsClause.superclass.name*/parent);

    Symbol cs = _declareSymbol(name, Symbol.CLASS, node: c);

    ClassDefinition cd = new ClassDefinition(c, name, parent);
    classDefinitions.add(cd);
    cs.extra = cd;
    if (ps != null) {
      cs.symbols.addAll(ps.symbols);
    }

    _definedClasses[name] = cd;

    Set<ClassDefinition> mixins = new Set<ClassDefinition>();
    Set<ClassDefinition> interfaces = new Set<ClassDefinition>();

    _generateInterfaces(c, cd, mixins, interfaces);

    _pushSymbolStack('@2');

    if (c.typeParameters != null) {
      var tp = c.typeParameters.typeParameters;
      for (int i = 0; i < tp.length; ++i) {
        _declareSymbol(tp[i].name.toString(), Symbol.TYPENAME, parent: cs,
                       extra: i);
      }
    }

    String pns = ps != null && ps.library != this
                 ? '${ps.library.namespace}::'
                 : '';

    String parentName = parent == 'd2c::Object' ? parent : '$pns$parent';

    out.writeln('class ${_getClassName(c.name)} : public $parentName {');
    out.writeln('public:');

    if (c.extendsClause != null) {
      out.writeln('typedef $parent super;');
    } else {
      out.writeln('typedef d2c::Object super;');
    }
    _declareSymbol('super', Symbol.CLASS, parent: cs);

    for (var m in c.members) {
      // Fields
      if (m is FieldDeclaration) {
        FieldDeclaration f = m;

        for (var v in f.fields.variables) {
          String vName = _getClassName(v.name);
          if (f.staticKeyword != null) {
            cd.staticFields[vName] = v;
          } else {
            cd.fields[vName] = v;

            if (v.initializer != null) {
              cd.initializedFields.add(v);
            }
          }
        }

        for (var v in f.fields.variables) {
          String vName = _getClassName(v.name);

          if (f.staticKeyword != null) {
            out.writeln('static d2c::Var &__get_$vName();');
          } else {
            out.writeln('d2c::Var $vName;');
          }

          _declareSymbol(vName, Symbol.VARIABLE, parent: cs, node: v,
                         isStatic: (f.staticKeyword != null));
        }

      // Constructor
      } else if (m is ConstructorDeclaration) {
        ConstructorDeclaration f = m;
        String fName = _getClassName(f.name);
        _declareSymbol(fName, Symbol.FUNCTION, parent: cs, extra: cd, node: m);

        if (f.externalKeyword != null) {
          hasExternals = true;
        }

        // We'll treat external constructors like external factories, so that
        // the external implementation in c++ can return a c++ implementation
        // of the class.
        if (f.externalKeyword != null || f.factoryKeyword != null) {
          // Factory constructor
          String fn = f.name != null ? fName : '';
          cd.factoryConstructors[fn] = f;
          out.write('static d2c::Var __factory_');
          _constructorFieldInitializers.clear();
          out.write(_getClassName(f.returnType));
          _visitNodeWithPrefix('_', f.name);

          _pushSymbolStack('@3');

          out.write('(const d2c::TypeSet &__types__');

          var params = f.parameters.parameters;
          bool hasNamed = false;
          for (var p in params) {
            if (p.kind == ParameterKind.NAMED) {
              hasNamed = true;
              break;
            }
            out.write(', ');
            _visitNode(p);
          }
          if (hasNamed) {
            out.write(', ');
            out.write('const d2c::Var &__named__ = '
                      'd2c::Var(new d2c::NamedArguments())');
          }
          out.write(')');

          _popSymbolStack('@3');
        } else {
          if (f.constKeyword != null) {
            // Const constructor
            String fn = f.name != null ? fName : '';
            cd.constConstructors[fn] = f;
            out.write('static d2c::Var __const_');
            _constructorFieldInitializers.clear();
            out.write(_getClassName(f.returnType));
            _visitNodeWithPrefix('_', f.name);

            _pushSymbolStack('@3');

            out.write('(const d2c::TypeSet &__types__');

            var params = f.parameters.parameters;
            bool hasNamed = false;
            for (var p in params) {
              if (p.kind == ParameterKind.NAMED) {
                hasNamed = true;
                break;
              }
              out.write(', ');
              _visitNode(p);
            }
            if (hasNamed) {
              out.write(', ');
              out.write('const d2c::Var &__named__ = '
                        'd2c::Var(new d2c::NamedArguments())');
            }
            out.writeln(');');

            _popSymbolStack('@3');
          }

          String constructorType = '';

          if (f.name != null) {
            // Named constructor
            cd.namedConstructors[fName] = f;
            constructorType = '__constructor__${_getClassName(fName)}__';
          } else {
            // Normal constructor
            cd.constructor = f;
            constructorType = '__constructor__';
          }

          out.write('struct $constructorType { ');
          out.write('d2c::TypeSet ids;');
          out.write(' $constructorType(const d2c::TypeSet &'
                    't = {}) : ids(t) { }');
          out.writeln(' };');

          out.write(_getClassName(c.name));
          out.write('(');

          int j = 0;

          out.write('const $constructorType &__types__');
          if (f.name == null && f.parameters.parameters.isEmpty) {
            out.write(' = {}');
          }
          j++;

          _constructorFieldInitializers.clear();

          _pushSymbolStack('@4');

          if (f.parameters.parameters.isNotEmpty) {
            if (j++ > 0) {
              out.write(', ');
            }
          }

          _visitParameterList(f.parameters.parameters);

          out.write(')');

          _popSymbolStack('@4');
        }

        out.writeln(';');

      // Method / operator
      } else if (m is MethodDeclaration) {
        _generateMethodDefinition(cd, cs, m);
      }
    }

    if (cd.constructor == null) {
      out.writeln('// Generated default constructor.');
      out.write('struct __constructor__ { ');
      out.write('d2c::TypeSet ids;');
      out.writeln(' __constructor__(const d2c::TypeSet &'
                  't = {}) : ids(t) { } };');
      out.write(_getClassName(c.name));
      out.writeln('(const __constructor__ &__types__ = {});\n');
    }

    if (cd.fields.isNotEmpty) {
      out.writeln('// Field getters/setters');
    }
    // Add getters and setters for fields
    for (var m in cd.fields.values) {
      String getter = '__get_${_renameKeyword(m.name)}';
      out.writeln('d2c::Var $getter();');
      _declareSymbol(getter, Symbol.FUNCTION, parent: cs, extra: cd);

      String setter = '__set_${_renameKeyword(m.name)}';
      out.writeln('d2c::Var $setter(const d2c::Var &value);');
      _declareSymbol(setter, Symbol.FUNCTION, parent: cs, extra: cd);
    }

    for (ClassDefinition mcd in mixins) {
      bool first = true;
      for (var m in mcd.methods.keys) {
        var f = mcd.methods[m];
        var ms = cs.getSymbol(m);
        if (f is MethodDeclaration && ms == null) {
          if (first) {
            out.writeln('// Mixin Methods From ${mcd.name}');
            first = false;
          }
          _generateMethodDefinition(cd, cs, f);
        }
      }
      first = true;
      for (var m in mcd.staticMethods.keys) {
        var f = mcd.staticMethods[m];
        var ms = cs.getSymbol(m);
        if (f != null && ms == null && f is MethodDeclaration) {
          if (first) {
            out.writeln('// Mixin Static Methods From ${mcd.name}');
            first = false;
          }
          _generateMethodDefinition(cd, cs, f);
        }
      }
    }

    for (ClassDefinition icd in interfaces) {
      bool first = true;
      for (var m in icd.methods.keys) {
        var f = icd.methods[m];
        var ms = cs.getSymbol(m);
        if (f != null && ms == null && f is MethodDeclaration) {
          if (first) {
            out.writeln('// Interface Methods From ${icd.name}');
            first = false;
          }
          _generateMethodDefinition(cd, cs, f, abstract: true);
        }
      }
    }

    // Add the initialize function, which prepares the methods table.
    out.writeln('virtual void initialize__();');

    out.writeln('static void updateClassInheritance__(d2c::Object *object,'
                'const d2c::TypeSet &__types__) {');
    bool first = true;
    out.writeln('object->addInheritance__<${_getClassName(c.name)}>'
                '(__types__);');

    // Add super class
    out.writeln('super::updateClassInheritance__(object, __types__);');

    // Add inherited classes from interfaces and mixins.
    for (var i in cd.interfaces) {
      var s = getSymbol(i);
      if (s != null && s.library != this && i is! PrefixedIdentifier) {
        out.write('${s.library.namespace}::');
      }
      _visitNode(i);
      out.writeln('::updateClassInheritance__(object, __types__);');
      first = false;
    }
    out.writeln('}');


    // Add getters for methods. Don't include getters and setters, as they've
    // already been delt with.
    for (var m in cd.methods.keys) {
      String suffix = '';
      int numArgs = 0;
      if (cd.methods[m] != null && cd.methods[m] is! int) {
        m = cd.methods[m];
      } else if (cd.methods[m] is int) {
        numArgs = cd.methods[m];
        suffix = '__${numArgs}';
        m = m.split('#')[0];
      }

      String n;
      if (m is String) {
        n = m;
      } else {
        n = m.name.toString();
        if (m.propertyKeyword != null) {
          if (m.propertyKeyword.toString() == 'get') {
            continue;
          } else if (m.propertyKeyword.toString() == 'set') {
            continue;
          }
        }
      }

      String tk = translator.getToken('${_renameKeyword(n)}');
      out.writeln('d2c::Var __get_${_renameKeyword(n)}${suffix}() { '
                  'return d2c::function(d2c::Var(this), '
                  'methods_[$tk][$numArgs]); }');
    }

    _popSymbolStack('@2');
    out.writeln('};\n');

    this.out = savedOut;

    return cd;
  }

  void _generateMethodDefinition(ClassDefinition cd, Symbol cs,
                                 MethodDeclaration f, {bool abstract: false}) {
    String fName = _getClassName(f.name);

    if (f.externalKeyword != null) {
      hasExternals = true;
    }

    bool isOperator = f.operatorKeyword != null;
    bool isStatic = false;
    String prefix = '';

    if (isOperator) {
      cd.operators.add(f);
    } else {
      int symType = Symbol.FUNCTION;

      if (f.propertyKeyword != null) {
        if (f.propertyKeyword.toString() == 'get') {
          prefix = '__get_';
          symType = Symbol.GETTER_SETTER;
        } else if (f.propertyKeyword.toString() == 'set') {
          prefix = '__set_';
          symType = Symbol.GETTER_SETTER;
        }
      }

      if (f.modifierKeyword != null) {
        if (f.modifierKeyword.toString() == 'static') {
          cd.staticMethods['$prefix$fName'] = f;
          isStatic = true;
        }
      }

      if (!isStatic) {
        cd.methods['$prefix$fName'] = !abstract ? f : null;
      }

      _declareSymbol(fName.toString(), symType, parent: cs, node: f, extra: cd);

      if (prefix != null) {
        _declareSymbol('$prefix$fName', symType, parent: cs, node: f,
                       extra: cd);
      }
    }

    _visitTokenWithSuffix(f.modifierKeyword, ' ');

    if (!isStatic) {
      out.write('virtual ');
    }

    out.write('d2c::Var ');

    if (isOperator && fName == '[]=') {
      out.write('__array_set');
    } else if (isOperator && fName == '~/') {
      out.write('__intDiv');
    } else if (isOperator && fName == '~/=') {
      out.write('__intDivAssign');
    } else {
      if (!isOperator) {
        out.write(prefix);
      } else {
        _visitTokenWithSuffix(f.operatorKeyword, ' ');
      }

      out.write(fName);
    }

    _functionParameters.clear();
    _optionalParameters.clear();
    _namedParameters.clear();

    if (f.isGetter) {
      out.write('()');
    } else {
      _pushSymbolStack('@5');
      _visitNode(f.parameters);
      _popSymbolStack('@5');
    }

    if (abstract) {
      out.writeln(' { return null; }');
    } else {
      out.writeln(';');
    }

    if (!isOperator) {
      int numArgs = _functionParameters.length;
      int numOptional = _optionalParameters.length;
      int numNamed = _namedParameters.length > 0 ? 1 : 0;
      int totalArgs = (numArgs > 0 ? 1 : 0) + numOptional + numNamed;

      if (numArgs == 0) {
        totalArgs++;
      }

      for (int i = 0; i < totalArgs; ++i) {
        if (isStatic) {
          out.write('static ');
          cd.staticMethods['${prefix}${fName}#${numArgs + i}'] = numArgs + i;
        } else {
          cd.methods['${prefix}${fName}#${numArgs + i}'] = numArgs + i;
        }

        out.write('d2c::Var ${prefix}${fName}__${numArgs + i}(');

        _pushSymbolStack('@5b');
        for (int j = 0; j < numArgs; ++j) {
          if (j != 0) {
            out.write(', ');
          }

          var parameter = _functionParameters[j];
          var s = _declareSymbol(parameter.identifier.toString(),
                                 Symbol.VARIABLE, node: parameter);
          _visitNode(_functionParameters[j]);
        }

        if (numNamed > 0 && i > 0) {
          if (numArgs != 0) {
            out.write(', ');
          }
          out.write('const d2c::Var &__named__ = '
                    'd2c::Var(new d2c::NamedArguments())) { ');
          out.write('return ${prefix}${fName}(');
          for (int j = 0; j < numArgs; ++j) {
            if (j != 0) {
              out.write(', ');
            }

            _visitNode(_functionParameters[j].identifier);
          }
          if (numArgs != 0) {
            out.write(', ');
          }
          out.writeln('__named__); }');

          _popSymbolStack('@5b');
          continue;
        }

        for (int j = 0; j < i && j < _optionalParameters.length; ++j) {
          if (numArgs != 0 || j != 0) {
            out.write(', ');
          }

          _visitNode(_optionalParameters[j]);
        }

        out.write(') { return ${prefix}${fName}(');

        for (int j = 0; j < numArgs; ++j) {
          if (j != 0) {
            out.write(', ');
          }

          _visitNode(_functionParameters[j].identifier);
        }

        for (int j = 0; j < i && j < _optionalParameters.length; ++j) {
          if (numArgs != 0 || j != 0) {
            out.write(', ');
          }

          _visitNode(_optionalParameters[j].identifier);
        }

        out.writeln('); }');
        _popSymbolStack('@5b');
      }
    }
  }

  void _generateStaticField(SourceWriter out, ClassDefinition c,
                            VariableDeclaration v) {
    String vName = _getClassName(v.name);
    out.write('d2c::Var &${c.name}::__get_${vName}() { ');
    out.write('static d2c::Var ${vName}__; ');
    out.write('if (${vName}__.object_ == NULL) { ${vName}__ = ');
    if (v.initializer == null) {
      out.write('null');
    } else {
      _visitNode(v.initializer);
    }
    out.write('; } ');
    out.writeln('return ${vName}__; }');
  }

  String generateImplementations() {
    out = new SourceWriter();

    _skipParameterValues = true;

    // Global Functions
    for (var f in functions.values) {
      _visitNode(f);
      out.writeln('');
    }
    if (functions.isNotEmpty) {
      out.writeln('');
    }

    // Classes
    for (var c in classDefinitions) {
      if (c.node == null) {
        continue;
      }

      _pushSymbolStack('@6');
      _generateClassImplementation(c, out);
      _popSymbolStack('@6');
    }
    if (classDefinitions.isNotEmpty) {
      out.writeln('');
    }

    return out.toString();
  }

  void _generateClassImplementation(ClassDefinition c, SourceWriter out) {
    _addFromSymbol(c.name);

    // Static Fields
    if (c.staticFields.isNotEmpty) {
      out.writeln('// ${c.name} Static Fields');
    }
    for (VariableDeclaration v in c.staticFields.values) {
      _generateStaticField(out, c, v);
    }

    // Static Methods
    if (c.staticMethods.isNotEmpty) {
      out.writeln('// ${c.name} Static Methods');
    }
    for (var m in c.staticMethods.values) {
      if (m == null || m is! MethodDeclaration) {
        continue;
      }
      String mName = _getClassName(m.name);

      String prefix = '';
      if (m.propertyKeyword != null) {
        if (m.propertyKeyword.toString() == 'get') {
          prefix = '__get_';
        } else if (m.propertyKeyword.toString() == 'set') {
          prefix = '__set_';
        }
      }

      out.write('d2c::Var ${c.name}::$prefix${mName}');

      _pushSymbolStack('@7');

      if (prefix == '__get_') {
        out.write('()');
      } else {
        _visitNode(m.parameters);
      }

      _nestedFunctionLevel++;
      if (m.externalKeyword != null) {
        out.write('{ return __external::');
        out.write('__static_${c.name}');
        if (m.name != null) {
          out.write('_${mName}');
        }
        out.write('(');
        bool first =  true;
        bool hasNamed = false;
        if (m.parameters != null) {
          for (var p in m.parameters.parameters) {
            if (p.kind == ParameterKind.NAMED) {
              hasNamed = true;
              break;
            }
            if (!first) {
              out.write(', ');
            }
            _visitNode(p.identifier);
            first = false;
          }
          if (hasNamed) {
            if (!first) {
              out.write(', ');
            }
            out.write('__named__');
          }
        }
        out.writeln('); }');
      } else {
        _visitNodeWithPrefix(' ', m.body);
      }

      _nestedFunctionLevel--;

      _popSymbolStack('@7');
    }

    out.writeln('');

    // Base constructor
    _generateConstructorImplementation(c, c.constructor);

    // Named constructors
    if (c.namedConstructors.isNotEmpty) {
      out.writeln('// ${c.name} Named Constructors');
    }
    for (var m in c.namedConstructors.values) {
      _pushSymbolStack('@8');
      _addFromSymbol(c.name);
      _generateConstructorImplementation(c, m);
      _popSymbolStack('@8');
    }

    // Const constructors
    if (c.constConstructors.isNotEmpty) {
      out.writeln('// ${c.name} Const Constructors');
    }
    for (var m in c.constConstructors.values) {
      String mName = _getClassName(m.name);
      out.write('d2c::Var ${_getClassName(c.name)}::');
      out.write('__const_${_getClassName(c.name)}');
      _visitNodeWithPrefix('_', m.name);

      _pushSymbolStack('@9');

      out.write('(const d2c::TypeSet &__types__');
      _declareSymbol('__types__', Symbol.VARIABLE);

      var params = m.parameters.parameters;
      bool hasNamed = false;
      for (var param in params) {
        _declareSymbol(param.identifier.toString(), Symbol.VARIABLE,
                       node: param);

        if (param.kind == ParameterKind.NAMED) {
          hasNamed = true;
          continue;
        }
        out.write(', ');
        _visitNode(param);
      }
      if (hasNamed) {
        out.write(', const d2c::Var &__named__');
      }
      out.writeln(') {');
      out.writeln('static std::unordered_map<size_t, d2c::Var> cache_;');
      out.write('size_t id = static_cast<size_t>(');
      hasNamed = false;
      bool first = true;
      for (var param in params) {
        if (param.kind == ParameterKind.NAMED) {
          hasNamed = true;
          break;
        }
        if (!first) {
          out.write(' + ');
        }
        first = false;
        _visitNode(param.identifier);
        out.write('.invoke(TK___get_hashCode__).intValue()');
      }
      if (hasNamed) {
        if (!first) {
          out.write(' + ');
        }
        first = false;
        out.write('__named__.invoke(TK___get_hashCode__).intValue()');
      }
      if (first) {
        out.write('0');
      }
      out.writeln(');');

      out.writeln('if (cache_.find(id) != cache_.end()) {');
      out.writeln('return cache_[id];');
      out.writeln('}');

      out.write('d2c::Var obj(new ${_getClassName(c.name)}');
      out.write('(');
      out.write('__constructor');
      _visitNodeWithPrefix('__', m.name);
      out.write('__(__types__)');

      hasNamed = false;
      for (var p in m.parameters.parameters) {
        if (p.kind == ParameterKind.NAMED) {
          hasNamed = true;
          break;
        }
        out.write(', ');
        _visitNode(p.identifier);
      }
      if (hasNamed) {
        out.write(', ');
        out.write('__named__');
      }
      out.writeln('));');
      out.writeln('cache_[id] = obj;');
      out.writeln('return obj;');
      out.writeln('}');

      _popSymbolStack('@9');
    }

    // Factory constructors
    if (c.factoryConstructors.isNotEmpty) {
      out.writeln('// ${c.name} Factory Constructors');
    }
    for (var m in c.factoryConstructors.values) {
      String mName = _getClassName(m.name);
      out.write('d2c::Var ${c.name}::__factory_${_getClassName(c.name)}');
      _visitNodeWithPrefix('_', m.name);

      _pushSymbolStack('@9');

      out.write('(const d2c::TypeSet &__types__');
      _declareSymbol('__types__', Symbol.VARIABLE);

      var params = m.parameters.parameters;
      bool hasNamed = false;
      for (var param in params) {
        _declareSymbol(param.identifier.toString(), Symbol.VARIABLE,
                       node: param);

        if (param.kind == ParameterKind.NAMED) {
          hasNamed = true;
          continue;
        }
        out.write(', ');
        _visitNode(param);
      }
      if (hasNamed) {
        out.write(', const d2c::Var &__named__');
      }
      out.write(') ');

      if (m.externalKeyword != null) {
        out.write(' { return __external::');
        out.write('__factory_${c.name}');
        if (m.name != null) {
          out.write('_${mName}');
        }
        out.write('(__types__');

        bool first =  false;
        bool hasNamed = false;
        for (var p in m.parameters.parameters) {
          if (p.kind == ParameterKind.NAMED) {
            hasNamed = true;
            break;
          }
          if (!first) {
            out.write(', ');
          }
          _visitNode(p.identifier);
          first = false;
        }
        if (hasNamed) {
          if (!first) {
            out.write(', ');
          }
          out.write('__named__');
        }
        out.writeln('); }');
      } else {
        if (m.redirectedConstructor != null) {
          bool isFactoryConstructor(Symbol s, String name) {
            if (s != null) {
              if ((s.node != null &&
                    s.node is ConstructorDeclaration &&
                    s.node.factoryKeyword != null)) {
                return true;
              }
              if (s.type == Symbol.CLASS && name.isNotEmpty) {
                ClassDefinition cd = s.extra;
                if (cd != null) {
                  for (var f in cd.factoryConstructors.keys) {
                    String cn = _getPrefixedNodeString('', f);
                    if (cn == name || (cn.isEmpty && name == s.name)) {
                      return true;
                    }
                  }
                }
              }
            }
            return false;
          }

          out.write('{ ');
          ConstructorName rc = m.redirectedConstructor;
          Symbol rcs = getSymbol(rc.type.name);
          String rcname = rc.name != null ? rc.name.toString() :
                    rc.type.name is PrefixedIdentifier ?
                    (rc.type.name as PrefixedIdentifier).identifier.toString() :
                    '';
          bool isFactory = false;

          // Check to see if this is a factory constructor.
          if (isFactoryConstructor(rcs, rcname)) {
            ClassDefinition cd = rcs.extra;

            if (cd != null) {
              isFactory = true;
              out.write('return ');
              // Factory constructor...
              if (rcs != null && rcs.library != this) {
                out.write('${rcs.library.namespace}::');
              }
              out.write('${cd.name}::__factory_${cd.name}');
              if (rcname.isNotEmpty && rcname != cd.name) {
                out.write('_${rcname}');
              }
            }
          }

          String className;
          String constName;

          if (!isFactory) {
            out.write(' return d2c::Var(new ');
            if (rcs != null && rcs.library != this) {
              out.write('${rcs.library.namespace}::');
            }

            if (rc.type.name is PrefixedIdentifier) {
              PrefixedIdentifier id = rc.type.name;
              Symbol ps = getSymbol(id.prefix);
              if (ps != null && ps.type == Symbol.LIBRARY){
                className = _getClassName(id.identifier);
              } else {
                className = _getClassName(id.prefix);
                constName = _renameKeyword(id.identifier);
              }
              out.write(className);
            } else {
              if (rc.name != null) {
                constName = _renameKeyword(rc.name);
              }
              className = _getClassName(rc.type.name);
              out.write(className);
            }
          }

          out.write('(');

          bool first = false;

          if (!isFactory) {
            if (rcs != null && rcs.library != this) {
              out.write('${rcs.library.namespace}::');
            }
            out.write('$className::__constructor__');

            if (constName != null) {
              out.write('${constName}__');
            }
            out.write('(__types__)');
            first = false;
          } else {
            out.write('__types__');
          }

          bool hasNamed = false;
          for (var p in m.parameters.parameters) {
            if (p.kind == ParameterKind.NAMED) {
              hasNamed = true;
              break;
            }
            if (!first) {
              out.write(', ');
            }
            _visitNode(p.identifier);
            first = false;
          }
          if (hasNamed) {
            if (!first) {
              out.write(', ');
            }
            out.write('__named__');
          }

          if (!isFactory) {
            out.writeln(')');
          }

          out.writeln('); }');
        } else {
          _functionDepth++;
          _nestedFunctionLevel++;
          _visitNode(m.body);
          _nestedFunctionLevel--;
          _functionDepth--;
        }
      }

      _popSymbolStack('@9');
    }

    // Field getters/setters
    if (c.fields.isNotEmpty) {
      out.writeln('// ${c.name} Fields');
    }
    for (var m in c.fields.values) {
      String mName = _renameKeyword(m.name);

      out.writeln('d2c::Var ${c.name}::__get_${mName}() { return ${mName}; }');

      out.writeln('d2c::Var ${c.name}::__set_${mName}(const d2c::Var &v__) '
                  '{ this->${mName} = v__; return this->${mName}; }');
    }

    // Operators
    if (c.operators.isNotEmpty) {
      out.writeln('// ${c.name} Operators');
    }
    for (var m in c.operators) {
      String mName = _getClassName(m.name);
      if (mName == '[]=') {
        out.write('d2c::Var ${c.name}::__array_set');
      } else if (mName == '~/') {
        out.write('d2c::Var ${c.name}::__intDiv');
      } else {
        out.write('d2c::Var ${c.name}::operator ${mName}');
      }

      _pushSymbolStack('@10');

      _visitNode(m.parameters);
      _nestedFunctionLevel++;

      if (m.body is EmptyFunctionBody) {
        if (mName == '~/') {
          out.write('{ return super::__intDiv(');
        } else if (mName == '[]=') {
          out.write('{ return super::__array_set(');
        } else {
          out.write('{ return super::operator ${mName}(');
        }
        bool first = true;
        var parameters = m.parameters.parameters;
        for (var p in parameters) {
          if (!first) {
            out.write(', ');
          }
          _visitNode(p.identifier);
          first = false;
        }
        out.writeln('); }');
      } else {
        _visitNodeWithPrefix(' ', m.body);
      }
      _nestedFunctionLevel--;
      out.writeln('');

      _popSymbolStack('@10');
    }

    // Instance Methods
    if (c.methods.isNotEmpty) {
      out.writeln('// ${c.name} Methods');
    }
    for (var mk in c.methods.keys) {
      var m = c.methods[mk];
      if (m == null || m is String || m is int) {
        continue;
      }
      String mName = _getClassName(m.name);
      String prefix = '';
      String n;
      if (m is String) {
        n = m;
      } else {
        n = mName;
        if (m.propertyKeyword != null) {
          if (m.propertyKeyword.toString() == 'get') {
            prefix = '__get_';
          } else if (m.propertyKeyword.toString() == 'set') {
            prefix = '__set_';
          }
        }
      }

      out.write('d2c::Var ${c.name}::${prefix}${n}');

      _pushSymbolStack('@11');

      if (m is String || m.parameters == null) {
        out.write('()');
      } else {
        _visitNode(m.parameters);
      }

      out.write(' ');
      _nestedFunctionLevel++;
      if (m is MethodDeclaration && m.externalKeyword != null) {
        out.write('{ return __external::__method_${c.name}_${prefix}${n}(');
        out.write('this');

        bool first =  false;
        bool hasNamed = false;
        if (m.parameters != null) {
          for (var p in m.parameters.parameters) {
            if (p.kind == ParameterKind.NAMED) {
              hasNamed = true;
              break;
            }
            if (!first) {
              out.write(', ');
            }
            _visitNode(p.identifier);
            first = false;
          }
        }
        if (hasNamed) {
          if (!first) {
            out.write(', ');
          }
          out.write('__named__');
        }

        out.writeln('); }');
      } else {
        _visitNode(m.body);
      }
      _nestedFunctionLevel--;
      _popSymbolStack('@11');
    }

    // initialize__ method
    out.writeln('void ${c.name}::initialize__() {');
    out.writeln('super::initialize__();');

    for (var m in c.fields.keys) {
      String mName = m;
      String tk = translator.getToken('$mName');
      out.writeln('members_[$tk] = &${mName};');
    }

    for (var m in c.methods.keys) {
      int numArgs = 0;
      bool hasArgs = false;
      if (c.methods[m] != null && c.methods[m] is! String &&
          c.methods[m] is! int) {
        m = c.methods[m];
      } else if (c.methods[m] is int) {
        numArgs = c.methods[m];
        hasArgs = true;
        m = m.split('#')[0];
      }

      String prefix = '';
      String n;
      if (m is String) {
        n = _renameKeyword(m);
      } else {
        String mName = _renameKeyword(m.name);
        n = mName;
        if (m.propertyKeyword != null) {
          if (m.propertyKeyword.toString() == 'get') {
            prefix = '__get_';
          } else if (m.propertyKeyword.toString() == 'set') {
            prefix = '__set_';
          }
        }
      }

      String tk = translator.getToken('$prefix$n');
      out.writeln('methods_[$tk][$numArgs] = d2c::bind(this, '
                  '&${c.name}::$prefix$n${hasArgs ? '__$numArgs' : ''});');

      if (prefix.isEmpty) {
        tk = translator.getToken('__get_$n');
        out.writeln('methods_[$tk][0] = d2c::bind(this, '
                    '&${c.name}::__get_$n${hasArgs ? '__$numArgs' : ''});');
      } else if (prefix == '__get_') {
        String ntk = translator.getToken(n);
        out.writeln('getters_[$ntk] = methods_[$tk][0];');
      }
    }

    for (var m in c.fields.values) {
      String mName = _getClassName(m.name);
      String tk = translator.getToken('__get_${mName}');
      out.writeln('methods_[$tk][0] = d2c::bind(this, '
                  '&${c.name}::__get_${mName});');

      tk = translator.getToken('__set_${mName}');
      out.writeln('methods_[$tk][1] = d2c::bind(this, '
                  '&${c.name}::__set_${mName});');
    }

    out.writeln('}');
  }

  void _generateConstructorImplementation(ClassDefinition c, var constructor) {
    String name = constructor != null && constructor.name != null ?
                  constructor.name.toString() : '';
    if (name.isNotEmpty) {
      out.writeln('// Named Constructor: ${c.name}.${name}');
    } else {
      out.writeln('// Constructor: ${c.name}');
    }
    out.write('${c.name}::${c.name}');
    _constructorFieldInitializers.clear();

    Map namedParams = constructor != null && constructor.parameters != null
        ? _getNamedParameters(constructor.parameters.parameters)
        : {};

    _pushSymbolStack('@12');

    if (constructor != null && constructor.parameters != null) {
      var parms = constructor.parameters.parameters;
      for (var p in parms) {
        _declareSymbol(p.identifier.toString(), Symbol.VARIABLE, node: p);
      }
    }

    String constructorType;
    constructorType = '__constructor__';
    if (name.isNotEmpty) {
      // Named constructor
      constructorType = '$constructorType${_renameKeyword(name)}__';
    }

    out.write('(');

    int j = 0;

    out.write('const $constructorType &__types__');
    j++;

    _constructorFieldInitializers.clear();

    if (constructor != null) {
      _pushSymbolStack('@13');
      if (constructor.parameters.parameters.isNotEmpty) {
        if (j++ > 0) {
          out.write(', ');
        }
      }
      _visitParameterList(constructor.parameters.parameters);
      _popSymbolStack('@13');
    }

    out.write(')');

    Set<String> initialized = new Set<String>();
    if (constructor != null && constructor.initializers.isNotEmpty) {
      for (int i = 0; i < constructor.initializers.length; ++i) {
        var ci = constructor.initializers[i];
        if (ci is ConstructorFieldInitializer) {
          ConstructorFieldInitializer v = ci;
          initialized.add(v.fieldName.toString());
        }
      }
    }

    int initliazerCount = 0;

    bool hasSuperInvocation = false;
    if (constructor == null) {
      out.write('\n: super(super::__constructor__(__types__.ids))');
      hasSuperInvocation = true;
    } else if (constructor.initializers.isNotEmpty) {
      for (int i = 0; i < constructor.initializers.length; ++i) {
        var ci = constructor.initializers[i];
        if (ci is SuperConstructorInvocation) {
          hasSuperInvocation = true;
          SuperConstructorInvocation sci = ci;
          if (initliazerCount > 0) {
            out.write('\n, ');
          } else {
            out.write('\n: ');
          }

          _inheritedNamedParams = namedParams;

          out.write('super(super::__constructor__');
          if (sci.constructorName != null) {
            out.write('${sci.constructorName}__');
          }
          out.write('(__types__.ids)');

          _nestedFunctionLevel++;
          _functionDepth++;
          bool first = true;
          var args = sci.argumentList.arguments;
          if (args.isNotEmpty) {
            out.write(', ');
          }

          _visitArguments(args);

          _functionDepth--;
          _nestedFunctionLevel--;
          out.write(')');

          _inheritedNamedParams = {};
        }
      }
    }

    if (!hasSuperInvocation) {
      if (initliazerCount > 0) {
        out.write('\n, ');
      } else {
        out.write('\n: ');
      }
      out.write('super(super::__constructor__(__types__.ids))');
    }

    out.writeln(' {');

    _functionDepth++;

    // Initialize inheritance set
    out.writeln('updateClassInheritance__(this, __types__.ids);');

    if (c.node.typeParameters != null) {
      var tp = c.node.typeParameters.typeParameters;
      for (int i = 0; i < tp.length; ++i) {
        TypeParameter t = tp[i];
        String tk = translator.getToken(t.name);
        out.writeln('typeParameters_[$tk] = $i < __types__.ids.size() ? '
                    'std::shared_ptr<d2c::Type>(new d2c::Type('
                    '__types__.ids[$i])) : '
                    'std::shared_ptr<d2c::Type>(new d2c::Type('
                    'd2c::objectType<d2c::Object>()));');
      }
    }

    if (_constructorFieldInitializers.isNotEmpty) {
      for (var m in _constructorFieldInitializers) {
        out.writeln('this->${_renameKeyword(m.identifier)} = '
                    '${_renameKeyword(m.identifier)};');
      }
    }

    if (constructor != null && constructor.externalKeyword != null) {
      out.write('__external::__constructor__${c.name}__');
      if (name.isNotEmpty) {
        // Named constructor
        out.write('_${_getClassName(name)}');
      }
      out.write('(this');
      if (constructor != null && constructor.parameters != null) {
        var parms = constructor.parameters.parameters;
        bool first = false;
        bool hasNamed = false;
        for (var p in parms) {
          if (p.kind == ParameterKind.NAMED) {
            hasNamed = true;
            break;
          }
          if (!first) {
            out.write(', ');
          }
          _visitNode(p.identifier);
          first = false;
        }
        if (hasNamed) {
          if (!first) {
            out.write(', ');
          }
          out.write('__named__');
        }
      }
      out.writeln(');');
    }

    if (constructor != null) {
      if (namedParams.isNotEmpty) {
        for (var p in namedParams.values) {
          String name;
          if (p.parameter is FunctionTypedFormalParameter) {
            var prm = p.parameter;
            name = prm.identifier.toString();
          } else {
            name = p.parameter.identifier.toString();
          }

          String tk = translator.getToken(name);

          String tkContainsKey = translator.getToken('containsKey');
          if (p.parameter is FieldFormalParameter) {
            out.write('this->${name} = ');
          } else {
            out.write('d2c::Var ${name} = ');
            _declareSymbol(name, Symbol.VARIABLE);
          }

          String dv = p.defaultValue.toString();
          var dvs = getSymbol(p.defaultValue);
          if (dvs != null) {
            dv = dvs.name;
            if (dvs.type == Symbol.VARIABLE && dvs.isStatic) {
              dv = '__get_${dv}()';
            }
            if (dvs.library != this) {
              dv = '${dvs.library.namespace}::$dv';
            }
          }

          out.writeln('d2c::cast<d2c::NamedArguments>(__named__).contains($tk) ? '
                      'd2c::cast<d2c::NamedArguments>(__named__).get($tk) : '
                      'd2c::Var(${dv});');
        }
      }
    }

    if (c.initializedFields.isNotEmpty) {
      for (int i = 0; i < c.initializedFields.length; ++i) {
        // don't use class-level field initializations when they are
        // initialized in the constructor.
        var f = c.initializedFields[i];
        if (initialized.contains(f.name.toString())) {
          continue;
        }

        _visitNode(f.name);
        out.write(' = ');
        _visitNode(f.initializer);
        out.writeln(';');
      }
    }

    if (constructor != null) {
      for (int i = 0; i < constructor.initializers.length; ++i) {
        var ci = constructor.initializers[i];
        if (ci is ConstructorFieldInitializer) {
          ConstructorFieldInitializer v = ci;
          if (v.keyword.toString() == 'this') {
            out.write('this->');
          }
          out.write('${_renameKeyword(v.fieldName)} = ');
          _visitNode(v.expression);
          out.writeln(';');
        }
      }

      if (constructor.body is! EmptyFunctionBody) {
        _inConstructor = true;
        _visitNodeList(constructor.body.block.statements);
        _inConstructor = false;
      }
    }

    _functionDepth--;

    out.writeln('}');
    _popSymbolStack('@12');
  }

  void _pushSymbolStack(String label) {
    symbolStack.add(new Map.from(symbolStack.last));
    symbolStackLabels.add(label);
  }

  void _popSymbolStack(String label) {
    symbolStack.removeLast();
    if (symbolStack.isEmpty) {
      throw 'Symbol stack underflow';
    }

    if (symbolStackLabels.last != label) {
      print('Unbalanced Symbol Stack: $label '
            '(${symbolStackLabels.last} found)');
    }
    symbolStackLabels.removeLast();
  }

  void _addFromSymbol(String symbol) {
    if (symbolStack.last.containsKey(symbol)) {
      symbolStack.last.addAll(symbolStack.last[symbol].symbols);
    }
  }

  Symbol _declareSymbol(String name, int type, {Symbol parent, var node,
                        var library, var extra, bool isStatic: false}) {
    Symbol s = new Symbol(name, type);
    symbolStack.last[name] = s;
    if (parent != null) {
      parent.symbols[name] = s;
    }
    s.library = library == null ? this : library;
    s.node = node;
    s.extra = extra;
    s.isStatic = isStatic;
    return s;
  }

  Symbol getSymbol(name, [Set<String> recursionProtector]) {
    if (recursionProtector == null) {
      recursionProtector = new Set<String>();
    }

    if (recursionProtector.contains(filePath)) {
      return null;
    }
    recursionProtector.add(filePath);

    if (name is PrefixedIdentifier) {
      String prefix = name.prefix.toString();
      Symbol ps = getSymbol(prefix);
      if (ps != null) {
        return ps.getSymbol(name.identifier);
      }
    }

    String n = _renameKeyword(name.toString());
    if (symbolStack.isNotEmpty && symbolStack.last.containsKey(n)) {
      return symbolStack.last[n];
    }

    // Check to see if the symbol is defined in an import.
    int size = imports.length;
    for (int i = size - 1; i >= 0; --i) {
      Symbol s = imports[i].getSymbol(name, recursionProtector);
      if (s != null) {
        return s;
      }
    }

    return null;
  }

  int _symbolType(name) {
    String n = name.toString();
    if (symbolStack.last.containsKey(n)) {
      return symbolStack.last[n].type;
    }
    return -1;
  }

  @override
  Object visitAdjacentStrings(AdjacentStrings node) {
    bool hasStringLit = false;
    int count = 0;
    for (var s in node.strings) {
      if (s is SimpleStringLiteral) {
        if (!hasStringLit) {
          if (count > 0) {
            out.write(' + ');
          }
          if (_inVariableDeclaration) {
            out.write('d2c::STR__("');
          } else {
            out.write('d2c::Var(d2c::STR__("');
          }
          hasStringLit = true;
        }

        out.write(_getStringLiteral(s));
        //out.write('${_escapeString(s.value, isRaw: s.isRaw)}');
      } else {
        if (hasStringLit) {
          if (_inVariableDeclaration) {
            out.write('")');
          } else {
            out.write('"))');
          }
          hasStringLit = false;
        }
        if (count > 0) {
          out.write(' + ');
        }
        _visitNode(s);
      }
      count++;
    }
    if (hasStringLit) {
      if (_inVariableDeclaration) {
        out.write('")');
      } else {
        out.write('"))');
      }
    }
    return null;
  }

  @override
  Object visitAnnotation(Annotation node) {
    return null;
  }

  int _closureNamedParamIndex = 0;

  void _functionExpressionClosure(FunctionExpression a) {
    var params = a.parameters.parameters;
    int numParams = params.length;

    _pushSymbolStack('@14');
    out.write('D2C_CLOSURE($numParams, [=](');

    Map namedParams = a.parameters != null
            ? _getNamedParameters(a.parameters.parameters)
            : {};

    Map localParams = {};
    Map localNamed = {};
    int numNamed = 0;
    int count = 0;
    for (int i = 0; i < numParams; ++i) {
      if (params[i].kind != null && params[i].kind.name == 'NAMED') {
        localNamed[params[i].identifier.name.toString()] = params[i];
        numNamed++;
        continue;
      }

      localParams[params[i].identifier.name.toString()] = params[i];

      if (count > 0) {
        out.write(', ');
      }
      out.write('d2c::Var ');
      _declareSymbol(params[i].identifier.name, Symbol.VARIABLE);
      _visitNode(params[i].identifier);
      count++;
    }

    if (numNamed > 0) {
      _closureNamedParamIndex++;
      if (count > 0) {
        out.write(', ');
      }
      out.write('d2c::Var __named${_closureNamedParamIndex}__');
    }

    out.writeln(')->d2c::Var{');

    if (namedParams.isNotEmpty) {
      for (var p in namedParams.values) {
        String name;
        if (p.parameter is FunctionTypedFormalParameter) {
          var prm = p.parameter;
          name = prm.identifier.toString();
        } else {
          name = p.parameter.identifier.toString();
        }

        if (!localParams.containsKey(name)) {
          String tk = translator.getToken(name);
          String tkContainsKey = translator.getToken('containsKey');

          String dv = p.defaultValue.toString();
          var dvs = getSymbol(p.defaultValue);
          if (dvs != null) {
            dv = dvs.name;
            if (dvs.type == Symbol.VARIABLE && dvs.isStatic) {
              dv = '__get_${dv}()';
            }
            if (dvs.library != this) {
              dv = '${dvs.library.namespace}::$dv';
            }
          }

          if (localNamed.containsKey(name)) {
            var n = '__named${_closureNamedParamIndex}__';
            out.writeln('d2c::Var ${name} = '
                'd2c::cast<d2c::NamedArguments>($n).contains($tk) ? '
                'd2c::cast<d2c::NamedArguments>($n).get($tk) : '
                'd2c::Var(${dv});');
          } else {
            var n = '__named__';
            out.writeln('d2c::Var ${name} = '
                'd2c::cast<d2c::NamedArguments>($n).contains($tk) ? '
                'd2c::cast<d2c::NamedArguments>($n).get($tk) : '
                'd2c::Var(${dv});');
          }
          _declareSymbol(name, Symbol.VARIABLE);
        }
      }
    }

    if (a.body is ExpressionFunctionBody) {
      ExpressionFunctionBody body = a.body;
      out.write('return ');
      _visitNode(body.expression);
      out.write(';\n})');
    } else if (a.body is BlockFunctionBody) {
      BlockFunctionBody body = a.body;
      _visitNodeListWithSeparator(body.block.statements, ' ');
      out.write(' return null;\n})');
    }

    _popSymbolStack('@14');
  }

  void _visitArguments(List arguments) {
    List namedArgs = [];
    int count = 0;
    for (var a in arguments) {
      if (a is NamedExpression) {
        namedArgs.add(a);
      } else {
        if (count > 0) {
          out.write(', ');
        }
        count++;
        if (a is SimpleIdentifier) {
          Symbol symbol = getSymbol(a);
          if (symbol != null && symbol.type == Symbol.FUNCTION) {
            out.write('d2c::bind(');
            if (symbol.extra is ClassDefinition) {
              if (symbol.node.modifierKeyword.toString() == 'static') {
                out.write('&');
              } else {
                out.write('this, &');
              }
            }
          }

          if (symbol != null && symbol.library != this) {
            out.write('${symbol.library.namespace}::');
          }

          if (symbol != null && symbol.type == Symbol.FUNCTION) {
            if (symbol.extra != null) {
              out.write('${symbol.extra.name}::');
            }
          }

          if (symbol != null && symbol.node != null &&
              symbol.node is DefaultFormalParameter &&
              symbol.node.kind == ParameterKind.NAMED) {
            var np = symbol.node;
            String tk = translator.getToken(a.toString());
            String tkContainsKey = translator.getToken('containsKey');
            String n = '__named__';

            String dv = np.defaultValue.toString();
            var dvs = getSymbol(np.defaultValue);
            if (dvs != null) {
              dv = dvs.name;
              if (dvs.type == Symbol.VARIABLE && dvs.isStatic) {
                dv = '__get_${dv}()';
              }
              if (dvs.library != this) {
                dv = '${dvs.library.namespace}::$dv';
              }
            }

            out.write('d2c::cast<d2c::NamedArguments>($n).contains($tk) ? '
                'd2c::cast<d2c::NamedArguments>($n).get($tk) : '
                'd2c::Var(${dv})');
          } else {
            _visitNode(a);
          }

          if (symbol != null && symbol.type == Symbol.FUNCTION) {
            out.write(')');
          }
        } else if (a is FunctionExpression) {
          _functionExpressionClosure(a);
        } else {
          _visitNode(a);
        }
      }
    }

    if (namedArgs.isNotEmpty) {
      if (count > 0) {
        out.write(', ');
      }
      out.write('d2c::namedArgs({');
      count = 0;
      for (var a in namedArgs) {
        if (count > 0) {
          out.write(', ');
        }
        String name = a.name.label.toString();
        String tk = translator.getToken(name);
        out.write(tk);
        count++;
      }
      out.write('}, {');

      count = 0;
      for (var a in namedArgs) {
        if (count > 0) {
          out.write(', ');
        }

        if (a.expression is FunctionExpression) {
          _functionExpressionClosure(a.expression);
        } else {
          bool processed = false;
          var e = a.expression;
          if (e is SimpleIdentifier || e is PrefixedIdentifier) {
            Symbol s = getSymbol(e);
            if (s != null) {
              if (s.type == Symbol.FUNCTION) {
                out.write('d2c::bind(');
                if (s.extra is ClassDefinition) {
                  if (s.node.modifierKeyword.toString() == 'static') {
                    out.write('&${s.extra.name}::');
                  } else {
                    out.write('this, &${s.extra.name}::');
                  }
                }
                _visitNode(e);
                out.write(')');
                processed = true;
              }
            }
          }
          if (!processed) {
            _visitNode(e);
          }
        }
        count++;
      }
      out.write('})');
    }
  }

  @override
  Object visitArgumentList(ArgumentList node) {
    out.write('(');
    _visitArguments(node.arguments);
    out.write(')');
    return null;
  }

  @override
  Object visitAsExpression(AsExpression node) {
    _visitNode(node.expression);
    return null;
  }

  @override
  Object visitAssertStatement(AssertStatement node) {
    out.write('d2c::assert__(');
    _visitNode(node.condition);
    out.writeln(');');
    return null;
  }

  void _visitMemberOperator(node, lhs) {
    bool hasInnerFunc = false;
    if (node.operator.lexeme != '=') {
      var op = node.operator.lexeme;
      op = op.substring(0, op.length - 1);

      if (lhs.prefix is Literal) {
        out.write('d2c::Var(');
        _visitNode(lhs.prefix);
        out.write(')');
      } else {
        _visitNode(lhs.prefix);
      }

      String tk = translator.getToken('__get_${_renameKeyword(lhs.identifier)}');
      out.write('.invoke($tk)');

      if (op == '~/') {
        out.write('.__intDiv(');
        hasInnerFunc = true;
      } else {
        out.write(' $op ');
      }
    }

    _visitNode(node.rightHandSide);

    if (hasInnerFunc) {
      out.write(')');
    }
  }

  @override
  Object visitAssignmentExpression(AssignmentExpression node) {
    _nestedFunctionLevel++;
    if (node.leftHandSide is PrefixedIdentifier) {
      PrefixedIdentifier lhs = node.leftHandSide;

      Symbol sym = getSymbol(lhs.prefix.toString());
      int symType = sym != null ? sym.type : 0;

      if (symType == Symbol.CLASS) {
        sym = sym.getSymbol(lhs.identifier);
        symType = sym != null ? sym.type : 0;

        // Static class setter
        if (symType == Symbol.GETTER_SETTER) {
          if (sym.library != this) {
            out.write('${sym.library.namespace}::');
          }
          out.write('${lhs.prefix}::__set_${_renameKeyword(lhs.identifier)}(');
          _visitNode(node.rightHandSide);
          out.write(')');
        } else {
          _visitNode(lhs);

          if (node.operator.lexeme == '~/=') {
            out.write('.__intDivAssign(');
            _visitNode(node.rightHandSide);
            out.write(')');
          } else {
            out.write(node.operator.lexeme);

            _visitNode(node.rightHandSide);
          }
        }

        _nestedFunctionLevel--;
        return null;
      } else if (symType == Symbol.LIBRARY) {
        out.write('${sym.library.namespace}::'
                  '${_renameKeyword(lhs.identifier)} = ');
        _visitNode(node.rightHandSide);
        _nestedFunctionLevel--;
        return null;
      }

      if (sym.library != this) {
        out.write('${sym.library.namespace}::');
      }

      if (lhs.prefix is Literal) {
        out.write('d2c::Var(');
        _visitNode(lhs.prefix);
        out.write(')');
      } else {
        _visitNode(lhs.prefix);
      }
      String tk = translator.getToken('__set_${_renameKeyword(lhs.identifier)}');
      out.write('.invoke($tk, ');

      _visitMemberOperator(node, lhs);

      out.write(')');
    } else {
      var lhs = node.leftHandSide;

      // Handle []= operator
      if (lhs is IndexExpression) {
        var ident = lhs.target;
        var index = lhs.index;

        Symbol sym = getSymbol(ident.toString());
        if (sym != null && sym.library != this) {
          out.write('${sym.library.namespace}::');
        }
        _visitNode(ident);
        out.write('.__array_set(');
        _visitNode(index);
        out.write(', ');

        if (node.operator.lexeme != '=') {
          if (sym != null && sym.library != this) {
            out.write('${sym.library.namespace}::');
          }
          _visitNode(ident);
          out.write('[');
          _visitNode(index);
          out.write(']');

          var op = node.operator.lexeme;
          op = op.substring(0, op.length - 1);

          if (op == '~/') {
            out.write('.__intDiv(');
            _visitNode(node.rightHandSide);
            out.write(')');
          } else {
            out.write(' $op ');
            _visitNode(node.rightHandSide);
          }
        } else {
          _visitNode(node.rightHandSide);
        }

        out.write(')');

        _nestedFunctionLevel--;
        return null;
      }

      Symbol sym = getSymbol(lhs.toString());
      int symType = sym != null ? sym.type : 0;

      // Instance setter
      if (symType == Symbol.GETTER_SETTER) {
        out.write('__set_${_renameKeyword(node.leftHandSide)}(');
        if (node.operator.lexeme != '=') {
          out.write('__get_${_renameKeyword(node.leftHandSide)}()');
          out.write(' ${node.operator.lexeme[0]} ');
        }
        _visitNode(node.rightHandSide);
        out.write(')');
      } else {
        if (sym != null && sym.library != this) {
          out.write('${sym.library.namespace}::');
        }

        if (node.operator.lexeme == '~/=') {
          _visitNode(node.leftHandSide);
          out.write('.__intDivAssign(');
          _visitNode(node.rightHandSide);
          out.write(')');
        } else {
          if (lhs is PropertyAccess) {
            if (lhs.target is ThisExpression) {
              out.write('this->');
              out.write('__set_${_renameKeyword(lhs.propertyName)}(');
            } else {
              _visitNode(lhs.target);
              out.write('.');

              String stk =
                  translator.getToken('__set_${_renameKeyword(lhs.propertyName)}');

              out.write('invoke($stk, ');
            }

            if (node.operator.lexeme != '=') {
              if (lhs.target is ThisExpression) {
                out.write('this->');
              } else {
                _visitNode(lhs.target);
                out.write('.');
              }
              out.write('__get_${_renameKeyword(lhs.propertyName)}()');
              out.write(' ${node.operator.lexeme[0]} ');
            }

            _visitNode(node.rightHandSide);
            out.write(')');
          } else {
            _visitNode(node.leftHandSide);
            out.write(' ');
            out.write(node.operator.lexeme);
            out.write(' ');
            _visitNode(node.rightHandSide);
          }
        }
      }
    }

    _nestedFunctionLevel--;
    return null;
  }

  bool _isNumberLiteral(l) => l is IntegerLiteral || l is DoubleLiteral ||
      (l is PrefixExpression && ((l.operand is IntegerLiteral) ||
          (l.operand is DoubleLiteral)));

  @override
  Object visitBinaryExpression(BinaryExpression node) {
    if (node.operator.lexeme == '~/') {
      out.write('d2c::Var(');
      _visitNode(node.leftOperand);
      out.write(').__intDiv(');
      _visitNode(node.rightOperand);
      out.write(')');
    } else if (node.operator.lexeme == '~/=') {
      out.write('d2c::Var(');
      _visitNode(node.leftOperand);
      out.write(').__intDivAssign(');
      _visitNode(node.rightOperand);
      out.write(')');
    } else {
      var l = node.leftOperand;
      var r = node.rightOperand;

      bool boolConv = false;

      // C++ will disable conditional short circuiting for objects that have
      // overloaded && and || operators (ie Var's). To re-enable short
      // circuiting (which is what the Dart VM is doing), the boolean
      // value is extracted from the variable and used for the conditional
      // logic. This forces C++ to use short circuiting.
      if (node.operator.lexeme == '&&' || node.operator.lexeme == '||') {
        if (l is! BooleanLiteral) {
          boolConv = true;
          out.write('d2c::bool_value(');
        }
      }

      if (node.operator.lexeme == '/') {
        if (_isNumberLiteral(r) && _isNumberLiteral(l)) {
          double n = (l is IntegerLiteral) ? l.value.toDouble() :
                     (l is DoubleLiteral) ? l.value : 0.0;
          double d = (r is IntegerLiteral) ? r.value.toDouble() :
                     (r is DoubleLiteral) ? r.value : 0.0;

          if (n == 0.0 && d == 0.0) {
            out.write('std::numeric_limits<double>::quiet_NaN()');
            return null;
          } else if (n == 1.0 && d == 0.0) {
            out.write('std::numeric_limits<double>::infinity()');
            return null;
          }
        }
      }

      bool bothLiteral = false;
      // Dart modulo % operator is unique in that it does euclidean modulo,
      // and differs from C++'s % operator. This means we need to ensure one
      // of the operands is a Var and we can't just wrap the whole expession
      // into a var, ensuring we call the objects % operator and not C++'s
      // built-in % operator.
      if (l is Literal && r is Literal &&
          node.operator.lexeme != '%') {
        bothLiteral = true;
        out.write('d2c::Var(');
      }

      if (!bothLiteral && _isNumberLiteral(l)) {
        out.write('d2c::Var(');
      }

      if (l is SimpleIdentifier) {
        Symbol s = getSymbol(l);
        if (s != null && s.library != this) {
          out.write('${s.library.namespace}::');
        }
      }

      _visitNode(l);

      if (!bothLiteral && _isNumberLiteral(l)) {
        out.write(')');
      }

      if (boolConv) {
        out.write(')');
      }

      out.write(' ${node.operator} ');

      // Wrap the right expression in parenthesis so that we ensure c++
      // does the correct operator precedence that dart expects.
      out.write('(');

      if (r is SimpleIdentifier) {
        Symbol s = getSymbol(r);
        if (s != null && s.library != this) {
          out.write('${s.library.namespace}::');
        }
      }

      boolConv = false;
      if (node.operator.lexeme == '&&' || node.operator.lexeme == '||') {
        if (r is! BooleanLiteral) {
          boolConv = true;
          out.write('d2c::bool_value(');
        }
      }

      if (!bothLiteral && _isNumberLiteral(r)) {
        out.write('d2c::Var(');
      }

      if (node.operator.lexeme == '/' && _isNumberLiteral(r)) {
        // If the denominator is a number literal, force it to
        // double for division to ensure C++ uses double division
        // and not integer division.
        out.write('static_cast<double>(');
      }

      _visitNode(r);

      if (node.operator.lexeme == '/' && _isNumberLiteral(r)) {
        out.write(')');
      }

      if (!bothLiteral && _isNumberLiteral(r)) {
        out.write(')');
      }

      if (bothLiteral) {
        out.write(')');
      }

      if (boolConv) {
        out.write(')');
      }

      out.write(')');
    }
    return null;
  }

  @override
  Object visitBlock(Block node) {
    out.writeln('{');
    _pushSymbolStack('@15');
    _visitNodeListWithSeparator(node.statements, ' ');
    _popSymbolStack('@15');
    out.writeln('}');
    return null;
  }

  Map _getNamedParameters(var params) {
    Map l = {};
    // handle the case where named parameters come from the constructor,
    // and a closure is being passed to the super constructor.
    l.addAll(_inheritedNamedParams);
    for (var p in params) {
      if (p.kind == ParameterKind.NAMED) {
        l[p.parameter.toString()] = p;
      }
    }
    return l;
  }

  @override
  Object visitBlockFunctionBody(BlockFunctionBody node) {
    var p = node.parent;
    Map namedParams = p.parameters != null
        ? _getNamedParameters(p.parameters.parameters)
        : {};

    out.writeln('{');
    if (namedParams.isNotEmpty) {
      _pushSymbolStack('@16');
    }

    if (namedParams.isNotEmpty) {
      for (var p in namedParams.values) {
        String name;
        if (p.parameter is FunctionTypedFormalParameter) {
          var prm = p.parameter;
          name = prm.identifier.toString();
        } else {
          name = p.parameter.identifier.toString();
        }

        String tk = translator.getToken(name);
        String tkContainsKey = translator.getToken('containsKey');
        String n = '__named__';

        String dv = p.defaultValue.toString();
        var dvs = getSymbol(_renameKeyword(p.defaultValue));
        if (dvs != null) {
          dv = dvs.name;
          if (dvs.type == Symbol.VARIABLE && dvs.isStatic) {
            dv = '__get_${dv}()';
          }
          if (dvs.library != this) {
            dv = '${dvs.library.namespace}::$dv';
          }
        }

        out.writeln('d2c::Var ${name} = '
            'd2c::cast<d2c::NamedArguments>($n).contains($tk) ? '
            'd2c::cast<d2c::NamedArguments>($n).get($tk) : '
            'd2c::Var($dv);');

        _declareSymbol(name, Symbol.VARIABLE);
      }
    }

    _nestedFunctionLevel++;
    _expandIdentifier++;
    _visitNode(node.block);
    _expandIdentifier--;
    _nestedFunctionLevel--;

    // This is always returning null in case the function doesn't have a
    // return statement. If the function does have a return statement, then
    // this will never get reached anyways, so no harm.
    out.writeln('return null;');

    if (namedParams.isNotEmpty) {
      _popSymbolStack('@16');
      out.writeln('}');
    } else {
      out.writeln('}');
    }

    return null;
  }

  @override
  Object visitBooleanLiteral(BooleanLiteral node) {
    out.write(node.literal.lexeme);
    return null;
  }

  @override
  Object visitBreakStatement(BreakStatement node) {
    if (node.label != null) {
      out.writeln('/* break ${node.label} */');
      out.writeln('____label_${node.label}____ = false;');
      out.write('goto');
      _visitNodeWithPrefix(' ', node.label);
    } else {
      out.write('break');
    }
    out.writeln(';');
    return null;
  }

  @override
  Object visitCascadeExpression(CascadeExpression node) {
    out.write('(');
    _visitNode(node.target);
    out.write(')');
    for (var c in node.cascadeSections) {
      if (c is AssignmentExpression) {
        String tk;
        if (c.leftHandSide is IndexExpression) {
          var lhs = c.leftHandSide;
          tk = translator.getToken('__array_set');
          out.write('.cascade($tk, ');
          if (lhs.target != null) {
            _visitNode(node.target);
            out.write('.getMember(TK_');
            _visitNode(lhs.target.propertyName);
            out.write('__), ');
          }
          _visitNode(lhs.index);
          out.write(', ');
          _visitNode(c.rightHandSide);
          out.write(')');
        } else {
          tk = translator.getToken('__set_${c.leftHandSide.propertyName}');
          out.write('.cascade($tk, ');
          _visitNode(c.rightHandSide);
          out.write(')');
        }
      } else if (c is FunctionExpressionInvocation) {
        out.write('.cascade(');
        c.function.target = node.target;
        _visitNode(c.function);
        _visitNode(c.argumentList);
        out.write(')');
      } else if (c is MethodInvocation) {
        String tk = translator.getToken(c.methodName);
        if (c.target != null) {
          Symbol ps = getSymbol(c.target.propertyName);
          String ptk = translator.getToken('__get_${c.target.propertyName}');
          out.write('.invoke($ptk).cascade(');
          _visitNode(node.target);
          out.write(', $tk');
          if (c.argumentList.arguments.isNotEmpty) {
            out.write(', ');
          }
          _visitArguments(c.argumentList.arguments);
          out.write(')');
        } else {
          out.write('.cascade($tk');
          for (var arg in c.argumentList.arguments) {
            out.write(', ');
            _visitNode(arg);
          }
          out.write(')');
        }
      }
    }
    return null;
  }

  @override
  Object visitCatchClause(CatchClause node) {
    //_visitNodeWithPrefix('on ', node.exceptionType);

    if (node.catchKeyword != null) {
      out.write('catch (d2c::Var ');
      _visitNode(node.exceptionParameter);
      out.write(') ');
    } else {
      out.write(' ');
    }

    if (node.stackTraceParameter != null) {
      // TODO get the stack trace
      out.writeln('{\nd2c::Var ${node.stackTraceParameter};');
    }

    _visitNode(node.body);

    if (node.stackTraceParameter != null) {
      out.writeln('}');
    }

    return null;
  }

  @override
  Object visitClassDeclaration(ClassDeclaration node) {
    return null;
  }

  @override
  Object visitClassTypeAlias(ClassTypeAlias node) {
    _visitNodeListWithSeparatorAndSuffix(node.metadata, ' ', ' ');
    out.write('class ');
    _visitNode(node.name);
    _visitNode(node.typeParameters);
    out.write(' = ');
    _visitNode(node.superclass);
    _visitNodeWithPrefix(' ', node.withClause);
    _visitNodeWithPrefix(' ', node.implementsClause);
    out.writeln(';');
    return null;
  }

  @override
  Object visitComment(Comment node) => null;

  @override
  Object visitCommentReference(CommentReference node) => null;

  @override
  Object visitCompilationUnit(CompilationUnit node) {
    ScriptTag scriptTag = node.scriptTag;
    NodeList<Directive> directives = node.directives;
    _visitNode(scriptTag);
    String prefix = scriptTag == null ? '' : ' ';
    _visitNodeListWithSeparatorAndPrefix(prefix, directives, ' ');
    prefix = scriptTag == null && directives.isEmpty ? '' : ' ';
    _visitNodeListWithSeparatorAndPrefix(prefix, node.declarations, ' ');
    return null;
  }

  @override
  Object visitConditionalExpression(ConditionalExpression node) {
    _visitNode(node.condition);
    out.write(' ? d2c::Var(');
    _visitNode(node.thenExpression);
    out.write(') : d2c::Var(');
    _visitNode(node.elseExpression);
    out.write(')');
    return null;
  }

  @override
  Object visitConstructorDeclaration(ConstructorDeclaration node) {
    return null;
  }

  @override
  Object visitConstructorFieldInitializer(ConstructorFieldInitializer node) {
    _visitTokenWithSuffix(node.keyword, '->');
    _visitNode(node.fieldName);
    out.write('(');
    _visitNode(node.expression);
    out.write(')');
    return null;
  }

  @override
  Object visitConstructorName(ConstructorName node) {
    _visitNode(node.type);
    _visitNodeWithPrefix('.', node.name);
    return null;
  }

  @override
  Object visitContinueStatement(ContinueStatement node) {
    if (node.label != null) {
      out.writeln('/* continue ${node.label} */');
      out.writeln('____label_${node.label}____ = true;');
      out.write('goto');
      _visitNodeWithPrefix(' ', node.label);
    } else {
      out.write('continue');
    }
    out.writeln(';');
    return null;
  }

  @override
  Object visitDeclaredIdentifier(DeclaredIdentifier node) {
    _visitNodeListWithSeparatorAndSuffix(node.metadata, ' ', ' ');
    _visitTokenWithSuffix(node.keyword, ' ');
    _visitNodeWithSuffix(node.type, ' ');
    _visitNode(node.identifier);
    return null;
  }

  @override
  Object visitDefaultFormalParameter(DefaultFormalParameter node) {
    _visitNode(node.parameter);
    if (!_skipParameterValues) {
      if (node.separator != null) {
        out.write(' =');
        _visitNodeWithPrefix(' ', node.defaultValue);
      } else {
        if (node.kind.isOptional) {
          out.write(' = null');
        }
      }
    }
    return null;
  }

  @override
  Object visitDoStatement(DoStatement node) {
    out.write('do ');
    _visitNode(node.body);
    out.write(' while (');
    _visitNode(node.condition);
    out.write(');');
    return null;
  }

  @override
  Object visitDoubleLiteral(DoubleLiteral node) {
    out.write('${node.literal}');
    return null;
  }

  @override
  Object visitEmptyFunctionBody(EmptyFunctionBody node) {
    var p = node.parent.parent;
    if (p is FunctionDeclaration && p.externalKeyword != null) {
      out.write('{ return __external::${p.name}(');

      var p2 = node.parent;
      var params = p2.parameters.parameters;
      for (int i = 0; i < params.length; ++i) {
        if (i > 0) {
          out.write(', ');
        }
        out.write(params[i].identifier.toString());
      }

      out.writeln('); }');
      //out.write('{ return /*__external::${p.name}*/ null; }');
    } else {
      out.writeln('{ return null; }');
    }
    return null;
  }

  @override
  Object visitEmptyStatement(EmptyStatement node) {
    out.writeln(';');
    return null;
  }

  @override
  Object visitExportDirective(ExportDirective node) {
    return null;
  }

  @override
  Object visitExpressionFunctionBody(ExpressionFunctionBody node) {
    var p = node.parent;
    Map namedParams = p.parameters != null
        ? _getNamedParameters(p.parameters.parameters)
        : {};

    if (namedParams.isNotEmpty) {
      _pushSymbolStack('@17');
    }
    out.writeln('{');

    _nestedFunctionLevel++;

    if (namedParams.isNotEmpty) {
      for (var p in namedParams.values) {
        String name;
        if (p.parameter is FunctionTypedFormalParameter) {
          var prm = p.parameter;
          name = prm.identifier.toString();
        } else {
          name = p.parameter.identifier.toString();
        }

        String tk = translator.getToken(name);
        String tkContainsKey = translator.getToken('containsKey');
        String n = '__named__';

        String dv = p.defaultValue.toString();
        var dvs = getSymbol(_renameKeyword(p.defaultValue));
        if (dvs != null) {
          dv = dvs.name;
          if (dvs.type == Symbol.VARIABLE && dvs.isStatic) {
            dv = '__get_${dv}()';
          }
          if (dvs.library != this) {
            dv = '${dvs.library.namespace}::$dv';
          }
        }

        out.writeln('d2c::Var ${name} = '
            'd2c::cast<d2c::NamedArguments>($n).contains($tk) ? '
            'd2c::cast<d2c::NamedArguments>($n).get($tk) : '
            'd2c::Var(${dv});');
        _declareSymbol(name, Symbol.VARIABLE);
      }
    }

    out.write('return ');
    _expandIdentifier++;
    _visitNode(node.expression);
    _expandIdentifier--;
    out.writeln(';');
    out.writeln('}');

    _nestedFunctionLevel--;

    if (namedParams.isNotEmpty) {
      _popSymbolStack('@17');
    }

    return null;
  }

  @override
  Object visitExpressionStatement(ExpressionStatement node) {
    _visitNode(node.expression);
    out.writeln(';');
    return null;
  }

  @override
  Object visitExtendsClause(ExtendsClause node) {
    out.write(': public ${node.superclass}');
    return null;
  }

  @override
  Object visitFieldDeclaration(FieldDeclaration node) {
    for (int i = 0; i < node.fields.variables.length; ++i) {
      var variable = node.fields.variables[i];

      _visitNodeListWithSeparatorAndSuffix(node.metadata, ' ', ' ');

      _visitTokenWithSuffix(node.staticKeyword, ' ');
      out.write('d2c::Var ');

      _visitNode(variable.name);

      if (node.staticKeyword == null) {
        _visitNodeWithPrefix('=', variable.initializer);
      }

      out.writeln(';');
    }

    return null;
  }

  @override
  Object visitFieldFormalParameter(FieldFormalParameter node) {
    _constructorFieldInitializers.add(node);
    out.write('const d2c::Var &');
    _visitNode(node.identifier);
    return null;
  }

  static int _iteratorIndex = 0;

  @override
  Object visitForEachStatement(ForEachStatement node) {
    DeclaredIdentifier loopVariable = node.loopVariable;
    _pushSymbolStack('@18');
    var iterVar = 'iterator${_iteratorIndex}__';
    _iteratorIndex++;

    out.write('d2c::Var $iterVar = ');
    if (node.iterator is ListLiteral) {
      out.write('d2c::Var(');
    }
    _visitNode(node.iterator);
    if (node.iterator is ListLiteral) {
      out.write(')');
    }
    out.writeln('.invoke(TK___get_iterator__);');
    out.writeln('while (${iterVar}.invoke(TK_moveNext__).boolValue()) {');
    if (loopVariable == null) {
      _visitNode(node.identifier);
    } else {
      var name = _renameKeyword(loopVariable.identifier);
      out.write('d2c::Var ${name}');
      _declareSymbol(name, Symbol.VARIABLE, node: loopVariable);
    }
    out.writeln(' = ${iterVar}.invoke(TK___get_current__);');

    _visitNode(node.body);
    out.writeln('}');
    _popSymbolStack('@18');
    return null;
  }

  void _visitParameterList(NodeList<FormalParameter> parameters) {
    int size = parameters.length;
    Map namedParams = {};

    int count = 0;
    int i;
    for (i = 0; i < size; i++) {
      FormalParameter parameter = parameters[i];
      var s = _declareSymbol(parameter.identifier.toString(), Symbol.VARIABLE,
                             node: parameter);

      if (parameter is DefaultFormalParameter) {
        if (parameter.kind == ParameterKind.NAMED) {
          _namedParameters.add(parameter);
          namedParams[parameter.parameter.toString()] = parameter;
          continue;
        } else {
          _optionalParameters.add(parameter);
        }
      } else {
        _functionParameters.add(parameter);
      }

      if (i > 0) {
        out.write(', ');
      }

      count++;
      parameter.accept(this);
    }

    if (namedParams.isNotEmpty) {
      if (count > 0) {
        out.write(', ');
      }
      out.write('const d2c::Var &__named__');
      if (!_skipParameterValues) {
        out.write(' = d2c::namedArgs({}, {})');
      }
    }
  }

  @override
  Object visitFormalParameterList(FormalParameterList node) {
    out.write('(');
    NodeList<FormalParameter> parameters = node.parameters;
    _visitParameterList(parameters);
    out.write(')');
    return null;
  }

  @override
  Object visitForStatement(ForStatement node) {
    Expression initialization = node.initialization;
    out.write('for (');
    if (initialization != null) {
      _visitNode(initialization);
    } else {
      _visitNode(node.variables);
    }
    out.writeln(';');
    _visitNodeWithPrefix(' ', node.condition);
    out.writeln(';');
    _visitNodeListWithSeparatorAndPrefix(' ', node.updaters, ', ');
    out.write(') ');
    _visitNode(node.body);
    return null;
  }

  @override
  Object visitFunctionDeclaration(FunctionDeclaration node) {
    _functionParameters.clear();
    _optionalParameters.clear();
    _namedParameters.clear();

    _nestedFunctionLevel++;
    _visitNodeListWithSeparatorAndSuffix(node.metadata, ' ', ' ');

    out.write('d2c::Var ');

    _visitTokenWithSuffix(node.propertyKeyword, ' ');

    String name = node.name.token.value().toString();
    _declareSymbol(name, _nestedFunctionLevel > 1 ?
        Symbol.VARIABLE : Symbol.FUNCTION);

    if (name == 'main') {
      out.write('main__');
    } else {
      _visitNode(node.name);
    }

    if (_nestedFunctionLevel > 1) {
      out.writeln(';');
      if (name == 'main') {
        out.write('main__');
      } else {
        _visitNode(node.name);
      }
    }

    _pushSymbolStack('@19');

    if (_nestedFunctionLevel > 1) {
      var params = node.functionExpression.parameters.parameters;
      int numParams = params.length;
      out.write(' = D2C_CLOSURE($numParams, [=](');
      for (int i = 0; i < numParams; ++i) {
        if (i > 0) {
          out.write(', ');
        }
        out.write('d2c::Var ${params[i].identifier}');
        _declareSymbol(params[i].identifier.toString(), Symbol.VARIABLE,
                       node: params[i]);
      }
      out.writeln(')->d2c::Var{');

      if (node.functionExpression.body is BlockFunctionBody) {
        BlockFunctionBody body = node.functionExpression.body;
        _visitNodeListWithSeparator(body.block.statements, ' ');
      } else {
        _visitNode(node.functionExpression.body);
      }
      out.write(' return null; });');
    } else {
      _visitNode(node.functionExpression);
    }

    _popSymbolStack('@19');

    _nestedFunctionLevel--;
    return null;
  }

  @override
  Object visitFunctionDeclarationStatement(FunctionDeclarationStatement node) {
    _visitNode(node.functionDeclaration);
    out.writeln(';');
    return null;
  }

  @override
  Object visitFunctionExpression(FunctionExpression node) {
    _functionDepth++;
    if (_functionDepth > 1 || _nestedFunctionLevel > 1) {
      _functionExpressionClosure(node);
    } else {
      _visitNode(node.parameters);
      out.write(' ');
      _visitNode(node.body);
    }
    _functionDepth--;
    return null;
  }

  @override
  Object visitFunctionExpressionInvocation(FunctionExpressionInvocation node) {
    _visitNode(node.function);
    _visitNode(node.argumentList);
    return null;
  }

  @override
  Object visitFunctionTypeAlias(FunctionTypeAlias node) {
    return null;
  }

  @override
  Object visitFunctionTypedFormalParameter(FunctionTypedFormalParameter node) {
    _visitNodeWithPrefix('const d2c::Var &', node.identifier);
    return null;
  }

  @override
  Object visitHideCombinator(HideCombinator node) {
    return null;
  }

  @override
  Object visitIfStatement(IfStatement node) {
    out.write('if (');
    _visitNode(node.condition);
    out.write(') ');
    _visitNode(node.thenStatement);
    _visitNodeWithPrefix(' else ', node.elseStatement);
    return null;
  }

  @override
  Object visitImplementsClause(ImplementsClause node) {
    return null;
  }

  @override
  Object visitImportDirective(ImportDirective node) {
    return null;
  }

  @override
  Object visitIndexExpression(IndexExpression node) {
    if (node.isCascaded) {
      throw 'Cascades not yet supported';
    } else {
      _visitNode(node.target);
    }
    out.write('[');
    _visitNode(node.index);
    out.write(']');
    return null;
  }

  String _getConstructorClassName(ConstructorName c) {
    var n = c.type.name;
    if (c.name != null) {
      if (n is PrefixedIdentifier) {
        return n.identifier.toString();
      }
      return n.toString();
    }

    if (n is PrefixedIdentifier) {
      return n.prefix.toString();
    }
    return n.toString();
  }

  String _getConstructorName(ConstructorName c) {
    if (c.name != null) {
      var n = c.name;
      if (n is PrefixedIdentifier) {
        return n.identifier.toString();
      }
      return n.toString();
    }

    var n = c.type.name;
    if (n is PrefixedIdentifier) {
      return n.identifier.toString();
    }
    return '';
  }

  String _getPrefixedNodeString(var p, var n) {
    if (n == null) {
      return '';
    }
    return '$p$n';
  }

  String _getConstructorLibrary(ConstructorName c) {
    if (c.name != null) {
      var n = c.type.name;
      if (n is PrefixedIdentifier) {
        return n.prefix.toString();
      }
    }
    return '';
  }

  void _visitTypeArguments(TypeArgumentList ta) {
    out.write('{');
    if (ta != null) {
      bool first = true;
      for (var a in ta.arguments) {
        if (!first) {
          out.write(', ');
        }
        first = false;

        Symbol s = getSymbol(a.name);

        if (s != null && s.type == Symbol.TYPENAME) {
          if (getSymbol('__types__') != null) {
            out.write('__types__[${s.extra}]/*${a.name}*/');
          } else {
            String tk = translator.getToken(a.name);
            out.write('*typeParameters_[$tk].get()/*${a.name}*/');
          }
        } else {
          if (a.name.toString() == 'dynamic') {
            out.write('0');
          } else {
            out.write('d2c::objectType<');
            if (s != null && a.name is SimpleIdentifier &&
                s.library != this) {
              out.write('${s.library.namespace}::');
            }

            _visitNode(a.name);

            out.write('>()');
          }
        }
      }
    }
    out.write('}');
  }

  @override
  Object visitInstanceCreationExpression(InstanceCreationExpression node) {
    String className = _getConstructorClassName(node.constructorName);
    String constructorName = _getConstructorName(node.constructorName);
    String libraryName = _getConstructorLibrary(node.constructorName);

    bool isConst = node.keyword.toString() == 'const';

    className = _getClassName(className);

    Library lib;
    if (libraryName.isNotEmpty) {
      if (importNames.containsKey(libraryName)) {
        lib = importNames[libraryName];
        libraryName = lib.namespace;
      } else {
        libraryName = '';
      }
    }

    Symbol sym = getSymbol(className);

    if (lib == null) {
      if (sym != null) {
        lib = sym.library;
        if (lib != this) {
          libraryName = lib.namespace;
        } else {
          libraryName = '';
        }
      }
    }

    if (isConst) {
      if (sym == null || sym.type != Symbol.CLASS) {
        throw 'Class expected';
      }
      ClassDefinition cd = sym.extra;
      for (var f in cd.constConstructors.keys) {
        String cn = _getPrefixedNodeString('', f);
        if (cn == constructorName) {
          // Const constructor...
          if (libraryName.isNotEmpty) {
            out.write('${libraryName}::');
          }
          out.write('${className}::__const_${cd.name}');
          if (constructorName.isNotEmpty) {
            out.write('_${constructorName}');
          }

          out.write('(');

          // Add type parameters
          _visitTypeArguments(node.constructorName.type.typeArguments);

          if (node.argumentList.arguments.isNotEmpty) {
            out.write(', ');
          }

          _visitArguments(node.argumentList.arguments);

          out.write(')');

          return null;
        }
      }
    }

    // Check to see if this is a factory constructor.
    if (sym != null && sym.type == Symbol.CLASS) {
      ClassDefinition cd = sym.extra;

      if (cd != null) {
        for (var f in cd.factoryConstructors.keys) {
          String cn = _getPrefixedNodeString('', f);
          if (cn == constructorName) {
            // Factory constructor...
            if (libraryName.isNotEmpty) {
              out.write('${libraryName}::');
            }
            out.write('${className}::__factory_${cd.name}');
            if (constructorName.isNotEmpty) {
              out.write('_${constructorName}');
            }

            out.write('(');

            // Add type parameters
            _visitTypeArguments(node.constructorName.type.typeArguments);

            if (node.argumentList.arguments.isNotEmpty) {
              out.write(', ');
            }

            _visitArguments(node.argumentList.arguments);

            out.write(')');

            return null;
          }
        }
      }
    }

    if (!_inVariableDeclaration) {
     out.write('d2c::Var(');
    }
    out.write('new ');

    if (constructorName.isNotEmpty) {
      if (libraryName.isEmpty) {
        if (_symbolType(className) == Symbol.LIBRARY) {
          libraryName = className;
          className = constructorName;
          constructorName = '';
        }
      }
    }

    String ns = (libraryName.isNotEmpty) ? '${libraryName}::' : '';

    // Named constructor
    out.write('${ns}${className}(');
    out.write('${ns}${className}::'
              '__constructor__');
    if (constructorName.isNotEmpty) {
      out.write('${_getClassName(constructorName)}__');
    }
    out.write('(');

    if (node.constructorName.type.typeArguments != null) {
      bool first = true;
      var ta = node.constructorName.type.typeArguments.arguments;
      for (int ai = 0; ai < ta.length; ++ai) {
        var a = ta[ai];
        var name = _getClassName(a.name.toString());
        var s = getSymbol(name);
        if (s != null && s.library != this) {
          name = '${s.library.namespace}::$name';
        }
        if (first) {
          out.write('{');
        }

        if (!first) {
          out.write(', ');
        }

        if (s != null && s.type == Symbol.TYPENAME) {
          int index = 0;
          if (getSymbol('__types__') != null) {
            out.write('__types__[${s.extra}]/*$name*/');
          } else {
            String tk = translator.getToken(name);
            out.write('*typeParameters_[$tk].get()/*$name*/');
          }
        } else if (name == 'dynamic' || name == 'var') {
          out.write('d2c::objectType<d2c::Object>()/*$name*/');
        } else {
          out.write('d2c::objectType<$name>()');
        }

        first = false;
      }

      if (!first) {
        out.write('}');
      }
    }

    out.write(')');

    if (node.argumentList.arguments.isNotEmpty) {
      out.write(', ');
    }
    _visitArguments(node.argumentList.arguments);

    out.write(')');

    if (!_inVariableDeclaration) {
      out.write(')');
    }

    return null;
  }

  @override
  Object visitIntegerLiteral(IntegerLiteral node) {
    out.write('${node.literal.lexeme}LL');
    return null;
  }

  @override
  Object visitInterpolationExpression(InterpolationExpression node) {
    out.write('d2c::Var(');
    _visitNode(node.expression);
    out.write(').toString()');
    return null;
  }

  @override
  Object visitInterpolationString(InterpolationString node) {
    return null;
  }

  @override
  Object visitIsExpression(IsExpression node) {
    if (node.notOperator != null) {
      out.write('!');
    }

    String t = _getClassName(node.type.name);
    String type = t;
    Symbol tsym = getSymbol(type);
    if (tsym != null && tsym.library != this) {
      type = '${tsym.library.namespace}::$type';
    }

    // TODO properly handle typedef.
    if (tsym != null && tsym.type == Symbol.TYPEDEF) {
      out.write('(');
      _visitNode(node.expression);
      out.write(').isFunction()');
    } else if (tsym != null && tsym.type == Symbol.TYPENAME) {
      out.write('(');
      _visitNode(node.expression);
      String tk = translator.getToken(type);
      out.write(').implementsType(*typeParameters_[$tk].get())');
    } else if (t == 'Function') {
      out.write('d2c::is_callable(');
      _visitNode(node.expression);
      out.write(')');
    } else {
      out.write('d2c::is_type<$type>(');
      _visitNode(node.expression);

      if (node.type.typeArguments != null) {
        out.write(', {');
        var args = node.type.typeArguments.arguments;
        bool first = true;
        for (var arg in args) {
          if (!first) {
            out.write(', ');
          }

          var n = arg.name;
          if (n.toString() == 'dynamic') {
            out.write('0');
          } else {
            out.write('d2c::objectType<');
            if (n is SimpleIdentifier) {
              String type = _getClassName(n);
              Symbol tsym = getSymbol(type);
              if (tsym != null && tsym.library != this) {
                type = '${tsym.library.namespace}::$type';
              }
              out.write(type);
            }
            out.write('>()');
          }
          first = false;
        }
        out.write('}');
      }

      out.write(')');
    }

    return null;
  }

  @override
  Object visitLabel(Label node) {
    out.writeln('bool ____label_${node.label}____ = true;');
    _visitNode(node.label);
    out.writeln(': if (____label_${node.label}____)');
    return null;
  }

  @override
  Object visitLabeledStatement(LabeledStatement node) {
    _visitNodeListWithSeparatorAndSuffix(node.labels, ' ', ' ');
    _visitNode(node.statement);
    return null;
  }

  @override
  Object visitLibraryDirective(LibraryDirective node) {
    return null;
  }

  @override
  Object visitLibraryIdentifier(LibraryIdentifier node) {
    out.write(node.name);
    return null;
  }

  @override
  Object visitListLiteral(ListLiteral node) {
    if (node.constKeyword != null) {
      out.write('d2c::const_list(');
    } else {
      out.write('d2c::list(');
    }

    _visitTypeArguments(node.typeArguments);
    out.write(', {');
    _visitNodeListWithSeparator(node.elements, ', ');
    out.write('}');
    out.write(')');
    return null;
  }

  @override
  Object visitMapLiteral(MapLiteral node) {
    if (node.constKeyword != null) {
      out.write('d2c::const_map(');
    } else {
      out.write('d2c::map(');
    }
    _visitTypeArguments(node.typeArguments);
    out.write(', {');
    _visitNodeListWithSeparator(node.entries, ', ');
    out.write('}');
    out.write(')');
    return null;
  }

  @override
  Object visitMapLiteralEntry(MapLiteralEntry node) {
    _visitNode(node.key);
    out.write(', ');
    _visitNode(node.value);
    return null;
  }

  @override
  Object visitMethodDeclaration(MethodDeclaration node) {
    return null;
  }

  int _numArguments(List args) {
    int numArgs = 0;
    int numNamed = 0;
    for (var a in args) {
      if (a is NamedExpression) {
        numNamed++;
      } else {
        numArgs++;
      }
    }
    return numArgs + ((numNamed > 0) ? 1 : 0);
  }

  @override
  Object visitMethodInvocation(MethodInvocation node) {
    if (node.target != null) {
      Symbol symbol;
      int symType = 0;

      if (node.target is PrefixedIdentifier) {
        PrefixedIdentifier t = node.target;
        Symbol prefix = getSymbol(t.prefix);
        int prefixType = prefix != null ? prefix.type : 0;

        if (prefixType == Symbol.LIBRARY) {
          symbol = prefix.library.getSymbol(t.identifier);
          symType = symbol != null ? symbol.type : 0;
        } else if (prefixType == Symbol.CLASS) {
          symbol = prefix.getSymbol(t.identifier);
          symType = symbol != null ? symbol.type : 0;;
        }
      } else if (node.target is PropertyAccess) {
        var nt = node.target;
        var target = nt.target;
        var property = nt.propertyName;

        if (target is PrefixedIdentifier) {
          PrefixedIdentifier t = target;
          Symbol prefix = getSymbol(t.prefix);
          int prefixType = prefix != null ? prefix.type : 0;

          if (prefixType == Symbol.LIBRARY) {
            prefix = prefix.library.getSymbol(t.identifier);
          } else if (prefixType == Symbol.CLASS) {
            prefix = prefix.getSymbol(t.identifier);
          }

          symbol = prefix != null ? prefix.getSymbol(property) :
                   getSymbol(property);

          symType = symbol != null ? symbol.type : 0;
        } else {
          Symbol prefix = getSymbol(target);

          symbol = prefix != null ? prefix.getSymbol(property) :
                   getSymbol(property);

          symType = symbol != null ? symbol.type : 0;
        }
      } else {
        symbol = getSymbol(node.target);
        symType = symbol != null ? symbol.type : 0;
      }

      if (symType == Symbol.LIBRARY) {
        // namespace
        out.write('${symbol.library.namespace}::');
        _visitNode(node.methodName);
        _visitNode(node.argumentList);
      } else if (symType == Symbol.CLASS) {
        if (symbol.library != this) {
          out.write('${symbol.library.namespace}::');
        }
        // static method
        _visitNode(node.target);
        out.write('::');
        _visitNode(node.methodName);
        _visitNode(node.argumentList);
      } else {
        String mName = _renameKeyword(node.methodName);
        String tk = translator.getToken('${mName}');

        if (node.target.toString() == 'this') {
          out.write('invoke__($tk');
        } else {
          if (symbol != null && symbol.isStatic) {
            _visitNode(node.target);
          } else {
            var t = node.target;

            out.write('d2c::Var(');
            if (t is SimpleIdentifier) {
              if (symbol != null && symbol.library != this) {
                out.write('${symbol.library.namespace}::');
              }
            }

            _visitNode(t);
            out.write(')');
          }

          out.write('.invoke($tk');
        }

        var args = node.argumentList.arguments;
        if (args.isNotEmpty) {
          out.write(', ');
          _visitArguments(args);
        }

        out.write(')');
      }

      return null;
    }

    Symbol symbol = getSymbol(node.methodName);
    if (node is! SimpleIdentifier && symbol != null && symbol.library != this) {
      out.write('${symbol.library.namespace}::');
    }

    _visitNode(node.methodName);
    _visitNode(node.argumentList);
    return null;
  }

  @override
  Object visitNamedExpression(NamedExpression node) {
    return null;
  }

  @override
  Object visitNativeClause(NativeClause node) {
    _visitNode(node.name);
    return null;
  }

  @override
  Object visitNativeFunctionBody(NativeFunctionBody node) {
    _visitNode(node.stringLiteral);
    out.write(';');
    return null;
  }

  @override
  Object visitNullLiteral(NullLiteral node) {
    out.write('null');
    return null;
  }

  @override
  Object visitParenthesizedExpression(ParenthesizedExpression node) {
    out.write('(');
    _visitNode(node.expression);
    out.write(')');
    return null;
  }

  @override
  Object visitPartDirective(PartDirective node) {
    return null;
  }

  @override
  Object visitPartOfDirective(PartOfDirective node) {
    return null;
  }

  @override
  Object visitPostfixExpression(PostfixExpression node) {
    _visitNode(node.operand);
    out.write(node.operator.lexeme);
    return null;
  }

  @override
  Object visitPrefixedIdentifier(PrefixedIdentifier node) {
    Symbol symbol = getSymbol(node.prefix);
    int symType = symbol != null ? symbol.type : 0;

    if (symType == Symbol.LIBRARY) {
      out.write('${symbol.library.namespace}::');
      _visitNode(node.identifier);
    } else if (symType == Symbol.CLASS) {
      if (symbol.library != this) {
        out.write('${symbol.library.namespace}::');
      }
      _visitNode(node.prefix);
      out.write('::');

      symbol = symbol.getSymbol(_renameKeyword(node.identifier));
      symType = symbol != null ? symbol.type : 0;

      if (symType == Symbol.GETTER_SETTER ||
          (symType == Symbol.VARIABLE && symbol.isStatic)) {
        out.write('__get_${_renameKeyword(node.identifier)}()');
      } else {
        _visitNode(node.identifier);
      }
    } else {
      if (symbol != null && symbol.library != this) {
        out.write('${symbol.library.namespace}::');
      }

      if (node.prefix is Literal) {
        out.write('d2c::Var(');
        _visitNode(node.prefix);
        out.write(')');
      } else {
        _visitNode(node.prefix);
      }

      String id = _renameKeyword(node.identifier);
      String tk = translator.getToken('__get_${id}');
      out.write('.invoke($tk)');
    }

    return null;
  }

  @override
  Object visitPrefixExpression(PrefixExpression node) {
    bool isSetter = false;
    Symbol symbol;
    if ((node.operator.lexeme == '++' || node.operator.lexeme == '--') &&
        node.operand is PropertyAccess) {
      isSetter = true;
      var n = node.operand;
      if (n.target.toString() == 'this') {
        String tk = translator.getToken('__set_${n.propertyName}');
        out.write('this->invoke__($tk, ');
      } else {
        if (n.target is PrefixedIdentifier) {
          var t = n.target;
          Symbol prefix = getSymbol(t.prefix);
          if (prefix != null) {
            if (prefix.type == Symbol.LIBRARY) {
              symbol = prefix.library.getSymbol(t.identifier);
            } else if (prefix.type == Symbol.CLASS) {
              symbol = prefix.getSymbol(t.identifier);
            } else if (prefix.type == Symbol.VARIABLE) {
              symbol = prefix.getSymbol(t.identifier);
            } else {
              throw 'Library, Class, or Variable name expected';
            }
          }
        } else {
          symbol = getSymbol(n.target);
        }

        bool isClass = (symbol != null && symbol.type == Symbol.CLASS);

        if (!isClass) {
          out.write('d2c::Var(');
        }
        _visitNode(n.target);
        if (!isClass) {
          out.write(')');
        }

        if (isClass) {
          out.write('::');
          _visitNode(n.propertyName);
          return null;
        }

        out.write('.');
        String tk = translator.getToken('__set_${n.propertyName}');
        out.write('invoke($tk, ');
      }
    }

    out.write(node.operator.lexeme);

    _visitNode(node.operand);

    if (isSetter) {
      out.write(')');
    }

    return null;
  }

  @override
  Object visitPropertyAccess(PropertyAccess node) {
    Symbol symbol;
    if (node.target.toString() == 'this') {
      String tk = translator.getToken('__get_${node.propertyName}');
      out.write('this->invoke__($tk)');
    } else {
      if (node.target is PrefixedIdentifier) {
        var t = node.target;
        Symbol prefix = getSymbol(t.prefix);
        if (prefix != null) {
          if (prefix.type == Symbol.LIBRARY) {
            symbol = prefix.library.getSymbol(t.identifier);
          } else if (prefix.type == Symbol.CLASS) {
            symbol = prefix.getSymbol(t.identifier);
          } else if (prefix.type == Symbol.VARIABLE) {
            symbol = prefix.getSymbol(t.identifier);
          } else {
            throw 'Library, Class, or Variable name expected';
          }
        }
      } else {
        symbol = getSymbol(node.target);
      }

      bool isClass = (symbol != null && symbol.type == Symbol.CLASS);

      if (!isClass) {
        out.write('d2c::Var(');
      }
      _visitNode(node.target);
      if (!isClass) {
        out.write(')');
      }

      if (isClass) {
        out.write('::');
        var sym = symbol.getSymbol(node.propertyName);
        if (sym != null && sym.isStatic) {
          out.write('__get_');
          _visitNode(node.propertyName);
          out.write('()');
        } else {
          _visitNode(node.propertyName);
        }
        return null;
      }

      out.write('.');
      String tk = translator.getToken('__get_${node.propertyName}');
      out.write('invoke($tk)');
    }



    return null;
  }

  @override
  Object visitRedirectingConstructorInvocation(RedirectingConstructorInvocation node) {
    return null;
  }

  @override
  Object visitRethrowExpression(RethrowExpression node) {
    out.write('throw e');
    return null;
  }

  @override
  Object visitReturnStatement(ReturnStatement node) {
    Expression expression = node.expression;
    if (expression == null) {
      if (_inConstructor) {
        out.writeln('return;');
      } else {
        out.writeln('return null;');
      }
    } else {
      out.write('return ');
      if (expression is Identifier) {
        Symbol s = getSymbol(expression);
        if (s != null && s.type == Symbol.FUNCTION &&
            s.node is MethodDeclaration) {
          ClassDefinition cd = s.extra;
          out.write('d2c::function(d2c::Var(this), '
                    'd2c::bind(this, &${cd.name}::');
          _visitNode(expression);
          out.write('))');
        } else {
          expression.accept(this);
        }
      } else {
        expression.accept(this);
      }
      out.writeln(';');
    }
    return null;
  }

  @override
  Object visitScriptTag(ScriptTag node) {
    out.write(node.scriptTag.lexeme);
    return null;
  }

  @override
  Object visitShowCombinator(ShowCombinator node) {
    return null;
  }

  @override
  Object visitSimpleFormalParameter(SimpleFormalParameter node) {
    out.write('const d2c::Var &');
    _visitNode(node.identifier);
    return null;
  }

  @override
  Object visitSimpleIdentifier(SimpleIdentifier node) {
    Symbol symbol = getSymbol(node.token);
    int symType = symbol != null ? symbol.type : 0;
    if ((_expandIdentifier > 0 && symType == Symbol.GETTER_SETTER) ||
        (symType == Symbol.VARIABLE && symbol.isStatic)) {
      out.write('__get_${_renameKeyword(node.token.lexeme)}()');
    } else {
      out.write(_renameKeyword(node.token.lexeme));
    }
    return null;
  }

  @override
  Object visitSimpleStringLiteral(SimpleStringLiteral node) {
    String s = _getStringLiteral(node);
    if (_inVariableDeclaration) {
      out.write('d2c::STR__("$s")');
    } else {
      out.write('d2c::Var(d2c::STR__("${s}"))');
    }
    return null;
  }

  String _getStringLiteral(SimpleStringLiteral node) {
    String s = node.literal.toString();
    int start = 1;
    int end = s.length - 1;

    if (node.isRaw) {
      start++;
    }

    String ss = s.substring(start, end);
    start = 0;
    end = ss.length;
    if (ss.isEmpty) {
      return "";
    }

    if (ss.startsWith("''") || s.startsWith('""')) {
      start += 2;
      end -= 2;
      String c = ss[start];
      if (c == '\r' || c == '\n') {
        start += 1;
      }
    }

    ss = ss.substring(start, end);
    start = 0;
    end = ss.length;

    return _escapeString(ss, start: start, end: end, isRaw: node.isRaw);
  }

  String subs_(String ls, int i, int l) {
    if (i - l < 0) {
      return '';
    }
    return ls.substring(i - l, i + 1);
  }

  String _escapeString(String ls, {int start, int end, bool isRaw: false}) {
    if (start == null) {
      start = 0;
    }
    if (end == null) {
      end = ls.length;
    }

    isHex(String c) {
      int ci = c.toLowerCase().codeUnits[0];
      int c0 = '0'.codeUnits[0];
      int c9 = '9'.codeUnits[0];
      int ca = 'a'.codeUnits[0];
      int cf = 'f'.codeUnits[0];
      return (ci >= c0 && ci <= c9) || (ci >= ca && ci <= cf);
    }

    StringBuffer s = new StringBuffer();
    bool hasSlash = false;
    for (int i = start; i < end; ++i) {
      String c = ls[i];
      String sc1 = subs_(ls, i, 1);
      String sc2 = subs_(ls, i, 2);
      if (c.codeUnits[0] == 0) {
        if (!hasSlash) {
          s.write('\\');
        }
        s.write('0');
        hasSlash = false;
        continue;
      } else if (sc1 == '\\u') {
        hasSlash = false;
        String code = '';
        while (i++ < end) {
          if (i >= end) {
            break;
          }
          String c = ls[i];
          if (c == '{') {
            continue;
          }
          if (c == '}' || !isHex(c)) {
            break;
          }
          code += c;
        }
        while (code.length > 1 && code[0] == '0') {
          code = code.substring(1);
        }
        while (code.length < 4) {
          code = '0' + code;
        }
        int x = int.parse('0x$code');
        if (x < 0x20) {
          s.write('x$code');
        } else {
          s.write('u$code');
        }
        continue;
      } else if (sc2 == '\\u{') {
        hasSlash = false;
        String uni = '';
        while (i++ < end) {
          String c = ls[i];
          if (c == '}') {
            break;
          }
          uni += c;
        }
        s.write(uni);
        continue;
      } else if (c == '\r') {
        hasSlash = false;
        continue;
      } else if (c == '\n') {
        hasSlash = false;
        s.write('\\n');
        hasSlash = false;
        continue;
      } else if (c == '\\') {
        if (isRaw) {
          //hasSlash = !hasSlash;
          //s.write('\\');
          //s.write('\\');
          s.write('\\');
        } else {
          hasSlash = !hasSlash;
          //s.write('\\');
        }
      } else {
        if (c == '"') {
          if (!hasSlash) {
            s.write('\\');
          }
        }
        hasSlash = false;
      }

      s.write(c);
    }

    if (hasSlash) {
      s.write('\\');
    }

    return s.toString();
  }

  @override
  Object visitStringInterpolation(StringInterpolation node) {
    bool first = true;
    for (var n in node.elements) {
      if (n is InterpolationString) {
        if (n.value.isEmpty) {
          continue;
        }
        if (!first) {
          out.write(' + ');
        }
        out.write('d2c::Var(d2c::STR__("${_escapeString(n.value)}"))');
        first = false;
      } else {
        if (!first) {
          out.write(' + ');
        }
        _visitNode(n);
        first = false;
      }
    }
    return null;
  }

  @override
  Object visitSuperConstructorInvocation(SuperConstructorInvocation node) {
    out.write('super');
    _visitNodeWithPrefix('.', node.constructorName);
    _visitNode(node.argumentList);
    return null;
  }

  @override
  Object visitSuperExpression(SuperExpression node) {
    out.write('super');
    return null;
  }

  int _switchCount = 0;
  int _switchFallthroughCount = 0;

  @override
  Object visitSwitchCase(SwitchCase node) {
    out.writeln('/* case ${node.expression} */');
    // Fallthrough...
    if (_switchFallthroughCount == 0) {
      if (_switchCount > 0) {
        out.write('else ');
      }

      out.write('if (');
    }

    if (_switchFallthroughCount > 0) {
      out.write(' || ');
    }

    out.write('(');
    var switchStatement = node.parent;
    _visitNode(switchStatement.expression);
    out.write(') == (');
    _visitNode(node.expression);
    out.write(')');

    if (node.statements.isEmpty) {
      _switchFallthroughCount++;
      return null;
    }

    _switchFallthroughCount = 0;

    out.writeln(') {');
    _visitNodeListWithSeparator(node.statements, ' ');
    out.writeln('}');

    _switchCount++;
    return null;
  }

  @override
  Object visitSwitchDefault(SwitchDefault node) {
    if (_switchFallthroughCount > 0) {
      out.writeln(') {');
      _visitNodeListWithSeparator(node.statements, ' ');
      out.writeln('}');
    }

    out.writeln('/* default: */');
    if (_switchCount > 0) {
      out.writeln('else {');
    }
    _visitNodeListWithSeparator(node.statements, ' ');
    if (_switchCount > 0) {
      out.writeln('}');
    }
    return null;
  }

  @override
  Object visitSwitchStatement(SwitchStatement node) {
    _switchCount = 0;
    _switchFallthroughCount = 0;
    out.writeln('/* switch (${node.expression})*/');
    out.writeln('for (int __switch__ = 0; __switch__ < 1; ++__switch__) {');
    _visitNodeListWithSeparator(node.members, ' ');
    out.writeln('}// switch');
    return null;
  }

  @override
  Object visitSymbolLiteral(SymbolLiteral node) {
    var tk = translator.getSymbol(node.components[0]);
    out.write(tk);
    return null;
  }

  @override
  Object visitThisExpression(ThisExpression node) {
    out.write('d2c::Var(this)');
    return null;
  }

  @override
  Object visitThrowExpression(ThrowExpression node) {
    out.write('throw d2c::Var(');
    _visitNode(node.expression);
    out.write(')');
    return null;
  }

  @override
  Object visitTopLevelVariableDeclaration(TopLevelVariableDeclaration node) {
    return null;
  }

  @override
  Object visitTryStatement(TryStatement node) {
    out.write('try ');

    _visitNode(node.body);

    if (node.catchClauses != null) {
      out.writeln(' catch (d2c::Var e) {');

      bool first = true;
      for (CatchClause c in node.catchClauses) {
        if (!first) {
          out.write('else ');
        }

        if (c.exceptionType != null) {
          String ns = '';
          var s = getSymbol(c.exceptionType);
          if (s != null && s.library != this) {
            ns = '${s.library.namespace}::';
          }
          out.write('if (d2c::is_type<$ns${_getClassName(c.exceptionType)}');
          out.writeln('>(e)) ');
        }

        out.writeln('{');

        if (c.stackTraceParameter != null) {
          String p = _getClassName(c.stackTraceParameter);
          out.writeln('d2c::Var $p; // TODO get stacktrace');
          _declareSymbol(p, Symbol.VARIABLE);
        }

        String p = _getClassName(c.exceptionParameter);
        if (p != 'e') {
          out.writeln('d2c::Var $p = e;');
          _declareSymbol(p, Symbol.VARIABLE);
        }
        _visitNode(c.body);

        out.writeln('}');

        first = false;
      }

      out.writeln('}');
    }

    if (node.finallyBlock != null) {
      out.writeln(' catch (...) {');
      out.writeln('}');
      out.writeln('//finally');
      out.writeln('{');
      _visitNodeList(node.finallyBlock.statements);
      out.writeln('}');
    }

    return null;
  }

  @override
  Object visitTypeArgumentList(TypeArgumentList node) {
    out.write('<');
    _visitNodeListWithSeparator(node.arguments, ', ');
    out.write('>');
    return null;
  }

  /**
   * C++ output uses the generic Var for all objects.
   */
  @override
  Object visitTypeName(TypeName node) {
    out.write('d2c::Var');
    return null;
  }

  @override
  Object visitTypeParameter(TypeParameter node) {
    return null;
  }

  @override
  Object visitTypeParameterList(TypeParameterList node) {
    return null;
  }

  @override
  Object visitVariableDeclaration(VariableDeclaration node) {
    _declareSymbol(node.name.toString(), Symbol.VARIABLE);
    _visitNode(node.name);
    if (node.initializer != null) {
      out.write('(');
    }
    if (node.initializer is Identifier) {
      Symbol s = getSymbol(node.initializer);
      print('!');
    }
    _visitNode(node.initializer);
    if (node.initializer != null) {
      out.write(')');
    }
    return null;
  }

  @override
  Object visitVariableDeclarationList(VariableDeclarationList node) {
    _visitNodeListWithSeparatorAndSuffix(node.metadata, ' ', ' ');
    // All variables are declared as a Var object in c++
    out.write('d2c::Var ');
    _visitNodeListWithSeparator(node.variables, ', ');
    return null;
  }

  @override
  Object visitVariableDeclarationStatement(VariableDeclarationStatement node) {
    _visitNode(node.variables);
    out.writeln(';');
    return null;
  }

  @override
  Object visitWhileStatement(WhileStatement node) {
    out.write('while (');
    _visitNode(node.condition);
    out.write(') ');
    _visitNode(node.body);
    return null;
  }

  @override
  Object visitWithClause(WithClause node) {
    return null;
  }

  /**
   * Visit the given function body, out.writeing the prefix before if given
   * body is not empty.
   *
   * @param prefix the prefix to be out.writeed if there is a node to visit
   * @param body the function body to be visited
   */
  void _visitFunctionWithPrefix(String prefix, FunctionBody body) {
    if (body is! EmptyFunctionBody) {
      out.write(prefix);
    }
    _visitNode(body);
  }

  /**
   * Safely visit the given node.
   *
   * @param node the node to be visited
   */
  void _visitNode(AstNode node) {
    if (node != null) {
      node.accept(this);
    }
  }

  /**
   * Print a list of nodes without any separation.
   *
   * @param nodes the nodes to be out.writeed
   * @param separator the separator to be out.writeed between adjacent nodes
   */
  void _visitNodeList(NodeList<AstNode> nodes) {
    _visitNodeListWithSeparator(nodes, '');
  }

  /**
   * Print a list of nodes, separated by the given separator.
   *
   * @param nodes the nodes to be out.writeed
   * @param separator the separator to be out.writeed between adjacent nodes
   */
  void _visitNodeListWithSeparator(NodeList<AstNode> nodes, String separator) {
    if (nodes != null) {
      int size = nodes.length;
      for (int i = 0; i < size; i++) {
        if (i > 0) {
          out.write(separator);
        }
        nodes[i].accept(this);
      }
    }
  }

  /**
   * Print a list of nodes, separated by the given separator.
   *
   * @param prefix the prefix to be out.writeed if the list is not empty
   * @param nodes the nodes to be out.writeed
   * @param separator the separator to be out.writeed between adjacent nodes
   */
  void _visitNodeListWithSeparatorAndPrefix(String prefix,
                                            NodeList<AstNode> nodes,
                                            String separator) {
    if (nodes != null) {
      int size = nodes.length;
      if (size > 0) {
        out.write(prefix);
        for (int i = 0; i < size; i++) {
          if (i > 0) {
            out.write(separator);
          }
          nodes[i].accept(this);
        }
      }
    }
  }

  /**
   * Print a list of nodes, separated by the given separator.
   *
   * @param nodes the nodes to be out.writeed
   * @param separator the separator to be out.writeed between adjacent nodes
   * @param suffix the suffix to be out.writeed if the list is not empty
   */
  void _visitNodeListWithSeparatorAndSuffix(NodeList<AstNode> nodes,
                                            String separator, String suffix) {
    if (nodes != null) {
      int size = nodes.length;
      if (size > 0) {
        for (int i = 0; i < size; i++) {
          if (i > 0) {
            out.write(separator);
          }
          nodes[i].accept(this);
        }
        out.write(suffix);
      }
    }
  }

  /**
   * Safely visit the given node, out.writeing the prefix before the node if it is non-`null`.
   *
   * @param prefix the prefix to be out.writeed if there is a node to visit
   * @param node the node to be visited
   */
  void _visitNodeWithPrefix(String prefix, AstNode node) {
    if (node != null) {
      out.write(prefix);
      node.accept(this);
    }
  }

  /**
   * Safely visit the given node, out.writeing the suffix after the node if it is non-`null`.
   *
   * @param suffix the suffix to be out.writeed if there is a node to visit
   * @param node the node to be visited
   */
  void _visitNodeWithSuffix(AstNode node, String suffix) {
    if (node != null) {
      node.accept(this);
      out.write(suffix);
    }
  }

  /**
   * Safely visit the given node, out.writeing the suffix after the node if it is non-`null`.
   *
   * @param suffix the suffix to be out.writeed if there is a node to visit
   * @param node the node to be visited
   */
  void _visitTokenWithSuffix(Token token, String suffix) {
    if (token != null) {
      out.write(token.lexeme);
      out.write(suffix);
    }
  }

  bool _inVariableDeclaration = false;
  bool _inConstructor = false;
  int _nestedFunctionLevel = 0;
  int _functionDepth = 0;
  bool _skipParameterValues = false;
  List<FormalParameter> _functionParameters = [];
  List<FormalParameter> _optionalParameters = [];
  List<FormalParameter> _namedParameters = [];
  List _constructorFieldInitializers = [];
  Map _inheritedNamedParams = {};
  int _expandIdentifier = 0;
}

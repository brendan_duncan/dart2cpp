/****************************************************************************
 * Copyright (C) 2014 by Brendan Duncan.                                    *
 *                                                                          *
 * This file is part of Dart2Cpp,                                           *
 * https://github.com/brendan-duncan/dart2cpp.                              *
 *                                                                          *
 * Licensed under the Apache License, Version 2.0 (the "License");          *
 * you may not use this file except in compliance with the License.         *
 * You may obtain a copy of the License at                                  *
 *                                                                          *
 * http://www.apache.org/licenses/LICENSE-2.0                               *
 *                                                                          *
 * Unless required by applicable law or agreed to in writing, software      *
 * distributed under the License is distributed on an "AS IS" BASIS,        *
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. *
 * See the License for the specific language governing permissions and      *
 * limitations under the License.                                           *
 ****************************************************************************/
part of dart2cpp;

class Symbol {
  static const int LIBRARY = 1;
  static const int CLASS = 2;
  static const int FUNCTION = 3;
  static const int VARIABLE = 4;
  static const int GETTER_SETTER = 5;
  static const int TYPEDEF = 6;
  static const int CLASS_ALIAS = 7;
  static const int TYPENAME = 8;

  String name;
  int type;
  Map symbols = {};
  Library library;
  var node;
  var extra;
  bool isStatic = false;

  Symbol(this.name, this.type);

  Symbol getSymbol(name) {
    if (type == LIBRARY) {
      return library.getSymbol(name);
    }
    String s = name.toString();
    if (symbols.containsKey(s)) {
      return symbols[s];
    }
    return null;
  }

  String toString() => '$name [$type]';
}

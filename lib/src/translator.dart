/****************************************************************************
 * Copyright (C) 2014 by Brendan Duncan.                                    *
 *                                                                          *
 * This file is part of Dart2Cpp,                                           *
 * https://github.com/brendan-duncan/dart2cpp.                              *
 *                                                                          *
 * Licensed under the Apache License, Version 2.0 (the "License");          *
 * you may not use this file except in compliance with the License.         *
 * You may obtain a copy of the License at                                  *
 *                                                                          *
 * http://www.apache.org/licenses/LICENSE-2.0                               *
 *                                                                          *
 * Unless required by applicable law or agreed to in writing, software      *
 * distributed under the License is distributed on an "AS IS" BASIS,        *
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. *
 * See the License for the specific language governing permissions and      *
 * limitations under the License.                                           *
 ****************************************************************************/
part of dart2cpp;

class Translator {
  String sdkPath;
  String packagePath;
  bool hasMainFunction = false;
  bool hasMainArguments = false;
  bool loadDartCore;

  Map<String, Library> libraryMap = {};
  List<Library> libraryList = [];
  Map<String, String> tokens = new Map<String, String>();
  Map<String, String> symbols = new Map<String, String>();

  Translator({this.loadDartCore: true}) {
    // On Windows, Platform.script starts with '/C:/', which
    // throws things off...pathSegments is ok.
    var p = Platform.script.pathSegments;
    if (Platform.isWindows) {
      packagePath = '';
    } else {
      packagePath = '/';
    }

    for (int i = 0; i < p.length - 1; ++i) {
      packagePath += '${p[i]}/';
    }
    packagePath += 'packages';

    sdkPath = new File(Platform.executable).parent.parent.path
              + '/lib';
  }

  String translate(String filepath) {
    libraryMap = {};
    libraryList = [];

    _initTokens();

    loadLibrary(filepath);

    SourceWriter out = new SourceWriter();

    String classList = _generateClassList();
    String definitions = _generateDefinitions();
    String implementations = _generateImplementations();

    out.writeln(_generateTokens());
    out.writeln(classList);
    out.writeln(definitions);
    out.writeln(implementations);

    for (var l in libraryList) {
      if (l.hasMainFunction) {
        hasMainFunction = true;
        hasMainArguments = l.hasMainArguments;
        break;
      }
    }

    return out.toString();
  }

  Library loadLibrary(String path) {
    path = path.replaceAll('\\', '/');
    if (libraryMap.containsKey(path)) {
      return libraryMap[path];
    }

    Library lib = new Library(path, this, libraryList.length);
    libraryMap[path] = lib;
    libraryList.add(lib);

    lib.load();

    return lib;
  }

  String getToken(s) {
    s = s.toString();
    String tk = 'TK_${s}__';
    if (!_coreTokens.contains(tk)) {
      tokens[tk] = s;
    }
    return tk;
  }

  String getSymbol(s) {
    s = s.toString();
    String tk = getToken(s);
    String sym = 'SYM_${s}';
    symbols[sym] = tk;
    return sym;
  }

  String _generateTokens() {
    SourceWriter out = new SourceWriter();
    out.writeln('//${'-' * 60}');
    out.writeln('// TOKENS AND SYMBOLS');
    out.writeln('//${'-' * 60}');

    for (String tk in tokens.keys) {
      out.writeln('const d2c::Token ${tk}("${tokens[tk]}");');
    }

    for (String s in symbols.keys) {
      out.writeln('const d2c::Var ${s}(new d2c::Symbol(${symbols[s]}));');
    }

    return out.toString();
  }

  String _generateClassList() {
    SourceWriter out = new SourceWriter();
    out.writeln('//${'-' * 60}');
    out.writeln('// CLASSES');
    out.writeln('//${'-' * 60}');

    for (int i = libraryList.length - 1; i >= 0; --i) {
      out.writeln('// LIBRARY ${libraryList[i].filePath}');
      out.writeln('//${'-' * 60}');
      out.writeln('namespace ${libraryList[i].namespace} {');
      out.write(libraryList[i].generateClassList());
      out.writeln('}//namespace ${libraryList[i].namespace}\n');
      out.writeln('//${'-' * 60}');
    }

    return out.toString();
  }

  String _generateDefinitions() {
    SourceWriter out = new SourceWriter();
    out.writeln('//${'-' * 60}');
    out.writeln('// DEFINITIONS');
    out.writeln('//${'-' * 60}');

    for (int i = libraryList.length - 1; i >= 0; --i) {
      out.writeln('// LIBRARY ${libraryList[i].filePath}');
      out.writeln('//${'-' * 60}');
      out.writeln('namespace ${libraryList[i].namespace} {');
      out.write(libraryList[i].generateDefinitions());
      out.writeln('}//namespace ${libraryList[i].namespace}\n');
      out.writeln('//${'-' * 60}');
    }

    return out.toString();
  }

  String _generateImplementations() {
    SourceWriter out = new SourceWriter();
    out.writeln('//${'-' * 60}');
    out.writeln('// IMPLEMENTATIONS');
    out.writeln('//${'-' * 60}');

    for (int i = libraryList.length - 1; i >= 0; --i) {
      var l = libraryList[i];
      out.writeln('// LIBRARY ${l.filePath}');
      out.writeln('//${'-' * 60}');
      out.writeln('namespace ${l.namespace} {');

      if (l.hasExternals) {
        String lib = l.libraryName;
        if (lib.startsWith('dart.')) {
          lib = lib.substring('dart.'.length);
        }
        out.writeln('namespace __external {');
        out.writeln('#include <dart/$lib/$lib.h>');
        out.writeln('}//namespace __external');
      }

      out.write(l.generateImplementations());

      out.writeln('}//namespace ${l.namespace}\n');
      out.writeln('//${'-' * 60}');
    }

    out.writeln('');
    return out.toString();
  }

  /**
   * The tokens for the core library are defined in dart/core/tokens.h.
   * Parse the file and load all of the token definitions, so they don't
   * cause collisions with the tokens being generated during the translation.
   */
  void _initTokens() {
    File script = new File(Platform.script.toFilePath());
    File fp = new File('${script.parent.parent.path}'
                       '/lib/cpp/dart2cpp/symbols.h');
    List lines = fp.readAsLinesSync();

    const String TOKEN_PREFIX = 'const d2c::Token ';

    for (String l in lines) {
      l = l.trim();
      if (l.startsWith(TOKEN_PREFIX)) {
        l = l.substring(TOKEN_PREFIX.length);
        try {
          String tk = l.split('(')[0];
          _coreTokens.add(tk);
        } catch (_) {
        }
      }
    }
  }

  Set<String> _coreTokens = new Set<String>();
}

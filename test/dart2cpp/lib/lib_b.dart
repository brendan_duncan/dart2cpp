/****************************************************************************
 * Copyright (C) 2014 by Brendan Duncan.                                    *
 *                                                                          *
 * This file is part of Dart2Cpp,                                           *
 * https://github.com/brendan-duncan/dart2cpp.                              *
 *                                                                          *
 * Licensed under the Apache License, Version 2.0 (the "License");          *
 * you may not use this file except in compliance with the License.         *
 * You may obtain a copy of the License at                                  *
 *                                                                          *
 * http://www.apache.org/licenses/LICENSE-2.0                               *
 *                                                                          *
 * Unless required by applicable law or agreed to in writing, software      *
 * distributed under the License is distributed on an "AS IS" BASIS,        *
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. *
 * See the License for the specific language governing permissions and      *
 * limitations under the License.                                           *
 ****************************************************************************/
library b;

const int FOO_CONSTANT = 42;

class Point {
  final int x, y;
  const Point(this.x, this.y);
}

const INT = 5;
const DOUBLE = 3.14;
const STRING = "str";
const POINT = const Point(1, 2);
const STRING2 = '123''456';
const STRING3 = '123${STRING}456';

int fooVar = 52;

int fooFunc(int a, [int b = 5, int c]) {
  return a + b + (c != null ? c : 0);
}

class FooBase {
  int get value => 42;

  FooBase._() {
  }

  FooBase() {
  }

  factory FooBase.which([int which = 0]) {
    if (which == 0) {
      return new BarFoo();
    } else {
      return new Bar();
    }
  }
}

class BarFoo extends FooBase {
  BarFoo() : super._();

  BarFoo.named(int foo) {
  }

  int get value => 52;
}

class Bar extends FooBase {
  Bar() : super._();

  static int bar = 40;

  int get value => 62;
}

class TestStaticConst {
  static const MIN_POSITIVE = const TestStaticConst(-10);

  final int value;
  const TestStaticConst(this.value);

  int ceil() { return value; }
}

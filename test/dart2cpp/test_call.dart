// Copyright (c) 2011, the Dart project authors.  Please see the AUTHORS file
// for details. All rights reserved. Use of this source code is governed by a
// BSD-style license that can be found in the LICENSE file.

const TOP_LEVEL_CONST = 1;
const TOP_LEVEL_CONST_REF = TOP_LEVEL_CONST;
const TOP_LEVEL_NULL = null;

var topLevel;

expectEquals(a, b) {
  assert(a == b);
}

expectFail(msg) {
  print(msg);
  assert(false);
}

class CallThroughGetterTest {

  static void testMain() {
    testTopLevel();
    testField();
    testGetter();
    testMethod();
    testEvaluationOrder();
  }

  static void testTopLevel() {
    topLevel = () {
      return 2;
    };
    expectEquals(1, TOP_LEVEL_CONST);
    expectEquals(1, TOP_LEVEL_CONST_REF);
    expectEquals(2, topLevel());

    expectThrowsNoSuchMethod(() {
      TOP_LEVEL_CONST(); /// static type warning
    });
    expectThrowsNoSuchMethod(() {
      (TOP_LEVEL_CONST)();  /// static type warning
    });
  }

  static void testField() {
    A a = new A();
    a.field = () => 42;
    expectEquals(42, a.field());
    expectEquals(42, (a.field)());

    a.field = () => 87;
    expectEquals(87, a.field());
    expectEquals(87, (a.field)());

    a.field = 99;
    expectThrowsNoSuchMethod(() { a.field(); });
    expectThrowsNoSuchMethod(() { (a.field)(); });
  }

  static void testGetter() {
    A a = new A();
    a.field = () => 42;
    expectEquals(42, a.getter());
    expectEquals(42, (a.getter)());

    a.field = () => 87;
    expectEquals(87, a.getter());
    expectEquals(87, (a.getter)());

    a.field = 99;
    expectThrowsNoSuchMethod(() { a.getter(); });
    expectThrowsNoSuchMethod(() { (a.getter)(); });
  }

  static void testMethod() {
    A a = new A();
    a.field = () => 42;
    expectEquals(true, a.method() is Function);
    expectEquals(42, a.method()());

    a.field = () => 87;
    expectEquals(true, a.method() is Function);
    expectEquals(87, a.method()());

    a.field = null;
    expectEquals(null, a.method());
  }

  static void testEvaluationOrder() {
    B b = new B();
    expectEquals("gf", b.g0());
    b = new B();
    expectEquals("gf", (b.g0)());

    b = new B();
    expectEquals("xgf", b.g1(b.x));

    // TODO need to figure out how to get C++ to evaluate b.g1 before b.x.
    //b = new B();
    //expectEquals("gxf", (b.g1)(b.x));

    //b = new B();
    //expectEquals("xygf", b.g2(b.x, b.y));
    //b = new B();
    //expectEquals("gxyf", (b.g2)(b.x, b.y));

    //b = new B();
    //expectEquals("xyzgf", b.g3(b.x, b.y, b.z));
    //b = new B();
    //expectEquals("gxyzf", (b.g3)(b.x, b.y, b.z));

    //b = new B();
    //expectEquals("yzxgf", b.g3(b.y, b.z, b.x));
    //b = new B();
    //expectEquals("gyzxf", (b.g3)(b.y, b.z, b.x));
  }

  static expectThrowsNoSuchMethod(fn) {
    var exception = catchException(fn);
    /*if (exception is! NoSuchMethodError) {
      expectFail("Wrong exception.  Expected: NoSuchMethodError"
          " got: ${exception}");
    }*/
  }

  static catchException(fn) {
    bool caught = false;
    var result = null;
    try {
      fn();
      expectEquals(true, false);  // Shouldn't reach this.
    } catch (e) {
      caught = true;
      result = e;
    }
    expectEquals(true, caught);
    return result;
  }

}


class A {

  A() { }
  var field;
  get getter { return field; }
  method() { return field; }

}


class B {

  B() : _order = "" { }

  get g0 { _mark('g'); return () { return _mark('f'); }; }
  get g1 { _mark('g'); return (x) { return _mark('f'); }; }
  get g2 { _mark('g'); return (x, y) { return _mark('f'); }; }
  get g3 { _mark('g'); return (x, y, z) { return _mark('f'); }; }

  get x { _mark('x'); return 0; }
  get y { _mark('y'); return 1; }
  get z { _mark('z'); return 2; }

  _mark(m) { _order += m; return _order; }
  String _order;

}

main() {
  CallThroughGetterTest.testMain();
}

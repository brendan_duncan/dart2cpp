/****************************************************************************
 * Copyright (C) 2014 by Brendan Duncan.                                    *
 *                                                                          *
 * This file is part of Dart2Cpp,                                           *
 * https://github.com/brendan-duncan/dart2cpp.                              *
 *                                                                          *
 * Licensed under the Apache License, Version 2.0 (the "License");          *
 * you may not use this file except in compliance with the License.         *
 * You may obtain a copy of the License at                                  *
 *                                                                          *
 * http://www.apache.org/licenses/LICENSE-2.0                               *
 *                                                                          *
 * Unless required by applicable law or agreed to in writing, software      *
 * distributed under the License is distributed on an "AS IS" BASIS,        *
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. *
 * See the License for the specific language governing permissions and      *
 * limitations under the License.                                           *
 ****************************************************************************/

class A {
  var foo;

  add(list) {
    foo = list;
    list.add(2.5);
    return this;
  }

  call(arg) => arg;
}

class Element {
  final Element parent;

  Element(this.parent);

  List<Element> get path0 {
    if (parent == null) {
      return <Element>[this];
    } else {
      return parent.path0..add(this);
    }
  }

  List<Element> get path1 {
    return (parent == null) ? <Element>[this] : parent.path1..add(this);
  }

  List<Element> get path2 {
    return (parent == null) ? <Element>[this] : (parent.path2..add(this));
  }
}

makeMap() => new Map()..[3] = 4 ..[0] = 11;

class MyClass {
  foo() => this..bar(3)..baz(4);
  bar(x) => x;
  baz(y) => y*2;
}

eq(a, b) {
  assert(a == b);
}

main() {
  var o = new MyClass();
  eq(o.foo(), o);
  var g = makeMap();
  eq(g[3], 4);
  eq(g[0], 11);

  var e = new Element(null);
  eq(2, e.path1.length);

  var foo = [42, 0];
  var a = new A();
  var bar = a..add(foo)('WHAT');
  assert(bar is A);
  a..foo[0] = new Object();


  var l = new List()..add(4);
  var m = l..add(5)..add(10);
  assert(m.length == 3);
  var x = []..add(5)..add(10);
  assert(x.length == 2);

  var len = [5].length;
  assert(len == 1);

  l = l..length = 1;
  assert(l.length == 1);
}

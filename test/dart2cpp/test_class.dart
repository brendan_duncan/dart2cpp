/****************************************************************************
 * Copyright (C) 2014 by Brendan Duncan.                                    *
 *                                                                          *
 * This file is part of Dart2Cpp,                                           *
 * https://github.com/brendan-duncan/dart2cpp.                              *
 *                                                                          *
 * Licensed under the Apache License, Version 2.0 (the "License");          *
 * you may not use this file except in compliance with the License.         *
 * You may obtain a copy of the License at                                  *
 *                                                                          *
 * http://www.apache.org/licenses/LICENSE-2.0                               *
 *                                                                          *
 * Unless required by applicable law or agreed to in writing, software      *
 * distributed under the License is distributed on an "AS IS" BASIS,        *
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. *
 * See the License for the specific language governing permissions and      *
 * limitations under the License.                                           *
 ****************************************************************************/

class TestNamed {
  TestNamed({n: 0}) {
    method(n: n);
  }

  TestNamed.named({n: 0}) {
    method(n: n);
  }

  factory TestNamed.factory({n: 0}) {
    return new TestNamed(n: n);
  }

  static staticMethod({n: 0}) {
    assert(n == 0);
  }

  method({n: 0}) {
    print(n == 0);
  }
}

class Foo {
  static int intStatic = 8;
  static List listStatic = [];

  static int FLAG_1 = 1;
  static int FLAG_2 = 2;
  static int FLAG_4 = 4;

  int flag = FLAG_2;

  get isFlagSet => flag & FLAG_2;

  static void voidStaticMethod0() {
  }

  static varStaticMethod0() {
    return new List();
  }

  static String stringStaticMethod0() {
    return "stringStaticMethod0";
  }

  static void voidStaticMethod2(int a, b) {
    assert(a != b);
  }

  static varStaticMethod2(String a, b) {
    return '$a:$b';
  }

  static String stringStaticMethod2(a, b) {
    return "stringStaticMethod2 $a $b";
  }

  static int sValue = 5;

  static get staticGetterSetter => sValue;

  static set staticGetterSetter(v) => sValue = v;

  String member0 = 'foo';
  int _member1;
  double _member2;

  Foo([this._member1 = 5]) :
    _member2 = 3.14 {
    var foo = stringStaticMethod2;
  }

  Foo.named(this._member1, {member2: 1.0})
      : _member2 = member2;

  int get member1 => _member1;

  double get member2 => _member2;

  set member2(double m) => _member2 = m;
}

class Bar<T extends Foo> extends Foo {
  T foo;
  Bar(this.foo, [int value = 5]) :
    super(value);

  @override
  int get member1 => foo._member1;

  testNamed(a, {b: 5, c}) {
    return a + b + (c != null ? c : 0);
  }

  testNamed2({b: 5, c}) {
    return b + (c != null ? c : 0);
  }
}

class Answer {
  final int answer;
  const Answer(this.answer);

  bool get isCorrect => answer == 42;
}

class Class1 {
  int value;

  Class1([int this.value = 6]) {
    print(value);
  }

  Class1.scaled(int value, int scaled)
      : this.value = value * scaled;

  bool get isCorrect => value == 5;
}

class Class2 implements Class1 {
  int value;

  Class2({this.value: 5});

  Class2.named({this.value: 5});

  bool get isCorrect => value == 42;
}


class FooBase {
  int get value => 42;

  FooBase._() {
  }

  FooBase() {
  }

  factory FooBase.which([int which = 0]) {
    if (which == 0) {
      return new Foo2();
    } else {
      return new Bar2();
    }
  }

  int foo() => this.value;
}

class Foo2 extends FooBase {
  Foo2() : super._();

  Foo2.named() {
  }

  int get value => 52;
}

class Bar2 extends FooBase {
  Bar2() : super._();

  int get value => 62;
}

class Timer {
  Timer(period, callback) {
  }
  factory Timer.periodic(var period, callback) {
  }
}

class Stream<T> {
  factory Stream.periodic(var period,
                          [T computation(int computationCount)]) {
    if (computation == null) computation = ((i) => null);

    var timer;
    var controller;

    void sendEvent() {
    }

    void startPeriodicTimer() {
      timer = new Timer.periodic(period, (timer) { });
    }

    controller = new StreamController<T>(sync: true,
        onListen: () {
        },
        onPause: () {
        },
        onResume: () {
        },
        onCancel: () {
        }
    );

    return controller.stream;
  }

  get last {
    var subscription = this.listen();
    return subscription;
  }

  listen() {
    void onError() {
    }
  }

  void timeout() {
    var timeout;
    if (timeout == null) {
      timeout = () { };
    }
  }
}

abstract class StreamController<T> {
  var stream;
  factory StreamController({bool sync, onListen, onPause, onResume, onCancel}) {
    return new _StreamController(sync: sync, onListen: onListen,
        onResume: onResume, onCancel: onCancel);
  }
}

class _StreamController implements StreamController {
  var stream;
  _StreamController({bool sync, onListen, onPause, onResume, onCancel}) {
  }
}

class _SyncStreamController<T> {
  final _onListen;

  _SyncStreamController(void this._onListen());
}

class StreamView<T> {
  var _stream;

  StreamView(this._stream);

  asBroadcastStream({void onListen(subscription),
                     void onCancel(subscription)})
      => _stream.asBroadcastStream(onListen: onListen, onCancel: onCancel);
}

class Foo3 {
  int value;
  factory Foo3({value: 3}) {
    return new Foo3._(value);
  }
  Foo3._(this.value);
}

class Foo4 {
  Foo3 foo;
  factory Foo4() {
    return new Foo4._(new Foo3(value: 5));
  }
  Foo4._(this.foo);

  void foo2(f) {

  }

  void foo3() {

  }

  void foo4() {
    var foo3 = (){};
    foo2(foo3);
  }
}

abstract class Foo5 {
  static const int FLAG_1 = 1;
  static const int FLAG_2 = 2;
  int flag = FLAG_1;
  get xyz;
  int foo();

  testFlag() => flag & FLAG_1;
}

abstract class Foo5_2 {
  void _sendData(data);
  void _sendError(error, stackTrace);
  void _sendDone();
}

class Foo6 {
  int _member1 = 5;
  double _member2 = 3.14;

  int get member1 => _member1;

  set member1(int i) => _member1 = i;

  double get member2 => _member2;

  set member2(double m) => _member2 = m;
}

abstract class Foo7 implements Foo5, Foo5_2 {
  int flag = Foo5.FLAG_1;
  double xyz = 3.0;
  int bar() => foo();

  void test() {
    Foo6 f = new Foo6();
    xyz = f.member2 = f.member1.toDouble();
    _sendData(f);
  }
}

class Foo8 extends Foo7 {
  int foo() => 5;

  void _sendData(data) { }
  void _sendError(error, stackTrace) { }
  void _sendDone() { }
  testFlag() => flag & Foo5.FLAG_1;
}

class Foo9 {
  var onDone;
  var onError;
  Foo9({this.onDone, this.onError});
}

void fOnDone() {
}

class Foo10 {
  Foo9 foo;
  Foo10() {
    foo = new Foo9(onDone: _onDone, onError: _onError);
    var f2 = new Foo9(onDone: sOnDone);
    var f3 = new Foo9(onDone: fOnDone);
  }
  void _onDone() {
  }
  void _onError() {
  }

  static void sOnDone() {
  }
}

class Foo11 {
  var func;
  Foo11(this.func);
}

class Foo12 extends Foo11 {
  var foo;

  Foo12({a, b})
      : super(({a}) => a + b) {
    foo = this;
  }

  Foo12._1({a, b})
        : super((a) => a + b);

  Foo12._2({c})
          : super(c);

  other() => this;

  isThis() => foo.other() == this;
}

class Foo13 extends Foo11 {
  Foo13({a})
      : super(a);

  static List<int> parseIPv6Address(String host) {
    void error(String msg) {
    }
    int parseHex(int start, int end) {
      int value = int.parse(host.substring(start, end), radix: 16);
      return value;
    }

    return [];
  }
}

class _TypeTest<T> {
  static int foo = 5;
  static double foo2() { return 4.0; }
  int m1;
  _TypeTest({this.m1});
  _TypeTest.n1({this.m1});
  _TypeTest.n2([this.m1]);
  bool test(v) => v is T;
  get f1 => 5;
  set f1(int f) => m1 = f;
}

class Query {
  split(tk) {
    return this;
  }

  fold(map, fn) {
  }
}

class Uri {
  static Map<String, String> splitQueryString(query, {encoding: 'UTF8'}) {
    return query.split("&").fold({}, (map, element) {
      int index = element.indexOf("=");
      if (index == -1) {
        if (element != "") {
          map[decodeQueryComponent(element, encoding: encoding)] = "";
        }
      } else if (index != 0) {
        var key = element.substring(0, index);
        var value = element.substring(index + 1);
        map[Uri.decodeQueryComponent(key, encoding: encoding)] =
            decodeQueryComponent(value, encoding: encoding);
      }
      return map;
    });
  }

  static String decodeQueryComponent(element, {encoding: 'UTF8'}) {
    return '';
  }
}

class TestStatic {
  static int ZERO = 0;
  static int FIVE = 5;
  static TestStatic global = new TestStatic(ZERO);

  int value;

  TestStatic(this.value);

  int getValue() => value;

  TestStatic next() => this;

  TestStatic setNext(TestStatic s) {
    return s;
  }

  factory TestStatic.factory() {
    return new TestStatic(FIVE);
  }

  void update() {
    global = global.setNext(global);
  }

  int get hashCode => 5;
}

TestStatic ts = new TestStatic.factory();

class TestStaticConst {
  static const MIN_POSITIVE = const TestStaticConst(-10);

  final int value;
  const TestStaticConst(this.value);

  int ceil() { return value; }
}

abstract class A {
  operator ==(Object b);

  int get foo => 5;
}

class B extends A {
  getFoo() => foo;

  void testFoo() {
    assert(foo == 5);
  }
}

class AA<T> {
  BB<T> alloc() {
    return new BB<T>();
  }
}

class BB<T> {
  BB();

  factory BB._() {
    return new BB<T>();
  }
}

class D {
  foo(x) => ++x.f;
}

class E {
  int f = 0;
}

class F<S, T> {
  factory F() => new F<S, T>._(new List<T>());
  List<T> list;
  F._(this.list);

  List<T> toList() { return <T>[]; }
}

class TestStatic2 {
  static int foo = 5;
}

main() {
  var ll = <dynamic>[];
  assert(ll is List<int>);
  var ff2 = new F<int, dynamic>();
  assert(ff2 is F<int, dynamic>);

  var b = new B();
  assert(b == b);
  assert(b.foo == 5);
  var aa = new AA<int>();
  var bb = aa.alloc();
  assert(aa is AA<int>);
  assert(bb is BB<int>);

  assert(TestStatic2.foo == 5);
  TestStatic2.foo = 7;
  assert(TestStatic2.foo == 7);

  var tn = new TestNamed(n: 0);
  tn.method(n: 0);
  TestNamed.staticMethod(n: 0);
  tn = new TestNamed.named(n: 0);
  tn = new TestNamed.factory(n: 0);

  var ff = new F<double, int>();
  assert(ff is F<double, int>);
  assert(ff is! F<double, String>);
  assert(ff.list is List<int>);

  var e = new E();
  var d = new D();
  d.foo(e);
  assert(e.f == 1);

  assert(TestStaticConst.MIN_POSITIVE.ceil() == -10);

  assert(TestStatic.global.value == 0);
  assert(TestStatic.global.getValue() == 0);
  Foo f = new Foo();
  assert(f.member1 == 5);

  f = new Foo(3);
  assert(f.member1 == 3);

  assert(f is Foo);
  assert(f is! Bar);

  assert(Foo.intStatic == 8);

  Foo.listStatic.add(10);
  assert(Foo.listStatic.length == 1);
  assert(Foo.listStatic[0] == 10);

  Bar bar = new Bar(f);
  Bar<Foo> foobar = new Bar<Foo>(f);

  assert(bar.member1 == 3);

  assert(bar.testNamed(1) == 6);
  assert(bar.testNamed(1, b: 3) == 4);
  assert(bar.testNamed(1, b: 3, c: 6) == 10);
  assert(bar.testNamed2(b: 7) == 7);
  assert(bar.testNamed2(c: 8) == 13);
  assert(bar.testNamed2(b: 3, c: 6) == 9);

  final c = new Answer(42);
  assert(c.answer == 42);
  assert(c.isCorrect);

  var c1 = new Class1();
  assert(c1.value == 6);
  var c2 = new Class1(13);
  assert(c2.value == 13);
  var c3 = new Class1.scaled(5, 5);
  assert(c3.value == 25);
  var c4 = new Class2();
  assert(c4.value == 5);
  var c5 = new Class2(value: 42);
  assert(c5.value == 42);

  int i = Foo.staticGetterSetter;
  assert(Foo.staticGetterSetter == 5);

  Foo.staticGetterSetter = 3;
  assert(Foo.staticGetterSetter == 3);

  assert(c4 is Class2);
  assert(c4 is Class1);
  assert(c4 is! Answer);

  FooBase f1 = new FooBase();
  assert(f1.value == 42);

  FooBase f2 = new FooBase.which();
  assert(f2.value == 52);

  FooBase f3 = new FooBase.which(0);
  assert(f3.value == 52);

  FooBase f4 = new FooBase.which(1);
  assert(f4.value == 62);

  bool b2 = new bool.fromEnvironment('PATH');
  assert(b2 == false);

  List l = [1,2,3];
  List l2 = new List.from(l);
  assert(l2.length == 3);
  assert(l2[0] == 1 && l2[1] == 2 && l2[2] == 3);

  var s = new Stream<int>.periodic(3);
  var s2 = new Stream<int>.periodic(3, (i) => i);

  var s3 = new _SyncStreamController((){});

  var f5 = new Foo4();

  var f8 = new Foo8();
  assert(f8.bar() == 5);

  var f12 = new Foo12();
  assert(f12.isThis());

  _TypeTest<int> tt = new _TypeTest<int>();
  assert(tt.test(5) == true);
  assert(tt.test(3.1) == false);

  _TypeTest tt2 = new _TypeTest();
  assert(tt2.test(5) == true);
  assert(tt2.test(3.1) == true);

  assert(ts.value == 5);
}

/****************************************************************************
 * Copyright (C) 2014 by Brendan Duncan.                                    *
 *                                                                          *
 * This file is part of Dart2Cpp,                                           *
 * https://github.com/brendan-duncan/dart2cpp.                              *
 *                                                                          *
 * Licensed under the Apache License, Version 2.0 (the "License");          *
 * you may not use this file except in compliance with the License.         *
 * You may obtain a copy of the License at                                  *
 *                                                                          *
 * http://www.apache.org/licenses/LICENSE-2.0                               *
 *                                                                          *
 * Unless required by applicable law or agreed to in writing, software      *
 * distributed under the License is distributed on an "AS IS" BASIS,        *
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. *
 * See the License for the specific language governing permissions and      *
 * limitations under the License.                                           *
 ****************************************************************************/

class _IterablePendingEvents<T> extends _PendingEvents {
  void clear() {
    assert(isScheduled == true);
  }
}

abstract class _PendingEvents {
  static const int _STATE_UNSCHEDULED = 0;
  static const int _STATE_SCHEDULED = 1;
  static const int _STATE_CANCELED = 3;
  int _state = _STATE_UNSCHEDULED;
  bool get isScheduled => _state == _STATE_SCHEDULED;

  static int _static = 0;
  static setStatic() {
    _PendingEvents._static = 0;
  }
}

var a;

class A {
  foo(a) => a[a.length];
  int get length => 42;
  operator[] (index) => 42;
}

class C {
  final int v;

  const C(this.v);

  get foo => v;
}

class D {
  var operator;
  D([this.operator]);
  get getter => operator;
  method() => operator;
  call(a) => a is num;
}

expectEqual(a, b) {
  assert(a == b);
}

expectTrue(a) {
  assert(a);
}

class E {
  A(a, [b, c]) {
    return 42;
  }

  B(a, {b, c}) {
    return 52;
  }
}

main() {
  var e = new E();
  expectEqual(e.A(1), 42);
  expectEqual(e.A(1, 2), 42);
  expectEqual(e.A(1, 2, 3), 42);
  expectEqual(e.B(1), 52);
  expectEqual(e.B(1, b: 2), 52);
  expectEqual(e.B(1, c: 3), 52);
  expectEqual(e.B(1, c: 3, b: 2), 52);

  var dd = new D();
  dd.operator = () => 42;
  expectTrue(dd.call(5));
  expectTrue((dd.call)(5));
  expectEqual(dd.operator(), 42);
  expectEqual((dd.operator)(), 42);
  expectEqual(dd.getter(), 42);
  expectEqual((dd.getter)(), 42);
  expectEqual(dd.method()(), 42);
  expectEqual((dd.method())(), 42);

  dd.operator = 42;
  try {
    dd.operator();
    assert(false);
  } catch (e) {
  }

  var topLevel = () {
    return 2;
  };
  assert(2 == topLevel());

  assert(new D(5).call(42));
  assert(new D(5)('foo') == false);
  var d = new D(5);
  print(d.operator);
  var c1 = new C(42);
  var c2 = new C(42);
  print(c1.hashCode);
  print(c2.hashCode);
  print('----');
  var c3 = const C(42);
  var c4 = const C(42);
  var c5 = const C(52);
  print(c3.hashCode);
  print(c4.hashCode);
  print(c5.hashCode);
  assert(c1.hashCode != c2.hashCode);
  assert(c3.hashCode == c4.hashCode);
  assert(c3.hashCode != c5.hashCode);

  _PendingEvents._static += 5;
  assert(_PendingEvents._static == 5);

  for (int i = 0; i < 10; i++) {
    if (a != null) new A().foo([]);
    assert(42 == new A().foo(new A()));
  }
}

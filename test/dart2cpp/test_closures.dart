/****************************************************************************
 * Copyright (C) 2014 by Brendan Duncan.                                    *
 *                                                                          *
 * This file is part of Dart2Cpp,                                           *
 * https://github.com/brendan-duncan/dart2cpp.                              *
 *                                                                          *
 * Licensed under the Apache License, Version 2.0 (the "License");          *
 * you may not use this file except in compliance with the License.         *
 * You may obtain a copy of the License at                                  *
 *                                                                          *
 * http://www.apache.org/licenses/LICENSE-2.0                               *
 *                                                                          *
 * Unless required by applicable law or agreed to in writing, software      *
 * distributed under the License is distributed on an "AS IS" BASIS,        *
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. *
 * See the License for the specific language governing permissions and      *
 * limitations under the License.                                           *
 ****************************************************************************/

/*class Foo {
  String value;
  Foo(this.value);
  String getValue() => value;

  static makeErrorHandler(controller) {
    return (e, s) { controller.addError(e, s); };
  }

  static makeErrorHandler2(controller) =>
    (e, s) { controller.addError(e, s); };
}

add(a, b) => a + b;

test(int foo) {
  int bar = foo + 5;
  _bar(e) {
    return foo + bar + e;
  }
  return _bar;
}

test2(int add(a, b)) {
  return add(3, 4);
}

test3({a, b}) {
}

class Bound {
  run() {
    return 42;
  }
}

class A {
  const A();
  foo() => 42;
}*/

class B {
  foo() => 52;

  namedArgs(a, {b: 2}) {
    return a + b;
  }
}

namedArgs(a, {b: 2}) {
  return a + b;
}

eq(a, b) {
  assert(a == b);
}

main() {
  var na = namedArgs;
  eq(na(40), 42);
  eq(na(40, b: 7), 47);

  var b = new B();
  var bc = b.namedArgs;
  eq(bc(40), 42);
  eq(bc(40, b: 5), 45);

  /*var array = [new A(), new A(), new B(), new B()];
  var set = [];
  for (var a in array) {
    print(a.foo.hashCode);
    //set.add(a.foo);
  }

  var runner = new Bound().run;
  assert(42 == runner());

  test3(a: (){}, b: (){});

  Foo foo = new Foo('hello');

  var z = foo.getValue;
  assert(z() == 'hello');

  var t = test(5);
  assert(t(6) == 21);

  var x = add;
  assert(x(2, 3) == 5);

  assert(test2((a, b) => a + b) == 7);

  int v = 0;
  increment() {
    v++;
  }
  assert(v == 0);
  increment();
  assert(v == 1);
  increment();
  assert(v == 2);

  int u;
  u = v = 3;
  increment();
  assert(u == 3);
  assert(v == 4);*/
}

/****************************************************************************
 * Copyright (C) 2014 by Brendan Duncan.                                    *
 *                                                                          *
 * This file is part of Dart2Cpp,                                           *
 * https://github.com/brendan-duncan/dart2cpp.                              *
 *                                                                          *
 * Licensed under the Apache License, Version 2.0 (the "License");          *
 * you may not use this file except in compliance with the License.         *
 * You may obtain a copy of the License at                                  *
 *                                                                          *
 * http://www.apache.org/licenses/LICENSE-2.0                               *
 *                                                                          *
 * Unless required by applicable law or agreed to in writing, software      *
 * distributed under the License is distributed on an "AS IS" BASIS,        *
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. *
 * See the License for the specific language governing permissions and      *
 * limitations under the License.                                           *
 ****************************************************************************/
import 'lib/lib_c.dart' as C;

abstract class RedirectFactory<T> extends C.Foo {
  factory RedirectFactory() = RedirectTest<T>;

  factory RedirectFactory.named(int value) = RedirectTest<T>.named;

  factory RedirectFactory.factory(int value) = RedirectTest<T>.factory;

  factory RedirectFactory.foo() = C.Foo;
}

class RedirectTest<T> implements RedirectFactory<T> {
  int value = 5;

  RedirectTest();

  RedirectTest.named(this.value);

  factory RedirectTest.factory(int value) = RedirectTest.named;

  toString() => value.toString();
}

abstract class F {
  factory F.named() = G.named;
  const F();
  factory F.namedConst() = G.namedConst;
}

class G extends F {
  factory G.named() => new G();
  G();
  const G.namedConst();
}


main() {
  var r = new RedirectFactory<int>();
  assert(r.toString() == '5');

  var r2 = new RedirectFactory<int>.named(3);
  assert(r2.toString() == '3');

  var r3 = new RedirectFactory<int>.factory(6);
  assert(r3.toString() == '6');
}
/****************************************************************************
 * Copyright (C) 2014 by Brendan Duncan.                                    *
 *                                                                          *
 * This file is part of Dart2Cpp,                                           *
 * https://github.com/brendan-duncan/dart2cpp.                              *
 *                                                                          *
 * Licensed under the Apache License, Version 2.0 (the "License");          *
 * you may not use this file except in compliance with the License.         *
 * You may obtain a copy of the License at                                  *
 *                                                                          *
 * http://www.apache.org/licenses/LICENSE-2.0                               *
 *                                                                          *
 * Unless required by applicable law or agreed to in writing, software      *
 * distributed under the License is distributed on an "AS IS" BASIS,        *
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. *
 * See the License for the specific language governing permissions and      *
 * limitations under the License.                                           *
 ****************************************************************************/

int foo = 5;
String bar = 'test';

class Point {
  final int x, y;
  const Point(this.x, this.y);
}

const INT = 5;
const DOUBLE = 3.14;
const STRING = "str";
const POINT = const Point(1, 2);
const STRING2 = '123''456';
const STRING3 = '123${STRING}456';

foo0() {
  foo = 10;
}

foo1(a) {
  foo = a;
}

foo2(a, int b) {
  foo = a + b;
}

foo3(a, [b = 5]) {
  return a + b;
}

foo4(a, {b: 5, c}) {
  return a + b + (c != null ? c : 0);
}

foo5({b, c: 5}) {
  return (b != null ? b : 0) + c;
}

foo6(bool test(error)) {
  return test('foo');
}

foo7({bool test(error)}) {
  return test('foo');
}

foo8(var a) {
  return a * 2;
}

dynamic foo9(dynamic a) {
  return a * 3;
}

foo10(a, b, c, d) { return a + b + c + d; }

foo11(a, b) =>
  ((c, d) => foo10(a, b, c, d));

eq(a, b) {
  assert(a == b);
}

class F {
  final f;
  const F(v) : f = v;
}

main() {
  // TODO this fails on VC++2013
  eq(const F(main), const F(main));

  try {
    INT();
    assert(false);
  } catch (e) {
  }

  eq(3, 7 % 4);
  eq(2, 9 % 7);
  eq(2, -7 % 9);
  eq(7, 7 % -9);
  eq(7, 7 % 9);
  eq(2, -7 % -9);
  var f = -7 % 9;
  assert(f == 2);
  assert((5).toDouble() == 5.0);
  assert((5 + 5).toDouble() == 10.0);
  assert(true.hashCode != 0);
  assert((!true).hashCode != 0);
  assert(true.toString() == 'true');
  assert((!true).toString() == 'false');
  int trueCount = 0;
  int falseCount = 0;
  bool getTrue() {
    trueCount++;
    return true;
  }
  bool getFalse() {
    falseCount++;
    return false;
  }

  trueCount = falseCount = 0;
  getFalse() && getTrue();
  assert(falseCount == 1);
  assert(trueCount == 0);

  trueCount = falseCount = 0;
  !getFalse() && getTrue();
  assert(falseCount == 1);
  assert(trueCount == 1);

  trueCount = falseCount = 0;
  getTrue() && getTrue();
  assert(falseCount == 0);
  assert(trueCount == 2);

  trueCount = falseCount = 0;
  !getTrue() && getTrue();
  assert(falseCount == 0);
  assert(trueCount == 1);

  trueCount = falseCount = 0;
  !getTrue() == true && getTrue();
  assert(falseCount == 0);
  assert(trueCount == 1);

  var bl = true && true || false;
  assert(bl == true);

  var a = 0;
  var b = 0;
  a = b = 5;
  b++;
  assert(a == 5);
  assert(b == 6);

  assert(INT == 5);
  assert(DOUBLE == 3.14);
  assert(STRING == "str");
  assert(STRING2 == '123456');
  assert(STRING3 == '123str456');
  assert(POINT.x == 1);
  assert(POINT.y == 2);
  assert(foo == 5);
  foo0();
  assert(foo == 10);
  foo1(15);
  assert(foo == 15);
  foo2(13, 7);
  assert(foo == 20);
  assert(foo3(1) == 6);
  assert(foo3(1, 3) == 4);
  assert(foo4(2) == 7);
  assert(foo4(2, b: 10) == 12);
  assert(foo4(2, b: 10, c: 5) == 17);
  assert(foo5() == 5);
  assert(foo5(b: 3) == 8);
  assert(foo5(c: 7) == 7);
  assert(foo5(c: 10, b: 15) == 25);
  assert(foo8(3) == 6);
  assert(foo8(5.5) == 11.0);
  assert(foo8('*') == '**');
  assert(foo9(6) == 18);
  assert(foo6((e) { assert(e == 'foo'); return true; }));
  assert(foo7(test: (e) { assert(e == 'foo'); return true; }));
  assert(foo10(1, 2, 3, 4) == 10);
  assert(foo11(1, 2)(3, 4) == 10);

  final int j = 5;
  assert(j == 5);
}

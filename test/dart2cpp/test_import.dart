/****************************************************************************
 * Copyright (C) 2014 by Brendan Duncan.                                    *
 *                                                                          *
 * This file is part of Dart2Cpp,                                           *
 * https://github.com/brendan-duncan/dart2cpp.                              *
 *                                                                          *
 * Licensed under the Apache License, Version 2.0 (the "License");          *
 * you may not use this file except in compliance with the License.         *
 * You may obtain a copy of the License at                                  *
 *                                                                          *
 * http://www.apache.org/licenses/LICENSE-2.0                               *
 *                                                                          *
 * Unless required by applicable law or agreed to in writing, software      *
 * distributed under the License is distributed on an "AS IS" BASIS,        *
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. *
 * See the License for the specific language governing permissions and      *
 * limitations under the License.                                           *
 ****************************************************************************/
import 'lib/lib_a.dart';
import 'lib/lib_b.dart' as Bar;
import 'lib/lib_c.dart';

class XYZ {
  XYZ.test({p: Bar.POINT});
}

class Foo2<E> extends Foo<E> {

}

void main() {
  assert(Bar.TestStaticConst.MIN_POSITIVE.ceil() == -10);

  assert(getB() == 40);
  assert(Bar.Bar.bar == 40);
  setB(10);
  assert(getB() == 10);
  assert(Bar.Bar.bar == 10);
  assert(Bar.POINT.x == 1);
}

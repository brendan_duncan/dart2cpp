/****************************************************************************
 * Copyright (C) 2014 by Brendan Duncan.                                    *
 *                                                                          *
 * This file is part of Dart2Cpp,                                           *
 * https://github.com/brendan-duncan/dart2cpp.                              *
 *                                                                          *
 * Licensed under the Apache License, Version 2.0 (the "License");          *
 * you may not use this file except in compliance with the License.         *
 * You may obtain a copy of the License at                                  *
 *                                                                          *
 * http://www.apache.org/licenses/LICENSE-2.0                               *
 *                                                                          *
 * Unless required by applicable law or agreed to in writing, software      *
 * distributed under the License is distributed on an "AS IS" BASIS,        *
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. *
 * See the License for the specific language governing permissions and      *
 * limitations under the License.                                           *
 ****************************************************************************/

main() {
  var l = <int>[3, 6, 4];
  assert(l is List<int>);
  assert(l is! List<String>);
  for (var i in l) {
    print(i);
  }

  int i = 0;
  loop1: while (true) {
    if (i > 20) break;
    loop2: while (true) {
      i++;
      if (i >= 10) {
        break loop1;
      }
      continue loop2;
      assert(false);
    }
    assert(false);
  }
  assert(i == 10);
}

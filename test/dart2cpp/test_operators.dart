/****************************************************************************
 * Copyright (C) 2014 by Brendan Duncan.                                    *
 *                                                                          *
 * This file is part of Dart2Cpp,                                           *
 * https://github.com/brendan-duncan/dart2cpp.                              *
 *                                                                          *
 * Licensed under the Apache License, Version 2.0 (the "License");          *
 * you may not use this file except in compliance with the License.         *
 * You may obtain a copy of the License at                                  *
 *                                                                          *
 * http://www.apache.org/licenses/LICENSE-2.0                               *
 *                                                                          *
 * Unless required by applicable law or agreed to in writing, software      *
 * distributed under the License is distributed on an "AS IS" BASIS,        *
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. *
 * See the License for the specific language governing permissions and      *
 * limitations under the License.                                           *
 ****************************************************************************/

class Foo {
  int value;

  int get scaledValue => value * 2;

  set scaledValue(int i) => value = i;

  Foo(this.value);

  Foo operator +(Foo other) {
    return new Foo(value + other.value);
  }

  operator ==(int v) {
    return value == v;
  }

  int getScaledValue() {
    int s = scaledValue;
    scaledValue = 5;
    return scaledValue;
  }

  List l = [1,2,3];

  operator []=(index, v) => l[index] = v;
  operator [](index) => l[index];

  operator ~/(v) => value ~/ v;
}

constants() {
  assert(0 == 499 >> 33);
  assert(0 == (499 << 33) & 0xFFFFFFFF);
}

foo(i) {
  if (i != 0) {
    y--;
    foo(i - 1);
    y++;
  }
}

var y;

// id returns [x] in a way that should be difficult to predict statically.
id(x) {
  y = x;
  foo(10);
  return y;
}

interceptors() {
  assert(0 == id(499) >> 33);
  assert(0 == (id(499) << 33) & 0xFFFFFFFF);
}

speculative() {
  var a = id(499);
  for (int i = 0; i < 1; i++) {
    assert(0 == a >> 33);
    assert(0 == (a << 33) & 0xFFFFFFFF);
  }
}

class AssignOpTest {
  AssignOpTest() {}

  static testMain() {
    var b = 0;
    b += 1;
    assert(1 == b);
    b *= 5;
    assert(5 == b);
    b -= 1;
    assert(4 == b);
    b ~/= 2;
    assert(2 == b);

    f = 0;
    f += 1;
    assert(1 == f);
    f *= 5;
    assert(5 == f);
    f -= 1;
    assert(4 == f);
    f ~/= 2;
    assert(2 == f);
    f /= 4;
    assert(.5 == f);

    AssignOpTest.f = 0;
    AssignOpTest.f += 1;
    assert(1 == AssignOpTest.f);
    AssignOpTest.f *= 5;
    assert(5 == AssignOpTest.f);
    AssignOpTest.f -= 1;
    assert(4 == AssignOpTest.f);
    AssignOpTest.f ~/= 2;
    assert(2 == AssignOpTest.f);
    AssignOpTest.f /= 4;
    assert(.5 == f);

    var o = new AssignOpTest();
    o.instf = 0;
    o.instf += 1;
    assert(1 == o.instf);
    o.instf *= 5;
    assert(5 == o.instf);
    o.instf -= 1;
    assert(4 == o.instf);
    o.instf ~/= 2;
    assert(2 == o.instf);
    o.instf /= 4;
    assert(.5 == o.instf);

    var x = 0xFF;
    x >>= 3;
    assert(0x1F == x);
    x <<= 3;
    assert(0xF8 == x);
    x |= 0xF00;
    assert(0xFF8 == x);
    x &=0xF0;
    assert(0xF0 == x);
    x ^=0x11;
    assert(0xE1 == x);

    var y = 100;
    y += 1 << 3;
    assert(108 == y);
    y *= 2 + 1;
    assert(324 == y);
    y -= 3 - 2;
    assert(323 == y);
    y += 3 * 4;
    assert(335 == y);

    var a = [1, 2, 3];
    var ix = 0;
    a[ix] |= 12;
    assert(13 == a[ix]);
  }

  static var f;
  var instf;
}

f() {
  assert(false);
  return true;
}

main() {
  int checkPointCounter = 1;
  int checkPoint1 = 0;
  int checkPoint2 = 0;
  int checkPoint3 = 0;
  int checkPoint4 = 0;
  int checkPoint5 = 0;
  int checkPoint6 = 0;
  for (int i = 0; i < 2; ++i) {
    if (i == 0) {
      checkPoint1 += checkPointCounter++;
      if (true || f() || f()) {
        checkPoint2 += checkPointCounter++;
      }
    } else {
      checkPoint3 += checkPointCounter++;
      if (false) {
        checkPoint4 = checkPointCounter++;  // Never reached.
        checkPoint4 = checkPointCounter++;  // Never reached.
        checkPoint4 = checkPointCounter++;  // Never reached.
        checkPoint4 = checkPointCounter++;  // Never reached.
        checkPoint4 = checkPointCounter++;  // Never reached.
      }
    }
    checkPoint5 += checkPointCounter++;
  }
  checkPoint6 += checkPointCounter++;
  assert(1 == checkPoint1);
  assert(2 == checkPoint2);
  assert(4 == checkPoint3);
  assert(0 == checkPoint4);
  assert(8 == checkPoint5);
  assert(6 == checkPoint6);

  AssignOpTest.testMain();

  var names = new List<int>();
  assert(names is List<int>);
  assert(names is !List<String>);
  assert((names as List<num>).length == 0);
  assert((names is List<int>));

  constants();
  interceptors();
  speculative();

  int j = 0;
  int k = 10;
  for (int i = 0; i < k; ++i) {
    j++;
  }
  assert(j == 10);
  final value = 0x22;
  final bitmask = 0x0f;
  assert((value & bitmask) == 0x02);
  assert((value & ~bitmask) == 0x20);
  assert((value | bitmask) == 0x2f);
  assert((value ^ bitmask) == 0x2d);
  assert((value << 4) == 0x220);
  assert((value >> 4) == 0x02);

  Foo a = new Foo(40);
  Foo b = new Foo(2);
  Foo c = a + b;
  assert(c.value == 42);

  assert(a[1] == 2);
  a[2] = 5;
  assert(a[2] == 5);

  assert(a ~/ 2 == 20);

  int i = 40;
  i ~/= 2;
  assert(i == 20);

  const c1 = 1;
  const c2 = 5;
  var s = 5;
  int r = 0;

  switch (s) {
    case c1:
      assert(false);
      break;
    case c2:
      r = 4;
      assert(true);
      break;
    default:
      assert(false);
  }
  assert(r == 4);

  switch (s) {
    default:
      r = 7;
      assert(true);
  }
  assert(r == 7);

  switch (s) {
    case c2:
    case c1:
      r = 9;
      assert(true);
      break;
    default:
      assert(false);
  }
  assert(r == 9);

  switch (s) {
    case c1:
      assert(false);
      break;
    case c2:
    default:
      r = 15;
      assert(true);
  }
  assert(r == 15);
}

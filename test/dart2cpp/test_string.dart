/****************************************************************************
 * Copyright (C) 2014 by Brendan Duncan.                                    *
 *                                                                          *
 * This file is part of Dart2Cpp,                                           *
 * https://github.com/brendan-duncan/dart2cpp.                              *
 *                                                                          *
 * Licensed under the Apache License, Version 2.0 (the "License");          *
 * you may not use this file except in compliance with the License.         *
 * You may obtain a copy of the License at                                  *
 *                                                                          *
 * http://www.apache.org/licenses/LICENSE-2.0                               *
 *                                                                          *
 * Unless required by applicable law or agreed to in writing, software      *
 * distributed under the License is distributed on an "AS IS" BASIS,        *
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. *
 * See the License for the specific language governing permissions and      *
 * limitations under the License.                                           *
 ****************************************************************************/
eq(a, b) {
  assert(a == b);
}

main() {
  var c1 = '"';
  eq(1, c1.length);

  var c2 = "'";
  eq(1, c2.length);

  var c3 = "\"";
  eq(1, c3.length);

  var c4 = '\"';
  eq(1, c4.length);

  var c5 = '\'';
  eq(1, c5.length);

  var c6 = '\n';
  eq(1, c6.length);

  var r1 = r"ab";
  eq(2, r1.length);

  var r2 = r"\n";
  eq(2, r2.length);


  String s1 = 'hello' + ' world';
  assert(s1 == 'hello world');
  String s2 = '-->$s1<---';
  assert(s2 == '-->hello world<---');
  String s3 = s1 + s2;
  assert(s3 == 'hello world-->hello world<---');
  String s5 = '-' * 5;
  assert(s5 == '-----');
  String s6 = '''
    hello
      world
  ''';
  assert(s6 == '    hello\n      world\n  ');

  String s7 = '${4} ${2}';
  assert(s7 == '4 2');

  String s8 = "'\"\"'";
  assert(s8 == "'\"\"'");

  String s9 = '""\'\'\"\"';
  assert(s9 == '""\'\'""');

  String _s10 = "'\"\"'";
  String s10 = "'\"$s1\"'";
  assert(s10 == "'\"hello world\"'");

  String s11 = '""\'$s1\'""';
  assert(s11 == '""\'hello world\'\"\"');

  String s12 = '1234';
  assert(s12.length == 4);

  String _s13 = r'\x\t$t\r\n';

  String s13 = r'""$s1""';
  assert(s13 == r'""$s1""');

  const String s14 =
        r'(?a)|'
        r'e(?b)|';
  assert(s14 == '(?a)|e(?b)|');

  String s15 = "Concat "
               "${15}"
               " ${16}"
               " String";
  assert(s15 == 'Concat 15 16 String');

  String s16 = "\\\\?\\";
  String s17 = '\\\\?\\';
  String s18 = '\'';
  String s19 = "\'";
  String s20 = '\"';
  String s21 = "\"";
  String s22 = '\\\"';
  String s23 = "\\\"";
  String s24 = r'\"';
  String s25 = r"\\";

  var t26 = 5;
  String s26 = "\\$t26}";

  String s27 = r'(?!' '$t26' r'\b(?!\$))[a-zA-Z$][\w$]*';

  String s28 = "Windows paths with \\\\?\\ prefix must be absolute";

  String s29 = """
a""" "b" "e";
  assert(s29 == 'abe');
}
